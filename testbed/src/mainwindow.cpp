#include "mainwindow.h"
#include "planner/windowframe-planner.h"
#include "trainer/windowframe-trainer.h"

#include <imgui.h>
#include <cassert>

namespace TB
{

MainWindow::MainWindow()
{
	agent = std::make_shared<GAIA::Agent>();
	// add all the modes in the order to appear in the combo box
	window_modes.push_back(std::make_unique<TrainerWindowFrame>(agent));
	window_modes.push_back(std::make_unique<PlannerWindowFrame>(agent));

	// now add all the mode names to the mode_name list
	for (const auto& frame : window_modes)
	{
		mode_names.push_back(frame->get_name());
	}

	// setup window styles here
	ImGuiStyle& style = ImGui::GetStyle();
	style.WindowRounding = 0.0f;
}

void MainWindow::draw()
{
	ImGui::SetNextWindowSize(ImVec2(310, 55), ImGuiWindowFlags_NoResize);
	ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiWindowFlags_NoMove);
	ImGui::Begin("SelectionWindow", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar);

	assert(!mode_names.empty());

	// draw the main window selector
	static int mode = 0;
	ImGui::Combo("Select Mode", &mode, mode_names.data(), mode_names.size());

	if (ImGui::Button("Load"))
	{

	}

	ImGui::SameLine();

	if (ImGui::Button("Save"))
	{

	}

	ImGui::End();

	// use the WindowFrame interface to initiate a draw for the selected mode
	window_modes[mode]->draw();
}

}
