#pragma once

#include <chrono>
#include <ctime>

template <typename clock>
class basic_stopwatch
{
	typename clock::time_point p;
	typename clock::duration   d;

public:
	void tick() { p = clock::now(); }
	void tock() { d = clock::now() - p; }
	void ticktock() { d = clock::now() - p; p = clock::now(); }
	//void reset() { d = clock::duration::zero(); }

	template <typename S>
	unsigned long long int report() const
	{
		return std::chrono::duration_cast<S>(d).count();
	}

	double report_ms() const
	{
		return static_cast<double>(report<std::chrono::microseconds>()) / 1000;
	}

	auto report_chrono() const
	{
		return d;
	}

	basic_stopwatch() : p(), d() { }
};

typedef basic_stopwatch<std::chrono::high_resolution_clock> StopWatch;

template <typename clock>
struct PerfMon
{
	PerfMon() : timeav(0), timecount(0), fps(0), time(0) { }

	/// Resets all counts and starts timer
	void start()
	{
		//sw.reset();
		timeav = 0;
		timecount = 0;
		fps = 0;
		time = 0;
		timehist.clear();
		sw.tick();
	}

	/// Updates with the new time
	void elapsed()
	{
		sw.ticktock();
		time = sw.report<std::chrono::microseconds>();

		timecount += time;
		timehist.push_front(time);

		// average over 1 sec
		while (timecount > 1000000)
		{
			timecount -= timehist.back();
			timehist.pop_back();
		}

		for (auto& t : timehist)
			timeav += (double)t;

		auto s = timehist.size();

		if (s > 0)
			timeav /= (double)s;

		if (timeav > 0)
			fps = (unsigned int)((double)timecount / timeav);
	}

	/// Capture a reference time point
	void mark() { sw.tick(); }

	/// Returns the current time epoch in ms
	unsigned long long get_time() const { return time / 1000; }

	/// Returns the average time epoch in ms
	double get_time_av() const { return timeav / 1000; }

	/// Returns the frames per second
	unsigned int get_fps() const { return fps; }

private:
	/// Measures the time between epochs
	basic_stopwatch<clock> sw;

	/// Measures the time taken for a frame (measured in microseconds)
	unsigned long long int time;

	/// Sum of frame time buffer
	unsigned long long int timecount;

	/// The frames per second
	unsigned int fps;

	/// The average frame time
	double timeav;

	/// Buffer of all the frame latency up to a specified amount based on timecount
	std::deque<unsigned long long int> timehist;
};

