#pragma once

#include "windowframe.h"
#include "agent.h"

#include <vector>
#include <memory>

namespace TB
{

class MainWindow
{
public:
	/// initialises the main window
	MainWindow();

	/// draws all parts of the main window
	void draw();

private:
	/// the list of interfaces for each display mode for the window
	std::vector<std::unique_ptr<IWindowFrame>> window_modes;

	/// mode names for each IWindowFrame
	std::vector<const char*> mode_names;

	/// the agent
	std::shared_ptr<GAIA::Agent> agent;
};

}
