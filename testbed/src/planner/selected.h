#pragma once

#include "planner/planner.h"


namespace TB
{

/// ensures the current selection remains valid
class Selected
{
public:
	/// ctor
	Selected();

	/// sets the selected eframe
	void set(const GAIA::PlannerFrame* pf, const GAIA::EntityGroup* eg, const GAIA::EntityFrame* ef);

	/// sets the highlighed frame to preview
	void set_highlight(const GAIA::EntityFrame* ef);

	/// sets the highlighed frames to preview
	void set_sequence_highlight(const GAIA::EFVector& sequence);

	/// clears the current selection and invalidates it
	void clear();

	/// gets the current planner frame
	const GAIA::PlannerFrame* get_planner_frame() const;

	/// gets the current entity group
	const GAIA::EntityGroup* get_entity_group() const;

	/// gets the current planner frame
	const GAIA::EntityFrame* get_entity_frame() const;

	/// gets the current planner frame
	const GAIA::EntityFrame* get_highlight_frame() const;

	/// gets the current planner frame
	const GAIA::EFVector& get_sequence_highlight() const;

	/// updates the current selection
	void update();

	/// returns true if a selection has been set
	bool is_valid() const;

	/// returns true if the selection was updated in the last or current frame
	bool is_updated() const;

	/// returns the currently selected depth
	int get_depth() const;

private:
	/// the current depth (frame level) of the selection
	int depth;

	/// the next depth (frame level) of the selection
	int depth_next;

	/// the currently selected entity frame
	const GAIA::EntityFrame* eframe;

	/// the currently selected planner frame
	const GAIA::PlannerFrame* pframe;

	/// the currently selected entity group
	const GAIA::EntityGroup* egroup;

	/// the currently selected entity frame
	const GAIA::EntityFrame* eframe_next;

	/// the currently selected planner frame
	const GAIA::PlannerFrame* pframe_next;

	/// the currently selected entity group
	const GAIA::EntityGroup* egroup_next;

	/// the currently selected entity frame
	const GAIA::EntityFrame* highlight_frame;

	/// the currently selected sequence of entities to highlight
	GAIA::EFVector sequence_highlight;

	/// indicates if the selection was updated
	bool updated;
};

}
