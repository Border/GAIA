#include "selected.h"

using namespace TB;
using namespace GAIA;

Selected::Selected() :
	eframe(nullptr),
	egroup(nullptr),
	pframe(nullptr),
	eframe_next(nullptr),
	egroup_next(nullptr),
	pframe_next(nullptr),
	updated(false),
	depth(0),
	depth_next(0)
{
}

void Selected::set(const GAIA::PlannerFrame* pf, const GAIA::EntityGroup* eg, const GAIA::EntityFrame* ef)
{
	assert(pf != nullptr);
	assert(eg != nullptr);
	assert(ef != nullptr);

	pframe_next = pf;
	egroup_next = eg;
	eframe_next = ef;
	
	depth_next = ef->depth;

	updated = true;
}

void Selected::set_highlight(const GAIA::EntityFrame* ef)
{
	highlight_frame = ef;
}

void Selected::set_sequence_highlight(const GAIA::EFVector& sequence)
{
	sequence_highlight = sequence;
}

void Selected::clear()
{
	pframe = nullptr;
	egroup = nullptr;
	eframe = nullptr;
	pframe_next = nullptr;
	egroup_next = nullptr;
	eframe_next = nullptr;
	highlight_frame = nullptr;
	sequence_highlight.clear();

	updated = true;
}

const GAIA::PlannerFrame* Selected::get_planner_frame() const { return pframe; }

const GAIA::EntityGroup* Selected::get_entity_group() const { return egroup; }

const GAIA::EntityFrame* Selected::get_entity_frame() const { return eframe; }

const GAIA::EntityFrame* Selected::get_highlight_frame() const { return highlight_frame; }

const EFVector& Selected::get_sequence_highlight() const { return sequence_highlight; }

void Selected::update()
{
	if (updated)
	{
		updated = pframe != pframe_next || egroup != egroup_next || eframe != eframe_next;
		if (updated)
		{
			pframe = pframe_next;
			egroup = egroup_next;
			eframe = eframe_next;
			depth = depth_next;
		}
	}
}

bool Selected::is_valid() const { return pframe != nullptr && egroup != nullptr && eframe != nullptr; }

bool Selected::is_updated() const { return updated; }

int Selected::get_depth() const { return depth; }
