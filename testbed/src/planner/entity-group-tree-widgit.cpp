#include "entity-group-tree-widgit.h"

#include <algorithm>

using namespace TB;
using namespace GAIA;

EntityGroupTreeWidgit::EntityGroupTreeWidgit(const GAIA::EntityGroup* egroup, std::shared_ptr<GAIA::Plan>& _plan) :
	persistent_id(egroup->get_persistent_id()),
	plan(_plan),
	view_depth(4),
	ent_size(30.0f, 20.0f),
	padding(5.0f, 10.0f)
{

}

unsigned int EntityGroupTreeWidgit::get_persistent_id() const { return persistent_id; }

void EntityGroupTreeWidgit::set_view_depth(unsigned int vdepth) { view_depth = vdepth; }

void EntityGroupTreeWidgit::enable_failed_frames_visible(bool visible) { failed_frames_visible = visible; }

void EntityGroupTreeWidgit::draw(const GAIA::EntityGroup* egroup, Selected& selected)
{
	generate_layer_list(egroup);

	float height = 40.0f + (ent_size.y + padding.y) * (float)egroup_layer_list.size();

	PushStyleVar(ImGuiStyleVar_ChildWindowRounding, 5.0f);
	BeginChild(("egroup" + std::to_string(egroup->get_persistent_id())).c_str(), ImVec2(0.0f, height), true);
	Text(("Entity Group: " + std::to_string(egroup->get_persistent_id())).c_str()); SameLine(0.0f, 150.0f);
	Text(("Unique ID: " + std::to_string(egroup->get_unique_id())).c_str());

	draw_tree(egroup, selected);

	ImGui::EndChild();
	ImGui::PopStyleVar();
}

void EntityGroupTreeWidgit::generate_layer_list(const GAIA::EntityGroup* egroup)
{
	egroup_layer_list.clear();
	egroup_layer_list.push_back(egroup);

	auto index_prev = egroup->get_prev(), index_next = egroup->get_next();

	for (unsigned int depth = 0; depth < view_depth && (index_prev != nullptr || index_next != nullptr); ++depth)
	{
		if (index_prev != nullptr)
		{
			egroup_layer_list.push_front(index_prev);
			index_prev = index_prev->get_prev();
		}
		else if (index_next != nullptr)
		{
			egroup_layer_list.push_back(index_next);
			index_next = index_next->get_next();
		}

		if (index_next != nullptr)
		{
			egroup_layer_list.push_back(index_next);
			index_next = index_next->get_next();
		}
		else if (index_prev != nullptr)
		{
			egroup_layer_list.push_front(index_prev);
			index_prev = index_prev->get_prev();
		}
	}
}

void EntityGroupTreeWidgit::draw_tree(const GAIA::EntityGroup* egroup, Selected& selected)
{
	ImGuiWindow* window = GetCurrentWindow();
	if (window->SkipItems) { return; }

	ImGuiContext& g = *GImGui;
	const ImGuiStyle& style = g.Style;

	const ImRect frame_bb(window->DC.CursorPos, window->DC.CursorPos + GetContentRegionAvail());
	const ImRect inner_bb(frame_bb.Min + style.FramePadding, frame_bb.Max - style.FramePadding);
	ItemSize(frame_bb, style.FramePadding.y);

	if (!ItemAdd(frame_bb, 0)) { return; }

	RenderFrame(frame_bb.Min, frame_bb.Max, ImColor(0, 0, 0), false, style.FrameRounding);

	// extend the scroll variables if it is too small
	while (plan->get_max_depth() > scroll_list.size())
	{
		auto size = scroll_list.size();
		auto egroup = std::find_if(std::begin(egroup_layer_list), std::end(egroup_layer_list),
			[size](const GAIA::EntityGroup* eg)
		{ return !eg->get_entities().empty() && eg->get_entities().front()->depth == size; });

		if (egroup != std::end(egroup_layer_list))
		{
			auto size = (*egroup)->get_entities().size();

			float width = (float)size * (ent_size.x + padding.x);

			scroll_list.push_back(inner_bb.GetCenter().x);
		}
		else
		{
			scroll_list.push_back(0.0f);
		}
	}

	// the bounding box for each tree layer
	ImRect bb(inner_bb.Min, ImVec2(inner_bb.Max.x, inner_bb.Min.y + ent_size.y + padding.y));

	for (auto eg_iter = std::begin(egroup_layer_list); eg_iter != std::end(egroup_layer_list); ++eg_iter)
	{
		(*eg_iter)->lock_shared();
		draw_tree_layer(window, bb, eg_iter, selected);
		(*eg_iter)->unlock_shared();

		// move the bounding box down for the next entity group tree layer
		bb.Min.y += ent_size.y + padding.y;
		bb.Max.y += ent_size.y + padding.y;
	}
}

void EntityGroupTreeWidgit::draw_tree_layer(ImGuiWindow* window, const ImRect& bb, std::deque<const GAIA::EntityGroup*>::iterator& eg_iter, Selected& selected)
{
	auto prev_egroup = selected.get_entity_group()->get_prev();
	auto next_egroup = selected.get_entity_group()->get_next();

	// if true, this is the currently selected entity group
	bool is_curr_eg = (*eg_iter)->get_unique_id() == selected.get_entity_group()->get_unique_id();
	// if true, this is the parent group of the currently selected entity group
	bool is_prev_eg = prev_egroup != nullptr && (*eg_iter)->get_unique_id() == prev_egroup->get_unique_id();
	// if true, this is the child group of the currently selected entity group
	bool is_next_eg = next_egroup != nullptr && (*eg_iter)->get_unique_id() == next_egroup->get_unique_id();

	std::vector<const EntityFrame*> frames;

	for (const auto& ef : (*eg_iter)->get_entities())
	{
		if (failed_frames_visible || ef->frame_state >= GAIA::EntitFrameState::merged)
		{
			frames.push_back(ef.get());
		}
	}

	// function to sort the entity frames such that siblings are grouped together
	auto group_sibling_frames = [](const GAIA::EntityFrame* f1, const GAIA::EntityFrame* f2)
	{
		const GAIA::EntityFrame* pf1 = nullptr;

		for (auto& dep1 : f1->dependencies)
		{
			auto parent1 = dep1.frame_genealogy.get_current();
			if (pf1 == nullptr)
			{
				pf1 = parent1;
			}
			else if (parent1->unique_id < pf1->unique_id)
			{
				pf1 = parent1;
			}
		}

		const GAIA::EntityFrame* pf2 = nullptr;
		for (auto& dep2 : f2->dependencies)
		{
			auto parent2 = dep2.frame_genealogy.get_current();
			if (pf2 == nullptr)
			{
				pf2 = parent2;
			}
			else if (parent2->unique_id < pf2->unique_id)
			{
				pf2 = parent2;
			}
		}

		if (pf1 == nullptr || pf2 == nullptr)
		{
			return f1->unique_id < f2->unique_id;
		}
		else
		{
			return pf1->unique_id == pf2->unique_id ? f1->unique_id < f2->unique_id : pf1->unique_id < pf2->unique_id;
		}
	};

	std::sort(std::begin(frames), std::end(frames), group_sibling_frames);

	ImVec2 center = bb.GetCenter();

	auto num_ents = frames.size();
	float width = (float)num_ents * (ent_size.x + padding.x);

	ImVec2 draw_pos(center.x - width * 0.5f, center.y - ent_size.y * 0.5f);

	float min_window = bb.Min.x, max_window = bb.Max.x;

	// check if the scroll bars need to be drawn
	if (width > bb.GetWidth())
	{
		// draw clickable scroll bars on either side
		{
			ImVec2 a(bb.Min.x, draw_pos.y);
			ImVec2 b(a.x + ent_size.x * 0.5f, draw_pos.y + ent_size.y);
			window->DrawList->AddRectFilled(a, b, ImColor(96, 96, 96));

			if (GImGui->IO.MouseDown[0])
			{
				ImVec2 mouse_pos = (GImGui->IO.MousePos - a);

				// if the mouse is within the rect
				if (mouse_pos.x >= 0
					&& mouse_pos.x <= ent_size.x * 0.5
					&& mouse_pos.y >= 0
					&& mouse_pos.y <= ent_size.y)
				{
					scroll_list[(*eg_iter)->get_entities().front()->depth] -= ent_size.x + padding.x;
				}
			}
		}

		// draw clickable scroll bars on either side
		{
			ImVec2 a(bb.Max.x - ent_size.x * 0.5f, draw_pos.y);
			ImVec2 b(bb.Max.x, draw_pos.y + ent_size.y);
			window->DrawList->AddRectFilled(a, b, ImColor(96, 96, 96));

			if (GImGui->IO.MouseDown[0])
			{
				ImVec2 mouse_pos = (GImGui->IO.MousePos - a);

				// if the mouse is within the rect
				if (mouse_pos.x >= 0
					&& mouse_pos.x <= ent_size.x * 0.5
					&& mouse_pos.y >= 0
					&& mouse_pos.y <= ent_size.y)
				{
					scroll_list[(*eg_iter)->get_entities().front()->depth] += ent_size.x + padding.x;
				}
			}
		}

		// use scroll wheel
		{
			if (std::abs(GImGui->IO.MouseWheel) > 0)
			{
				// if the mouse is within the rect
				if (GImGui->IO.MousePos.y >= bb.Min.y
					&& GImGui->IO.MousePos.y <= bb.Max.y
					&& GImGui->IO.MousePos.x >= bb.Min.x
					&& GImGui->IO.MousePos.x <= bb.Max.x)
				{
					scroll_list[(*eg_iter)->get_entities().front()->depth] += GImGui->IO.MouseWheel * (ent_size.x + padding.x);
				}
			}
		}

		draw_pos.x = scroll_list[(*eg_iter)->get_entities().front()->depth] - width * 0.5f;

		// subtract/add the size of the scrollbars and padding from min and max window
		min_window += padding.x + ent_size.x * 0.5f;
		max_window -= padding.x + ent_size.x * 0.5f;
	}


	for (const auto& ef : frames)
	{
		// clip if outside window
		if (draw_pos.x < min_window ||
			draw_pos.x + ent_size.x > max_window)
		{
			draw_pos.x += ent_size.x + padding.x;
			continue;
		}

		auto select = ef->unique_id == selected.get_entity_frame()->unique_id;

		// determine if this entity is part of the plan
		bool is_goal_ancestor = false;
		for (const auto& g : plan->get_goal_success_frames())
		{
			for (const auto& dep : g->dependencies)
			{
				if (dep.frame_genealogy.is_ancestor(ef))
				{
					is_goal_ancestor = true;
					break;
				}
			}
		}

		ImColor colour(64, 64, 64);

		// colour selection
		if (is_curr_eg)
		{
			if (select)
			{
				colour = ImColor(255, 255, 255);
			}
			else
			{
				for (const auto& dep : selected.get_entity_frame()->dependencies)
				{
					auto parent = dep.frame_genealogy.get_current();

					if (std::any_of(std::begin(parent->nodes), std::end(parent->nodes),
						[ef](const std::shared_ptr<GAIA::EntityFrame>& f) { return f->unique_id == ef->unique_id; }))
					{
						colour = ImColor(160, 160, 160);
						break;
					}
				}
			}
		}
		else if (is_prev_eg)
		{
			for (const auto& dep : selected.get_entity_frame()->dependencies)
			{
				if (dep.frame_genealogy.get_current() == ef)
				{
					colour = ImColor(0, 128, 255);
				}
			}
		}
		else if (is_next_eg)
		{
			auto is_child = std::any_of(std::begin(selected.get_entity_frame()->nodes), std::end(selected.get_entity_frame()->nodes),
				[&](const std::shared_ptr<GAIA::EntityFrame>& f) { return f->unique_id == ef->unique_id; });

			if (is_child)
			{
				colour = ImColor(0, 128, 0);
			}
		}
		else
		{
			for (const auto& dep : selected.get_entity_frame()->dependencies)
			{
				if (std::any_of(std::begin(dep.dependency_genealogy), std::end(dep.dependency_genealogy),
					[ef](const GAIA::EntityDependencyGenealogy& dep_gen) { return dep_gen.contains_eframe(ef); }))
				{
					colour = ImColor(160, 0, 128);
					break;
				}
			}
		}

		// draw the frame rect
		ImVec2 b = draw_pos + ent_size;
		window->DrawList->AddRectFilled(draw_pos, b, colour);

		// additions to indicate the entity state or relationship
		if (ef->frame_state == GAIA::success_goal)
		{
			ImVec2 a = draw_pos + ImVec2(ent_size.x * 0.25f, ent_size.y * 0.25f);
			ImVec2 b = a + ImVec2(ent_size.x * 0.5f, ent_size.y * 0.5f);
			window->DrawList->AddRectFilled(a, b, ImColor(255, 192, 0));
		}
		else if (std::any_of(std::begin(selected.get_sequence_highlight()), std::end(selected.get_sequence_highlight()),
			[ef](const GAIA::EntityFrame* frame) { return ef->unique_id ==frame->unique_id; }))
		{
			ImVec2 b = draw_pos + ImVec2(ent_size.x * 0.5f, ent_size.y * 0.5f);
			window->DrawList->AddCircleFilled(b, ent_size.y * 0.25f, ImColor(255, 255, 255));
		}
		else if (is_goal_ancestor)
		{
			ImVec2 b = draw_pos + ImVec2(ent_size.x * 0.5f, ent_size.y * 0.5f);
			window->DrawList->AddCircleFilled(b, ent_size.y * 0.25f, ImColor(255, 192, 0));
		}
		else if (ef->frame_state < GAIA::valid)
		{
			draw_cross_rect(window, ImColor(255, 0, 0), draw_pos, b);
		}

		auto highlight_frame = selected.get_highlight_frame();
		if (highlight_frame != nullptr && highlight_frame->unique_id == ef->unique_id)
		{
			window->DrawList->AddRect(draw_pos, b, ImColor(255, 255, 255));
		}

		// if the mouse is hovering over the rect, highlight it
		if (ItemHoverable(ImRect(draw_pos, b), 0))
		{
			window->DrawList->AddRect(draw_pos, b, select ? ImColor(0, 255, 0) : ImColor(255, 255, 255));

			// if the mouse is also clicked, set the selected values
			if (GImGui->IO.MouseClicked[0] && !select)
			{
				//std::begin(egroup_layer_list)

				auto layer_num = std::distance(std::begin(egroup_layer_list), eg_iter);

				if (layer_num == 0 || layer_num == 4)
				{
					auto pframe = plan->get_frame_at_depth(ef->depth);

					if (pframe != nullptr)
					{
						selected.set(pframe, *eg_iter, ef);
					}
				}
				else
				{
					selected.set(selected.get_planner_frame(), *eg_iter, ef);
				}
			}
		}

		draw_pos.x += ent_size.x + padding.x;
	}
}

void EntityGroupTreeWidgit::draw_cross_rect(ImGuiWindow* window, ImColor colour, ImVec2& a, ImVec2& b) const
{
	// a --- c
	// |  x  |
	// d --- b
	ImVec2 c(b.x, a.y), d(a.x, b.y);

	window->DrawList->AddRect(a, b, colour);
	window->DrawList->AddLine(a, b, colour);
	window->DrawList->AddLine(c, d, colour);
}
