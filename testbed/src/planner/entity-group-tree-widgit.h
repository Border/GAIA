#pragma once

#include "planner.h"
#include "planner-entity.h"
#include "selected.h"

#include <imgui.h>
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>

#include <vector>
#include <deque>

using namespace ImGui;

namespace TB
{

class EntityGroupTreeWidgit
{
public:
	/// ctor
	EntityGroupTreeWidgit(const GAIA::EntityGroup* egroup, std::shared_ptr<GAIA::Plan>& _plan);

	/// returns true if the entity group is valid for this object
	unsigned int get_persistent_id() const;

	/// sets the depth either side of the selected entity group such that total height =
	/// view_depth * 2 + 1
	void set_view_depth(unsigned int vdepth);

	/// Make failed frame visible
	void enable_failed_frames_visible(bool visible);

	/// draws the 
	void draw(const GAIA::EntityGroup* egroup, Selected& selected);

private:
	/// generated the layer list
	void generate_layer_list(const GAIA::EntityGroup* egroup);

	/// draws the actual EntityGroup tree
	void draw_tree(const GAIA::EntityGroup* egroup, Selected& selected);

	/// draws a single tree layer of an entity group
	void draw_tree_layer(ImGuiWindow* window, const ImRect& inner_bb, std::deque<const GAIA::EntityGroup*>::iterator& eg_iter, Selected& selected);

	/// draws a rectangle with a cross
	void draw_cross_rect(ImGuiWindow* window, ImColor colour, ImVec2& a, ImVec2& b) const;

	/// the plan used to generate the display
	std::shared_ptr<GAIA::Plan> plan;

	/// the persistent ID of the entity group
	unsigned int persistent_id;

	/// the list of entities in each layer of the entity group
	std::deque<const GAIA::EntityGroup*> egroup_layer_list;

	/// the scroll list for all entity group layers, where the layer number is used as the
	/// index
	std::vector<float> scroll_list;

	/// the depth either side of the selected entity group
	/// hence total height = view_depth * 2 + 1
	unsigned int view_depth;

	/// size of each entity object
	ImVec2 ent_size;

	/// the padding between each entity object
	ImVec2 padding;

	/// Show failed frames
	bool failed_frames_visible;
};

}
