#include "windowframe-planner.h"
#include "scenario.h"

#include <imgui.h>
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>

#include "container-algorithms.h"

#include <thread>
#include <algorithm>
#include <string>

#define ENUM_TO_STR(ENUM) "#ENUM"

using namespace ImGui;
using namespace TB;

inline std::string process_mode_to_string(GAIA::ProcessMode mode)
{
	switch (mode)
	{
	case GAIA::generate:		return "generate";
	case GAIA::generate_ping:	return "generate_ping";
	case GAIA::generate_pong:	return "generate_pong";
	case GAIA::merge:			return "merge";
	case GAIA::evaluate:		return "evaluate";
	case GAIA::resolve:			return "resolve";
	case GAIA::add:				return "add";
	default:					return "";
	}
}

PlannerWindowFrame::PlannerWindowFrame(const std::shared_ptr<GAIA::Agent>& _agent):
	agent(_agent),
	generating_plan(false),
	pause(true),
	threads(std::thread::hardware_concurrency()),
	scenario_names({ "Unrestricted", "Restricted", "Unrestricted multi entity", "Restricted multi entity", "Semi-restricted multi entity", "multi entity collision" })
{
}

const char* PlannerWindowFrame::get_name() { return "Test Planner"; }

void PlannerWindowFrame::draw()
{
	draw_panel();

	draw_display();

	selected.update();
}

void PlannerWindowFrame::generate_plan(PlanType type)
{
	GAIA::SingleTest plan_data;

	// indicate there is no goal yet (as we haven't read it yet
	auto params = GAIA::PlanParams(nullptr);

	// set the number of threads to use based on selection
	// start paused so we can step through
	// use debug mode so we can see failed frames
	params.threads = (unsigned int)threads;
	params.paused = true;
	params.debug = true;

	switch (type)
	{
	case unrestricted: plan_data = GAIA::IO::read_single_test("scenarios//plan-move.json"); break;
	case restricted: plan_data = GAIA::IO::read_single_test("scenarios//plan-move-restricted.json"); break;
	case unrestricted_multi_entity: plan_data = GAIA::IO::read_single_test("scenarios//plan-move-multi-entity.json"); break;
	case restricted_multi_entity: plan_data = GAIA::IO::read_single_test("scenarios//plan-move-restricted-multi-entity.json"); break;
	case semi_restricted_multi_entity: plan_data = GAIA::IO::read_single_test("scenarios//plan-move-semi-restricted-multi-entity.json"); params.max_depth = 12; break;
	case multi_entity_collision: plan_data = GAIA::IO::read_single_test("scenarios//plan-move-multi-entity-collision.json"); params.max_depth = 12; break;
	default: return;
	}

	// set the goal
	params.goal = plan_data.goal;

	planner = std::make_unique<GAIA::Planner>();
	plan = planner->generate_plan_async(params, plan_data.start.get());
}

void PlannerWindowFrame::draw_display()
{
	ImGui::SetNextWindowSize(ImVec2(ImGui::GetIO().DisplaySize.x - 330.0f, ImGui::GetIO().DisplaySize.y - 10.0f));
	ImGui::SetNextWindowPos(ImVec2(320, 0), ImGuiWindowFlags_NoMove);
	ImGui::Begin("DisplayWindow", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

	if (plan == nullptr)
	{
		if (generating_plan)
		{
			ImGui::Text("Generating plan...");
		}
	}
	else if(selected.is_valid())
	{
		for (const auto& eg : selected.get_planner_frame()->get_entity_groups())
		{
			auto pid = eg->get_persistent_id();
			auto matched_egroup = std::find_if(std::begin(entity_group_widgits), std::end(entity_group_widgits),
				[pid](const std::pair<EntityGroupTreeWidgit, const GAIA::EntityGroup*>& egroup)
			{ return egroup.first.get_persistent_id() == pid; });

			if (matched_egroup != std::end(entity_group_widgits))
			{
				matched_egroup->second = eg.get();
			}
			else
			{
				calg::insert_sorted(entity_group_widgits, std::make_pair(EntityGroupTreeWidgit(eg.get(), plan), eg.get()),
					[](const std::pair<EntityGroupTreeWidgit, const GAIA::EntityGroup*>& eg1, const std::pair<EntityGroupTreeWidgit, const GAIA::EntityGroup*>& eg2)
				{ return eg1.second->get_persistent_id() < eg2.second->get_persistent_id(); });
			}
		}

		for (auto& eg_widget : entity_group_widgits)
		{
			eg_widget.first.draw(eg_widget.second, selected);
		}
	}

	ImGui::End();
}

void PlannerWindowFrame::draw_panel()
{
	ImGui::SetNextWindowSize(ImVec2(310, ImGui::GetIO().DisplaySize.y - 75.0f));
	ImGui::SetNextWindowPos(ImVec2(0, 55), ImGuiWindowFlags_NoMove);
	ImGui::Begin("PanelWindow", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

	static int scenario = -1;
	ImGui::Combo("Scenario", &scenario, scenario_names.data(), scenario_names.size());

	if (ImGui::Button("Generate") && scenario >= 0 && !generating_plan)
	{
		// clear the previous plan and selection
		plan.reset();
		selected.clear();

		// start generating the plan
		auto thread = std::thread(&PlannerWindowFrame::generate_plan, this, (PlanType)scenario);

		thread.detach();

		generating_plan = true;
	}

	if (plan == nullptr)
	{
		if (!generating_plan)
		{
			ImGui::SameLine();

			ImGui::PushItemWidth(80);
			ImGui::InputInt("threads", &threads);
			ImGui::PopItemWidth();
			threads = std::min(threads, (int)std::thread::hardware_concurrency());

			ImGui::Text("Select a scenario and generate a plan");
		}
	}
	else
	{
		if (!planner->done())
		{
			ImGui::SameLine();
			if (pause)
			{
				if (ImGui::Button("Play"))
				{
					planner->play();
					pause = false;
				}
			}
			else
			{
				if (ImGui::Button("Pause"))
				{
					planner->pause();
					pause = true;
				}
			}

			ImGui::SameLine();
			if (ImGui::Button("Step"))
			{
				planner->step();
			}

			ImGui::SameLine();
			if (ImGui::Button("Step State"))
			{
				planner->step_state();
			}
		}

		ImGui::Text("Mode:"); ImGui::SameLine();
		switch (planner->get_process_mode())
		{
		case GAIA::ProcessMode::generate: ImGui::TextColored(ImColor::HSV(0.32f, 1.0f, 1.0f), "Generate"); break;
		case GAIA::ProcessMode::generate_ping: ImGui::TextColored(ImColor::HSV(0.32f, 1.0f, 1.0f), "Generate Ping"); break;
		case GAIA::ProcessMode::generate_pong: ImGui::TextColored(ImColor::HSV(0.32f, 1.0f, 1.0f), "Generate Pong"); break;
		case GAIA::ProcessMode::merge: ImGui::TextColored(ImColor::HSV(0.16f, 1.0f, 1.0f), "Merge"); break;
		case GAIA::ProcessMode::evaluate: ImGui::TextColored(ImColor::HSV(0.1f, 1.0f, 1.0f), "Evaluate"); break;
		case GAIA::ProcessMode::resolve: ImGui::TextColored(ImColor::HSV(0.0f, 1.0f, 1.0f), "Resolve"); break;
		case GAIA::ProcessMode::add: ImGui::TextColored(ImColor::HSV(0.8f, 1.0f, 1.0f), "Add"); break;
		default:
		{
			if (planner->done())
			{
				ImGui::Text("Done");
			}
			else
			{
				ImGui::Text("None");
			}

			break;
		}
		}

		auto stats = planner->get_queue_stats();

		ImGui::Separator();
		ImGui::Columns(2, "queuecol"); // 2-ways, with border
		ImGui::SetColumnWidth(0, 120.0f);
		ImGui::SetColumnWidth(1, 50.0f);
		for (size_t i = 1; i < stats.mode_frames.size(); i++)
		{
			if (stats.current_mode == GAIA::ProcessMode(i))
			{
				ImGui::TextColored(ImColor::HSV(0.32f, 1.0f, 1.0f), process_mode_to_string(GAIA::ProcessMode(i)).c_str());
			}
			else
			{
				ImGui::Text(process_mode_to_string(GAIA::ProcessMode(i)).c_str());
			}
			ImGui::NextColumn();
			ImGui::Text(std::to_string(stats.mode_frames[i]).c_str()); ImGui::NextColumn();
		}
		ImGui::Text("total frames"); ImGui::NextColumn();
		ImGui::Text(std::to_string(stats.total_frames).c_str()); ImGui::NextColumn();
		ImGui::Text("processed frames"); ImGui::NextColumn();
		ImGui::Text(std::to_string(stats.total_frames_processed).c_str()); ImGui::NextColumn();

		ImGui::Columns(1);
		ImGui::Separator();

		int depth = selected.get_depth();
		ImGui::SliderInt("depth", &depth, 0, plan->get_max_depth() - 1);

		static bool failed_frames_visible = true;
		bool show = failed_frames_visible;
		ImGui::Checkbox("Show failed frames", &show);

		if (failed_frames_visible != show)
		{
			failed_frames_visible = show;
			for (auto& eg_widget : entity_group_widgits)
			{
				eg_widget.first.enable_failed_frames_visible(failed_frames_visible);
			}
		}

		// if the depth slider changes, update the plan
		if (selected.get_depth() != depth)
		{
			const GAIA::PlannerFrame* pframe = plan->get_frame_at_depth(depth);
			const GAIA::EntityGroup* egroup = nullptr;
			const GAIA::EntityFrame* eframe = nullptr;

			auto pid = selected.get_entity_group()->get_persistent_id();
			for (const auto& eg : pframe->get_entity_groups())
			{
				if (eg->get_persistent_id() == pid)
				{
					egroup = eg.get();
					break;
				}
			}

			if (egroup != nullptr)
			{
				if (selected.get_depth() > depth)
				{
					// the depth went backwards, so find an ancestor and select it
					if (!selected.get_entity_frame()->dependencies.empty())
					{
						//auto ef = selected.get_entity_frame()->dependencies.front().frame_genealogy.get_level(depth - 1);

						auto ef = std::find_if(std::begin(egroup->get_entities()), std::end(egroup->get_entities()),
							[&](const std::shared_ptr<GAIA::EntityFrame>& frame)
						{ return std::any_of(std::begin(frame->nodes), std::end(frame->nodes),
							[&](const std::shared_ptr<GAIA::EntityFrame>& child) { return child->unique_id == selected.get_entity_frame()->unique_id; }); });

						if (ef != std::end(egroup->get_entities()))
						{
							eframe = ef->get();
						}
					}
					else
					{
						eframe = egroup->get_entities().front().get();
					}
				}
				else
				{
					// the depth went forwards, so just select the first eframe that is a decendent of the selected
					for (const auto& ef : egroup->get_entities())
					{
						auto uid = ef->unique_id;
						if (std::any_of(std::begin(selected.get_entity_frame()->nodes), std::end(selected.get_entity_frame()->nodes),
							[uid](const std::shared_ptr<GAIA::EntityFrame>& frame) { return frame->unique_id == uid && frame->frame_state >= GAIA::valid; }))
						{
							eframe = ef.get();
							break;
						}
					}
				}

				if (eframe != nullptr)
				{
					selected.set(pframe, egroup, eframe);
				}
			}
			else
			{
				egroup = pframe->get_entity_groups().front().get();
				eframe = egroup->get_entities().front().get();
				selected.set(pframe, egroup, eframe);
			}
		}

		ImGui::Spacing();

		if (generating_plan)
		{
			// select the first entity from the first group from the first frame when a
			// plan has just been generated
			depth = 0;
			auto pframe = plan->get_frame_at_depth(depth);
			auto egroup = pframe->get_entity_groups().front().get();
			auto eframe = egroup->get_entities().front().get();
			selected.set(pframe, egroup, eframe);
			generating_plan = false;
		}

		draw_entity_frame_info(selected.get_entity_frame());
	}

	ImGui::End();
}

void PlannerWindowFrame::draw_entity_frame_info(const GAIA::EntityFrame* eframe)
{
	if (eframe == nullptr) { return; }

	if (ImGui::CollapsingHeader("Entity Frame", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
	{
		// display the
		// - unique_id
		// - operation data to produce this frame (if any)
		// - list of child node
		// - dependency tree

		ImGui::Text(("Unique ID: " + std::to_string(eframe->unique_id)).c_str());

		ImGui::Text("Entity State: "); ImGui::SameLine();

		switch (eframe->frame_state)
		{
		case GAIA::EntitFrameState::fail_generate:					ImGui::TextColored(ImColor(255, 0 ,0), "fail_generate"); break;
		case GAIA::EntitFrameState::fail_evaluate:					ImGui::TextColored(ImColor(255, 0, 0), "fail_evaluate"); break;
		case GAIA::EntitFrameState::fail_final_dependency:			ImGui::TextColored(ImColor(255, 0, 0), "fail_final_dependency"); break;
		case GAIA::EntitFrameState::fail_final_dependency_resolve:	ImGui::TextColored(ImColor(255, 0, 0), "fail_final_dependency_resolve"); break;
		case GAIA::EntitFrameState::valid:							ImGui::TextColored(ImColor(255, 255, 0), "valid"); break;
		case GAIA::EntitFrameState::success:						ImGui::TextColored(ImColor(0, 255, 0), "success"); break;
		case GAIA::EntitFrameState::success_goal:					ImGui::TextColored(ImColor(0, 255, 0), "success_goal"); break;
		}

		if (ImGui::TreeNode("Child node frames"))
		{
			int child_node_selected = -1;

			std::vector<const char*> node_names;

			for (const auto& node : eframe->nodes)
			{
				std::string str;

				switch (node->frame_state)
				{
				case GAIA::EntitFrameState::fail_generate:
				case GAIA::EntitFrameState::fail_evaluate:
				case GAIA::EntitFrameState::fail_final_dependency:
				case GAIA::EntitFrameState::fail_final_dependency_resolve:	str = "[F] "; break;
				case GAIA::EntitFrameState::valid:							str = "[V] "; break;
				case GAIA::EntitFrameState::success:						str = "[S] "; break;
				case GAIA::EntitFrameState::success_goal:					str = "[G] "; break;
				}

				str += std::to_string(node->unique_id);
				node_names.push_back(strdup(str.c_str()));
			}

			ImGui::Unindent(ImGui::GetTreeNodeToLabelSpacing());
			ImGui::BeginChild("child_list", ImVec2(0, 100), true);
			ImGui::PushStyleColor(ImGuiCol_FrameBg, (ImVec4)ImColor(0,0,0));
			ImGui::PushItemWidth(-1);
			ImGui::ListBox("", &child_node_selected, node_names.data(), node_names.size(), 4);
			ImGui::PopItemWidth();
			ImGui::PopStyleColor();
			ImGui::EndChild();
			ImGui::Indent(ImGui::GetTreeNodeToLabelSpacing());

			if (child_node_selected >= 0)
			{
				selected.set(plan->get_frame_at_depth(selected.get_depth()), selected.get_entity_group()->get_next(), eframe->nodes[child_node_selected].get());
			}

			ImGui::TreePop();
		}

		size_t height = std::max<size_t>(100, std::min<size_t>(400, eframe->dependencies.size() * 50));

		if (ImGui::TreeNode("Ancestors"))
		{
			ImGui::Unindent(ImGui::GetTreeNodeToLabelSpacing());
			ImGui::BeginChild("ancestor_list", ImVec2(0, (float)height), true);

			if (eframe->dependencies.empty())
			{
				ImGui::Text("None!");
			}
			else
			{
				for (int i = 0; i < (int)eframe->dependencies.size(); i++)
				{
					bool expanded = ImGui::TreeNode((void*)(intptr_t)i, "Ancestor %d", i);
					auto& deps = eframe->dependencies[i];
					if (IsItemHovered())
					{
						selected.set_sequence_highlight(deps.frame_genealogy.get_ancestors());
					}
					if (expanded)
					{
						for (auto eframe : deps.frame_genealogy.get_ancestors())
						{
							if (ImGui::Selectable(std::to_string(eframe->unique_id).c_str()))
							{
								auto pframe = plan->get_frame_at_depth(eframe->depth);
								auto pid = selected.get_entity_group()->get_persistent_id();

								for (const auto& eg : pframe->get_entity_groups())
								{
									if (eg->get_persistent_id() == pid)
									{
										selected.set(pframe, eg.get(), eframe);
										break;
									}
								}
							}
							if (IsItemHovered())
							{
								selected.set_highlight(eframe);
							}
						}
						ImGui::TreePop();
					}
				}
			}

			ImGui::EndChild();
			ImGui::TreePop();
			ImGui::Indent(ImGui::GetTreeNodeToLabelSpacing());
		}

		if (ImGui::TreeNode("Dependencies"))
		{
			ImGui::Unindent(ImGui::GetTreeNodeToLabelSpacing());
			ImGui::BeginChild("dependency_list", ImVec2(0, (float)height), true);

			if (eframe->dependencies.empty())
			{
				ImGui::Text("None!");
			}
			else
			{
				for (int i = 0; i < (int)eframe->dependencies.size(); i++)
				{
					if (ImGui::TreeNode((void*)(intptr_t)i, "Dependency Group %d", i))
					{
						auto& deps = eframe->dependencies[i];

						for (int j = 0; j < (int)deps.dependency_genealogy.size(); j++)
						{
							auto& gene = deps.dependency_genealogy[j];
							auto current_dependencies = gene.get_current_dependencies();

							if (ImGui::TreeNode((void*)(intptr_t)j, "Dependencies %d", j))
							{
								for (auto eframe : current_dependencies)
								{
									if (ImGui::Selectable(std::to_string(eframe->unique_id).c_str()))
									{
										auto pframe = plan->get_frame_at_depth(eframe->depth);
										auto pid = selected.get_entity_group()->get_persistent_id();

										for (const auto& eg : pframe->get_entity_groups())
										{
											if (eg->get_persistent_id() == pid)
											{
												selected.set(pframe, eg.get(), eframe);
												break;
											}
										}
									}
									if (IsItemHovered())
									{
										selected.set_highlight(eframe);
									}
								}
								ImGui::TreePop();
							}
						}
						ImGui::TreePop();
					}
				}
			}

			ImGui::EndChild();
			ImGui::TreePop();
			ImGui::Indent(ImGui::GetTreeNodeToLabelSpacing());
		}


		if (ImGui::TreeNode("Goal distance"))
		{
			ImGui::Unindent(ImGui::GetTreeNodeToLabelSpacing());

			ImGui::BeginChild("distancelist", ImVec2(0, 80), true);

			ImGui::Columns(3, "distancecol"); // 3-ways, with border
			ImGui::SetColumnWidth(0, 50.0f);
			ImGui::SetColumnWidth(1, 80.0f);
			ImGui::Text("ID"); ImGui::NextColumn();
			ImGui::Text("Type"); ImGui::NextColumn();
			ImGui::Text("Value"); ImGui::NextColumn();
			ImGui::Separator();

			for (const auto& dist : eframe->goal_distance)
			{
				ImGui::Text(std::to_string(dist.first).c_str()); ImGui::NextColumn();

				switch (dist.second.get_type())
				{
				case GAIA::Gint: ImGui::Text("Int"); break;
				case GAIA::Gdouble: ImGui::Text("Double"); break;
				case GAIA::GVec: ImGui::Text("Vec2"); break;
				}
				ImGui::NextColumn();
				ImGui::Text(dist.second.to_string().c_str());
				ImGui::NextColumn();
			}

			ImGui::Columns(1);
			ImGui::Separator();
			ImGui::EndChild();

			ImGui::TreePop();
			ImGui::Indent(ImGui::GetTreeNodeToLabelSpacing());
		}
	}

	if (ImGui::CollapsingHeader("Entity Data", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
	{
		// display the
		// - type
		// - state
		// - delta

		ImGui::Text(("Type ID: " + std::to_string(eframe->get_type_id())).c_str());

		ImGui::Text("State:");
		ImGui::BeginChild("statelist", ImVec2(0, 120), true);

		ImGui::Columns(3, "statecol"); // 3-ways, with border
		ImGui::SetColumnWidth(0, 50.0f);
		ImGui::SetColumnWidth(1, 80.0f);
		ImGui::Text("ID"); ImGui::NextColumn();
		ImGui::Text("Type"); ImGui::NextColumn();
		ImGui::Text("Value"); ImGui::NextColumn();
		ImGui::Separator();

		for (const auto& state : eframe->state)
		{
			ImGui::Text(std::to_string(state.first).c_str()); ImGui::NextColumn();

			switch (state.second.get_type())
			{
			case GAIA::Gint: ImGui::Text("Int"); break;
			case GAIA::Gdouble: ImGui::Text("Double"); break;
			case GAIA::GVec: ImGui::Text("Vec2"); break;
			}
			ImGui::NextColumn();
			ImGui::Text(state.second.to_string().c_str());
			ImGui::NextColumn();
		}

		ImGui::Columns(1);
		ImGui::Separator();
		ImGui::EndChild();

		if (eframe->op != nullptr)
		{
			ImGui::Text("Delta:");
			ImGui::BeginChild("deltalist", ImVec2(0, 60), true);

			ImGui::Columns(3, "deltastatecol"); // 3-ways, with border
			ImGui::SetColumnWidth(0, 50.0f);
			ImGui::SetColumnWidth(1, 80.0f);
			ImGui::Text("ID"); ImGui::NextColumn();
			ImGui::Text("Type"); ImGui::NextColumn();
			ImGui::Text("Value"); ImGui::NextColumn();
			ImGui::Separator();

			for (const auto& delta : eframe->op->delta)
			{
				ImGui::Text(std::to_string(delta.first).c_str()); ImGui::NextColumn();

				switch (delta.second.get_type())
				{
				case GAIA::Gint: ImGui::Text("Int"); break;
				case GAIA::Gdouble: ImGui::Text("Double"); break;
				case GAIA::GVec: ImGui::Text("Vec2"); break;
				}
				ImGui::NextColumn();
				ImGui::Text(delta.second.to_string().c_str());
				ImGui::NextColumn();
			}

			ImGui::Columns(1);
			ImGui::Separator();
			ImGui::EndChild();
		}
	}
}
