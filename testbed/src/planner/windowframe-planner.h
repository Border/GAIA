#pragma once

#include "windowframe.h"
#include "agent.h"
#include "planner.h"
#include "entity-group-tree-widgit.h"
#include "selected.h"

#include <string>

namespace TB
{

enum PlanType
{
	unrestricted,
	restricted,
	unrestricted_multi_entity,
	restricted_multi_entity,
	semi_restricted_multi_entity,
	multi_entity_collision,
	size
};

class PlannerWindowFrame : public IWindowFrame
{
public:
	/// ctor
	explicit PlannerWindowFrame(const std::shared_ptr<GAIA::Agent>& agent);

	/// returns the name of the scenario
	virtual const char* get_name();

	/// draws based on this scenario
	virtual void draw();
private:
	/// generates the plan
	void generate_plan(PlanType type);

	/// draw the main display
	void draw_display();

	/// draw the side panel
	void draw_panel();

	/// draw the provided entity frame info
	void draw_entity_frame_info(const GAIA::EntityFrame* eframe);

	/// indicates a plan is being generated
	bool generating_plan;

	/// the agent
	std::shared_ptr<GAIA::Agent> agent;

	/// the planner
	std::unique_ptr<GAIA::Planner> planner;

	/// the plan used to generate the display
	std::shared_ptr<GAIA::Plan> plan;

	/// the list of plan scenario names
	std::vector<char*> scenario_names;

	/// the list of entity group widgit
	std::vector<std::pair<EntityGroupTreeWidgit, const GAIA::EntityGroup*>> entity_group_widgits;

	/// the currently selected plan items
	Selected selected;

	/// pause the planner
	bool pause;

	/// the number of threads to use for the planner
	int threads;
};



}
