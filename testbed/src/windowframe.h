#pragma once

namespace TB
{

class IWindowFrame
{
public:
	/// The pure virtual destructor
	virtual ~IWindowFrame() = 0;

	/// draws based on this scenario
	virtual void draw() = 0;

	/// returns the name of the scenario
	virtual const char* get_name() = 0;
};

inline IWindowFrame::~IWindowFrame() { }

}
