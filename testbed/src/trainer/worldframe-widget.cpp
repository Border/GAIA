#include "worldframe-widget.h"
#include "affordance.h"
#include "memory.h"

#include <array>

using namespace GAIA;
using namespace ImGui;


void WorldFrameWidget::draw()
{
	BeginChild("world_display", ImVec2(0.0f, 0.0f), true, ImGuiWindowFlags_AlwaysAutoResize);

	Columns(2, "worldframe_column");

	SetColumnWidth(0, ImGui::GetIO().DisplaySize.x - 630.0f);
	SetColumnWidth(1, 310.0f);

	draw_window();

	NextColumn();

	draw_entity_panel();

	Columns(1);

	EndChild();
}

void WorldFrameWidget::set_frame(const std::shared_ptr<WorldFrame>& wframe, const std::shared_ptr<WorldFrame>& prev_wframe)
{
	prev_frame = prev_wframe;
	frame = wframe;
}

void WorldFrameWidget::set_level_data(const GAIA::Level* _level)
{
	level = _level;
	auto size = level->boundary_max - level->boundary_min;
	boundary.x = (float)size.x;
	boundary.y = (float)size.y;
}

void WorldFrameWidget::set_test_state_data(const std::vector<TestState>& test_valid)
{
	test_validity = test_valid;
}

void WorldFrameWidget::draw_window()
{
	ImGuiWindow* window = GetCurrentWindow();
	if (window->SkipItems) { return; }

	ImGuiContext& g = *GImGui;
	const ImGuiStyle& style = g.Style;

	ImVec2 padding(20.0f, 20.0f);

	const ImRect frame_bb(window->DC.CursorPos, window->DC.CursorPos + GetContentRegionAvail());
	const ImRect inner_bb(frame_bb.Min + padding, frame_bb.Max - padding);
	ItemSize(frame_bb, style.FramePadding.y);

	if (!ItemAdd(frame_bb, 0)) { return; }

	RenderFrame(frame_bb.Min, frame_bb.Max, ImColor(0, 0, 0), false, style.FrameRounding);

	ImVec2 wscale = (inner_bb.Max - inner_bb.Min) / boundary;

	// use square aspect ratio
	if (wscale.x < wscale.y)
	{
		wscale.y = wscale.x;
	}
	else
	{
		wscale.x = wscale.y;
	}

	// invert y direction as imgui is reversed
	wscale.y = -wscale.y;

	draw_world(window, inner_bb, wscale);

	draw_tests(window, inner_bb, wscale);

	draw_entities(window, inner_bb, wscale);
}

void WorldFrameWidget::draw_world(ImGuiWindow* window, const ImRect& bb, const ImVec2& wscale)
{
	std::array<ImVec2, 6> line;

	static ImVec2 step(std::sqrtf(3.0f) / 2.0f, 0.5f);

	auto index_count = boundary / step;

	int i_count = (int)std::ceil(index_count.x) + 1;
	int j_count = (int)std::ceil(index_count.y) + 1;

	int imin = -i_count / 2;
	int imax = i_count / 2;

	int jmin = -j_count / 2;
	int jmax = j_count / 2;

	for (int i = imin; i < imax; ++i)
	{
		for (int j = jmin; j < jmax; ++j)
		{
			//ImVec2 pos(5.0f + i * step.x, 1.0f + j + i * step.y);
			ImVec2 pos(i * step.x, j + i * step.y);
			bool draw_item = true;

			for (int k = 0; k < 6; ++k)
			{
				float a = k * (float)M_PI / 3.0f;
				float x = std::cosf(a) / std::sqrtf(3.0f);
				float y = std::sinf(a) / std::sqrtf(3.0f);
				if (pos.x <= level->boundary_min.x ||
					pos.x >= level->boundary_max.x ||
					pos.y <= level->boundary_min.y ||
					pos.y >= level->boundary_max.y)
				{
					draw_item = false;
					break;
				}
				line[k] = bb.GetCenter() + (pos + ImVec2(x, y)) * wscale;
			}

			if (draw_item)
			{
				//window->DrawList->AddConvexPolyFilled(line.data(), line.size(), ImColor(32, 32, 32));
				window->DrawList->AddPolyline(line.data(), line.size(), ImColor(96, 96, 96), true, 1.0f);
			}
		}
	}

	ImVec2 min((float)level->boundary_min.x, (float)level->boundary_min.y);
	ImVec2 max((float)level->boundary_max.x, (float)level->boundary_max.y);

	window->DrawList->AddRect(bb.GetCenter() + min * wscale, bb.GetCenter() + max * wscale, ImColor(128, 0, 0), 0.0f, 0, 3.0f);
}

void WorldFrameWidget::draw_tests(ImGuiWindow* window, const ImRect& bb, const ImVec2& wscale)
{
	static std::vector<ImColor> colour =
	{
		ImColor(192, 0, 192),
		ImColor(192, 192, 0),
		ImColor(0, 192, 192),
		ImColor(255, 0, 0),
		ImColor(0, 255, 0),
		ImColor(0, 0, 255)
	};

	size_t id = 0;
	for (const auto& test : test_validity)
	{
		for (const auto& entity : test.test->goal->get_entities())
		{
			auto ent = entity.second;
			auto& entity_state = ent->get_state();
			auto entity_position = entity_state.find(Position);
			assert(entity_position != std::end(entity_state));
			auto& ent_pos = entity_position->second.get_const_ref<GVec2>();

			ImVec2 pos((float)ent_pos.x, (float)ent_pos.y);

			if (boundary_check(pos))
			{
				auto center = bb.GetCenter() + pos * wscale;

				auto shade = colour[id].Value;
				shade.w = 0.3f;
				draw_hex(window, shade, center, wscale);
				draw_hex(window, colour[id], center, wscale, false, 2.0f);

				if (test.goal_attained)
				{
					window->DrawList->AddCircleFilled(center, 0.2f * wscale.x, ImColor(255, 255, 255), 32);
				}
			}
		}

		++id;
	}
}

void WorldFrameWidget::draw_entities(ImGuiWindow* window, const ImRect& bb, const ImVec2& wscale)
{
	for (auto& entity : frame->get_entities())
	{
		auto ent = entity.second;
		auto& entity_state = ent->get_state();
		auto entity_position = entity_state.find(Position);
		assert(entity_position != std::end(entity_state));
		auto& ent_pos = entity_position->second.get_const_ref<GVec2>();

		ImVec2 pos((float)ent_pos.x, (float)ent_pos.y);

		if (boundary_check(pos))
		{
			draw_hex(window,
				ent->get_type_id() == 1 ? ImColor(0, 128, 0) : ImColor(128, 0, 0),
				bb.GetCenter() + pos * wscale,
				wscale);
		}
	}
}

void WorldFrameWidget::draw_hex(ImGuiWindow* window, ImColor colour, const ImVec2& center, const ImVec2& wscale, bool filled, float thickness)
{
	std::array<ImVec2, 6> line;

	for (int k = 0; k < 6; ++k)
	{
		float a = k * (float)M_PI / 3.0f;
		float x = std::cosf(a) / std::sqrtf(3.0f);
		float y = std::sinf(a) / std::sqrtf(3.0f);
		line[k] = center + (ImVec2(x, y) * wscale);
	}

	if (filled)
	{
		window->DrawList->AddConvexPolyFilled(line.data(), line.size(), colour);
	}
	else
	{
		window->DrawList->AddPolyline(line.data(), line.size(), colour, true, thickness);
	}
}

bool WorldFrameWidget::boundary_check(const ImVec2& pos) const
{
	return !(pos.x <= level->boundary_min.x ||
		pos.x >= level->boundary_max.x ||
		pos.y <= level->boundary_min.y ||
		pos.y >= level->boundary_max.y);
}

void WorldFrameWidget::draw_entity_panel()
{
	ImGui::Text(("Frame: " + std::to_string(frame->get_frame_number())).c_str());

	Entity* ent = frame->get_entities().begin()->second.get();

	if (ImGui::CollapsingHeader("Entity Info", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
	{
		ImGui::Text(("Pers ID: " + std::to_string(ent->get_persistent_id())).c_str());
		ImGui::Text(("Type ID: " + std::to_string(ent->get_type_id())).c_str());

		ImGui::Text("State:");
		ImGui::BeginChild("statelist", ImVec2(0, 120), true);

		ImGui::Columns(3, "statecol"); // 3-ways, with border
		ImGui::SetColumnWidth(0, 50.0f);
		ImGui::SetColumnWidth(1, 80.0f);
		ImGui::Text("ID"); ImGui::NextColumn();
		ImGui::Text("Type"); ImGui::NextColumn();
		ImGui::Text("Value"); ImGui::NextColumn();
		ImGui::Separator();

		for (const auto& state : ent->state)
		{
			ImGui::Text(std::to_string(state.first).c_str()); ImGui::NextColumn();

			switch (state.second.get_type())
			{
			case GAIA::Gint: ImGui::Text("Int"); break;
			case GAIA::Gdouble: ImGui::Text("Double"); break;
			case GAIA::GVec: ImGui::Text("Vec2"); break;
			}
			ImGui::NextColumn();
			ImGui::Text(state.second.to_string().c_str());
			ImGui::NextColumn();
		}

		ImGui::Columns(1);
		ImGui::Separator();
		ImGui::EndChild();

		if (prev_frame != nullptr)
		{
			auto pent = prev_frame->get_entities().begin()->second.get();

			ImGui::Text("Delta:");
			ImGui::BeginChild("deltalist", ImVec2(0, 60), true);

			ImGui::Columns(3, "deltastatecol"); // 3-ways, with border
			ImGui::SetColumnWidth(0, 50.0f);
			ImGui::SetColumnWidth(1, 80.0f);
			ImGui::Text("ID"); ImGui::NextColumn();
			ImGui::Text("Type"); ImGui::NextColumn();
			ImGui::Text("Value"); ImGui::NextColumn();
			ImGui::Separator();

			for (const auto& delta : pent->delta)
			{
				ImGui::Text(std::to_string(delta.first).c_str()); ImGui::NextColumn();

				switch (delta.second.get_type())
				{
				case GAIA::Gint: ImGui::Text("Int"); break;
				case GAIA::Gdouble: ImGui::Text("Double"); break;
				case GAIA::GVec: ImGui::Text("Vec2"); break;
				}
				ImGui::NextColumn();
				ImGui::Text(delta.second.to_string().c_str());
				ImGui::NextColumn();
			}

			ImGui::Columns(1);
			ImGui::Separator();
			ImGui::EndChild();


			int selected_action = -1;

			std::vector<const char*> action_names;

			for (const auto& action : pent->actions)
			{
				std::string data_str = "[" + std::to_string(action->get_type_id()) + "] ";

				for (auto& data : action->get_data())
				{
					switch (data.data_type)
					{
					case 0: data_str += data.data.to_string(); break;
					default: data_str += "?"; break;
					}
				}

				action_names.push_back(strdup((data_str).c_str()));
			}

			//ImGui::Unindent(ImGui::GetTreeNodeToLabelSpacing());
			ImGui::BeginChild("action_list", ImVec2(0, 110), true);
			ImGui::PushStyleColor(ImGuiCol_FrameBg, (ImVec4)ImColor(0, 0, 0));
			ImGui::PushItemWidth(-1);
			ImGui::ListBox("", &selected_action, action_names.data(), action_names.size(), 4);
			ImGui::PopItemWidth();
			ImGui::PopStyleColor();
			ImGui::EndChild();
			//ImGui::Indent(ImGui::GetTreeNodeToLabelSpacing());
		}

		int selected_affordance = -1;

		std::vector<const char*> affordance_names;

		const auto affordances = GAIA::Memory::Instance()->get_entity_affordances(ent->get_type_id());

		for (const auto affd : affordances)
		{
			affordance_names.push_back(strdup(std::to_string(affd->get_unique_id()).c_str()));
		}

		//ImGui::Unindent(ImGui::GetTreeNodeToLabelSpacing());
		ImGui::BeginChild("affordance_list", ImVec2(0, 200), true);
		ImGui::PushStyleColor(ImGuiCol_FrameBg, (ImVec4)ImColor(0, 0, 0));
		ImGui::PushItemWidth(-1);
		ImGui::ListBox("", &selected_affordance, affordance_names.data(), affordance_names.size(), 6);
		ImGui::PopItemWidth();
		ImGui::PopStyleColor();
		ImGui::EndChild();
		//ImGui::Indent(ImGui::GetTreeNodeToLabelSpacing());
	}
}
