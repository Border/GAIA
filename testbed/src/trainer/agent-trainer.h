#pragma once

#include "simulator.h"
#include "scenario.h"
#include "agent.h"
#include "worldframe.h"


class AgentTrainer
{
public:
	explicit AgentTrainer(const std::shared_ptr<GAIA::Agent>& agent);

	void load_scenario(const std::string& filename);

	const GAIA::Scenario& get_scenario() const;

	void process_frame();

	/// returns true when all levels for the scenario are completed
	bool is_done() const;

	const std::vector<GAIA::TestState>& get_test_validity() const;

	size_t get_level_index() const;

	const std::shared_ptr<GAIA::WorldFrame>& get_frame() const;

	const std::shared_ptr<GAIA::WorldFrame>& get_prev_frame() const;

	unsigned int get_reset_count() const;

private:
	/// runs the simulator to generate a frame
	void update_world();

	/// checks the current levels tests for the scenario
	void validate_tests();

	/// runs the agent to generate the desired actions (if any)
	void update_agent();

	/// resets the simulator and agent to start frame, but does not clear what the agent has learned
	void reset();

	/// incriments the level if more levels are available
	void load_next_level();

	/// returns true if the goal entity can be matched by any entity
	bool validate_goal_entity(const GAIA::Entity* goal_ent) const;

	/// returns true if the test is passed
	bool validate_test_goal(const GAIA::Test& test) const;

	/// returns true if all test conditions are met
	bool validate_test_conditions(const GAIA::Test& test) const;

	/// checks no entities have moved outside the boundary
	bool validate_boundary() const;

private:
	std::shared_ptr<GAIA::Agent> agent;

	GAIA::Scenario scenario;

	Simulator simulator;

	GAIA::Level* current_level;
	size_t index_level;

	std::vector<GAIA::TestState> test_validity;

	unsigned int reset_count;
};
