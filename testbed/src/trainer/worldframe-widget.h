#pragma once

#include "scenario.h"
#include "worldframe.h"
#include "vecmath.h"

#include <imgui.h>
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>


class WorldFrameWidget
{
public:
	void draw();

	void set_frame(const std::shared_ptr<GAIA::WorldFrame>& wframe, const std::shared_ptr<GAIA::WorldFrame>& prev_wframe);

	void set_level_data(const GAIA::Level* level);

	void set_test_state_data(const std::vector<GAIA::TestState>& test_valid);

private:
	void draw_window();

	void draw_world(ImGuiWindow* window, const ImRect& bb, const ImVec2& wscale);

	void draw_tests(ImGuiWindow* window, const ImRect& bb, const ImVec2& wscale);

	void draw_entities(ImGuiWindow* window, const ImRect& bb, const ImVec2& wscale);

	void draw_hex(ImGuiWindow* window, ImColor colour, const ImVec2& center, const ImVec2& wscale, bool filled = true, float thickness = 1.0f);

	bool boundary_check(const ImVec2& pos) const;

	void draw_entity_panel();

private:
	std::shared_ptr<GAIA::WorldFrame> frame;
	std::shared_ptr<GAIA::WorldFrame> prev_frame;

	const GAIA::Level* level;

	ImVec2 boundary;

	std::vector<GAIA::TestState> test_validity;
};
