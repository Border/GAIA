#pragma once

#include "scenario.h"
#include "worldframe.h"
#include "action-entity.h"
#include "action-domain.h"
#include "entity.h"
#include "datamap.h"

#include <memory>
#include <deque>


enum EntityStateID
{
	sim_hp = GAIA::DataMapType::End,
	sim_state,
	sim_end,
	// temporary values for processing
	sim_total_dmg
};

enum StateType
{
	state_none = 0,
	state_move,
	state_attack,
	state_defend,
	state_heal
};

class Simulator
{
public:
	/// define the action types that can be performed
	void initialise(const std::vector<std::shared_ptr<GAIA::IActionDomain>>& actions);

	/// reset all the entities back to the desired start frame
	void reset(const std::shared_ptr<GAIA::WorldFrame>& start_frame);

	/// performs all the queued actions and world operations for a new frame
	void process();

	/// generates a new frame
	void generate_frame();

	const std::shared_ptr<GAIA::WorldFrame>& get_frame() const;

	const std::shared_ptr<GAIA::WorldFrame>& get_prev_frame() const;

	void add_actions(const std::vector<GAIA::EntityAction*>& acts);

private:
	void process_actions();

	void process_world();

private:
	std::shared_ptr<GAIA::WorldFrame> current_frame;
	std::shared_ptr<GAIA::WorldFrame> previous_frame;

	std::deque<GAIA::EntityAction*> actions;

	std::vector<std::shared_ptr<GAIA::IActionDomain>> action_types;
};
