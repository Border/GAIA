#include "windowframe-trainer.h"

#include <imgui.h>
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>

#include <json.hpp>

#include <fstream>

using namespace ImGui;
using namespace TB;
using namespace GAIA;


TrainerWindowFrame::TrainerWindowFrame(const std::shared_ptr<GAIA::Agent>& agent):
	pause(true),
	trainer(agent)
{
	// load scenario list from file
	std::ifstream ifs("scenarios//scenario-list.json");

	nlohmann::json j = nlohmann::json::parse(ifs);

	auto scenarios_list = j["scenarios"];

	for (const auto& item : scenarios_list)
	{
		scenario_names.push_back(strdup(item["name"].get<std::string>().c_str()));
		scenario_locations.push_back(item["location"].get<std::string>());
	}
}

const char* TrainerWindowFrame::get_name() { return "Trainer"; }

void TrainerWindowFrame::draw()
{
	draw_panel();

	draw_display();
}

void TrainerWindowFrame::draw_display()
{
	ImGui::SetNextWindowSize(ImVec2(ImGui::GetIO().DisplaySize.x - 320.0f, ImGui::GetIO().DisplaySize.y - 10.0f));
	ImGui::SetNextWindowPos(ImVec2(320, 0), ImGuiWindowFlags_NoMove);
	ImGui::Begin("DisplayWindow", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

	if (initialised)
	{
		wf_widget.draw();
	}

	ImGui::End();
}

void TrainerWindowFrame::draw_panel()
{
	ImGui::SetNextWindowSize(ImVec2(310, ImGui::GetIO().DisplaySize.y - 75.0f));
	ImGui::SetNextWindowPos(ImVec2(0, 65), ImGuiWindowFlags_NoMove);
	ImGui::Begin("PanelWindow", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

	// initially the scenario is hard coded, but later the scenario functionality can be
	// generalised such that a scenario can be defined in the same file where the
	// level/tutorials are. A master file can be used to store the list of scenario files,
	// making the scenario_names vector used to make the combo list below.

	static int scen_id = -1;
	int scen_id_prev = scen_id;
	ImGui::Combo("Scenario", &scen_id, scenario_names.data(), scenario_names.size());
	scen_id = 0; // set to 0 to auto load the first scenario

	// the scenario then has a series of levels/tutorials which can be shown in a list with
	// the current one high lighted. The agent starts at the begining and works its way
	// though them.

	if (scen_id == -1)
	{
		ImGui::End();
		return;
	}

	if(scen_id != scen_id_prev)
	{
		// if the scenario is changed, load the scenario data from file
		trainer.load_scenario(scenario_locations[scen_id]);
		initialised = true;
	}

	if (pause)
	{
		if (ImGui::Button("Play"))
		{
			pause = false;
		}

		ImGui::SameLine();
		if (ImGui::Button("Step"))
		{
			trainer.process_frame();
		}
	}
	else
	{
		trainer.process_frame();

		if (ImGui::Button("Pause"))
		{
			pause = true;
		}
	}

	ImGui::SameLine();
	if (ImGui::Button("Reset"))
	{
		trainer.load_scenario(scenario_locations[scen_id]);
		wf_widget.set_level_data(trainer.get_scenario().levels[trainer.get_level_index()].get());
	}

	wf_widget.set_frame(trainer.get_frame(), trainer.get_prev_frame());
	wf_widget.set_test_state_data(trainer.get_test_validity());

	// show display options to select between:
	// world display
	// affordance data display
	// intrinsic display?

	if (ImGui::CollapsingHeader("Scenario Info", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
	{
		static int level_selected = -1;

		if(level_selected != trainer.get_level_index())
		{
			// level changed, register tests with wf_widgit
			wf_widget.set_level_data(trainer.get_scenario().levels[trainer.get_level_index()].get());
		}

		std::vector<const char*> level_names;

		for (const auto& level : trainer.get_scenario().levels)
		{
			level_names.push_back(strdup(level->name.c_str()));
		}

		ImGui::BeginChild("level_list", ImVec2(0, 110), true);
		ImGui::PushStyleColor(ImGuiCol_FrameBg, (ImVec4)ImColor(0, 0, 0));
		ImGui::PushItemWidth(-1);
		ImGui::ListBox("", &level_selected, level_names.data(), level_names.size(), 4);
		ImGui::PopItemWidth();
		ImGui::PopStyleColor();
		ImGui::EndChild();

		level_selected = trainer.get_level_index();

		if (level_selected >= 0)
		{
			const auto& level = trainer.get_scenario().levels[level_selected];

			ImGui::Text(("Level: " + std::to_string(level->id)).c_str());
			ImGui::Text(("Test count: " + std::to_string(level->tests.size())).c_str());
			ImGui::Text(("Resets: " + std::to_string(trainer.get_reset_count())).c_str());

			int test_selected = -1;
			std::vector<const char*> test_state;

			for (size_t i = 0, ilen = level->tests.size(); i < ilen; ++i)
			{
				std::string name;
				switch (trainer.get_test_validity()[i].validity)
				{
				case TestState::valid: name = "] Valid"; break;
				case TestState::invalid_condition: name = "] Invalid condition"; break;
				case TestState::invalid_goal: name = "] Invalid goal"; break;
				}

				test_state.push_back(strdup(("[" + std::to_string(i) + name).c_str()));
			}

			ImGui::BeginChild("test_list", ImVec2(0, 110), true);
			ImGui::PushStyleColor(ImGuiCol_FrameBg, (ImVec4)ImColor(0, 0, 0));
			ImGui::PushItemWidth(-1);
			ImGui::ListBox("", &test_selected, test_state.data(), test_state.size(), 4);
			ImGui::PopItemWidth();
			ImGui::PopStyleColor();
			ImGui::EndChild();

			ImGui::TextWrapped(level->comment.c_str());
		}
	}

	ImGui::End();
}
