#include "agent-trainer.h"

#include <imgui.h>
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>

#include <json.hpp>

using namespace GAIA;

AgentTrainer::AgentTrainer(const std::shared_ptr<GAIA::Agent>& _agent):
	agent(_agent)
{
}

void AgentTrainer::load_scenario(const std::string& filename)
{
	// the action ranges are added to the persisten entity generator in the read_scenario
	// function
	scenario = GAIA::IO::read_scenario(filename);
	current_level = scenario.levels.front().get();
	index_level = 0;

	test_validity.clear();

	for (auto& test : current_level->tests)
	{
		test_validity.push_back({ &test, TestState::invalid_goal, false });
	}

	// TODO: currently this does not reset the agent properly
	reset();

	// hard reset for the agent
	agent->reset();

	agent->set_filtered_condition_features(current_level->ignore_features);

	reset_count = 0;
}

const Scenario& AgentTrainer::get_scenario() const
{
	return scenario;
}

void AgentTrainer::process_frame()
{
	// if there are no levels, dont process
	if (scenario.levels.empty()) { return; }

	// run agent to process frame information
	update_agent();

	// set the current frame as previous and copy it to the current frame
	simulator.generate_frame();

	// perform all world/enviornment operations (including processing actions from agent)
	update_world();

	// check if tests passed
	validate_tests();
}

bool AgentTrainer::is_done() const
{
	return index_level >= scenario.levels.size();
}

const std::vector<TestState>& AgentTrainer::get_test_validity() const
{
	return test_validity;
}

size_t AgentTrainer::get_level_index() const
{
	return index_level;
}

const std::shared_ptr<GAIA::WorldFrame>& AgentTrainer::get_frame() const
{
	return simulator.get_frame();
}

const std::shared_ptr<GAIA::WorldFrame>& AgentTrainer::get_prev_frame() const
{
	return simulator.get_prev_frame();
}

unsigned int AgentTrainer::get_reset_count() const
{
	return reset_count;
}

void AgentTrainer::update_world()
{
	// register all actions from the agent (from the previous frame, as the agents frame
	// hasn't been updated yet)
	simulator.add_actions(agent->get_actions());

	// process the next frame of the world
	simulator.process();
}

void AgentTrainer::validate_tests()
{
	// use index to index test_validity
	TestState::TestValidity valid = TestState::valid;

	// check boundary
	if (!validate_boundary())
	{
		reset();
		return;
	}

	for (auto& test : test_validity)
	{
		auto conditions = validate_test_conditions(*test.test);
		auto goal = validate_test_goal(*test.test);

		if (goal)
		{
			test.goal_attained = true;
			// reset test restrictions
		}

		if (!conditions)
		{
			test.validity = TestState::invalid_condition;
			if (valid != TestState::invalid_goal)
			{
				valid = TestState::invalid_condition;
			}
		}
		else if (!goal)
		{
			test.validity = TestState::invalid_goal;
			valid = TestState::invalid_goal;
		}
	}

	// If all tests were completed
	if (valid == TestState::valid)
	{
		load_next_level();
	}
	else if (valid == TestState::invalid_condition)
	{
		// conditions are specified that mean the level must be reset (i.e too many moves,
		// bad states etc)
	}
}

void AgentTrainer::update_agent()
{
	agent->add_world_frame(simulator.get_frame());

	agent->process();
}

void AgentTrainer::reset()
{
	agent->clear_frames();

	auto start = current_level->start->copy(
		[](const std::shared_ptr<Entity>& ent)
	{
		auto newent = std::make_shared<ScenarioEntity>(ent);
		newent->frame = 0; // the ctor auto incriments, so set back to 0 as its a copy
		return newent;
	});

	simulator.reset(start);

	for (auto& test : test_validity)
	{
		test.goal_attained = false;
		test.validity = TestState::invalid_goal;
	}

	++reset_count;
}

void AgentTrainer::load_next_level()
{
	++index_level;

	if (is_done())
	{
		current_level = scenario.levels[index_level].get();

		agent->set_filtered_condition_features(current_level->ignore_features);

		// reset for the next level
		reset();
	}
}

bool AgentTrainer::validate_goal_entity(const Entity* goal_ent) const
{
	// get all the entities of the same type
	auto items = simulator.get_frame()->get_entities().equal_range(goal_ent->get_type_id());

	for (; items.first != items.second; ++items.first)
	{
		// calculate the distance
		auto goal_distance = goal_ent->state - items.first->second->state;

		bool goal = true;
		for (auto& gd : goal_distance)
		{
			const auto& d = gd.second;

			if (d.get_type() == Gint)
			{
				if (std::abs(d.get_const_ref<int>()) != 0) { goal = false; break; }
			}
			else if (d.get_type() == Gdouble)
			{
				if (std::abs(d.get_const_ref<double>()) > feature_error_thresh) { goal = false; break; }
			}
			else if (d.get_type() == GVec)
			{
				if (d.get_const_ref<GVec2>().Length() > feature_error_thresh) { goal = false; break; }
			}
			else { throw std::runtime_error("Unknown type!"); }
		}

		if (goal) { return true; }
	}

	return false;
}

bool AgentTrainer::validate_test_goal(const Test& test) const
{
	for (const auto& goal_ent : test.goal->get_entities())
	{
		if (!validate_goal_entity(goal_ent.second.get()))
		{
			return false;
		}
	}

	return true;
}

bool AgentTrainer::validate_test_conditions(const GAIA::Test& test) const
{
	return simulator.get_frame()->get_frame_number() < test.depth_max;
}

bool AgentTrainer::validate_boundary() const
{
	bool valid = true;
	for (auto& ent : simulator.get_frame()->get_entities())
	{
		auto& entity_state = ent.second->get_state();
		auto entity_position = entity_state.find(Position);
		assert(entity_position != std::end(entity_state));
		auto& pos = entity_position->second.get_const_ref<GVec2>();

		if (pos.x < current_level->boundary_min.x ||
			pos.x > current_level->boundary_max.x ||
			pos.y < current_level->boundary_min.y ||
			pos.y > current_level->boundary_max.y)
		{
			valid = false;
		}
	}

	return valid;
}
