#pragma once

#include "windowframe.h"
#include "trainer/agent-trainer.h"
#include "worldframe-widget.h"

#include <string>

namespace TB
{

class TrainerWindowFrame : public IWindowFrame
{
public:
	/// ctor
	explicit TrainerWindowFrame(const std::shared_ptr<GAIA::Agent>& agent);

	/// returns the name of the scenario
	virtual const char* get_name();

	/// draws based on this scenario
	virtual void draw();
private:

	/// draw the main display
	void draw_display();

	/// draw the side panel
	void draw_panel();

private:
	/// the list of plan scenario names and location
	std::vector<const char*> scenario_names;
	std::vector<std::string> scenario_locations;

	/// a class specifically designed for training the agent
	AgentTrainer trainer;

	bool pause;
	bool initialised;

	/// world frame widgit display
	WorldFrameWidget wf_widget;
};

}
