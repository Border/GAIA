#include "simulator.h"

using namespace GAIA;

void Simulator::initialise(const std::vector<std::shared_ptr<IActionDomain>>& actions)
{
	action_types = actions;
}

void Simulator::reset(const std::shared_ptr<GAIA::WorldFrame>& start_frame)
{
	previous_frame = nullptr;
	current_frame = start_frame;
	actions.clear();
}

void Simulator::process()
{
	process_actions();

	process_world();
}

const std::shared_ptr<GAIA::WorldFrame>& Simulator::get_frame() const
{
	return current_frame;
}

const std::shared_ptr<GAIA::WorldFrame>& Simulator::get_prev_frame() const
{
	return previous_frame;
}

void Simulator::add_actions(const std::vector<EntityAction*>& acts)
{
	for (const auto act : acts)
	{
		actions.push_back(act);
	}
}

void Simulator::generate_frame()
{
	previous_frame = current_frame;
	current_frame = current_frame->copy_next(
		[](const std::shared_ptr<Entity>& ent) { return std::make_shared<ScenarioEntity>(ent); });
}

void Simulator::process_actions()
{
	for (auto& ent : current_frame->get_entities())
	{
		ent.second->state.emplace(sim_state, Generic((int)state_none));
	}

	for (; !actions.empty(); actions.pop_front())
	{
		const auto action = actions.front();
		const auto id = static_cast<ScenarioEntity*>(action->get_entity())->id;

		auto entity = std::find_if(std::begin(current_frame->get_entities()), std::end(current_frame->get_entities()),
			[id](const std::pair<unsigned int, std::shared_ptr<Entity>>& ent) { return static_cast<ScenarioEntity*>(ent.second.get())->id == id; });

		const auto& action_data = action->get_data();

		if (entity != std::end(current_frame->get_entities()))
		{
			switch (action->get_type_id())
			{
			case 0: // move
			{
				const auto& act = action_data[0].data;
				assert(act.get_type() == Gint);

				auto position = entity->second->state.find(Position);
				assert(position != std::end(entity->second->state));

				GVec2 new_pos = position->second.get_const_ref<GVec2>();

				switch (act.get_const_ref<int>())
				{
				case 0: new_pos += GVec2(0.866025403784439, -0.5); break;
				case 1: new_pos += GVec2(0.0, -1.0); break;
				case 2: new_pos += GVec2(0.866025403784439, 0.5); break;
				case 3: new_pos += GVec2(-0.866025403784439, -0.5); break;
				case 4: new_pos += GVec2(0.0, 1.0); break;
				case 5: new_pos += GVec2(-0.866025403784439, 0.5); break;
				}

				// now check none of the entities in the previous frame occupy this position
				bool occupied = false;
				for (const auto& ent : current_frame->get_entities())
				{
					auto& state = ent.second->get_state();
					auto position = state.find(Position);
					assert(position != std::end(state));

					if (vDistance(position->second.get_const_ref<GVec2>(), new_pos) < 1e-6)
					{
						occupied = true;
						break;
					}
				}

				const auto& state = entity->second->state.find(sim_state);
				assert(state != std::end(entity->second->state));

				if (occupied)
				{
					// something occupied the position, so dont move
					state->second.set<int>(state_none);
				}
				else
				{
					// the position is not occupied, so move to it
					state->second.set<int>(state_move);
					position->second.set(new_pos);
				}

				break;
			}
			case 1: // attack
			{
				const auto& act = action_data[0].data;
				assert(act.get_type() == Gint);

				const auto entity_pid = static_cast<unsigned int>(act.get_const_ref<int>());

				// find the entity via the pid
				auto target_entity = current_frame->find_entity(entity_pid);

				auto& entity_state = entity->second->state;

				auto& state_sim = entity_state.find(sim_state);
				assert(state_sim != std::end(entity_state));

				if (target_entity != nullptr)
				{
					auto entity_position = entity_state.find(Position);
					assert(entity_position != std::end(entity_state));

					auto& target_state = target_entity->get_state();
					auto target_position = target_state.find(Position);
					assert(target_position != std::end(target_state));

					// determine if the entity is within range, if so, then:
					if (std::abs(vDistance(target_position->second.get_const_ref<GVec2>(), entity_position->second.get_const_ref<GVec2>()) - 1.0) < 1e-6)
					{
						// calculate damage:
						// damage = total damage - defend mode
						auto dmg = target_entity->state.find(sim_total_dmg);

						// however, as multiple entities can attack simultaneously just
						// calculate the damage and store it
						if (dmg != std::end(target_entity->state))
						{
							dmg->second.get_ref<int>() += 1;
						}
						else
						{
							target_entity->state.emplace(sim_total_dmg, 1);
						}

						state_sim->second.set<int>(state_attack);

						break;
					}
				}

				// cant attack for some reason
				state_sim->second.set<int>(state_none);

				break;
			}
			case 2: // defend
			{
				// set entity state to defense
				const auto& state = entity->second->state.find(sim_state);
				assert(state != std::end(entity->second->state));

				state->second.set<int>(state_defend);

				break;
			}
			case 3: // heal
			{
				// add +1 to hp if less than max hp
				const auto& hp = entity->second->state.find(sim_hp);
				assert(hp != std::end(entity->second->state));

				const auto& state = entity->second->state.find(sim_state);
				assert(state != std::end(entity->second->state));

				if (hp->second.get_const_ref<int>() < 10)
				{
					hp->second.get_ref<int>() += 1;

					state->second.set<int>(state_heal);
				}
				else
				{
					state->second.set<int>(state_none);
				}

				break;
			}
			default: break;
			}
		}
	}
}

void Simulator::process_world()
{
	// post process all the actions and all non action based world 'stuff'
	auto& entities = current_frame->get_entities_mut();

	for (auto it = std::begin(entities); it != std::end(entities); )
	{
		auto& ent = it->second;

		auto state = ent->state.find(sim_state);
		assert(state != std::end(ent->state));

		auto dmg = ent->state.find(sim_total_dmg);
		if (dmg != std::end(ent->state))
		{
			auto hp = ent->state.find(sim_hp);
			assert(hp != std::end(ent->state));

			hp->second.get_ref<int>() -= dmg->second.get_const_ref<int>() - state->second.get_const_ref<int>() == state_defend ? 1 : 0;

			if (hp->second.get_const_ref<int>() <= 0)
			{
				// Entity needs to be removed. There's the issue of how the agent percieves
				// the sudden disapearance of the entity. Would it be better to let the
				// agent see the entity 'dead' for a frame, then remove it, or just make it
				// disapear immediately? For the moment, just make it disapear immediately.

				it = entities.erase(it);
				continue;
			}

			ent->state.erase(dmg);
		}

		ent->state.erase(state);

		++it;
	}
}
