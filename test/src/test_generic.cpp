#include "catch.hpp"

#include "generic.h"

using namespace GAIA;

TEST_CASE("Test Generic Integers", "[Generic]")
{
	Generic generic1(10);

	REQUIRE(generic1.get_type() == GAIA::GType::Gint);
	REQUIRE(generic1.get_const_ref<int>() == 10);

	SECTION("Member Functions")
	{
		REQUIRE(generic1.is_zero() == false);
		REQUIRE(generic1.to_string() == "10");
	}

	SECTION("Copy Construction rvalue")
	{
		Generic generic2(generic1);

		REQUIRE(generic2.get_type() == GAIA::GType::Gint);
		REQUIRE(generic2.get_const_ref<int>() == 10);
	}

	SECTION("Copy Construction lvalue")
	{
		Generic generic2(Generic(20));

		REQUIRE(generic2.get_type() == GAIA::GType::Gint);
		REQUIRE(generic2.get_const_ref<int>() == 20);
	}

	Generic generic2(20);

	REQUIRE(generic2.get_type() == GAIA::GType::Gint);
	REQUIRE(generic2.get_const_ref<int>() == 20);

	SECTION("Operator +")
	{
		auto res = generic1 + generic2;

		REQUIRE(res.get_type() == GAIA::GType::Gint);
		REQUIRE(res.get_const_ref<int>() == 30);
	}

	SECTION("Operator -()")
	{
		auto res = -generic1;

		REQUIRE(res.get_type() == GAIA::GType::Gint);
		REQUIRE(res.get_const_ref<int>() == -10);
	}

	SECTION("Operator -")
	{
		auto res = generic1 - generic2;

		REQUIRE(res.get_type() == GAIA::GType::Gint);
		REQUIRE(res.get_const_ref<int>() == -10);
	}

	SECTION("Operator *")
	{
		auto res = generic1 * generic2;

		REQUIRE(res.get_type() == GAIA::GType::Gint);
		REQUIRE(res.get_const_ref<int>() == 200);
	}

	SECTION("Operator /")
	{
		auto res = generic2 / generic1;

		REQUIRE(res.get_type() == GAIA::GType::Gint);
		REQUIRE(res.get_const_ref<int>() == 2);
	}

	SECTION("Operator +=")
	{
		auto res = generic2 += generic1;

		REQUIRE(res.get_type() == GAIA::GType::Gint);
		REQUIRE(res.get_const_ref<int>() == 30);
	}

	SECTION("Operator -=")
	{
		auto res = generic2 -= generic1;

		REQUIRE(res.get_type() == GAIA::GType::Gint);
		REQUIRE(res.get_const_ref<int>() == 10);
	}

	SECTION("Operator ==")
	{
		REQUIRE(!(generic1 == generic2));
		REQUIRE(generic1 == Generic(10));
	}

	SECTION("Operator !=")
	{
		REQUIRE(generic1 != generic2);
		REQUIRE(!(generic1 != Generic(10)));
	}

	SECTION("Operator <")
	{
		REQUIRE(generic1 < generic2);
		REQUIRE(!(generic1 < Generic(1)));
	}
}

TEST_CASE("Test Generic Double", "[Generic]")
{
	Generic generic1(10.0);

	REQUIRE(generic1.get_type() == GAIA::GType::Gdouble);
	REQUIRE(generic1.get_const_ref<double>() == 10.0);

	SECTION("Member Functions")
	{
		REQUIRE(generic1.is_zero() == false);
		REQUIRE(generic1.to_string() == "10");
	}

	SECTION("Copy Construction rvalue")
	{
		Generic generic2(generic1);

		REQUIRE(generic2.get_type() == GAIA::GType::Gdouble);
		REQUIRE(generic2.get_const_ref<double>() == 10.0);
	}

	SECTION("Copy Construction lvalue")
	{
		Generic generic2(Generic(20.0));

		REQUIRE(generic2.get_type() == GAIA::GType::Gdouble);
		REQUIRE(generic2.get_const_ref<double>() == 20.0);
	}

	Generic generic2(20.0);

	REQUIRE(generic2.get_type() == GAIA::GType::Gdouble);
	REQUIRE(generic2.get_const_ref<double>() == 20.0);

	SECTION("Operator +")
	{
		auto res = generic1 + generic2;

		REQUIRE(res.get_type() == GAIA::GType::Gdouble);
		REQUIRE(res.get_const_ref<double>() == 30.0);
	}

	SECTION("Operator -()")
	{
		auto res = -generic1;

		REQUIRE(res.get_type() == GAIA::GType::Gdouble);
		REQUIRE(res.get_const_ref<double>() == -10.0);
	}

	SECTION("Operator -")
	{
		auto res = generic1 - generic2;

		REQUIRE(res.get_type() == GAIA::GType::Gdouble);
		REQUIRE(res.get_const_ref<double>() == -10.0);
	}

	SECTION("Operator +=")
	{
		auto res = generic2 += generic1;

		REQUIRE(res.get_type() == GAIA::GType::Gdouble);
		REQUIRE(res.get_const_ref<double>() == 30);
	}

	SECTION("Operator -=")
	{
		auto res = generic2 -= generic1;

		REQUIRE(res.get_type() == GAIA::GType::Gdouble);
		REQUIRE(res.get_const_ref<double>() == 10);
	}

	SECTION("Operator *")
	{
		auto res = generic1 * generic2;

		REQUIRE(res.get_type() == GAIA::GType::Gdouble);
		REQUIRE(res.get_const_ref<double>() == 200.0);
	}

	SECTION("Operator /")
	{
		auto res = generic2 / generic1;

		REQUIRE(res.get_type() == GAIA::GType::Gdouble);
		REQUIRE(res.get_const_ref<double>() == 2.0);
	}

	SECTION("Operator ==")
	{
		REQUIRE(!(generic1 == generic2));
		REQUIRE(generic1 == Generic(10.0));
	}

	SECTION("Operator !=")
	{
		REQUIRE(generic1 != generic2);
		REQUIRE(!(generic1 != Generic(10.0)));
	}

	SECTION("Operator <")
	{
		REQUIRE(generic1 < generic2);
		REQUIRE(!(generic1 < Generic(1.0)));
	}
}

TEST_CASE("Test Generic GVec2", "[Generic]")
{
	Generic generic1(GVec2(1,2));

	REQUIRE(generic1.get_type() == GAIA::GType::GVec);
	REQUIRE(generic1.get_const_ref<GVec2>() == GVec2(1, 2));

	SECTION("Member Functions")
	{
		REQUIRE(generic1.is_zero() == false);
		REQUIRE(generic1.to_string() == "1,2");
	}

	SECTION("Copy Construction rvalue")
	{
		Generic generic2(generic1);

		REQUIRE(generic2.get_type() == GAIA::GType::GVec);
		REQUIRE(generic2.get_const_ref<GVec2>() == GVec2(1, 2));
	}

	SECTION("Copy Construction lvalue")
	{
		Generic generic2(Generic(GVec2(3, 2)));

		REQUIRE(generic2.get_type() == GAIA::GType::GVec);
		REQUIRE(generic2.get_const_ref<GVec2>() == GVec2(3, 2));
	}

	Generic generic2(GVec2(3, 2));

	REQUIRE(generic2.get_type() == GAIA::GType::GVec);
	REQUIRE(generic2.get_const_ref<GVec2>() == GVec2(3, 2));

	SECTION("Operator +")
	{
		auto res = generic1 + generic2;

		REQUIRE(res.get_type() == GAIA::GType::GVec);
		REQUIRE(res.get_const_ref<GVec2>() == GVec2(4, 4));
	}

	SECTION("Operator -()")
	{
		auto res = -generic1;

		REQUIRE(res.get_type() == GAIA::GType::GVec);
		REQUIRE(res.get_const_ref<GVec2>() == GVec2(-1, -2));
	}

	SECTION("Operator -")
	{
		auto res = generic1 - generic2;

		REQUIRE(res.get_type() == GAIA::GType::GVec);
		REQUIRE(res.get_const_ref<GVec2>() == GVec2(-2, 0));
	}

	SECTION("Operator +=")
	{
		auto res = generic2 += generic1;

		REQUIRE(res.get_type() == GAIA::GType::GVec);
		REQUIRE(res.get_const_ref<GVec2>() == GVec2(4, 4));
	}

	SECTION("Operator -=")
	{
		auto res = generic2 -= generic1;

		REQUIRE(res.get_type() == GAIA::GType::GVec);
		REQUIRE(res.get_const_ref<GVec2>() == GVec2(2, 0));
	}

	SECTION("Operator *")
	{
		auto res = generic1 * generic2;

		REQUIRE(res.get_type() == GAIA::GType::GVec);
		REQUIRE(res.get_const_ref<GVec2>() == GVec2(3, 4));
	}

	SECTION("Operator /")
	{
		auto res = generic2 / generic1;

		REQUIRE(res.get_type() == GAIA::GType::GVec);
		REQUIRE(res.get_const_ref<GVec2>() == GVec2(3, 1));
	}

	SECTION("Operator ==")
	{
		REQUIRE(!(generic1 == generic2));
		REQUIRE(generic1 == Generic(GVec2(1, 2)));
	}

	SECTION("Operator !=")
	{
		REQUIRE(generic1 != generic2);
		REQUIRE(!(generic1 != Generic(GVec2(1, 2))));
	}

	SECTION("Operator <")
	{
		REQUIRE(generic1 < generic2);
		REQUIRE(!(generic1 < Generic(GVec2(1, 1))));
	}
}
