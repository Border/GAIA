#include "catch.hpp"

#include "agent.h"
#include "planner.h"
#include "affordance-classifier.h"

TEST_CASE("Test basic affordances", "[Agent]")
{
	GAIA::Agent agent;
	agent.match_compare = [](const std::shared_ptr<GAIA::Entity>& a, const std::shared_ptr<GAIA::Entity>& b)
	{
		if (a == nullptr || b == nullptr) { return 1.0; }
		else { return static_cast<GAIA::ScenarioEntity*>(a.get())->id == static_cast<GAIA::ScenarioEntity*>(b.get())->id ? 0.0 : 1.0; }
	};

	SECTION("Test move affordances")
	{
		auto scenario = GAIA::IO::read_exemplar_scenarios("scenarios//move.json");

		REQUIRE(scenario.size() == 60);

		for (auto& frame_pair : scenario)
		{
			REQUIRE(frame_pair.first->get_entities().size() == 1);
			REQUIRE(frame_pair.second->get_entities().size() == 1);

			agent.add_frame_pair(frame_pair.first, frame_pair.second);
		}

		// now test affordances are correct
		for (auto& frame_pair : scenario)
		{
			auto results = agent.test_frame_pair(frame_pair.first, frame_pair.second);

			for (auto& r : results)
			{
				REQUIRE(r.second.size() == 1);
			}
		}

		auto affordances = GAIA::AffordanceClassifier::Instance()->get_affordances();

		REQUIRE(affordances.size() == 6);
	}

	SECTION("Test failed actions")
	{
		auto scenario = GAIA::IO::read_exemplar_scenarios("scenarios//move-fail.json");

		REQUIRE(scenario.size() == 60);

		for (auto& frame_pair : scenario)
		{
			REQUIRE(frame_pair.first->get_entities().size() == 2);
			REQUIRE(frame_pair.second->get_entities().size() == 2);

			agent.add_frame_pair(frame_pair.first, frame_pair.second);
		}
	}

	SECTION("Test restricted plan")
	{
		auto plan_data = GAIA::IO::read_single_test("scenarios//plan-move-restricted.json");

		GAIA::Planner planner;

		auto plan = planner.generate_plan(GAIA::PlanParams(plan_data.goal), plan_data.start.get());

		REQUIRE(plan->get_num_entity_solutions() == 2);
	}

	SECTION("Test restricted plan multi entity")
	{
		auto plan_data = GAIA::IO::read_single_test("scenarios//plan-move-restricted-multi-entity.json");

		GAIA::Planner planner;

		auto plan = planner.generate_plan(GAIA::PlanParams(plan_data.goal), plan_data.start.get());

		REQUIRE(plan->get_num_entity_solutions() == 2);
	}

	SECTION("Test plan unrestricted")
	{
		auto plan_data = GAIA::IO::read_single_test("scenarios//plan-move.json");

		GAIA::Planner planner;

		auto plan = planner.generate_plan(GAIA::PlanParams(plan_data.goal), plan_data.start.get());

		REQUIRE(plan->get_num_entity_solutions() == 2);
	}

	// this must go last after testing all plans and scenario learning
	SECTION("Test load action ranges")
	{
		auto action_ranges = GAIA::IO::read_action_ranges("scenarios//actions.json");

		REQUIRE(action_ranges.size() == 1);

		auto action = action_ranges.front()->generate_action(nullptr);

		agent.add_action_ranges(action_ranges);
	}
}
