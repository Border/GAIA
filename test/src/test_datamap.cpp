#include "catch.hpp"

#include "datamap.h"

using namespace GAIA;

TEST_CASE("Test DataMap operators - equal size", "[DataMap]")
{
	DataMap datamap1;

	for (int i = 0; i < 20; ++i)
	{
		datamap1.emplace(i, i);
	}

	REQUIRE(datamap1.size() == 20);

	SECTION("Negation operator")
	{
		DataMap datamapnew = -datamap1;

		REQUIRE(datamapnew.size() == 20);

		for (auto& d : datamapnew)
		{
			auto i = datamap1.find(d.first);
			REQUIRE(i != datamap1.end());
			REQUIRE(d.second == -i->second);
		}
	}

	DataMap datamap2;

	for (int i = 19; i >= 0; --i)
	{
		datamap2.emplace(i, i * i);
	}

	REQUIRE(datamap2.size() == 20);

	SECTION("Addition operator")
	{
		DataMap datamapnew = datamap1 + datamap2;

		REQUIRE(datamapnew.size() == 20);

		for (auto& d : datamapnew)
		{
			auto i = datamap1.find(d.first);
			REQUIRE(i != datamap1.end());
			auto j = datamap2.find(d.first);
			REQUIRE(j != datamap2.end());

			auto sum = i->second + j->second;

			REQUIRE(d.second == sum);
		}
	}

	SECTION("Multiplication operator")
	{
		DataMap datamapnew = datamap1 * datamap2;

		REQUIRE(datamapnew.size() == 20);

		for (auto& d : datamapnew)
		{
			auto i = datamap1.find(d.first);
			REQUIRE(i != datamap1.end());
			auto j = datamap2.find(d.first);
			REQUIRE(j != datamap2.end());

			auto mul = i->second * j->second;

			REQUIRE(d.second == mul);
		}
	}

	SECTION("Division operator")
	{
		DataMap datamapnew = datamap1 / datamap2;

		REQUIRE(datamapnew.size() == 20);

		for (auto& d : datamapnew)
		{
			auto i = datamap1.find(d.first);
			REQUIRE(i != datamap1.end());
			auto j = datamap2.find(d.first);
			REQUIRE(j != datamap2.end());

			if (j->second.is_zero())
			{
				REQUIRE(d.second == j->second);
			}
			else
			{
				auto mul = i->second / j->second;

				REQUIRE(d.second == mul);
			}
		}
	}

	DataMap datamap3;

	for (int i = 0; i < 20; ++i)
	{
		datamap3.emplace(i, i);
	}

	SECTION("Equality operator")
	{
		auto res = datamap1 == datamap3;

		REQUIRE(res == true);
	}
}

TEST_CASE("Test DataMap operators - different size", "[DataMap]")
{
	DataMap datamap1;

	datamap1.emplace(0, 10);
	datamap1.emplace(1, 10);
	datamap1.emplace(3, 10);
	datamap1.emplace(9, 10);

	DataMap datamap2;

	datamap2.emplace(1, 10);
	datamap2.emplace(9, 10);

	REQUIRE(datamap1.size() == 4);
	REQUIRE(datamap2.size() == 2);

	SECTION("Addition operator")
	{
		DataMap datamapnew = datamap1 + datamap2;

		REQUIRE(datamapnew.size() == 4);

		{
			auto a = datamapnew.find(0);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 10);
		}
		{
			auto a = datamapnew.find(1);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 20);
		}
		{
			auto a = datamapnew.find(3);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 10);
		}
		{
			auto a = datamapnew.find(9);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 20);
		}
	}

	SECTION("Subtraction operator v1")
	{
		DataMap datamapnew = datamap1 - datamap2;

		REQUIRE(datamapnew.size() == 4);

		{
			auto a = datamapnew.find(0);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 10);
		}
		{
			auto a = datamapnew.find(1);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 0);
		}
		{
			auto a = datamapnew.find(3);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 10);
		}
		{
			auto a = datamapnew.find(9);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 0);
		}
	}

	SECTION("Subtraction operator v2")
	{
		DataMap datamapnew = datamap2 - datamap1;

		REQUIRE(datamapnew.size() == 4);

		{
			auto a = datamapnew.find(0);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == -10);
		}
		{
			auto a = datamapnew.find(1);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 0);
		}
		{
			auto a = datamapnew.find(3);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == -10);
		}
		{
			auto a = datamapnew.find(9);
			REQUIRE(a != datamapnew.end());
			REQUIRE(a->second == 0);
		}
	}

}
