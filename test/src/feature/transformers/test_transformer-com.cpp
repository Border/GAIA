#include "catch.hpp"

#include "transformers/transformer-com.h"
#include "feature-transformer.h"
#include "frame-context.h"
#include "vecmath.h"

using namespace GAIA;


// ============================================================================
// Test integer based transformer centre of mass (COM)

TEST_CASE("[int] TransformerCOM", "[TransformerCOM]")
{
	FrameContext ex(nullptr, nullptr);
	std::vector<FeatureDataContext> mock;

	for (int i = 0; i < 10; ++i)
	{
		mock.push_back({ ex, FeatureReferenceType::internal, Generic(3), 0, nullptr });
	}

	Transformer_com<int> transformer(mock);

	REQUIRE(transformer.get_type() == TransformerType::com);

	REQUIRE(transformer.generate_data() == Generic(3));


	SECTION("No error")
	{
		auto error = transformer.generate_error(Generic(3));

		REQUIRE(error->get_error() == Generic(0));
		REQUIRE(error->get_error_rbf() == 0.0);

		transformer.update_feature(error.get());

		REQUIRE(transformer.get_const_ref_feature().count == 11);
		REQUIRE(transformer.get_const_ref_feature().mean == 3);
		REQUIRE(transformer.get_const_ref_feature().stdev == 0);
	}

	SECTION("error")
	{
		auto error = transformer.generate_error(Generic(4));

		REQUIRE(error->get_error() == Generic(1));
		REQUIRE(error->get_error_rbf() == 1.0);

		transformer.update_feature(error.get());

		REQUIRE(transformer.get_const_ref_feature().count == 11);
		REQUIRE(transformer.get_const_ref_feature().mean == 3);
		REQUIRE(transformer.get_const_ref_feature().stdev == 0);
	}
}

// ============================================================================
// Test double based transformer centre of mass (COM)

TEST_CASE("[double] TransformerCOM", "[TransformerCOM]")
{
	FrameContext ex(nullptr, nullptr);
	std::vector<FeatureDataContext> mock;

	for (int i = 0; i < 10; ++i)
	{
		mock.push_back({ ex, FeatureReferenceType::internal, Generic((double)i), 0, nullptr });
	}

	Transformer_com<double> transformer(mock);

	REQUIRE(transformer.get_type() == TransformerType::com);

	REQUIRE(transformer.generate_data() == Generic(4.5));


	SECTION("No error")
	{
		auto error = transformer.generate_error(Generic(4.5));

		REQUIRE(error->get_error() == Generic(0.0));
		REQUIRE(error->get_error_rbf() == 0.0);

		transformer.update_feature(error.get());

		REQUIRE(transformer.get_const_ref_feature().count == 11);
		REQUIRE(transformer.get_const_ref_feature().mean == 4.5);
		REQUIRE(std::abs(transformer.get_const_ref_feature().stdev - 2.7386127875258306) < 1e-10);
	}

	SECTION("error")
	{
		auto error = transformer.generate_error(Generic(100.0));

		REQUIRE(error->get_error() == Generic(95.5));
		REQUIRE(error->get_error_rbf() == 1.0);

		transformer.update_feature(error.get());

		REQUIRE(transformer.get_const_ref_feature().count == 11);
		REQUIRE(transformer.get_const_ref_feature().mean == 13.181818181818181818181818181818);
		REQUIRE(std::abs(transformer.get_const_ref_feature().stdev - 27.590572111166662) < 1e-10);
	}
}

// ============================================================================
// Test GVec2 based transformer centre of mass (COM)

