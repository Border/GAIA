//#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_RUNNER  // Indicates we are supplying our own main()
#include "catch.hpp"

int main(int argc, char* argv[])
{
	// global setup...
	int result = Catch::Session().run(argc, argv);

	// global clean-up...

	return (result < 0xff ? result : 0xff);
}
