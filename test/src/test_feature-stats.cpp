#include "catch.hpp"

#include "feature-stats.h"
#include "frame-context.h"
#include "vecmath.h"

#include <limits>

using namespace GAIA;


// ============================================================================
// Test integer based feature stats

TEST_CASE("[int] default construction", "[FeatureStats]")
{
	FeatureStats<int> stats;

	REQUIRE(stats.get_type() == GType::Gint);
	REQUIRE(stats.count == 0);
	REQUIRE(stats.S == 0);
	REQUIRE(stats.stdev == 0);
	REQUIRE(stats.sum == 0);
	REQUIRE(stats.max == 0);
	REQUIRE(stats.min == 0);
	REQUIRE(stats.mean == 0);

	int x = 5;
	stats.update_specific(x);

	REQUIRE(stats.count == 1);
	REQUIRE(stats.S == 0);
	REQUIRE(stats.stdev == 0);
	REQUIRE(stats.sum == 5);
	REQUIRE(stats.max == 5);
	REQUIRE(stats.min == 5);
	REQUIRE(stats.mean == 5);

	SECTION("Add a second different element")
	{
		int y = 2;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == 3);
		REQUIRE(stats.stdev == 1);
		REQUIRE(stats.sum == 7);
		REQUIRE(stats.max == 5);
		REQUIRE(stats.min == 2);
		REQUIRE(stats.mean == 3);
	}

	SECTION("Add a second identical element")
	{
		int y = 5;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == 0);
		REQUIRE(stats.stdev == 0);
		REQUIRE(stats.sum == 10);
		REQUIRE(stats.max == 5);
		REQUIRE(stats.min == 5);
		REQUIRE(stats.mean == 5);
	}

	SECTION("Add a second 0 element")
	{
		int y = 0;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == 10);
		REQUIRE(stats.stdev == 2);
		REQUIRE(stats.sum == 5);
		REQUIRE(stats.max == 5);
		REQUIRE(stats.min == 0);
		REQUIRE(stats.mean == 2);
	}
}

TEST_CASE("[int] Generic construction", "[FeatureStats]")
{
	FeatureStats<int> stats(Generic(5));

	REQUIRE(stats.get_type() == GType::Gint);
	REQUIRE(stats.count == 1);
	REQUIRE(stats.S == 0);
	REQUIRE(stats.stdev == 0);
	REQUIRE(stats.sum == 5);
	REQUIRE(stats.max == 5);
	REQUIRE(stats.min == 5);
	REQUIRE(stats.mean == 5);

	SECTION("Add a second different element")
	{
		int y = 2;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == 3);
		REQUIRE(stats.stdev == 1);
		REQUIRE(stats.sum == 7);
		REQUIRE(stats.max == 5);
		REQUIRE(stats.min == 2);
		REQUIRE(stats.mean == 3);
	}

	SECTION("Add a second identical element")
	{
		int y = 5;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == 0);
		REQUIRE(stats.stdev == 0);
		REQUIRE(stats.sum == 10);
		REQUIRE(stats.max == 5);
		REQUIRE(stats.min == 5);
		REQUIRE(stats.mean == 5);
	}

	SECTION("Add a second 0 element")
	{
		int y = 0;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == 10);
		REQUIRE(stats.stdev == 2);
		REQUIRE(stats.sum == 5);
		REQUIRE(stats.max == 5);
		REQUIRE(stats.min == 0);
		REQUIRE(stats.mean == 2);
	}
}



// ============================================================================
// Test double based feature stats

TEST_CASE("[double] default construction", "[FeatureStats]")
{
	FeatureStats<double> stats;

	REQUIRE(stats.get_type() == GType::Gdouble);
	REQUIRE(stats.count == 0);
	REQUIRE(stats.S == 0.0);
	REQUIRE(stats.stdev == 0.0);
	REQUIRE(stats.sum == 0.0);
	REQUIRE(stats.max == 0.0);
	REQUIRE(stats.min == 0.0);
	REQUIRE(stats.mean == 0.0);

	double x = 5.0;
	stats.update_specific(x);

	REQUIRE(stats.count == 1);
	REQUIRE(stats.S == 0.0);
	REQUIRE(stats.stdev == 0.0);
	REQUIRE(stats.sum == 5.0);
	REQUIRE(stats.max == 5.0);
	REQUIRE(stats.min == 5.0);
	REQUIRE(stats.mean == 5.0);

	SECTION("Add a second different element")
	{
		double y = 2.0;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(std::abs(stats.S - 4.5) < std::numeric_limits<double>::epsilon());
		REQUIRE(std::abs(stats.stdev - 1.5) < std::numeric_limits<double>::epsilon());
		REQUIRE(stats.sum == 7.0);
		REQUIRE(stats.max == 5.0);
		REQUIRE(stats.min == 2.0);
		REQUIRE(stats.mean == 3.5);
	}

	SECTION("Add a second identical element")
	{
		double y = 5.0;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == 0.0);
		REQUIRE(stats.stdev == 0.0);
		REQUIRE(stats.sum == 10.0);
		REQUIRE(stats.max == 5.0);
		REQUIRE(stats.min == 5.0);
		REQUIRE(stats.mean == 5.0);
	}

	SECTION("Add a second 0 element")
	{
		double y = 0.0;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(std::abs(stats.S - 12.5) < std::numeric_limits<double>::epsilon());
		REQUIRE(std::abs(stats.stdev - 2.5) < std::numeric_limits<double>::epsilon());
		REQUIRE(stats.sum == 5);
		REQUIRE(stats.max == 5);
		REQUIRE(stats.min == 0);
		REQUIRE(std::abs(stats.mean - 2.5) < std::numeric_limits<double>::epsilon());
	}
}

TEST_CASE("[double] Generic construction", "[FeatureStats]")
{
	FeatureStats<double> stats(Generic(5.0));

	REQUIRE(stats.get_type() == GType::Gdouble);
	REQUIRE(stats.count == 1);
	REQUIRE(stats.S == 0.0);
	REQUIRE(stats.stdev == 0.0);
	REQUIRE(stats.sum == 5.0);
	REQUIRE(stats.max == 5.0);
	REQUIRE(stats.min == 5.0);
	REQUIRE(stats.mean == 5.0);

	SECTION("Add a second different element")
	{
		double y = 2.0;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(std::abs(stats.S - 4.5) < std::numeric_limits<double>::epsilon());
		REQUIRE(std::abs(stats.stdev - 1.5) < std::numeric_limits<double>::epsilon());
		REQUIRE(stats.sum == 7.0);
		REQUIRE(stats.max == 5.0);
		REQUIRE(stats.min == 2.0);
		REQUIRE(stats.mean == 3.5);
	}

	SECTION("Add a second identical element")
	{
		double y = 5.0;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == 0.0);
		REQUIRE(stats.stdev == 0.0);
		REQUIRE(stats.sum == 10.0);
		REQUIRE(stats.max == 5.0);
		REQUIRE(stats.min == 5.0);
		REQUIRE(stats.mean == 5.0);
	}

	SECTION("Add a second 0 element")
	{
		double y = 0.0;
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(std::abs(stats.S - 12.5) < std::numeric_limits<double>::epsilon());
		REQUIRE(std::abs(stats.stdev - 2.5) < std::numeric_limits<double>::epsilon());
		REQUIRE(stats.sum == 5);
		REQUIRE(stats.max == 5);
		REQUIRE(stats.min == 0);
		REQUIRE(std::abs(stats.mean - 2.5) < std::numeric_limits<double>::epsilon());
	}
}



// ============================================================================
// Test GVec2 based feature stats

TEST_CASE("[GVec2] default construction", "[FeatureStats]")
{
	FeatureStats<GVec2> stats;

	REQUIRE(stats.get_type() == GType::GVec);
	REQUIRE(stats.count == 0);
	REQUIRE(stats.S == GVec2(0.0,0.0));
	REQUIRE(stats.stdev == GVec2(0.0, 0.0));
	REQUIRE(stats.sum == GVec2(0.0, 0.0));
	REQUIRE(stats.max == GVec2(0.0, 0.0));
	REQUIRE(stats.min == GVec2(0.0, 0.0));
	REQUIRE(stats.mean == GVec2(0.0, 0.0));

	GVec2 x(5.0, 4.0);
	stats.update_specific(x);

	REQUIRE(stats.count == 1);
	REQUIRE(stats.S == GVec2(0.0, 0.0));
	REQUIRE(stats.stdev == GVec2(0.0, 0.0));
	REQUIRE(stats.sum == GVec2(5.0, 4.0));
	REQUIRE(stats.max == GVec2(5.0, 4.0));
	REQUIRE(stats.min == GVec2(5.0, 4.0));
	REQUIRE(stats.mean == GVec2(5.0, 4.0));

	SECTION("Add a second different element")
	{
		GVec2 y(2.0, 1.0);
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE((stats.S - GVec2(4.5, 4.5)).Length() < std::numeric_limits<double>::epsilon());
		REQUIRE((stats.stdev - GVec2(1.5, 1.5)).Length() < std::numeric_limits<double>::epsilon());
		REQUIRE(stats.sum == GVec2(7.0, 5.0));
		REQUIRE(stats.max == GVec2(5.0, 4.0));
		REQUIRE(stats.min == GVec2(2.0, 1.0));
		REQUIRE(stats.mean == GVec2(3.5, 2.5));
	}

	SECTION("Add a second identical element")
	{
		GVec2 y(5.0, 4.0);
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == GVec2(0.0, 0.0));
		REQUIRE(stats.stdev == GVec2(0.0, 0.0));
		REQUIRE(stats.sum == GVec2(10.0, 8.0));
		REQUIRE(stats.max == GVec2(5.0, 4.0));
		REQUIRE(stats.min == GVec2(5.0, 4.0));
		REQUIRE(stats.mean == GVec2(5.0, 4.0));
	}

	SECTION("Add a second 0 element")
	{
		GVec2 y(0.0,0.0);
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE((stats.S - GVec2(12.5, 8.0)).Length() < std::numeric_limits<double>::epsilon());
		REQUIRE((stats.stdev - GVec2(2.5, 2.0)).Length() < std::numeric_limits<double>::epsilon());
		REQUIRE(stats.sum == GVec2(5.0, 4.0));
		REQUIRE(stats.max == GVec2(5.0, 4.0));
		REQUIRE(stats.min == GVec2(0.0, 0.0));
		REQUIRE(stats.mean == GVec2(2.5, 2.0));
	}
}

TEST_CASE("[GVec2] Generic construction", "[FeatureStats]")
{
	FeatureStats<GVec2> stats(Generic(GVec2(5.0, 4.0)));

	REQUIRE(stats.get_type() == GType::GVec);
	REQUIRE(stats.count == 1);
	REQUIRE(stats.S == GVec2(0.0, 0.0));
	REQUIRE(stats.stdev == GVec2(0.0, 0.0));
	REQUIRE(stats.sum == GVec2(5.0, 4.0));
	REQUIRE(stats.max == GVec2(5.0, 4.0));
	REQUIRE(stats.min == GVec2(5.0, 4.0));
	REQUIRE(stats.mean == GVec2(5.0, 4.0));

	SECTION("Add a second different element")
	{
		GVec2 y(2.0, 1.0);
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE((stats.S - GVec2(4.5, 4.5)).Length() < std::numeric_limits<double>::epsilon());
		REQUIRE((stats.stdev - GVec2(1.5, 1.5)).Length() < std::numeric_limits<double>::epsilon());
		REQUIRE(stats.sum == GVec2(7.0, 5.0));
		REQUIRE(stats.max == GVec2(5.0, 4.0));
		REQUIRE(stats.min == GVec2(2.0, 1.0));
		REQUIRE(stats.mean == GVec2(3.5, 2.5));
	}

	SECTION("Add a second identical element")
	{
		GVec2 y(5.0, 4.0);
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE(stats.S == GVec2(0.0, 0.0));
		REQUIRE(stats.stdev == GVec2(0.0, 0.0));
		REQUIRE(stats.sum == GVec2(10.0, 8.0));
		REQUIRE(stats.max == GVec2(5.0, 4.0));
		REQUIRE(stats.min == GVec2(5.0, 4.0));
		REQUIRE(stats.mean == GVec2(5.0, 4.0));
	}

	SECTION("Add a second 0 element")
	{
		GVec2 y(0.0, 0.0);
		stats.update_specific(y);

		REQUIRE(stats.count == 2);
		REQUIRE((stats.S - GVec2(12.5, 8.0)).Length() < std::numeric_limits<double>::epsilon());
		REQUIRE((stats.stdev - GVec2(2.5, 2.0)).Length() < std::numeric_limits<double>::epsilon());
		REQUIRE(stats.sum == GVec2(5.0, 4.0));
		REQUIRE(stats.max == GVec2(5.0, 4.0));
		REQUIRE(stats.min == GVec2(0.0, 0.0));
		REQUIRE(stats.mean == GVec2(2.5, 2.0));
	}
}



// ============================================================================
// Test rbf error function

TEST_CASE("[double] error_rbf", "[RBF_Error]")
{
	REQUIRE(error_rbf(0.0, 0.0) == 0.0);
	REQUIRE(error_rbf(1.0, 0.0) == 1.0);
	REQUIRE(error_rbf(1.0e-3, 0.0) == 1.0);
	REQUIRE(error_rbf(0.0, 1.0) < 1e-10);
	REQUIRE(std::abs(error_rbf(1.0, 1.0) - 0.537882842739902) < 1e-10);
	REQUIRE(std::abs(error_rbf(10.0, 1.0) - 1.0) < 1e-10);
	REQUIRE(std::abs(error_rbf(100.0, 1.0) - 1.0) < 1e-10);
	REQUIRE(std::abs(error_rbf(0.98503182334774596, 1.0) - 0.5) < 1e-10);
	REQUIRE(std::abs(error_rbf(0.1, 0.1) - 0.537882842739902) < 1e-10);
}

TEST_CASE("[GVec2] error_rbf", "[RBF_Error]")
{
	GVec2 error_0(0.0, 0.0);
	GVec2 error_1(1.0, 1.0);
	GVec2 error_tiny(1.0e-3, 1.0e-3);
	GVec2 error_10(10.0, 10.0);
	GVec2 error_100(100.0, 100.0);
	GVec2 error_match(0.98503182334774596, 0.0);
	GVec2 error_small(0.1, 0.1);

	GVec2 stdev_0(0.0, 0.0);
	GVec2 stdev_1(1.0, 1.0);
	GVec2 stdev_small(0.1, 0.1);

	REQUIRE(error_rbf(error_0, stdev_0) == 0.0);
	REQUIRE(error_rbf(error_1, stdev_0) == 1.0);
	REQUIRE(error_rbf(error_tiny, stdev_0) == 1.0);
	REQUIRE(error_rbf(error_0, stdev_1) < 1e-10);
	REQUIRE(std::abs(error_rbf(error_1, stdev_1) - 0.537882842739902) < 1e-10);
	REQUIRE(std::abs(error_rbf(error_10, stdev_1) - 1.0) < 1e-10);
	REQUIRE(std::abs(error_rbf(error_100, stdev_1) - 1.0) < 1e-10);
	REQUIRE(std::abs(error_rbf(error_match, stdev_1) - 0.5) < 1e-10);
	REQUIRE(std::abs(error_rbf(error_small, stdev_small) - 0.537882842739902) < 1e-10);
}
