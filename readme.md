GAIA - **G**eneralised **A**ffordance-based **I**ntelligent **A**gent
=================================================================

The focus of this project is on generalising and learning various affordances which characterise an entities interactions/actions/reactions. A temporal difference approach is taken to interpreting how the state space is changed over time. Features are identified and learned based on the temporal differences of entities between observed frames. These features are then used to form events, which are groups of features that describe a change in an entities state. The events are then used to form part of an affordances identity.

In order to properly characterise an affordance, the initial and final conditions must be known. These conditions are characterised by groups of relevant features. They are identified and learned based on the events that occur that are observed by the agent. When the agent observes the same event occurring, it compares the initial state of all the observed instances, identifying commonalities among them. A group of features, or condition state can then be created. The same is done for the final state.

Features can be generalised by identifying invariances by using related independent variables. Another method to generalise is to recognise various forms of symmetry, which can be used to simplify features. This generalisation enables the agent to learn common features and recognise associations and dependencies between events/states.

The agent will use motivated learning to explore its environment and learn how to optimise its interactions. Interest and novelty will be the primary motivators for this. Reinforcement learning will be used to control the agents learning behaviour as well as learn favourable or undesirable behaviour and states.

The project is in its early stages, with only temporal difference recognition and basic feature learning implemented. Work is focused on developing a solid framework for learning and generalising features before higher level frameworks are implemented.
