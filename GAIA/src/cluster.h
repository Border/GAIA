#pragma once

#include "feature-stats.h"

#include <vector>
#include <unordered_map>
#include <memory>
#include <algorithm>

namespace GAIA
{

/// An enum indicating the state of an exemplar cluster when performing clustering
enum ClusterState
{
	unvisited,
	visited,
	noise,
};

/// Contains matching exemplars based on the a data type
template<typename E, class T>
struct ExemplarGroup
{
	/// ctor - brand new
	ExemplarGroup(E ex, T ex_data) :
		data(std::move(ex_data)),
		exemplars({ std::move(ex) })
	{
	}

	/// adds the exemplar to the list of exemplars
	void add_exemplar(E ex)
	{
		exemplars.push_back(std::move(ex));
	}

	/// The type which defines this group
	const T data;

	/// A list of exemplars that occurred for T
	std::vector<E> exemplars;
};

/// A temporary structure for an exemplar group to hold temporary cluster data
template<typename E, class T>
struct ExemplarCluster
{
	/// ctor
	ExemplarCluster(ExemplarGroup<E, T>* _exgrp) :
		exgrp(_exgrp),
		cluster_state(ClusterState::unvisited)
	{
	}

	/// the exemplar group this struct is wrapping
	ExemplarGroup<E, T>* exgrp;

	/// A ist of exemplar neighbours within a specified range
	std::vector<std::pair<double, std::weak_ptr<ExemplarCluster<E, T>>>> neighbours;

	/// The state of the exemplar group while performing clustering
	ClusterState cluster_state;
};

/// Contains exemplar groups which have been determined to form a cluster. This struct also
/// provides function to perform operations during cluster generation and at the end of
/// cluster generation.
template<typename E, class T>
struct Cluster
{
	/// ctor
	Cluster() {}

	/// a pure virtual function used to call update processing for the cluster
	virtual void update_cluster(unsigned int i) = 0;

	/// a pure virtual function used to call any extra processing for the cluster
	virtual void process_cluster() = 0;

	/// adds an exemplar cluster wraper for the list of exemplars
	void add_exemplar(const std::shared_ptr<ExemplarCluster<E, T>>& ex)
	{
		exemplars.push_back(ex);
	}

	/// the exemplars contained within the determined cluster
	std::vector<std::shared_ptr<ExemplarCluster<E, T>>> exemplars;
};

/// Performs DBSCAN clustering on exemplars that are added to this object. The specific
/// method for calculating distance and statistics is provided by a derrived class. The
/// exemplar type T must be provided as well as the custer type C.
template<typename E, class T, class C>
class ClusterGenerator
{
public:
	/// default ctor
	ClusterGenerator() :
		neighbour_thresh(0.4),
		min_neighbours(2)
	{}

	/// ctor
	ClusterGenerator(const E& ex, const T& data) :
		neighbour_thresh(0.4),
		min_neighbours(2)
	{
		exemplar_groups.push_back(std::make_unique<ExemplarGroup<E, T>>(ex, data));
	}

	/// adds and exemplar to the group
	virtual void add_exemplar(const E& ex)
	{
		// get the data for the exemplar to compare its distance
		auto& data = get_data(ex);
		// check if the exemplar matches an existing one
		bool matched = false;
		for (const auto& exgrp : exemplar_groups)
		{
			// if any of the exemplars is parallel (0 distance) add it to the existing exemplar group
			if (exemplar_distance(exgrp->data, data) < 1e-4)
			{
				exgrp->add_exemplar(ex);
				matched = true;
				break;
			}
		}

		if (!matched)
		{
			// if not, add a new exemplar group to the list of exemplar groups
			exemplar_groups.push_back(std::make_unique<ExemplarGroup<E, T>>(ex, data));
		}

		// update the stats based on external implementation
		update_stats(data);
	}

	/// clusters the exemplar groups
	std::vector<std::unique_ptr<C>> cluster() const
	{
		std::vector<std::unique_ptr<C>> cluster_list;
		const std::vector<std::shared_ptr<ExemplarCluster<E, T>>> cluster_groups = generate_cluster_groups();

		for (const auto& clgrp : cluster_groups)
		{
			if (clgrp->cluster_state != unvisited) { continue; }

			if (clgrp->neighbours.size() < min_neighbours)
			{
				// if there are a lot of samples at this point, then its likely a singular, so
				// create a cluster from it
				if (clgrp->exgrp->exemplars.size() > 2 * min_neighbours)
				{
					clgrp->cluster_state = visited;

					// create a new cluster
					cluster_list.push_back(make_cluster(clgrp));
				}
				else
				{
					// otherwise, its just noise that can be ignored
					clgrp->cluster_state = noise;
				}
			}
			else
			{
				// there are sufficient neighbours to start forming a cluster
				clgrp->cluster_state = visited;

				// create a new cluster
				cluster_list.push_back(make_cluster(clgrp));
			}
		}

		return cluster_list;
	}

	/// cleans the exemplar group list of the specified exemplar group
	void remove_exemplar(ExemplarGroup<E, T>* exgrp)
	{
		auto erase = std::find_if(std::begin(exemplar_groups), std::end(exemplar_groups),
			[exgrp](const std::unique_ptr<ExemplarGroup<E, T>>& exg) { return exg.get() == exgrp; });
		exemplar_groups.erase(erase);
	}

	/// indicates if there are no exemplar groups
	bool exemplars_empty() const
	{
		return exemplar_groups.empty();
	}

protected:
	/// updates the stats
	virtual void update_stats(const T& exemplar) = 0;

	/// lets the derrived class provide the correct implementation to obtain the
	/// appropriate data from the exemplar
	virtual const T& get_data(const E& ex) const = 0;

	/// calculates the distance between 2 exemplars (handled by the derrived class)
	virtual double exemplar_distance(const T& ex1, const T& ex2) const = 0;

	/// updates exemplar group, including the similarity data of all groups
	/// the ex is moved, hence ex should not be used after this function call
	std::vector<std::shared_ptr<ExemplarCluster<E, T>>> generate_cluster_groups() const
	{
		std::vector<std::shared_ptr<ExemplarCluster<E, T>>> exemplar_clusters;

		for (const auto& ex : exemplar_groups)
		{
			exemplar_clusters.push_back(std::make_shared<ExemplarCluster<E, T>>(ex.get()));
		}

		// determine neighbours
		for (auto it = std::begin(exemplar_clusters); it != std::end(exemplar_clusters); ++it)
		{
			for (auto jt = std::next(it); jt != std::end(exemplar_clusters); ++jt)
			{
				double distance = exemplar_distance((*it)->exgrp->data, (*jt)->exgrp->data);

				// check if the distance is less than the neighborhood threshold
				if (distance < neighbour_thresh)
				{
					(*it)->neighbours.push_back(std::make_pair(distance, *jt));
				}
			}

			// sort the neighbour list based on distance
			std::sort(std::begin((*it)->neighbours), std::end((*it)->neighbours),
				[](const std::pair<double, std::weak_ptr<ExemplarCluster<E, T>>>& a,
					const std::pair<double, std::weak_ptr<ExemplarCluster<E, T>>>& b)
			{ return a.first < b.first; });
		}

		return exemplar_clusters;
	}

	/// make a new cluster starting with the supplied exemplar group
	std::unique_ptr<C> make_cluster(const std::shared_ptr<ExemplarCluster<E, T>>& exemplarcl) const
	{
		// create a new cluster
		std::unique_ptr<C> cluster = std::make_unique<C>();
		cluster->add_exemplar(exemplarcl);

		for (size_t i = 0; i < cluster->exemplars.size(); ++i)
		{
			auto excl = cluster->exemplars[i];

			cluster->update_cluster(i);

			for (const std::pair<double, std::weak_ptr<ExemplarCluster<E, T>>>& n : excl->neighbours)
			{
				const std::shared_ptr<ExemplarCluster<E, T>>& neighbour_exemplar = n.second.lock();
				if (neighbour_exemplar->cluster_state != visited)
				{
					// maybe need to factor the number of times each exemplar has occurred.
					neighbour_exemplar->cluster_state = visited;
					if (neighbour_exemplar->neighbours.size() >= min_neighbours)
					{
						cluster->add_exemplar(neighbour_exemplar);
					}
				}
			}
		}

		cluster->process_cluster();

		return cluster;
	}


	/// the threshold for a group to be considered a neighbour
	double neighbour_thresh;

	/// the minimum number of neighbours to not consider the point as noise
	unsigned int min_neighbours;

	/// contains an instance where the specific combination of action and delta types are
	/// observed
	std::vector<std::unique_ptr<ExemplarGroup<E, T>>> exemplar_groups;
};

}
