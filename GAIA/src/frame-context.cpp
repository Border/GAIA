#include "frame-context.h"
#include "entity.h"

using namespace GAIA;

FrameContext::FrameContext(Entity* ent, const std::shared_ptr<WorldFrame>& _frame)
	: entity(ent)
	, frame(_frame)
{
}
