#include "datamap.h"

#include <assert.h>

namespace GAIA
{

DataMap operator - (const DataMap& a)
{
	DataMap b;

	for (const auto& item : a)
		b.emplace(item.first, -item.second);

	return b;
}

DataMap operator + (const DataMap& a, const DataMap& b)
{
	DataMap c;

	// iterate over data
	for (const auto& item : a)
	{
		auto i = item.first;
		auto bit = b.find(i);
		auto& adata = item.second;

		if (bit == b.end()) { c.emplace(i, adata); continue; }

		c.emplace(i, adata + bit->second);
	}

	// iterate over b to check if there is a value in b that is not in a
	for (const auto& item : b)
	{
		auto i = item.first;
		if (a.find(i) == a.end()) { c.emplace(i, item.second); }
	}

	return c;
}

DataMap operator - (const DataMap& a, const DataMap& b)
{
	DataMap c;

	// iterate over data
	for (const auto& item : a)
	{
		auto i = item.first;
		auto bit = b.find(i);
		auto& adata = item.second;

		if (bit == b.end()) { c.emplace(i, adata); continue; }

		c.emplace(i, adata - bit->second);
	}

	// iterate over b to check if there is a value in b that is not in a
	for (const auto& item : b)
	{
		auto i = item.first;
		if (a.find(i) == a.end()) { c.emplace(i, -item.second); }
	}

	return c;
}

DataMap operator * (const DataMap& a, const DataMap& b)
{
	assert(a.size() == b.size());

	DataMap c;

	// iterate over data
	for (const auto& item : a)
	{
		auto i = item.first;
		auto bit = b.find(i);
		auto& adata = item.second;

		if (bit == b.end()) { continue; }

		c.emplace(i, adata * bit->second);
	}

	return c;
}

template <typename T>
DataMap operator * (const T& a, const DataMap& b)
{
	DataMap c;

	// iterate over data
	for (const auto& item : b)
		c.emplace(item.first, a * item.second);

	return c;
}

DataMap operator / (const DataMap& a, const DataMap& b)
{
	assert(a.size() == b.size());

	DataMap c;

	// iterate over data
	for (const auto& item : a)
	{
		auto i = item.first;
		auto bit = b.find(i);
		auto& adata = item.second;

		if (bit == b.end()) { continue; }

		auto& bdata = bit->second;

		if (bdata.is_zero())
		{
			c.emplace(i, bdata);
		}
		else
		{
			c.emplace(i, adata / bdata);
		}
	}

	return c;
}

DataMap operator += (DataMap& a, const DataMap& b)
{
	// iterate over data
	for (const auto& item : b)
	{
		auto ait = a.find(item.first);

		if (ait == std::end(a))
		{
			a.emplace(item);
		}
		else
		{
			ait->second += item.second;
		}
	}

	return a;
}

bool operator == (const DataMap& a, const DataMap& b)
{
	const auto size = a.size();
	if (size != b.size()) { return false; }

	// iterate over data
	// this checks if a is a subset of b
	for (const auto& item : a)
	{
		const auto i = item.first;
		const auto bit = b.find(i);

		if (bit == b.end()) { return false; }

		if (item.second != bit->second) { return false; }
	}

	// iterate over b to check if there is a value in b that is not in a
	for (const auto& item : b)
	{
		const auto i = item.first;
		if (a.find(i) == a.end()) { return false; }
	}

	return true;
}

bool operator != (const DataMap& a, const DataMap& b)
{
	return !(a == b);
}

bool IsZero(const DataMap& m)
{
	for (const auto& item : m)
		if (!item.second.is_zero()) { return false; }

	return true;
}

DataMap datamap_delta(const DataMap& a, const DataMap& b)
{
	DataMap c;

	// iterate over data
	for (const auto& item : a)
	{
		const auto i = item.first;
		const auto bit = b.find(i);
		const auto& adata = item.second;

		if (bit == std::end(b))
		{
			c.emplace(i, adata);
			continue;
		}

		const auto data = adata - bit->second;

		if (!data.is_zero())
		{
			c.emplace(i, data);
		}
	}

	// iterate over b to check if there is a value in b that is not in a
	for (const auto& item : b)
	{
		const auto i = item.first;
		if (a.find(i) == std::end(a))
		{
			c.emplace(i, -item.second);
		}
	}

	return c;
}

}
