#include "entity.h"
#include "affordance-classifier.h"
#include "affordance.h"
#include "feature-data.h"
#include "intrinsic.h"
#include "container-algorithms.h"

#include <algorithm>
#include <stdexcept>

using namespace GAIA;

Entity::Entity(DataMap& dmap) :
	state(std::move(dmap)),
	type_id(setup_id()),
	frame(0)
{
}

Entity::Entity(const std::shared_ptr<Entity>& ent, const DataMap& delta) :
	delta(delta),
	state(ent->state + delta),
	frame(ent->frame + 1),
	type_id(ent->get_type_id()),
	pentity(ent->pentity),
	prev(ent)
{
}

Entity::Entity(const std::shared_ptr<Entity>& ent) :
	state(ent->state),
	frame(ent->frame + 1),
	type_id(ent->get_type_id()),
	pentity(ent->pentity),
	prev(ent)
{
}

unsigned int Entity::setup_id()
{
	auto tid = state.find(GAIA::Type);

	if (tid == std::end(state))
	{
		throw std::runtime_error("No type data for entity! Entity must have type data.");
	}

	return tid->second.get_const_ref<int>();
}

unsigned int Entity::get_type_id() const { return type_id; }

void Entity::update(DataMap _delta, size_t _frame, const std::shared_ptr<Entity>& _prev)
{
	std::lock_guard<std::mutex> lock(m_entity);

	delta = std::move(_delta);
	frame = _frame;
	prev = _prev;
}

void Entity::update(DataMap _delta, size_t _frame)
{
	std::lock_guard<std::mutex> lock(m_entity);

	delta = std::move(_delta);
	frame = _frame;
}

void Entity::compose_persistent_entity()
{
	std::lock_guard<std::mutex> lock(m_entity);
	pentity = PersistentEntityGenerator::Instance()->gen_entity(type_id);
}

void Entity::obtain_persistent_entity(const Entity* ent)
{
	pentity = ent->pentity;
}

unsigned int Entity::get_persistent_id() const { return pentity->id; }

std::shared_ptr<PersistentEntity> Entity::get_persistent_entity() const { return pentity; }

std::vector<std::shared_ptr<EntityAction>> Entity::get_entity_actions() const { return actions; }

std::shared_ptr<EntityInterface> Entity::get_prev_entity() const
{
	assert(!prev.expired());

	return prev.lock();
}

const DataMap& GAIA::Entity::get_state() const { return state; }

std::vector<Affordance*> Entity::get_affordances() const { return affordances; }

CVector Entity::get_causes() const
{
	assert("Unimplemented!" && false);
	CVector causes;

	for (auto affd : affordances)
	{
		//affd->;
	}

	return causes;
}

std::vector<std::shared_ptr<EntityAction>> Entity::get_actions() const { return actions; }

void Entity::add_affordances(const std::vector<Affordance*>& affds)
{
	for (auto affd : affds)
	{
		if (std::any_of(std::begin(affordances), std::end(affordances),
			[affd](const Affordance* affordance){ return affordance->get_unique_id() == affd->get_unique_id(); }))
		{
			affordances.push_back(affd);
		}
	}
}

void Entity::generate_actions(const std::shared_ptr<WorldFrame>& frame)
{
	for (const auto& action : pentity->actions)
	{
		actions.push_back(action.action_domain->generate_action(this, action.intrinsic_data));
	}
}

void Entity::set_persistent_entity(std::shared_ptr<PersistentEntity> p) { pentity = std::move(p); }

void Entity::update_intrinsics(const std::shared_ptr<WorldFrame>& frame)
{
	// Generate a discrete pdf for all possible affordance types

	// Group affordances into their action range/set types and generate a pdf in order to select an action range to perform.
	// Use the max of each affordance range/set type to construct the pdf
	// However, it may be possible for multiple actions to be performed

	// Could use affordance prototype here as well. In fact we should as this will help the agent explore the things it needs to.

	// Clear the intrinsic data
	for (auto& entity_action : pentity->actions)
	{
		entity_action.intrinsic_data.clear();
	}

	// Generate the intrinsic data
	for (auto affd : AffordanceClassifier::Instance()->get_affordances())
	{
		// TODO: add support for multiple causes
		FrameContext context(this, frame);
		context.feature_references = FeatureContext(affd->get_causes().front(), this); // TODO: this only uses the first cause

		if (affd->match_initial_condition(context))
		{
			// now check if it is a member of an action domain
			// if so, then add the action to an object and update the max novelty if necessary
			// in order to pass the object into the action domain so it can generate a pdf to
			// select the actual action to perform.
			for (auto& entity_action : pentity->actions)
			{
				if (entity_action.action_domain->is_entity_type_valid(type_id))
				{
					for (auto& affd_action : affd->get_actions())
					{
						for (auto action : affd_action->get_actions())
						{
							auto features = entity_action.action_domain->get_matching_action_features(action);
							if (! features.empty())
							{
								entity_action.intrinsic_data.push_back({ features.front()->get_type_id(), affd->get_intrinsic(), std::move(features) });
							}
						}
					}
				}
			}
		}
	}

	// The intrinsic/novelty is only relevant to this entity, so a data is stored in the persistent entity
}
