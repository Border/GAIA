#include "entity-persistent.h"

#include <algorithm>

using namespace GAIA;

std::unique_ptr<PersistentEntityGenerator> PersistentEntityGenerator::singleton = nullptr;



PersistentEntity::PersistentEntity(unsigned int uid, std::vector<EntityActionData>& entity_actions):
	id(uid),
	actions(std::move(entity_actions))
{

}



PersistentEntityGenerator::PersistentEntityGenerator(const PersistentEntityGenerator& peg) :
	n_entities(0)
{
}

PersistentEntityGenerator::PersistentEntityGenerator() :
	n_entities(0)
{
}

PersistentEntityGenerator* PersistentEntityGenerator::Instance() { return singleton.get(); }

void PersistentEntityGenerator::Create()
{
	if (singleton == nullptr)
	{
		singleton = std::make_unique<PersistentEntityGenerator>();
	}
}

std::shared_ptr<PersistentEntity> PersistentEntityGenerator::gen_entity(unsigned int type_id)
{
	std::vector<EntityActionData> entity_actions;

	// go through the list of ActionRanges and create a list of actions that the entity of
	// type [type_id] can use.

	for (const auto& action_domain : actions)
	{
		if (action_domain->is_entity_type_valid(type_id))
		{
			entity_actions.push_back({ action_domain, { } });
		}
	}

	auto pentity = std::make_shared<PersistentEntity>(++n_entities, std::move(entity_actions));

	m_entities.lock();
	entities.push_back(pentity);
	m_entities.unlock();

	return pentity;
}

std::shared_ptr<PersistentEntity> PersistentEntityGenerator::find_entity(unsigned int pid) const
{
	std::lock_guard<std::mutex> lock(m_entities);

	// binary search, the container should be sorted
	auto match = std::lower_bound(std::begin(entities), std::end(entities), pid,
		[](const std::shared_ptr<PersistentEntity>& e, unsigned int id) { return e->id < id; });

	if (match != std::end(entities) && !(pid < (*std::begin(entities))->id))
	{
		return *match;
	}
	else
	{
		return nullptr;
	}
}

void PersistentEntityGenerator::add_action_ranges(std::vector<std::shared_ptr<IActionDomain>>& action_ranges)
{
	actions.insert(std::end(actions), std::begin(action_ranges), std::end(action_ranges));
}
