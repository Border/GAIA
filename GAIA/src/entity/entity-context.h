#pragma once

#include "frame-context.h"
#include "generic.h"

#include <memory>
#include <unordered_map>

namespace GAIA
{

class WorldFrame;
class Entity;

struct EntityContext
{
	/// ctor
	EntityContext(Entity* ent, const FrameContext& fcontext) :
		entity(ent),
		context(fcontext)
	{
	}

	/// An entity from frame context, of which is the focus in this entity context
	Entity* entity;
	/// the frame exemplar
	const FrameContext& context;
};

}
