#pragma once

#include "action-domain.h"
#include "intrinsic.h"
#include "action-types.h"
#include "datamap.h"
#include "generic.h"

#include <vector>
#include <memory>
#include <mutex>

namespace GAIA
{

class PersistentEntity
{
public:
	PersistentEntity(unsigned int uid, std::vector<EntityActionData>& entity_actions);

	double get_interest() const;


	/// A unique id for this persistent entity
	const unsigned int id;

	/// list of possible actions this entity can perform
	std::vector<EntityActionData> actions;
};

class PersistentEntityGenerator
{
protected:
	/// copy ctor, but nothing should be coppied and protected so it cant be created on stack
	PersistentEntityGenerator(const PersistentEntityGenerator& peg);
public:
	/// ctor
	PersistentEntityGenerator();

	/// Obtains an instance of this class
	/// \return The instance for this class
	static PersistentEntityGenerator* Instance();

	/// Creates an instance
	static void Create();

	/// Generates a persistent entity based on the supplied type_id
	std::shared_ptr<PersistentEntity> gen_entity(unsigned int type_id);

	/// finds a matching persistent entity based on the id and returns it. If not found
	/// returns nullptr.
	std::shared_ptr<PersistentEntity> find_entity(unsigned int pid) const;

	/// adds the supplied action ranges to the list of action ranges that can be added to a
	/// persistent entity
	void add_action_ranges(std::vector<std::shared_ptr<IActionDomain>>& action_ranges);

private:
	/// The number of entities -  used to create a unique id for a persistent entity
	unsigned int n_entities;

	/// the list of persistent entities
	std::vector<std::shared_ptr<PersistentEntity>> entities;

	/// mutex for synchonisation of entities
	mutable std::mutex m_entities;

	/// The list of all actions for every entity
	std::vector<std::shared_ptr<IActionDomain>> actions;

	/// A singleton
	static std::unique_ptr<PersistentEntityGenerator> singleton;
};

}
