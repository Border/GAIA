#pragma once

#include <vector>
#include <algorithm>
#include <numeric>
#include <math.h>


namespace calg
{

/// Specialised sorting templates for vectors
template <typename T, typename Compare>
inline std::vector<int> sort_permutation(const std::vector<T>& vec, Compare compare)
{
	std::vector<int> p(vec.size());
	std::iota(std::begin(p), std::end(p), 0);
	std::sort(std::begin(p), std::end(p), [&](int i, int j) { return compare(vec[i], vec[j]); });
	return p;
}

/// apply sorting to vec based on p
template <typename T>
inline std::vector<T> apply_permutation(const std::vector<T>& vec, const std::vector<int>& p)
{
	std::vector<T> sorted_vec(p.size());
	std::transform(std::begin(p), std::end(p), std::begin(sorted_vec), [&](int i) { return vec[i]; });
	return sorted_vec;
}

/// returns index of item or -1 if vec does not contain item. The vec does not have to be sorted.
template <typename T>
inline int find_vector_index(const std::vector<T>& vec, const T& item)
{
	for (size_t i = 0, ilen = vec.size(); i < ilen; ++i)
		if (vec[i] == item) { return i; }

	return -1;
}

/// returns index of item or -1 if vec does not contain item. The vec does not have to be sorted.
template <typename T, typename Compare>
inline int find_vector_index(const std::vector<T>& vec, const T& item, Compare compare)
{
	for (size_t i = 0, ilen = vec.size(); i < ilen; ++i)
		if (compare(vec[i], item)) { return i; }

	return -1;
}

/// returns true if vector a contents that sequentially matches b, returns false if the sizes do not match and if a single item does not match
template <typename Iter1, typename Iter2>
inline bool matches(Iter1 first1, const Iter1 last1, Iter2 first2, const Iter2 last2)
{
	if (std::distance(first1, last1) != std::distance(first2, last2)) { return false; }

	for (; first1 != last1; ++first1, ++first2)
		if (*first1 != *first2) { return false; }

	return true;
}

/// returns true if vector a contents that sequentially matches b (so and b must be
/// sorted), returns false if the sizes do not match and if a single item does not match
/// the matching is done using the supplied compare function
template <typename Iter1, typename Iter2, typename Compare>
inline bool matches(Iter1 first1, const Iter1 last1, Iter2 first2, const Iter2 last2, Compare compare)
{
	if (std::distance(first1, last1) != std::distance(first2, last2)) { return false; }

	for (; first1 != last1; ++first1, ++first2)
		if (!compare(*first1,*first2)) { return false; }

	return true;
}

/// returns true if the items from Iter1 match any combination of items from Iter2, based on the supplied compare function
template <typename Iter1, typename Iter2, typename Compare>
inline bool unsorted_match(Iter1 first1, const Iter1 last1, const Iter2 first2, const Iter2 last2, Compare compare)
{
	if (std::distance(first1, last1) != std::distance(first2, last2)) { return false; }

	for (; first1 != last1; ++first1)
	{
		if (std::find_if(first2, last2, [&](const auto& a) { return compare(*first1, a); }) == last2) { return false; }
	}

	return true;
}

/// returns a vector of the order to produce the minimum difference bewteen the 2 supplied
/// vecetors elements. The difference is calculated by the compare function supplied.
template <typename Iter1, typename Iter2, typename Compare>
inline std::pair<double, std::vector<int>> best_match(const Iter1 first1, const Iter1 last1, const Iter2 first2, const Iter2 last2, Compare compare)
{
	const size_t asize = std::distance(first1, last1);
	const size_t bsize = std::distance(first2, last2);

	// sizes must not be 0
	assert(asize > 0);
	assert(bsize > 0);

	const size_t sizemax = std::max(asize, bsize);

	std::vector<int> order(sizemax);
	std::iota(order.begin(), order.end(), -std::abs(static_cast<int>(asize) - static_cast<int>(bsize)));

	// singleton case
	if (asize == 1 && bsize == 1)
	{
		return std::make_pair(0, order);
	}

	std::vector<int> min_order(sizemax);

	double min = std::numeric_limits<double>::max();

	// could do an optimisation which checks for elements that precisely match and removes
	// them from the best match search (appending them back later).

	do {
		auto iter1 = first1;
		double d = 0;
		for (size_t i = 0; i < asize; ++i, ++iter1)
		{
			if (order[i] >= 0)
				d += compare(*iter1, *std::next(first2, order[i]));
			else
				d += 1;
		}

		if (d < min)
		{
			min = d;
			min_order = order;
			if (std::abs(d) < std::numeric_limits<double>::epsilon()) break;
		}

	} while (std::next_permutation(order.begin(), order.end()));

	return std::make_pair(min, min_order);
}

/// returns the first occurrence of an intersection in a pair of iterators for the 2 container ranges specified
template<class InputIt, class Compare>
std::pair<InputIt, InputIt> first_intersection(InputIt first1, InputIt last1, InputIt first2, InputIt last2, Compare comp)
{
	for (; first1 != last1; ++first1)
	{
		for (; first2 != last2; ++first2)
		{
			if (comp(*first1, *first2)) { return std::make_pair(first1, first2); }
		}
	}

	return std::make_pair(last1, last2);
}


/// finds the number of item that matches vec based on the supplied compare function
template <typename T, typename Compare>
inline size_t match_count(const std::vector<T>& vec, const T item, Compare compare)
{
	size_t count = 0;
	for (auto& vecitem : vec)
		if (compare(vecitem, item)) { ++count; }

	return count;
}

/// returns a vector of items that match item based on the supplied compare function
template <typename T, typename Compare>
inline const std::vector<T> find_matches(const std::vector<T>& vec, const T& item, Compare compare)
{
	std::vector<T> matches;
	for (auto& vecitem : vec)
		if (compare(vecitem, item)) { matches.push_back(vecitem); }

	return matches;
}

/// returns true if each element in the vector is unique, otherwise returns false. The vector must be sorted.
template <class InputIt>
inline bool unique(InputIt first, const InputIt last)
{
	if (std::distance(first, last) <= 1) { return true; }

	auto prev = first;

	for (++first; first != last; ++first, ++prev)
		if (*first == *prev) { return false; }

	return true;
}

/// returns true if each element in the vector is unique, otherwise returns false
template <class InputIt>
inline bool unsorted_unique(InputIt first, const InputIt last)
{
	if (std::distance(first, last) <= 1) { return true; }

	for (; first != last; ++first)
		for (auto it = std::next(first); it != last; ++it)
			if (*first == *it) { return false; }

	return true;
}

/// deletes the pointer contents of container a
template <typename Container>
inline void clear_container_all(Container& a)
{
	for (auto& item : a) { delete item; }
	a.clear();
}

/// copies the vector of pointers making copies of the data that each pointer points to
template <typename T>
inline std::vector<T*> deep_vector_copy(const std::vector<T*>& vec)
{
	std::vector<T*> copy;

	for (T* a : vec)
		copy.push_back(new T(a));

	return copy;
}

/// inserts the item into the appropriate location so as to keep the vector sorted
template<typename T, typename Pred>
typename std::vector<T>::iterator
insert_sorted(std::vector<T>& vec, T const& item, Pred pred)
{
	return vec.insert(
		std::upper_bound(std::begin(vec), std::end(vec), item, pred),
		item
	);
}

/// inserts the item into the appropriate location so as to keep the container sorted
template<class T, class C, typename Pred>
typename C::iterator
insert_sorted(C& container, T&& item, Pred pred)
{
	return container.insert(
		std::upper_bound(std::begin(container), std::end(container), item, pred),
		std::move(item)
	);
}

}
