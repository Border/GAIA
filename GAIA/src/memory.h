#pragma once

#include "worldframe.h"

#include <mutex>
#include <deque>
#include <unordered_map>
#include <memory>

namespace GAIA
{

class LTEntityMemory
{
public:
	/// affordances that the entities of this type can use
	std::vector<Affordance*> affordances;
};

class Memory
{
protected:
	/// copy ctor, but nothing should be coppied and protected so it cant be created on stack
	Memory(const Memory& mem);

public:
	/// ctor
	Memory();

	/// dtor
	~Memory();

	/// Obtains an instance of this class
	/// \return The instance for this class
	static Memory* Instance();

	/// Creates an instance
	static void Create();

	/// Obtains a frame map describing the current state of thw world
	/// \param frame The frame to add to the agents memory
	void add_world_frame(const std::shared_ptr<WorldFrame>& frame);

	/// clears all frames from the frame history
	void clear_frames();

	/// Obtains the current and previous world frame pair
	/// \return a pair containing the current and previous world frames
	std::pair<std::shared_ptr<WorldFrame>, std::shared_ptr<WorldFrame>> get_frame_pair() const;

	/// Obtains the current world frame
	/// \return The current world frame
	std::shared_ptr<WorldFrame> get_current_frame() const;

	/// Returns the next unique causailty id
	unsigned int get_next_causality_id();

	/// finds the LTEntityMemory affordance for the specified type_id, returns an empty list if no matches
	std::vector<Affordance*> get_entity_affordances(unsigned int type_id) const;

	/// adds the affordance to the specified type_id LTEntityMemory
	void add_entity_affordances(unsigned int type_id, const std::vector<Affordance*>& affordances);

private:
	/// Controls access to the world frame buffer
	/// Maybe use shared mutex to handle writes, allowing reads to occur simultaneously
	mutable std::mutex m_wframe;

	/// Controls access to n_causality
	std::mutex m_causality;

	/// The agents perception of the world at a specific frame
	std::deque<std::shared_ptr<WorldFrame>> worldbuffer;

	/// The number of causality (Cause/Effect) objects that have been created. This needs
	/// to be set to the max number of prior decided actions as actions are user defined.
	unsigned int n_causality;

	/// The long term memory for entities, grouped based on their types
	std::unordered_map<unsigned int, LTEntityMemory> long_term_memory;

	/// synchronises access to long_term_memory
	mutable std::mutex m_ltm;

	/// A singleton
	static std::unique_ptr<Memory> singleton;
};

}
