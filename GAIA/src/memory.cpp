#include "memory.h"
#include "affordance.h"

#include <algorithm>

using namespace GAIA;

std::unique_ptr<Memory> Memory::singleton = nullptr;


Memory* Memory::Instance()
{
	return singleton.get();
}

void Memory::Create()
{
	if (singleton == nullptr)
	{
		singleton = std::make_unique<Memory>();
	}
}

Memory::Memory() :
	n_causality(0)
{ }

Memory::Memory(const Memory& mem) :
	n_causality(0)
{ }

Memory::~Memory() { }

void Memory::add_world_frame(const std::shared_ptr<WorldFrame>& frame)
{
	std::lock_guard<std::mutex> lock(m_wframe);

	if (worldbuffer.size() > 0)
	{
		frame->set_previous_frame(worldbuffer.front());
	}

	worldbuffer.push_front(frame);

	if (worldbuffer.size() > 1000)
	{
		worldbuffer.pop_back();
	}
}

void Memory::clear_frames()
{
	std::lock_guard<std::mutex> lock(m_wframe);

	worldbuffer.clear();
}

std::pair<std::shared_ptr<WorldFrame>, std::shared_ptr<WorldFrame>> Memory::get_frame_pair() const
{
	std::lock_guard<std::mutex> lock(m_wframe);

	if (worldbuffer.size() == 0)
	{
		return std::make_pair(nullptr, nullptr);
	}

	auto curr = worldbuffer.front();

	if (worldbuffer.size() == 1)
	{
		return std::make_pair(curr, nullptr);
	}

	auto prev = worldbuffer[1];

	return std::make_pair(curr, prev);
}

std::shared_ptr<WorldFrame> Memory::get_current_frame() const
{
	std::lock_guard<std::mutex> lock(m_wframe);

	if (worldbuffer.size() == 0)
	{
		return nullptr;
	}

	return worldbuffer.front();
}

unsigned int Memory::get_next_causality_id() { std::lock_guard<std::mutex> lock(m_causality); return ++n_causality; }

std::vector<Affordance*> Memory::get_entity_affordances(unsigned int type_id) const
{
	std::lock_guard<std::mutex> guard(m_ltm);

	std::vector<Affordance*> affordances;

	auto entity_ltm = long_term_memory.find(type_id);

	if (entity_ltm != std::end(long_term_memory))
	{
		affordances = entity_ltm->second.affordances;
	}

	return affordances;
}

void Memory::add_entity_affordances(unsigned int type_id, const std::vector<Affordance*>& affordances)
{
	std::lock_guard<std::mutex> guard(m_ltm);

	auto entity_ltm = long_term_memory.find(type_id);

	if (entity_ltm != std::end(long_term_memory))
	{
		for (auto affd : affordances)
		{
			if (std::none_of(std::begin(entity_ltm->second.affordances), std::end(entity_ltm->second.affordances),
				[affd](const Affordance* a) { return a->get_unique_id() == affd->get_unique_id(); }))
			{
				entity_ltm->second.affordances.push_back(affd);
			}
		}
	}
	else
	{
		LTEntityMemory ltem;

		ltem.affordances = affordances;
		long_term_memory.emplace(type_id, std::move(ltem));
	}
}
