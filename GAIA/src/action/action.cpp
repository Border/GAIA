#include "action.h"
#include "state.h"
#include "entity.h"
#include "feature-classifier.h"
#include "feature-association.h"
#include "action-entity.h"

namespace GAIA
{

Action::Action(ActionFeatures& action_features, unsigned int uid, unsigned int tid) :
	id_unique(uid),
	id_type(tid),
	features(std::move(action_features))
{
	FeatureHasher hasher;
	hasher.update<ActionFeatures::iterator>(std::begin(action_features), std::end(action_features), action_hasher);
	std::tie(hash_type, hash_unique) = hasher.generate();
}

Action::Action(ActionFeatures& action_features, unsigned int _hash_type, unsigned int _hash_unique, unsigned int uid, unsigned int tid) :
	id_unique(uid),
	id_type(tid),
	hash_type(_hash_type),
	hash_unique(_hash_unique),
	features(std::move(action_features))
{
}

unsigned int Action::get_type_hash() const { return hash_type; }

unsigned int Action::get_unique_hash() const { return hash_unique; }

unsigned int Action::get_unique_id() const { return id_unique; }

unsigned int Action::get_type_id() const { return id_type; }

size_t Action::get_size() const { return features.size(); }

bool Action::match_action(const EntityAction* action, const FeatureContext& context) const
{
	if ((unsigned int)action->get_type_id() != id_type) { return false; }

	for (const auto& fcont : features)
	{
		auto type_match = std::find_if(std::begin(action->get_data()), std::end(action->get_data()),
			[&](const ActionDataContainer& x) { return x.data_type == fcont.feature_type; });

		if (type_match != std::end(action->get_data()))
		{
			for (auto f : fcont.features)
			{
				if (!GAIA::match_feature(f, context, type_match->data))
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}

	return true;
}

bool Action::match(const FrameContext& context) const
{
	for (const auto& action : context.entity->actions)
	{
		if (match_action(action.get(), context.feature_references))
		{
			return true;
		}
	}

	return false;
}

bool Action::match_hash(unsigned int unique_hash) const { return hash_unique == unique_hash; }

bool Action::operator == (const Action* a) const { return hash_unique == a->get_unique_hash(); }

const FVector& Action::get_features(unsigned int type) const
{
	for (const auto& f : features)
	{
		if (f.feature_type == type) { return f.features; }
	}

	return FVector();
}

}
