#pragma once

#include "action-entity.h"
#include "feature.h"
#include "generic.h"

#include <vector>
#include <tuple>
#include <random>

namespace GAIA
{

class Entity;

class IActionDomain
{
public:
	/// virtual dtor
	virtual ~IActionDomain() = default;

	/// returns the type id of the action
	virtual unsigned int get_id() const = 0;

	/// returns true if the supplied type_id is a valid entity id for this ActionRange
	virtual bool is_entity_type_valid(unsigned int type_id) const = 0;

	/// generates an EntityAction based on the action range
	virtual std::shared_ptr<EntityAction> generate_action(Entity* ent, const std::vector<ActionDomainIntrinsicData>& action_intrinsics) const = 0;

	virtual FVector get_matching_action_features(Action* action) const = 0;
};

struct ActionRangeDataContainer
{
	unsigned int type;
	FeatureReferenceType ref_type;
	Generic min;
	Generic max;
};

class ActionRange : public IActionDomain
{
public:
	/// ctor
	ActionRange(unsigned int _id, std::vector<unsigned int>& ent_types, std::vector<ActionRangeDataContainer>& data);

	/// returns the type id of the action
	unsigned int get_id() const override;

	/// returns true if the supplied type_id is a valid entity id for this ActionRange
	bool is_entity_type_valid(unsigned int type_id) const override;

	/// generates an EntityAction based on the action range
	std::shared_ptr<EntityAction> generate_action(Entity* ent, const std::vector<ActionDomainIntrinsicData>& action_intrinsics) const override;

	FVector get_matching_action_features(Action* action) const override;

private:
	/// The type of the action
	unsigned int id;

	/// A list of valid entity types
	std::vector<unsigned int> entity_type_ids;

	/// Data required to perfom the action - this should be sorted by the unsigned int
	std::vector<ActionRangeDataContainer> action_data;

	/// Rand generator
	mutable std::default_random_engine rndgen;
};

struct ActionSetDataContainer
{
	unsigned int type;
	FeatureReferenceType ref_type;
	std::vector<Generic> data;
};

class ActionSet : public IActionDomain
{
public:
	/// ctor
	ActionSet(unsigned int _id, std::vector<unsigned int>& ent_types, std::vector<ActionSetDataContainer>& data);

	/// returns the type id of the action
	unsigned int get_id() const override { return id; }

	/// returns true if the supplied type_id is a valid entity id for this ActionRange
	bool is_entity_type_valid(unsigned int type_id) const override;

	/// generates an EntityAction based on the action range
	std::shared_ptr<EntityAction> generate_action(Entity* ent, const std::vector<ActionDomainIntrinsicData>& action_intrinsics) const override;

	FVector get_matching_action_features(Action* action) const override;

private:
	/// The type of the action
	unsigned int id;

	/// A list of valid entity types
	std::vector<unsigned int> entity_type_ids;

	/// Data required to perfom the action - this should be sorted by the unsigned int
	std::vector<ActionSetDataContainer> action_data;

	/// Rand generator
	mutable std::default_random_engine rndgen;
};

}
