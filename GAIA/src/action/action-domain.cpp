#include "action-domain.h"
#include "action.h"
#include "entity-persistent.h"

#include <algorithm>

using namespace GAIA;

ActionRange::ActionRange(unsigned int _id, std::vector<unsigned int>& ent_types, std::vector<ActionRangeDataContainer>& data) :
	id(_id),
	action_data(std::move(data)),
	entity_type_ids(ent_types),
	rndgen(std::random_device()())
{
}

unsigned int ActionRange::get_id() const { return id; }

bool ActionRange::is_entity_type_valid(unsigned int type_id) const
{
	return std::find(std::begin(entity_type_ids), std::end(entity_type_ids), type_id) != std::end(entity_type_ids);
}

std::shared_ptr<EntityAction> ActionRange::generate_action(Entity* ent, const std::vector<ActionDomainIntrinsicData>& action_intrinsics) const
{
	std::vector<ActionDataContainer> data;

	for (const auto& a : action_data)
	{
		// generate random value between min and max
		// ideally a distribution will be passed to this function in order to
		// modify the probabilites of selection within the range

		Generic val;

		switch (a.min.get_type())
		{
		case Gint:
		{
			std::uniform_int_distribution<int> uniform_dist(a.min.get_const_ref<int>(), a.max.get_const_ref<int>());
			val = Generic(uniform_dist(rndgen));
			break;
		}
		case Gdouble:
		{
			std::uniform_real_distribution<double> uniform_dist(a.min.get_const_ref<double>(), a.max.get_const_ref<double>());
			val = Generic(uniform_dist(rndgen));
			break;
		}
		case GVec:
		{
			std::uniform_real_distribution<double> uniform_dist_x(a.min.get_const_ref<GVec2>().x, a.max.get_const_ref<GVec2>().x);
			std::uniform_real_distribution<double> uniform_dist_y(a.min.get_const_ref<GVec2>().y, a.max.get_const_ref<GVec2>().y);
			val = Generic(GVec2(uniform_dist_x(rndgen), uniform_dist_y(rndgen)));
			break;
		}
		default: throw std::runtime_error("Unknown type");
		}

		data.push_back({ a.type, a.ref_type, std::move(val) });
	}

	return std::make_shared<EntityAction>(id, data, ent);
}

FVector ActionRange::get_matching_action_features(Action* action) const
{
	for (auto data : action_data)
	{
		return action->get_features(data.type);
	}

	return FVector();
}



ActionSet::ActionSet(unsigned int _id, std::vector<unsigned int>& ent_types, std::vector<ActionSetDataContainer>& data) :
	id(_id),
	action_data(std::move(data)),
	entity_type_ids(ent_types),
	rndgen(std::random_device()())
{
}

bool ActionSet::is_entity_type_valid(unsigned int type_id) const
{
	return std::find(std::begin(entity_type_ids), std::end(entity_type_ids), type_id) != std::end(entity_type_ids);
}

std::shared_ptr<EntityAction> ActionSet::generate_action(Entity* ent, const std::vector<ActionDomainIntrinsicData>& action_intrinsics) const
{
	double max_interest = 0.5; // this should be the max interest in action_intrinsics (clamped to the range [0,1))

	std::vector<ActionDataContainer> data;

	for (const auto& a : action_data)
	{
		std::vector<double> action_probability;
		std::vector<IFeature*> action_features;
		double sum = 0;

		for (auto& action_intrinsic : action_intrinsics)
		{
			if (action_intrinsic.type == a.type)
			{
				double interest = action_intrinsic.intrinsic.generate_interest() / action_intrinsic.features.size();
				double ef = std::exp(interest / (1 - max_interest)); // face interest will always be < 1.0
				sum += ef * action_intrinsic.features.size();
				for (auto feature : action_intrinsic.features)
				{
					action_probability.push_back(ef);
					action_features.push_back(feature);
				}
			}
		}

		for (auto& pr : action_probability)
		{
			pr /= sum;
		}

		std::uniform_real_distribution<double> rnd(0, 1);

		const double xrand = rnd(rndgen);
		double x = 0;
		double result = 0;
		double step_size = 0.01;// 1.0 / a.data.size(); // TODO: work out a way to calculate a good min step
		size_t index = 0;

		while (result < xrand)
		{
			double cdf = 0;
			index = std::min((size_t)std::round((a.data.size() - 1) * x), a.data.size() - 1);
			for (size_t i = 0, ilen = action_probability.size(); i < ilen; ++i)
			{
				double fcdf = action_features[i]->generate_cdf(a.data[index]);
				cdf += action_probability[i] * fcdf;
			}

			result = (1.0 - max_interest) * x + cdf * max_interest * 0.5;
			x += step_size;
		}

		// Clamp xtest
		if (x > 1.0)
			x = 1.0;

		// double xtestpdf = 0; // generate pdf for display purposes
		// for(size_t i = 0; i < 100; ++i)
		// {
		// 	pdf[i] = FacePDF(action_probability,paffds,state.face->interest,xtestpdf);
		// 	xtestpdf += 0.01;
		// }

		data.push_back({ a.type, a.ref_type, a.data[index] });
	}

	return std::make_shared<EntityAction>(id, data, ent);
}

FVector ActionSet::get_matching_action_features(Action* action) const
{
	for (auto data : action_data)
	{
		return action->get_features(data.type);
	}

	return FVector();
}
