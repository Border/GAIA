#include "action-entity.h"
#include "container-algorithms.h"
#include "mhash.h"

using namespace GAIA;

EntityAction::EntityAction(tid_t type, std::vector<ActionDataContainer>& data, Entity* _ent) :
	id(type),
	hash(generate_hash(std::begin(data), std::end(data), [](const ActionDataContainer& a) { return a.data_type; })),
	action_data(std::move(data)),
	ent(_ent)
{
}

tid_t EntityAction::get_type_id() const { return id; }

Entity* EntityAction::get_entity() const { return ent; }

const std::vector<ActionDataContainer>& EntityAction::get_data() const { return action_data; }

bool EntityAction::match(const EntityAction* action) const
{
	return action->get_type_id() == id &&
		calg::matches(std::begin(action->get_data()),
			std::end(action->get_data()),
			std::begin(action_data),
			std::end(action_data),
			[](const ActionDataContainer& a, const ActionDataContainer& b) { return a.data == b.data; });
}

hash_t EntityAction::get_hash() const { return hash; }
