#pragma once

#include "feature.h"
#include "frame-context.h"
#include "feature-types.h"
#include "action-types.h"
#include "causality.h"
#include "types.h"

#include <vector>
#include <unordered_map>

namespace GAIA
{

class EntityAction;

/// Desscribes the change bewteen 2 frames using a series of known features
class Action
{
	friend class IO;
	friend class DebugDisplay;
public:
	/// Constructor - move features and generate hashes
	Action(ActionFeatures& features, unsigned int uid, unsigned int tid);
	/// Constructor - move features and copy hashes
	Action(ActionFeatures& features, unsigned int hash_type, unsigned int hash_unique, unsigned int uid, unsigned int tid);

	/// returns the type hash - a hash generated from the type id of the features
	unsigned int get_type_hash() const;
	/// returns the unique hash - a hash generated from the unique id of the features
	unsigned int get_unique_hash() const;
	///  returns the unique id of the action
	unsigned int get_unique_id() const;
	///  returns the id indicating the type of action
	unsigned int get_type_id() const;
	/// Returns the number of features that make up the association
	size_t get_size() const;
	/// Returns true if the entity action matches this action
	bool match_action(const EntityAction* action, const FeatureContext& context) const;
	/// Returns true if the FVector matches
	bool match(const FrameContext& context) const;
	/// Returns true if the hash matches
	bool match_hash(unsigned int unique_hash) const;
	/// Returns true if the action matches
	bool operator == (const Action* a) const;
	/// Checks if the specified feature type id is present in the association
	const FVector& get_features(unsigned int type) const;

protected:
	/// a unique id for generating hashes of groups of actions
	const unsigned int id_unique;
	/// the action type id
	const unsigned int id_type;
	/// the hash of the features unique id's that form the association
	unsigned int hash_unique;
	/// the hash of the feature id's that for the association
	unsigned int hash_type;

	/// vector of fvectors which describe the action
	ActionFeatures features;
};

}
