#include "action-classifier.h"
#include "state-classifier.h"
#include "feature-classifier.h"
#include "feature-container.h"
#include "feature-association.h"
#include "mhash.h"
#include "memory.h"

using namespace GAIA;

std::unique_ptr<ActionClassifier> ActionClassifier::singleton = nullptr;



ActionClassifier::ActionClassifier() :
	n_actions(0),
	n_features(0)
{
}

ActionClassifier::ActionClassifier(const ActionClassifier& evcl) :
	n_actions(0),
	n_features(0)
{
}

ActionClassifier* ActionClassifier::Instance() { return singleton.get(); }

void ActionClassifier::Create()
{
	if (singleton == nullptr)
	{
		singleton = std::make_unique<ActionClassifier>(ActionClassifier());
	}
}

Action* ActionClassifier::compose_action(const ActionSamples& action_feature_samples)
{
	ActionFeatures action_features;
	tid_t type_id = action_feature_samples.type;

	for (const auto& feature_contexts : action_feature_samples.feature_samples)
	{
		// return a matching series of features or a new series of features
		action_features.push_back({ feature_contexts.feature_type, feature_contexts.ref_type, compose_features(feature_contexts.feature_type, feature_contexts.data_samples) });
	}

	// generate hashes from the state_features to see if there is an existing state
	FeatureHasher hasher;
	hash_t type_hash, unique_hash;
	hasher.update<ActionFeatures::iterator>(std::begin(action_features), std::end(action_features), action_hasher);
	std::tie(type_hash, unique_hash) = hasher.generate();

	auto action = match_action(type_id, unique_hash);

	if (action == nullptr)
	{
		auto type_match = actions.find(type_id);

		if (type_match != std::end(actions))
		{
			auto result = type_match->second.emplace(unique_hash, std::make_unique<Action>(action_features, type_hash, unique_hash, ++n_actions, type_id));

			// the result should be true for insertion, otherwise a state already exists with the same hash
			assert(result.second);

			action = result.first->second.get();
		}
		else
		{
			std::unordered_map<unsigned int, std::unique_ptr<Action>> naction;
			auto result = naction.emplace(unique_hash, std::make_unique<Action>(action_features, type_hash, unique_hash, ++n_actions, type_id));

			// the result should be true for insertion, otherwise a state already exists with the same hash
			assert(result.second);

			actions.emplace(type_id, std::move(naction));

			action = result.first->second.get();
		}
	}

	return action;
}

std::vector<IFeature*> ActionClassifier::compose_features(unsigned int id, const std::vector<FeatureDataContext>& feature_data_samples)
{
	// find the best combination of features that most closely match the feature exemplars,
	// creating a new feature if necessary

	// obtain the feature data type
	const auto type = feature_data_samples.front().data.get_type();

	// the list of features that best describe the feature exemplars
	std::vector<IFeature*> action_features;

	// create transformer(s) from feature exemplars
	std::vector<std::unique_ptr<ITransformer>> transformers = FeatureClassifier::Instance()->make_feature_transformers(feature_data_samples);

	// filter out all other features that are not of the desired transformers
	auto filtered_features = features.find(id);
	if (filtered_features != std::end(features))
	{
		// match created transformers with the filtered list of features
		// if no match, create a new feature using the transformer

		for (auto& t : transformers)
		{
			auto range = filtered_features->second.equal_range(t->get_type());
			std::vector<std::pair<double, IFeature*>> matched_list;

			for (; range.first != range.second; ++range.first)
			{
				auto match_error = range.first->second->match(t.get());

				if (match_error < 0.2)
				{
					matched_list.push_back(std::make_pair(match_error, range.first->second.get()));
				}
			}

			// no matches were found so create a new feature
			if (matched_list.empty())
			{
				action_features.push_back(make_feature(id, type, filtered_features->second, t));
			}
			else
			{
				// sort the list
				std::sort(std::begin(matched_list), std::end(matched_list), [](const std::pair<double, IFeature*>& a, const std::pair<double, IFeature*>& b) { return a.first < b.first; });
				// add the smallest error item to the state_features list
				action_features.push_back(matched_list.front().second);
			}
		}
	}
	else
	{
		// no features with this id exists yet, so make them
		for (auto& t : transformers)
		{
			ActionClassifier::feature_map flist;
			action_features.push_back(make_feature(id, type, flist, t));
			features.emplace(id, std::move(flist));
		}
	}

	return action_features;
}

IFeature* ActionClassifier::make_feature(unsigned int id, GType type, ActionClassifier::feature_map& filtered_feature, std::unique_ptr<ITransformer>& transformer)
{
	IFeature* fnew = nullptr;

	auto transformer_type = transformer->get_type();

	switch (type)
	{
	case Gint:		fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<int>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	case Gdouble:	fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<double>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	case GVec:		fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<GVec2>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	default:		throw std::runtime_error("Unknown type"); break;
	}

	return fnew;
}

Action* ActionClassifier::match_action(unsigned int type_id, unsigned int unique_hash) const
{
	auto type_match = actions.find(type_id);

	if (type_match != std::end(actions))
	{
		auto result = type_match->second.find(unique_hash);

		return result != std::end(type_match->second) ? result->second.get() : nullptr;
	}
	else
	{
		return nullptr;
	}
}
