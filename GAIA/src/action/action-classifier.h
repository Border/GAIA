#pragma once

#include "action.h"
#include "feature.h"
#include "feature-transformer.h"
#include "frame-context.h"
#include "action-types.h"
#include "feature-types.h"
#include "generic.h"

#include <mutex>
#include <unordered_map>

namespace GAIA
{

class ActionClassifier
{
	using feature_map = std::unordered_multimap<TransformerType, std::unique_ptr<IFeature>, std::hash<size_t>>;
protected:
	/// ctor - protected so it cant be created on stack
	ActionClassifier();

public:
	/// copy ctor, but nothing should be coppied
	ActionClassifier(const ActionClassifier& evcl);

	/// Obtains an instance of this class
	/// \return The instance for this class
	static ActionClassifier* Instance();

	/// Creates an instance
	static void Create();

	/// Composes a event, attempts to use an existing one if matches, otherwise creates a new one
	Action* compose_action(const ActionSamples& action_feature_samples);

	/// Composes one or more features to encompass the feature exemplars, either unsing existing known features or creating new ones
	std::vector<IFeature*> compose_features(unsigned int id, const std::vector<FeatureDataContext>& feature_data_samples);

	/// Gets a list of actions which match the action type hash
	// std::vector<Action*> get_action_types(unsigned int type_hash) const;

private:
	/// Creates a new features based on the transformer
	IFeature* make_feature(unsigned int id, GType type, feature_map& filtered_feature, std::unique_ptr<ITransformer>& transformer);

	/// Returns an action if it matches the unique hash
	Action* match_action(unsigned int type_id, unsigned int unique_hash) const;


	/// protects access
	/// Maybe use shared mutex to handle writes, allowing reads to occur simultaneously
	mutable std::mutex m_wframe;

	/// list of action feature grouped based on thier type, this owns them
	std::unordered_map<unsigned int, feature_map> features;

	/// list of actions that form affordances
	std::unordered_map<unsigned int, std::unordered_map<unsigned int, std::unique_ptr<Action>>> actions;

	/// the current number of action based features known to the agent
	unsigned int n_features;

	/// The number of actions the agent has created
	unsigned int n_actions;

	/// A singleton
	static std::unique_ptr<ActionClassifier> singleton;
};

}
