#pragma once

#include "feature-context.h"
#include "entity-interface.h"
#include "feature-types.h"
#include "generic.h"
#include "types.h"

#include <memory>
#include <vector>

namespace GAIA
{

class WorldFrame;

struct FrameContext
{
	FrameContext() = delete;
	FrameContext(Entity* ent, const std::shared_ptr<WorldFrame>& _frame);

	/// The entity which holds the perspective of this context.
	Entity* const entity;

	// Features - these are populated by processing the FrameContext when generating affordances (in the agent)
	// Or when an action is newly created (requiring all previously unidentified samples to be updated with the newly formed action).
	// For the moment it is assumed there can only be one reference per frame context.
	FeatureContext feature_references;

	/// The frame this context is built around
	std::shared_ptr<WorldFrame> frame;
};

}
