#pragma once

#include "frame-context.h"
#include "worldframe.h"
#include "action/action.h"
#include "feature-context.h"
#include "feature.h"
#include "causality.h"
#include "datamap.h"
#include "generic.h"

#include <vector>
#include <memory>

namespace GAIA
{

class Entity;
class Affordance;

struct AffordanceContext
{
	AffordanceContext(Entity* _ent, const std::shared_ptr<WorldFrame>& init_fr, const std::shared_ptr<WorldFrame>& final_fr);

	/// generates a hash based on the entity delta types
	unsigned int generate_event_type_hash() const;

	/// generate a hash based on the action data types
	unsigned int generate_action_type_hash() const;

	/// The delta between the initial and final frame of the reference entity
	const DataMap& delta;

	FeatureContext feature_context;

	/// The initial frame exemplar
	FrameContext init_frame;

	/// The final frame exemplar
	FrameContext final_frame;

	/// Event type hashes
	/// The list of hashes of permutations of the possible event types
	/// This list *should* be ordered by event delta count.
	std::vector<std::pair<uint32_t, std::vector<std::pair<unsigned int, Generic>>>> event_type_hashes;

	// List of matched events
	std::vector<Event*> matched_events;

	// List of matched affordance actions
	std::vector<Action*> matched_actions;

	// List of matched affordances
	std::vector<Affordance*> matched_affordances;
};

}
