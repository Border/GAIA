#pragma once

#include "affordance-action-classifier.h"
#include "affordance-condition-classifier.h"
#include "affordance-condition.h"
#include "affordance.h"
#include "affordance-context.h"
#include "event.h"

#include <vector>
#include <memory>

namespace GAIA
{
/*
struct CausalData
{
	std::vector<EntityAction*> m_actions;
	std::vector<Affordance*> m_affordances;
	std::vector<EntityAction*> m_target_actions;
};

class CausalCluster : public Cluster<FrameContext, CausalData>
{
public:
	CausalCluster();

	/// updates the thresh values for this cluster
	void update_cluster(unsigned int i) override;

	/// calculates the thresh
	void process_cluster() override;

	/// used to determine when there is sufficient mass of the cluster to form an
	/// affordance
	double thresh;
};

class CausalGroup : public ClusterGenerator<FrameContext, CausalData, CausalCluster>
{
public:
	CausalGroup();
	CausalGroup(FrameContext& ex);

private:
	/// updates the stats
	void update_stats(const CausalData& action) override;

	/// obtains the delta from the affordance context
	const CausalData& get_data(const FrameContext& ex) const override;

	/// calculate the distance between the action data
	double exemplar_distance(const CausalData& act1, const CausalData& act2) const override;

	/// the stats for each data type for each action type
	std::vector<std::pair<unsigned int, std::vector<std::pair<unsigned int, IFeatureStats*>>>> stats;
};*/

class AffordancePrototype
{
public:
	AffordancePrototype(Event* ev, std::vector<AffordanceContext> sample_contexts);

	std::unique_ptr<Affordance> add_sample_context(AffordanceContext& ex);

	Event* get_event() const;

private:
	Event* m_event;
	ActionGroup m_actions;
	ConditionGroup m_init_cond;
	ConditionGroup m_final_cond;
	ConditionGroup m_fail_cond;

	unsigned int m_action_type_hash;

	std::vector<AffordanceContext> m_sample_contexts;
};

}
