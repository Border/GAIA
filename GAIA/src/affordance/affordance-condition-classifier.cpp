#include "affordance-condition-classifier.h"
#include "state-classifier.h"
#include "feature-classifier.h"
#include "feature-context.h"
#include "worldframe.h"
#include "entity.h"
#include "container-algorithms.h"
#include "mhash.h"

#include <algorithm>

using namespace GAIA;

std::unique_ptr<ConditionClassifier> ConditionClassifier::singleton = nullptr;



FeatureGroup::FeatureGroup(Entity* ent, FrameContext context) :
	state(ent->state),
	entity_states({ std::make_pair(ent, std::move(context)) }),
	count(1),
	cluster_state(ClusterState::unvisited)
{
}



ConditionCluster::ConditionCluster() :
	neighbour_thresh(0.4),
	thresh(0),
	min_neighbours(2)
{
}

std::vector<std::vector<FeatureGroup*>> ConditionCluster::cluster()
{
	std::vector<std::vector<FeatureGroup*>> feature_group_clusters;

	// create the list of feature groups
	for (auto excl : exemplars)
	{
		const auto sgrp = excl->exgrp;

		for (const auto& ent : sgrp->data.frame->get_entities())
		{
			auto e = ent.second.get();

			// add the state values to stats
			for (auto& s : e->state)
			{
				const auto match_stats = std::find_if(std::begin(stats), std::end(stats),
					[&](const std::pair<unsigned int, IFeatureStats*>& x) { return s.first == x.first; });

				if (match_stats != std::end(stats))
				{
					match_stats->second->update(s.second);
				}
				else
				{
					stats.push_back(std::make_pair(s.first, make_stats_from_generic(s.second)));
				}
			}

			// check for identical state matches and combine
			for (const auto& fg : feature_groups)
			{
				double distance = 0;
				// calculate the error distance based on the current stats
				for (const auto& s : fg->state)
				{
					const auto match_state = fg->state.find(s.first);
					assert(match_state != std::end(fg->state));

					const auto match_feature_stats = std::find_if(std::begin(stats), std::end(stats),
						[&](const std::pair<unsigned int, IFeatureStats*>& x) { return s.first == x.first; });
					assert(match_feature_stats != std::end(stats));

					double error = generic_error(s.second, match_state->second, match_feature_stats->second);
					distance += error * error;
				}

				if (distance < std::numeric_limits<double>::epsilon())
				{
					++fg->count;
					continue;
				}
			}

			// e is the actual entity used for this set of feature samples, the FrameContext entity is the entity this condition is being generated for.
			feature_groups.push_back(std::make_unique<FeatureGroup>(e, sgrp->data));
			if (sgrp->exemplars.size() > 1)
			{
				feature_groups.back()->count += sgrp->exemplars.size() - 1;
			}
		}
	}

	min_neighbours = std::max(2, static_cast<int>(std::round(feature_groups.size() / 20)));

	unsigned int cluster_max = 0;

	for (const auto& s : stats)
	{
		auto feature_clusters = cluster_features(s);

		// is cluster count greater than max cluster count
		if (feature_clusters.size() > cluster_max)
		{
			cluster_max = feature_clusters.size();
			feature_group_clusters = feature_clusters;
		}

		// reset cluster_state for all FeatureGroups
		for (const auto& fg : feature_groups)
		{
			fg->cluster_state = ClusterState::unvisited;
			fg->neighbours.clear();
		}
	}

	return feature_group_clusters;
}

std::vector<std::pair<unsigned int, IFeatureStats*>> ConditionCluster::get_stats_ref() const { return stats; }

std::vector<std::vector<FeatureGroup*>> ConditionCluster::cluster_features(const std::pair<const unsigned int, IFeatureStats*>& fstat)
{
	// generate the neighbours for each feature group
	for (const auto& fg : feature_groups)
	{
		std::vector<std::pair<double, FeatureGroup* const>> neighbours;

		// iterate over all other feature groups
		for (const auto& fg_comp : feature_groups)
		{
			if (fg != fg_comp)
			{
				auto fg_feature = fg->state.find(fstat.first);
				auto fg_comp_feature = fg_comp->state.find(fstat.first);

				if (fg_feature != std::end(fg->state) && fg_comp_feature != std::end(fg_comp->state))
				{
					double distance = generic_error(fg_feature->second, fg_comp_feature->second, fstat.second);

					// check if the distance is less than the neighborhood threshold
					if (distance < neighbour_thresh)
					{
						neighbours.push_back(std::make_pair(distance, fg_comp.get()));
					}
				}
			}
		}

		// maybe compare the change in neighbours?
		std::swap(fg->neighbours, neighbours);
	}

	// feature cluster vector
	std::vector<std::vector<FeatureGroup*>> feature_clusters;

	for (const auto& fg : feature_groups)
	{
		if (fg->cluster_state != unvisited) { continue; }

		if (fg->neighbours.size() < min_neighbours)
		{
			// if there are a lot of samples at this point, then its likely a singular, so
			// create a cluster from it
			if (fg->count > 2 * min_neighbours)
			{
				fg->cluster_state = visited;

				// create a new cluster
				feature_clusters.push_back(make_feature_cluster(fg.get()));
			}
			else
			{
				fg->cluster_state = noise;
			}
		}
		else
		{
			fg->cluster_state = visited;

			// create a new cluster
			feature_clusters.push_back(make_feature_cluster(fg.get()));
		}
	}

	return feature_clusters;
}

std::vector<FeatureGroup*> ConditionCluster::make_feature_cluster(FeatureGroup* fg)
{
	std::vector<FeatureGroup*> feature_cluster;
	feature_cluster.push_back(fg);

	// recurse through all the neighbours of st, adding them to the cluster
	for (size_t i = 0; i < feature_cluster.size(); ++i)
	{
		auto fc = feature_cluster[i];

		for (const auto& n : fc->neighbours)
		{
			if (n.second->cluster_state != visited)
			{
				// maybe need for factor the number of times each exemplar has occurred.
				n.second->cluster_state = visited;
				if (n.second->neighbours.size() >= min_neighbours)
				{
					feature_cluster.push_back(n.second);
				}
			}
		}
	}

	return feature_cluster;
}

void ConditionCluster::update_cluster(unsigned int i)
{
	thresh += std::exp(0.1 * (exemplars[i]->exgrp->exemplars.size() - 1)) - 1;
}

void ConditionCluster::process_cluster()
{
	thresh += exemplars.size();
	thresh /= exemplars.size();
}

double ConditionCluster::get_thresh() const { return thresh; }



ConditionGroup::ConditionGroup():
	ClusterGenerator<FrameContext, FrameContext, ConditionCluster>()
{
}

void ConditionGroup::add_exemplar(const FrameContext& ex)
{
	// Cluster each feature type (for all entites in the frame ex) of a discrete nature use
	// the action target and event source as well as the frequency of categories for
	// discrete types to calculate the weighted COM. The nearest entity to the calculated
	// COM is the reference.

	// this estimation is used to determine if the feature is relative or absolute



	// now add the exemplar
	ClusterGenerator::add_exemplar(ex);
}

void ConditionGroup::update_stats(const FrameContext& ex)
{
	for (const auto& entity : ex.frame->get_entities())
	{
		for (const auto& s : entity.second->state)
		{
			auto match_stats = std::find_if(std::begin(stats), std::end(stats),
				[&](const std::pair<unsigned int, IFeatureStats*>& x) { return s.first == x.first; });

			if (match_stats != std::end(stats))
			{
				match_stats->second->update(s.second);
			}
			else
			{
				stats.push_back(std::make_pair(s.first, make_stats_from_generic(s.second)));
			}
		}
	}

	min_neighbours = std::max(min_neighbours, (unsigned int)stats.size());
}

const FrameContext& ConditionGroup::get_data(const FrameContext& ex) const { return ex; }

double ConditionGroup::exemplar_distance(const FrameContext& context1, const FrameContext& context2) const
{
	const auto& ents_a = context1.frame->get_entities();
	const auto& ents_b = context2.frame->get_entities();

	auto match_result = calg::best_match(std::begin(ents_a), std::end(ents_a), std::begin(ents_b), std::end(ents_b),
		[&](const std::pair<const unsigned int, std::shared_ptr<Entity>>& a, const std::pair<const unsigned int, std::shared_ptr<Entity>>& b)
	{
		double d = 0;

		// calculate the error distance based on the current stats
		for (const auto& s : a.second->state)
		{
			auto& id = s.first;

			const auto result = b.second->state.find(s.first);
			assert(result != std::end(b.second->state));

			const auto match_stats = std::find_if(std::begin(stats), std::end(stats),
				[id](const std::pair<unsigned int, IFeatureStats*>& x) { return x.first == id; });
			assert(match_stats != std::end(stats));

			auto feature_a = context1.feature_references.get_reference(id, a.second.get());
			if (feature_a.second.is_none())
			{
				feature_a.second = s.second;
			}
			else
			{
				feature_a.second = s.second - feature_a.second;
			}

			auto feature_b = context2.feature_references.get_reference(id, b.second.get());
			if (feature_b.second.is_none())
			{
				feature_b.second = result->second;
			}
			else
			{
				feature_b.second = result->second - feature_b.second;
			}

			double error = generic_error(feature_a.second, feature_b.second, match_stats->second);

			d += error * error;
		}

		return std::sqrt(d);
	});

	return match_result.first;
}


ConditionClassifier::ConditionClassifier() :
	n_conditions(0)
{
}

ConditionClassifier::ConditionClassifier(ConditionClassifier& cc) :
	n_conditions(0)
{
	// there should not be a copy ctor, so we just leave this blank
	// need to be explicit due to the mutex
}

ConditionClassifier* ConditionClassifier::Instance()
{
	return singleton.get();
}

void ConditionClassifier::Create()
{
	if (singleton == nullptr)
	{
		singleton = std::make_unique<ConditionClassifier>();
	}
}

std::vector<AffordanceCondition*> ConditionClassifier::compose_affordance_conditions(const ConditionGroup& cond)
{
	std::vector<AffordanceCondition*> cond_list;

	auto clusters = cond.cluster();

	for (const auto& condition_cluster : clusters)
	{
		if (condition_cluster->get_thresh() > 1.3)
		{
			cond_list.push_back(compose_affordance_condition(condition_cluster.get()));
		}
		else
		{
			// if any cluster is not ready, clear the list
			cond_list.clear();
			break;
		}
	}

	return cond_list;
}

AffordanceCondition* ConditionClassifier::compose_affordance_condition(ConditionCluster* cluster)
{
	// cluster each entity in this cluster based on their features
	auto feature_clusters = cluster->cluster();

	// the list of states required for this cluster to be a match
	std::vector<State*> state_list;

	// for each feature cluster use the feature classifier to identify features
	for (const auto& feature_groups : feature_clusters)
	{
		std::unordered_map<unsigned int, std::vector<FeatureDataContext>> state_feature_samples;

		for (auto feature_group : feature_groups)
		{
			for (const auto& entity_state : feature_group->entity_states)
			{
				for (const auto& s : entity_state.first->state)
				{
					tid_t ftype = s.first;
					// If the feature type is in the ignore list, filter it out
					if (std::any_of(std::begin(filtered_features), std::end(filtered_features),
						[ftype](tid_t type){ return ftype == type; }))
					{
						continue;
					}

					auto feature_sample_list = state_feature_samples.find(ftype);

					if (feature_sample_list == std::end(state_feature_samples))
					{
						feature_sample_list = state_feature_samples.emplace(ftype, std::vector<FeatureDataContext>()).first;
					}

					// TODO: [old?] check the frame context for reference features (that are 'in scope')
					// and use the reference result for the state feature, rather than the absolute
					// feature. Also include/translate the ref_type (maybe this isn't required?).

					Generic feature;
					FeatureReferenceType ref_type;
					std::tie(ref_type, feature) = entity_state.second.feature_references.get_reference(ftype, entity_state.first);
					if (feature.is_none())
					{
						feature = s.second;
					}
					else
					{
						feature = s.second - feature;
						// Ignore feature if its the entity, a reference and its 0
						if (entity_state.first->get_persistent_id() == entity_state.second.entity->get_persistent_id() && ref_type == FeatureReferenceType::relative)
						{
							if (feature.is_zero())
							{
								continue;
							}
						}
					}

					// { context, ref_type, data, type id, target }
					feature_sample_list->second.push_back({ entity_state.second, ref_type, feature, ftype, entity_state.first });
				}
			}
		}

		// make/match state and add to the states list
		state_list.push_back(StateClassifier::Instance()->compose_state(state_feature_samples));
	}

	auto affd_cond = match_affordance_condition(state_list);

	if (affd_cond.first == nullptr)
	{
		// create affordance condition and add to cond_list
		auto result = conditions.emplace(affd_cond.second, std::make_unique<AffordanceCondition>(++n_conditions, affd_cond.second, state_list));
		assert(result.second);
		affd_cond.first = result.first->second.get();
	}

	return affd_cond.first;
}

void ConditionClassifier::set_filtered_features(const std::vector<tid_t>& _filtered_features)
{
	filtered_features = _filtered_features;
}

std::pair<AffordanceCondition*, unsigned int> ConditionClassifier::match_affordance_condition(std::vector<State*>& state_list) const
{
	unsigned int hash = generate_hash(std::begin(state_list), std::end(state_list),
		[](const State* s) { return s->get_unique_hash(); });

	std::pair<AffordanceCondition*, unsigned int> aff_cond = std::make_pair(nullptr, hash);

	auto match = conditions.find(hash);

	if (match != std::end(conditions))
	{
		aff_cond.first = match->second.get();
	}

	return aff_cond;
}
