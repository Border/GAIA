#include "affordance-action.h"
#include "feature-association.h"
#include "entity.h"
#include "container-algorithms.h"

using namespace GAIA;

AffordanceAction::AffordanceAction(const AVector& _actions, unsigned int uid, unsigned int _hash) :
	id_unique(uid),
	hash_unique(_hash),
	actions(_actions)
{
}

bool AffordanceAction::match(Cause* _cause) const { return _cause->get_unique_id() == id_unique; }

bool AffordanceAction::match(const FrameContext& context) const
{
	return match_action(context) && !match_fail_condition(context);
}

CausalType AffordanceAction::get_causal_type() const { return CausalType::Causal_Action; }

unsigned int AffordanceAction::get_unique_id() const { return id_unique; }

unsigned int AffordanceAction::get_unique_hash() const { return hash_unique; }

void AffordanceAction::set_fail_conditions(ACVector& conditions)
{
	fail_conditions = std::move(conditions);
}

bool AffordanceAction::match_action(const FrameContext& context) const
{
	auto entity_actions = context.feature_references.entity->get_entity_actions();
	// quick check if the types match
	if (entity_actions.size() != actions.size() &&
		!calg::unsorted_match(
			std::begin(entity_actions),
			std::end(entity_actions),
			std::begin(actions),
			std::end(actions),
			[](const std::shared_ptr<EntityAction>& a1, Action* a2) { return a1->get_type_id() == a2->get_type_id(); }))
	{
		return false;
	}

	for (const auto& a : actions)
	{
		if (!a->match(context)) { return false; }
	}

	return true;
}

bool AffordanceAction::match_fail_condition(const FrameContext& context) const
{
	for (auto& cond : fail_conditions)
	{
		if (cond->match(context)) { return true; }
	}

	return false;
}

const ACVector & AffordanceAction::get_fail_conditions() const { return fail_conditions; }

std::vector<tid_t> AffordanceAction::get_action_data_feature_types() const
{
	std::vector<tid_t> types;

	for (auto action : actions)
	{
		auto type_id = action->get_type_id();
		if (std::none_of(std::begin(types), std::end(types), [type_id](const tid_t& id){ return id == type_id; }))
		{
			types.push_back(type_id);
		}
	}

	return types;
}

Action* AffordanceAction::find_action(EntityAction* entity_action, const FrameContext& context) const
{
	for (auto action : actions)
	{
		if (action->match_action(entity_action, context.feature_references))
		{
			return action;
		}
	}

	return nullptr;
}

const AVector& AffordanceAction::get_actions() const { return actions; }
