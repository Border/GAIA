#include "affordance-classifier.h"
#include "affordance-condition-classifier.h"
#include "affordance-action-classifier.h"
#include "action-classifier.h"
#include "memory.h"
#include "entity-context.h"
#include "container-algorithms.h"

#include <algorithm>
#include <cmath>
#include <iostream>

using namespace GAIA;

std::unique_ptr<AffordanceClassifier> AffordanceClassifier::m_singleton = nullptr;



AffordanceCluster::AffordanceCluster() :
	thresh(0)
{
}

void AffordanceCluster::update_cluster(unsigned int i)
{
	thresh += std::exp(0.1 * (exemplars[i]->exgrp->exemplars.size() - 1)) - 1;
}

void AffordanceCluster::process_cluster()
{
	thresh += exemplars.size();
	thresh /= exemplars.size();
}


EventGroup::EventGroup(unsigned int _hash, AffordanceContext& ex) :
	ClusterGenerator<AffordanceContext, DataMap, AffordanceCluster>(ex, ex.delta),
	hash(_hash)
{
	min_neighbours = std::max(min_neighbours, (unsigned int)ex.delta.size());

	for (const auto& d : ex.delta)
	{
		stats.push_back(std::make_pair(d.first, make_stats_from_generic(d.second)));
	}
}

void EventGroup::update_stats(const DataMap& delta)
{
	// update the stats of each delta variable
	for (const auto& d : delta)
	{
		auto match_stats = std::find_if(std::begin(stats), std::end(stats), [&](const std::pair<unsigned int, IFeatureStats*>& x) { return x.first == d.first; });
		// there *should* be a match
		assert(match_stats != std::end(stats));
		match_stats->second->update(d.second);
	}
}

const DataMap& EventGroup::get_data(const AffordanceContext& ex) const { return ex.delta; }

unsigned int EventGroup::get_hash() const { return hash; }

double EventGroup::exemplar_distance(const DataMap& ex1, const DataMap& ex2) const
{
	double distance = 0;

	// calculate the distance based on the current stats
	for (const auto& d : ex1)
	{
		auto match_stats = std::find_if(std::begin(stats), std::end(stats), [&](const std::pair<unsigned int, IFeatureStats*>& x) { return x.first == d.first; });
		assert(match_stats != std::end(stats));

		const auto& delta = ex2.find(d.first)->second;

		double error = generic_error(d.second, delta, match_stats->second);

		distance += error * error;
	}

	return std::sqrt(distance);
}



AffordanceClassifier::AffordanceClassifier() { }

AffordanceClassifier::AffordanceClassifier(const AffordanceClassifier& ac)
{
	// there should not be a copy ctor, so we just leave this blank
	// need to be explicit due to the mutex
}

AffordanceClassifier* AffordanceClassifier::Instance()
{
	return m_singleton.get();
}

void AffordanceClassifier::Create()
{
	if (m_singleton == nullptr)
	{
		m_singleton = std::make_unique<AffordanceClassifier>();
	}
}

/// recursive function for generating permutations (stores them in perms)
template<typename Iter, typename PermList>
void recurse_heterogeneous_sequence_perm(const Iter& iter, const Iter& end, size_t size, PermList temp, std::vector<unsigned int> type_id_list, std::vector<PermList>& perms)
{
	if (size > 0)
	{
		for (auto it = std::next(iter); it != end; ++it)
		{
			std::vector<unsigned int> heterogenous_sequence;
			std::vector<unsigned int> temp_type_id_list;
			for(auto& delta : it->second.second)
			{
				temp_type_id_list.push_back(delta.first);
			}

			std::set_symmetric_difference(
				std::begin(type_id_list), std::end(type_id_list),
				std::begin(temp_type_id_list), std::end(temp_type_id_list),
				std::back_inserter(heterogenous_sequence));

			// if the size between the combined sequence and the sum of the 2 sequences is not equal, there was a duplication.
			if ((temp_type_id_list.size() + type_id_list.size()) == heterogenous_sequence.size())
			{
				temp.push_back(*it);
				recurse_heterogeneous_sequence_perm(it, end, size - 1, temp, std::move(heterogenous_sequence), perms);
				temp.pop_back();
			}
		}
	}
	else
	{
		perms.push_back(std::move(temp));
	}
}

// The list of deltas and their type id
using delta_list = std::vector<std::pair<unsigned int, Generic>>;
// A pair of a delta list and the hash generated by the delta type id
using hash_delta = std::pair<uint32_t, delta_list>;
// A list of events and associated deltas that combine to form a unique combination
using event_hash_delta_list = std::vector<std::pair<Event*, hash_delta>>;

template<typename Iter>
std::vector<event_hash_delta_list> gen_heterogeneous_sequence(const Iter& first, const Iter& last)
{
	std::vector<event_hash_delta_list> affd_unique_union_list;
	size_t size = std::distance(first, last);
	for (auto it = first; it != last; ++it)
	{
		std::vector<unsigned int> type_id_list;
		for(auto& delta : it->second.second)
		{
			type_id_list.push_back(delta.first);
		}
		// generate a single permutation of features
		recurse_heterogeneous_sequence_perm(it, last, size - 1, { *it }, std::move(type_id_list), affd_unique_union_list);
	}

	return affd_unique_union_list;
}

void AffordanceClassifier::match_events(AffordanceContext& ex)
{
	event_hash_delta_list matched_events;

	for (auto& hash_delta : ex.event_type_hashes)
	{
		// filter out events whos type hash does not match
		auto filtered_events = EventClassifier::Instance()->get_event_types(hash_delta.first);

		// check each affordance for a match
		for (auto ev : filtered_events)
		{
			if (ev->match(ex.final_frame))
			{
				matched_events.push_back(std::make_pair(ev, hash_delta));
			}
		}
	}

	// if all feature types have a valid match (either a single action based
	// affordance match, or multiple action based affordance match), return
	// the list of affordances and clear event_type_hashes from the exemplar.
	event_hash_delta_list best_list;
	std::vector<event_hash_delta_list> event_unique_union_list = gen_heterogeneous_sequence(std::begin(matched_events), std::end(matched_events));

	for (auto& event_unique_union : event_unique_union_list)
	{
		if (event_unique_union.size() > best_list.size())
		{
			best_list = std::move(event_unique_union);
		}
	}

	// Remove all entries from ex.event_type_hashes that have the same type id
	// in the delta list (even if its only a partial match).
	for (const auto& event_delta : best_list)
	{
		for (const auto& delta : event_delta.second.second)
		{
			for (auto it = std::begin(ex.event_type_hashes); it != std::end(ex.event_type_hashes);)
			{
				if (std::any_of(std::begin(it->second), std::end(it->second),
					[&](const std::pair<unsigned int, Generic>& d) { return d.first == delta.first; }))
				{
					it = ex.event_type_hashes.erase(it);
				}
				else
				{
					++it;
				}
			}
		}
	}

	std::vector<Event*> matched_event_list;
	for (auto& event_list : best_list)
	{
		matched_event_list.push_back(event_list.first);
	}

	ex.matched_events = std::move(matched_event_list);
}

void AffordanceClassifier::match_actions(AffordanceContext& context)
{
	const auto& entity_actions = context.init_frame.entity->get_actions();

	if (entity_actions.empty())
	{
		return;
	}

	// any unmatched actions will have to be handled in the affordance prototype.
}

std::vector<Affordance*> AffordanceClassifier::match_affordances(AffordanceContext& ex)
{
	std::vector<Affordance*> matched_affordances;

	for (auto event_iter = std::begin(ex.matched_events); event_iter != std::end(ex.matched_events); )
	{
		auto affordance = m_affordances.find((*event_iter)->get_unique_hash());

		if (affordance != std::end(m_affordances))
		{
			auto affd = affordance->second.get();
			if (affd->match(ex))
			{
				matched_affordances.push_back(affd);
				ex.matched_affordances.push_back(affd);

				// if an affordance matches, then the event should be removed from matched_events in the ex
				event_iter = ex.matched_events.erase(event_iter);
				continue;
			}
		}

		++event_iter;
	}

	return matched_affordances;
}

// Create new events by adding to the appropriate event group.
// When a new event is created, pass the exemplars from the event group to the condition cluster
// Until the conditions are created (and an affordance is created) add exemplars to the condition cluster

void AffordanceClassifier::add_exemplar(AffordanceContext& ex)
{
	// if there aren't any deltas to form events, no point trying to make one
	if (ex.event_type_hashes.empty())
	{
		return;
	}

	// The top element should be the largest and only combination of all permutation
	// and contain the hash of the remaining type ids.
	unsigned int hash = ex.event_type_hashes.front().first;

	// if no affordance matched, check the m_event_groups
	const auto matched_event_group = std::find_if(std::begin(m_event_groups), std::end(m_event_groups),
		[hash](const std::unique_ptr<EventGroup>& eg) { return eg->get_hash() == hash; });

	if (matched_event_group != std::end(m_event_groups))
	{
		auto event_group = matched_event_group->get();
		event_group->add_exemplar(ex); // TODO: check this doesn't consume the exemplar

		auto event_cluster_list = event_group->cluster();

		for (const auto& event_cluster : event_cluster_list)
		{
			// check if the threshold is reached to form an event
			// need to check volatility, but cant just use the stats as there could be a
			// localised stable area. Still make it more difficult for a localised stable
			// to be formed into an event as it may be that the agent is stuck in a
			// local 'area' of actions and cannot perform the required actions.

			if (event_cluster->thresh > 1.3)
			{
				std::lock_guard<std::mutex> lock(m_affordance_mutex);
				auto affd_protos = make_affordance_prototype(event_cluster.get());
				for (auto& affd_proto : affd_protos)
				{
					ex.matched_events.push_back(affd_proto.get_event());

					// move the affd_proto to the list of prototypes
					m_affordance_prototypes.push_back(std::move(affd_proto));

					
				}

				if (! affd_protos.empty())
				{
					// Remove the exemplar groups of this event cluster from the event
					// groups list. This may cause an empty pointer issue for neighbours.
					// Though its very unlikely as the DBSCAN algorithm should not have
					// neighbours that are not part of its cluster.
					for (const auto& excl : event_cluster->exemplars)
					{
						event_group->remove_exemplar(excl->exgrp);
					}
				}
			}
		}

		// check if there are any ExemplarGroups left
		if (event_group->exemplars_empty())
		{
			// if not, remove the event from the table
			m_event_groups.erase(matched_event_group);
		}
	}
	else
	{
		std::lock_guard<std::mutex> lock(m_event_groups_mutex);
		// add it to the list of groups if it does not exist
		m_event_groups.push_back(std::make_unique<EventGroup>(hash, ex));
	}
}

std::vector<Affordance*> AffordanceClassifier::add_prototype_exemplar(AffordanceContext& context)
{
	std::vector<Affordance*> affordances;
	for (auto event_iter = std::begin(context.matched_events); event_iter != std::end(context.matched_events); ++event_iter)
	{
		unsigned int hash = (*event_iter)->get_unique_hash();
		auto prototype = std::find_if(std::begin(m_affordance_prototypes), std::end(m_affordance_prototypes),
			[hash](const AffordancePrototype& affd_proto) { return affd_proto.get_event()->get_unique_hash() == hash; });
		if (prototype != std::end(m_affordance_prototypes))
		{
			auto affordance = prototype->add_sample_context(context);
			if (affordance != nullptr)
			{
				// Check features for matches to ensure its valid.
				// This should not be required, but its more for debugging
				if (affordance->match(context))
				{
					affordances.push_back(affordance.get());
					m_affordances.emplace(affordance->get_event_hash(), std::move(affordance));
					m_affordance_prototypes.erase(prototype);
				}
				else
				{
					// TODO: when this happens it seems the initial condition is failing to match
					// This seems to be becuase a feature reference isn't being created
					std::cout << "Created an affordance that didn't match the current exemplar?!" << std::endl;
				}
			}
		}
	}

	return affordances;
}

std::vector<Affordance*> AffordanceClassifier::get_affordances() const
{
	std::vector<Affordance*> affds;

	for (const auto& affd : m_affordances) { affds.push_back(affd.second.get()); }

	return affds;
}

std::vector<AffordancePrototype> AffordanceClassifier::make_affordance_prototype(Cluster<AffordanceContext, DataMap>* cluster)
{
	std::vector<AffordancePrototype> affordance_prototypes;

	for (const auto& excl : cluster->exemplars)
	{
		// a list of features which represent the event of the affordance (characterising the delta)
		std::unordered_map<unsigned int, std::vector<FeatureDataContext>> event_samples;
		std::vector<AffordanceContext> sample_contexts;

		for (auto& ex : excl->exgrp->exemplars)
		{
			EntityContext entity_context(ex.final_frame.entity, ex.final_frame);

			for (const auto& d : ex.delta)
			{
				auto exemplars = event_samples.find(d.first);
				if (exemplars != std::end(event_samples))
				{
					exemplars->second.push_back({ex.final_frame, FeatureReferenceType::internal, d.second, d.first, ex.final_frame.entity});
				}
				else
				{
					event_samples.emplace(d.first, std::vector<FeatureDataContext>({ { ex.final_frame, FeatureReferenceType::internal, d.second, d.first, ex.final_frame.entity } }));
				}
			}

			sample_contexts.push_back(ex);
		}

		Event* ev = EventClassifier::Instance()->compose_event(event_samples);
		if (ev != nullptr)
		{
			affordance_prototypes.push_back(AffordancePrototype(ev, std::move(sample_contexts)));
		}
	}

	return affordance_prototypes;
}
