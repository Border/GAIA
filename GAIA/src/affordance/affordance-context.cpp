#include "affordance-context.h"
#include "entity.h"
#include "feature-association.h"
#include "mhash.h"

using namespace GAIA;

AffordanceContext::AffordanceContext(Entity* _ent, const std::shared_ptr<WorldFrame>& init_fr, const std::shared_ptr<WorldFrame>& final_fr) :
	delta(_ent->delta),
	feature_context(static_cast<Entity*>(_ent->get_prev_entity().get())),
	init_frame(static_cast<Entity*>(_ent->get_prev_entity().get()), init_fr),
	final_frame(_ent, final_fr),
	event_type_hashes(gen_type_hash_perm(_ent->delta.begin(), _ent->delta.end()))
{
	init_frame.feature_references = feature_context;
	final_frame.feature_references = feature_context;
	using type_hash = std::pair<uint32_t, std::vector<std::pair<unsigned int, Generic>>>;
	std::sort(std::begin(event_type_hashes), std::end(event_type_hashes),
		[](const type_hash& a, const type_hash& b){ return a.second.size() > b.second.size(); });
}

unsigned int AffordanceContext::generate_event_type_hash() const
{
	return generate_hash(
		std::begin(final_frame.entity->delta),
		std::end(final_frame.entity->delta),
		[](const std::pair<unsigned int, Generic>& d) { return d.first; });
}

unsigned int AffordanceContext::generate_action_type_hash() const
{
	return generate_hash(
		std::begin(final_frame.entity->actions),
		std::end(final_frame.entity->actions),
		[](const std::shared_ptr<EntityAction>& a) { return a->get_type_id(); });
}
