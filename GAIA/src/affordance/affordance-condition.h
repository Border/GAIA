#pragma once

#include "state.h"
#include "frame-context.h"

#include <vector>
#include <tuple>

namespace GAIA
{

class AffordanceCondition
{
public:
	/// The affordance condition ctor
	AffordanceCondition(unsigned int _id, unsigned int _hash, std::vector<State*>& _states);

	/// performs a match with the provided entity and world frame to see if they match the
	/// condition
	bool match(const FrameContext& context) const;

	/// returns the states for this condition
	const std::vector<State*>& get_states() const;

	/// returns the unique id
	unsigned int get_unique_id() const;

	/// returns the hash generated from the unique id of each state that forms this
	/// condition
	unsigned int get_unique_hash() const;

private:
	/// the unique id of the condition
	const unsigned int id_unique;
	/// the hash generated from the state id's that form the condition
	const unsigned int hash_unique;
	/// the states of all other entities potentially relevant to the condition
	const std::vector<State*> states;
};

using ACVector = std::vector<AffordanceCondition*>;
using ACIter = std::vector<AffordanceCondition*>::iterator;

}
