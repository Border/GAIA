#include "affordance.h"
#include "event.h"
#include "affordance-context.h"
#include "frame-context.h"
#include "container-algorithms.h"

using namespace GAIA;

Affordance::Affordance(unsigned int _id, CVector& _causes, Event* event, ACVector& init, ACVector& final) :
	id_unique(_id),
	causes(_causes),
	m_event(event),
	init_cond(init),
	final_cond(final)
{ }

bool Affordance::match(AffordanceContext& context) const
{
	return
		// this matching of effects only checks for matching features for an event, it does not check for other affordances which may be an effect
		m_event->match(context.final_frame) &&
		std::any_of(std::begin(causes), std::end(causes), [&](Cause* c) { return c->match(context.init_frame); }) &&
		match_initial_condition(context.init_frame) &&
		match_final_condition(context.final_frame);
}

bool Affordance::match_initial_condition(const FrameContext& context) const
{
	for (auto c : init_cond)
	{
		if (c->match(context)) { return true; }
	}

	return false;
}

bool Affordance::match_final_condition(const FrameContext& context) const
{
	for (auto c : final_cond)
	{
		if (c->match(context)) { return true; }
	}

	return false;
}

std::vector<DataMap> Affordance::generate_deltas(const FeatureContext& context) const
{
	std::vector<DataMap> deltas;

	DataMap delta;

	for (const auto& f_group : m_event->get_features())
	{
		// for the moment only use the first feature, need to add other dependent
		// functionality first
		auto f = f_group.second.front();

		delta.emplace(f->get_type_id(), f->get_feature());
	}

	deltas.push_back(std::move(delta));

	return deltas;
}

bool Affordance::match(Cause* _cause) const { return _cause->get_unique_id() == id_unique; }

bool Affordance::match(const FrameContext& context) const { return false; }

CausalType Affordance::get_causal_type() const { return CausalType::Causal_Affordance; }

unsigned int Affordance::get_unique_id() const { return id_unique; }

std::vector<AffordanceAction*> Affordance::get_actions() const
{
	std::vector<AffordanceAction*> actions;

	// iterate over the cause list and find those that are actions, then return them.
	for (auto& cause : causes)
	{
		if (cause->get_causal_type() == CausalType::Causal_Action)
		{
			actions.push_back(static_cast<AffordanceAction*>(cause));
		}
	}

	return actions;
}

unsigned int Affordance::get_event_cause_type_hash() const { return event_cause_type_hash; }

unsigned int Affordance::get_event_hash() const { return m_event->get_unique_hash(); }

const CVector Affordance::get_causes() const { return causes; }

Intrinsic& Affordance::get_intrinsic() { return intrinsic; }
