#pragma once

#include "event-classifier.h"
#include "affordance.h"
#include "affordance-prototype.h"
#include "affordance-condition.h"
#include "affordance-context.h"
#include "cluster.h"
#include "feature-stats.h"

#include <vector>
#include <unordered_map>
#include <optional>


namespace GAIA
{

class AffordanceCluster : public Cluster<AffordanceContext, DataMap>
{
public:
	AffordanceCluster();

	/// updates the thresh values for this cluster
	void update_cluster(unsigned int i) override;

	/// calculates the thresh
	void process_cluster() override;

	/// used to determine when there is sufficient mass of the cluster to form an
	/// affordance
	double thresh;
};

class EventGroup : public ClusterGenerator<AffordanceContext, DataMap, AffordanceCluster>
{
public:
	EventGroup(unsigned int _hash, AffordanceContext& ex);

	/// returns the hash of this group
	unsigned int get_hash() const;

	/// 
	std::vector<Event*> match_events(AffordanceContext& ex);

	void match_actions(AffordanceContext& ex);

private:
	/// updates the stats based on the delta
	void update_stats(const DataMap& delta) override;

	/// obtains the delta from the affordance exemplar
	const DataMap& get_data(const AffordanceContext& ex) const override;

	/// calculates the distance between 2 exemplars
	double exemplar_distance(const DataMap& ex1, const DataMap& ex2) const override;

	/// the hash of the action and delta types
	const unsigned int hash;

	/// the stats for each delta
	std::vector<std::pair<unsigned int, IFeatureStats*>> stats;

	/// Events that describe the events this EventGroup contains
	std::vector<Event*> events;
};

class AffordanceClassifier
{
	friend class DebugDisplay;
public:
	AffordanceClassifier();
	AffordanceClassifier(const AffordanceClassifier& ac);

	/// Obtains an instance of this class
	/// \return The instance for this class
	static AffordanceClassifier* Instance();

	/// Creates an instance
	static void Create();

	/// returns a list of directly matching known event types
	void match_events(AffordanceContext& context);

	/// returns a list of directly matching known event types
	void match_actions(AffordanceContext& context);

	/// returns a list of direct matched affordances to the exemplar
	std::vector<Affordance*> match_affordances(AffordanceContext& ex);

	/// adds an exemplar to the event list
	void add_exemplar(AffordanceContext& ex);

	/// adds an exemplar to the relevant affordance prototypes, returns one or more affordances if an affordance is formed.
	std::vector<Affordance*> add_prototype_exemplar(AffordanceContext& ex);

	/// adds a fail exemplar to the relevant affordance prototypes, returns one or more affordances if an affordance is formed.
	std::vector<Affordance*> add_prototype_fail_exemplar(AffordanceContext& ex);

	/// returns a list of affordances
	std::vector<Affordance*> get_affordances() const;

private:
	/// Make an affordance prototype from the cluster
	std::vector<AffordancePrototype> make_affordance_prototype(Cluster<AffordanceContext, DataMap>* cluster);


	/// singleton instance
	static std::unique_ptr<AffordanceClassifier> m_singleton;

	/// Stores a list of exemplars until they can be used to form an affordance
	std::vector<std::unique_ptr<EventGroup>> m_event_groups;

	/// Stores a list of generated affordances
	std::unordered_map<unsigned int, std::unique_ptr<Affordance>> m_affordances;

	/// Stores a list of temporary affordance prototypes
	std::vector<AffordancePrototype> m_affordance_prototypes;

	/// protects access to the action event table
	std::mutex m_event_groups_mutex;

	/// protects access to the affordance map
	std::mutex m_affordance_mutex;
};

}
