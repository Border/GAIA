#include "affordance-prototype.h"
#include "memory.h"

using namespace GAIA;

AffordancePrototype::AffordancePrototype(Event* ev, std::vector<AffordanceContext> sample_contexts)
	: m_event(ev)
	, m_sample_contexts(std::move(sample_contexts))
{
	for (auto& sample_context : m_sample_contexts)
	{
		m_actions.add_exemplar(sample_context.init_frame);
		m_init_cond.add_exemplar(sample_context.init_frame);
		m_final_cond.add_exemplar(sample_context.final_frame);
	}
}

std::unique_ptr<Affordance> AffordancePrototype::add_sample_context(AffordanceContext& sample_context)
{
	m_sample_contexts.push_back(sample_context);

	// check if there are any actions
	if (! sample_context.init_frame.entity->actions.empty())
	{
		// m_action_exemplars.push_back(sample_context);
		m_actions.add_exemplar(sample_context.init_frame);
	}
	else
	{
		// find an entity which has an action targeting the ref entity.
	}

	// Attempt to identify action features for 1 or more actions
	auto causes = AffordanceActionClassifier::Instance()->compose_affordance_actions(m_actions);

	if (causes.empty()) { return nullptr; }

	m_init_cond.add_exemplar(sample_context.init_frame);
	m_final_cond.add_exemplar(sample_context.final_frame);

	// remove all processed contexts
	m_sample_contexts.clear();

	// check if the conditions are valid, if not, dont create the affordance
	auto icon = ConditionClassifier::Instance()->compose_affordance_conditions(m_init_cond);
	auto fcon = ConditionClassifier::Instance()->compose_affordance_conditions(m_final_cond);

	if (icon.empty() || fcon.empty()) { return nullptr; }

	// now form an affordance
	return std::make_unique<Affordance>(Memory::Instance()->get_next_causality_id(), causes, m_event, icon, fcon);
}

Event* AffordancePrototype::get_event() const { return m_event; }
