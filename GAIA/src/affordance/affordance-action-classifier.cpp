#include "affordance-action-classifier.h"
#include "feature-classifier.h"
#include "feature-container.h"
#include "feature-association.h"
#include "action-classifier.h"
#include "entity-context.h"
#include "memory.h"
#include "entity.h"
#include "container-algorithms.h"
#include "mhash.h"

using namespace GAIA;

std::unique_ptr<AffordanceActionClassifier> AffordanceActionClassifier::singleton = nullptr;



ActionCluster::ActionCluster() :
	thresh(0)
{
}

void ActionCluster::update_cluster(unsigned int i)
{
	thresh += std::exp(0.1 * (exemplars[i]->exgrp->exemplars.size() - 1)) - 1;
}

void ActionCluster::process_cluster()
{
	thresh += exemplars.size();
	thresh /= exemplars.size();
}



ActionGroup::ActionGroup() : ClusterGenerator<FrameContext, action_data, ActionCluster>()
{
}

ActionGroup::ActionGroup(FrameContext& ex) :
	ClusterGenerator<FrameContext, action_data, ActionCluster>(ex, get_data(ex))
{
	const auto& actions = get_data(ex);
	for (const auto& action : actions)
	{
		min_neighbours = std::max(min_neighbours, (unsigned int)action->get_data().size());
	}

	exemplar_groups.push_back(std::make_unique<ExemplarGroup<FrameContext, action_data>>(ex, actions));

	update_stats(actions);
}

void ActionGroup::update_stats(const action_data& actions)
{
	for (const auto& action : actions)
	{
		auto match_stats_type = std::find_if(std::begin(stats), std::end(stats),
			[&](const std::pair<unsigned int, std::vector<std::pair<unsigned int, IFeatureStats*>>>& x) { return action->get_type_id() == x.first; });
		if (match_stats_type == std::end(stats))
		{
			stats.push_back(std::make_pair(action->get_type_id(), std::vector<std::pair<unsigned int, IFeatureStats*>>()));
			match_stats_type = std::prev(stats.end());
		}

		for (const auto& d : action->get_data())
		{
			auto match_stats = std::find_if(std::begin(match_stats_type->second), std::end(match_stats_type->second),
				[&](const std::pair<unsigned int, IFeatureStats*>& x) { return d.data_type == x.first; });
			if (match_stats != std::end(match_stats_type->second))
			{
				match_stats->second->update(d.data);
			}
			else
			{
				match_stats_type->second.push_back({ d.data_type, make_stats_from_generic(d.data) });
			}
		}
	}
}

const action_data& ActionGroup::get_data(const FrameContext& context) const
{
	return context.entity->actions;
}

double ActionGroup::exemplar_distance(const action_data& act1, const action_data& act2) const
{
	assert(act1.size() == act2.size());

	// action_data must be sorted

	double distance = 0;

	for (auto ait1 = std::begin(act1); ait1 != std::end(act1); ++ait1)
	{
		// copy the action type for quick reference
		const auto type = (*ait1)->get_type_id();

		// find the range of actions that are of the same type for act1
		auto act1_match_end = ait1;
		while (act1_match_end != std::end(act1) && (*act1_match_end)->get_type_id() == type)
		{
			act1_match_end = std::next(act1_match_end);
		}

		// find the range of actions that are of the same type for act2
		auto matched_action_begin = std::find_if(std::begin(act2), std::end(act2), [type](const std::shared_ptr<EntityAction>& a) { return a->get_type_id() == type; });
		assert(matched_action_begin != std::end(act2));
		auto matched_action_end = matched_action_begin;
		while (matched_action_end != std::end(act2) && (*matched_action_end)->get_type_id() == type)
		{
			matched_action_end = std::next(matched_action_end);
		}

		// find the stats for this action type
		auto match_stats_type = std::find_if(std::begin(stats), std::end(stats),
			[type](const std::pair<unsigned int, std::vector<std::pair<unsigned int, IFeatureStats*>>>& x) { return x.first == type; });
		assert(match_stats_type != std::end(stats));

		// find the best combination of actions which produce the smallest difference
		auto match_result = calg::best_match(ait1, act1_match_end, matched_action_begin, matched_action_end,
			[&](const std::shared_ptr<EntityAction>& a, const std::shared_ptr<EntityAction>& b)
		{
			double d = 0;

			for (size_t i = 0, size = a->get_data().size(); i < size; ++i)
			{
				const auto& data_a = a->get_data()[i];
				const auto& data_b = b->get_data()[i];

				// get the stats for this data feature type
				auto feature_stats = std::find_if(std::begin(match_stats_type->second), std::end(match_stats_type->second),
					[&](const std::pair<unsigned int, IFeatureStats*>& x) { return data_a.data_type == x.first; });

				double error = 1.0;
				// Make sure the reference type is the same
				if (data_a.ref_type == data_b.ref_type)
				{
					error = generic_error(data_a.data, data_b.data, feature_stats->second);
				}

				d += error * error;
			}

			return std::sqrt(d);
		});

		distance += match_result.first;
	}

	return distance;
}



AffordanceActionClassifier::AffordanceActionClassifier() { }

AffordanceActionClassifier::AffordanceActionClassifier(const AffordanceActionClassifier& evcl) { }

AffordanceActionClassifier* AffordanceActionClassifier::Instance()
{
	return singleton.get();
}

void AffordanceActionClassifier::Create()
{
	if (singleton == nullptr)
	{
		singleton = std::make_unique<AffordanceActionClassifier>();
	}
}

CVector AffordanceActionClassifier::compose_affordance_actions(const ActionGroup& action_group)
{
	CVector composed_actions;

	auto action_clusters = action_group.cluster();

	for (const auto& cluster : action_clusters)
	{
		composed_actions.push_back(compose_affordance_action(cluster.get(), false));
	}

	return composed_actions;
}

void AffordanceActionClassifier::add_fail_exemplar(FrameContext&& context)
{
	// check for actions currently in the process of learning fail conditions
	auto matched_action = std::find_if(std::begin(fail_conditions), std::end(fail_conditions),
		[&](const std::pair<AffordanceAction*, ConditionGroup>& x) { return x.first->match(context); });

	if (matched_action != std::end(fail_conditions))
	{
		matched_action->second.add_exemplar(context);

		auto condition_cluster = matched_action->second.cluster();

		ACVector conditions;
		for (const auto& cl : condition_cluster)
		{
			if (cl->get_thresh() > 1.3)
			{
				auto cond = ConditionClassifier::Instance()->compose_affordance_condition(cl.get());

				if (cond != nullptr)
				{
					conditions.push_back(cond);
					// remove exemplars for created affordance conditions
					for (auto context_cluster : cl->exemplars)
					{
						matched_action->second.remove_exemplar(context_cluster->exgrp);
					}
				}
			}
			else
			{
				conditions.clear();
				break;
			}
		}

		if (!conditions.empty())
		{
			matched_action->first->set_fail_conditions(conditions);
		}

		// check if there are any ExemplarGroups left
		if (matched_action->second.exemplars_empty())
		{
			// if not, remove the fail action from the list
			fail_conditions.erase(matched_action);
		}
	}
	else
	{
		// generate a hash based on all the action types and thier data types
		unsigned int hash = generate_hash(
			std::begin(context.entity->actions),
			std::end(context.entity->actions),
			[](const std::shared_ptr<EntityAction>& a) { return a->get_type_id(); });

		// check for existing actions
		auto filtered_actions = actions.equal_range(hash);
		for (; filtered_actions.first != filtered_actions.second; ++filtered_actions.first)
		{
			auto action = filtered_actions.first->second.get();

			// check if the action matches
			if (action->match_action(context))
			{
				// it does, now check if the fail conditions match
				if (!action->match_fail_condition(context))
				{
					// WARNING: if the action already has fail conditions, but there are
					//          some new ones that were not learned, this will effectively
					//          overwite them, potentiially ignoring the prior learned
					//          ones. Ideally some process will be required which merges
					//          the previous and new fail conditions

					// they dont, so we need to create a fail conditions entry
					ConditionGroup conds;
					conds.add_exemplar(context);
					fail_conditions.push_back(std::make_pair(action, std::move(conds)));
				}

				return;
			}
		}

		const auto matched_action_table = std::find_if(std::begin(action_table), std::end(action_table),
			[hash](const std::pair<unsigned int, ActionGroup>& ag) { return ag.first == hash; });

		if (matched_action_table != std::end(action_table))
		{
			matched_action_table->second.add_exemplar(context);

			auto action_clusters = matched_action_table->second.cluster();

			for (const auto& cl : action_clusters)
			{
				if (cl->thresh > 1.4)
				{
					const auto affd_action = compose_affordance_action(cl.get(), true);

					if (affd_action != nullptr)
					{
						// remove exemplars for created affordance actions
						for (auto excl : cl->exemplars)
						{
							matched_action_table->second.remove_exemplar(excl->exgrp);
						}
					}
				}
			}
		}
		else
		{
			auto p = std::make_pair(hash, ActionGroup(context));
			action_table.push_back(std::move(p));
		}
	}
}

std::pair<std::pair<const unsigned int, const unsigned int>, AffordanceAction*> AffordanceActionClassifier::match_affordance_action(AVector& action_list) const
{
	unsigned int hash_type = generate_hash(std::begin(action_list), std::end(action_list), [](Action* a) { return a->get_type_id(); });
	unsigned int hash_unique = generate_hash(std::begin(action_list), std::end(action_list), [](Action* a) { return a->get_unique_id(); });

	auto filtered_actions = actions.equal_range(hash_type);

	for (; filtered_actions.first != filtered_actions.second; ++filtered_actions.first)
	{
		auto action = filtered_actions.first->second.get();

		if (action->get_unique_hash() == hash_unique)
		{
			return std::make_pair(std::make_pair(hash_type, hash_unique), action);
		}
	}

	return std::make_pair(std::make_pair(hash_type, hash_unique), nullptr);
}

AffordanceAction* AffordanceActionClassifier::compose_affordance_action(const ActionCluster* cluster, bool fail)
{
	std::vector<ActionSamples> action_samples;
	ConditionGroup fail_conds;

	for (const auto& excl : cluster->exemplars)
	{
		const auto exgrp = excl->exgrp;
		for (auto& action : exgrp->data)
		{
			auto action_type_samples = std::find_if(std::begin(action_samples), std::end(action_samples),
				[&](const ActionSamples& a) { return a.type == action->get_type_id(); });

			// Check if one exists, create if not
			if (action_type_samples == std::end(action_samples))
			{
				action_samples.push_back({ action->get_type_id(), std::vector<ActionFeaturesList>() });
				action_type_samples = std::prev(action_samples.end());
			}

			auto& action_feature_samples = action_type_samples->feature_samples;

			// This handles the features
			for (const auto& d : action->get_data())
			{
				auto action_feature_sample_type = std::find_if(std::begin(action_feature_samples), std::end(action_feature_samples),
					[&](const ActionFeaturesList& a) { return a.feature_type == d.data_type; });

				if (action_feature_sample_type == std::end(action_feature_samples))
				{
					// The cluster has all the same ref_type
					action_feature_samples.push_back({ d.data_type, d.ref_type, std::vector<FeatureDataContext>() });
					action_feature_sample_type = std::prev(action_feature_samples.end());
				}

				for (const auto& context : exgrp->exemplars)
				{
					action_feature_sample_type->data_samples.push_back({ context, d.ref_type, d.data, d.data_type, nullptr });
					if (fail) { fail_conds.add_exemplar(context); }
				}
			}
		}
	}

	AVector action_list;

	for (auto& a : action_samples)
	{
		action_list.push_back(ActionClassifier::Instance()->compose_action(a));
	}

	// sort action list by the type id
	std::sort(std::begin(action_list), std::end(action_list), [](Action* a1, Action* a2) { return a1->get_unique_id() < a2->get_unique_id(); });

	auto affd_action = match_affordance_action(action_list);

	if (affd_action.second == nullptr)
	{
		// need to search for a match first before creating a new one
		affd_action.second = actions.emplace(affd_action.first.first, std::make_unique<AffordanceAction>(action_list, Memory::Instance()->get_next_causality_id(), affd_action.first.second))->second.get();

		if (fail)
		{
			// add the new affordance action to the failed condition list
			fail_conditions.push_back(std::make_pair(affd_action.second, std::move(fail_conds)));
		}
	}

	return affd_action.second;
}
