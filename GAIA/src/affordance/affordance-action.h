#pragma once

#include "affordance-condition.h"
#include "causality.h"
#include "action.h"
#include "frame-context.h"
#include "feature-types.h"

#include <vector>

namespace GAIA
{

class EntityAction;

/// 
class AffordanceAction : public Cause
{
public:
	/// ctor - copy features and generate hashes
	AffordanceAction(const AVector& _actions, unsigned int uid, unsigned int _hash);

	/// returns true if this cause matches the supplied one
	virtual bool match(Cause* cause) const;
	/// Returns true if the FrameContext matches
	virtual bool match(const FrameContext& context) const;
	/// returns the causal type
	virtual CausalType get_causal_type() const;
	/// returns the causality id
	virtual unsigned int get_unique_id() const;
	/// returns the hash generated from the unique action id's of each action which forms this affordance action
	unsigned int get_unique_hash() const;
	/// moves the fail conditions for the actions into this AffordanceAction
	void set_fail_conditions(ACVector& conditions);
	/// returns true if only the action match, regardless of the fail conditions
	bool match_action(const FrameContext& context) const;
	/// returns true if the exemplar matches the fail conditions of thia action
	bool match_fail_condition(const FrameContext& context) const;
	/// returns a cons reference to the fail conditions of the affordance action
	const ACVector& get_fail_conditions() const;
	/// returns a list of the data feature types used in all the actions
	std::vector<tid_t> get_action_data_feature_types() const;
	/// returns the action which matches the supplied entitiy action
	Action* find_action(EntityAction* entity_action, const FrameContext& context) const;
	/// returns the list of actions
	const AVector& get_actions() const;

private:
	/// The unique id of the affordance action
	const unsigned int id_unique;
	/// the hash generated from the action id's that form the condition
	const unsigned int hash_unique;

	/// The list of actions that must be matched/performed
	const AVector actions;

	/// The fail conditions (if any) for this action
	ACVector fail_conditions;
};

typedef std::vector<AffordanceAction*> AAVector;
typedef std::vector<AffordanceAction*>::iterator AAIter;

}
