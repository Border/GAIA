#pragma once

#include "affordance-condition-classifier.h"
#include "feature.h"
#include "feature-stats.h"
#include "affordance-action.h"
#include "action-entity.h"
#include "frame-context.h"
#include "cluster.h"

#include <mutex>
#include <unordered_map>
#include <memory>

namespace GAIA
{

using action_data = std::vector<std::shared_ptr<EntityAction>>;
class State;

class ActionCluster : public Cluster<FrameContext, action_data>
{
public:
	/// ctor
	ActionCluster();

	/// updates the thresh values for this cluster
	virtual void update_cluster(unsigned int i);

	/// calculates the thresh
	virtual void process_cluster();

	/// used to determine when there is sufficient mass of the cluster to form an
	/// affordance
	double thresh;
};

class ActionGroup : public ClusterGenerator<FrameContext, action_data, ActionCluster>
{
public:
	/// default ctor
	ActionGroup();

	/// ctor
	ActionGroup(FrameContext& ex);

private:
	/// updates the stats
	virtual void update_stats(const action_data& action);

	/// obtains the delta from the affordance exemplar
	virtual const action_data& get_data(const FrameContext& ex) const;

	/// calculate the distance between the action data
	virtual double exemplar_distance(const action_data& act1, const action_data& act2) const;

	/// the stats for each data type for each action type
	std::vector<std::pair<unsigned int, std::vector<std::pair<unsigned int, IFeatureStats*>>>> stats;
};

class AffordanceActionClassifier
{
protected:
	/// copy ctor, but nothing should be coppied
	AffordanceActionClassifier(const AffordanceActionClassifier& evcl);

public:
	AffordanceActionClassifier();

	/// Obtains an instance of this class
	/// \return The instance for this class
	static AffordanceActionClassifier* Instance();

	/// Creates an instance
	static void Create();

	/// match or create affordance actions, returing the list
	CVector compose_affordance_actions(const ActionGroup& action_group);

	/// adds a failed exemplar to the appropriate action group
	void add_fail_exemplar(FrameContext&& ex);

private:
	/// find a matching affordance action
	std::pair<std::pair<const unsigned int, const unsigned int>, AffordanceAction*> match_affordance_action(AVector& action_list) const;

	/// match or create affordance actions, returing the list
	AffordanceAction* compose_affordance_action(const ActionCluster* action_clusters, bool fail);

	/// protects access
	/// Maybe use shared mutex to handle writes, allowing reads to occur simultaneously
	mutable std::mutex m_wframe;

	/// the list of AffordanceActions used by affordances
	std::unordered_multimap<unsigned int, std::unique_ptr<AffordanceAction>> actions;

	/// a list of exemplars for which all actions failed.
	std::vector<std::pair<unsigned int, ActionGroup>> action_table;

	/// a list of AffordanceActions and thier in process learning of failed conditions
	std::vector<std::pair<AffordanceAction*, ConditionGroup>> fail_conditions;

	/// A singleton
	static std::unique_ptr<AffordanceActionClassifier> singleton;
};

}
