#include "affordance-condition.h"
#include "worldframe.h"

#include <algorithm>

using namespace GAIA;

AffordanceCondition::AffordanceCondition(unsigned int _id, unsigned int _hash, std::vector<State*>& _states) :
	id_unique(_id),
	hash_unique(_hash),
	states(std::move(_states))
{
}

bool AffordanceCondition::match(const FrameContext& context) const
{
	// get a list of entities from the frame
	for (const auto& entity : context.frame->get_entities())
	{
		// match at least one of the states
		bool matched = false;

		for (const auto& s : states)
		{
			matched |= s->match(entity.second->state, context.feature_references);
		}

		if (!matched) { return false; }
	}

	return true;
}

const std::vector<State*>& AffordanceCondition::get_states() const { return states; }

unsigned int AffordanceCondition::get_unique_id() const { return id_unique; }

unsigned int AffordanceCondition::get_unique_hash() const { return hash_unique; }
