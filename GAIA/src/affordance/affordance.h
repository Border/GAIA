#pragma once

#include "affordance-condition.h"
#include "affordance-action.h"
#include "event.h"
#include "action-entity.h"
#include "causality.h"
#include "intrinsic.h"
#include "datamap.h"

#include <vector>

namespace GAIA
{

struct AffordanceContext;
struct FrameContext;

/// Characterises the transformation between 2 frames and specifies the initial and final
/// conditions for it to occur, as well as the stimulation/action required to perform to
/// incite the transformation
class Affordance : public Cause, public Effect
{
	friend class FrameGroup;
public:
	/// ctor
	Affordance(unsigned int _id, CVector& _causes, Event* event, ACVector& init, ACVector& final);

	/// checks if the exemplar data matches the characteristics of the affordance
	bool match(AffordanceContext& ex) const;

	/// checks if the initial state is a match for this affordance
	bool match_initial_condition(const FrameContext& ex) const;

	/// checks if the final state is a match for this affordance
	bool match_final_condition(const FrameContext& ex) const;

	/// generates a range of deltas from the event
	std::vector<DataMap> generate_deltas(const FeatureContext& context) const;

	/// checks if the supplied cause matches this affordance
	virtual bool match(Cause* _causes) const;

	/// checks if the supplied feature vector contians a match to the affordances effect
	virtual bool match(const FrameContext& ex) const;

	/// returns the causality id
	virtual unsigned int get_unique_id() const;

	std::vector<AffordanceAction*> get_actions() const;

	unsigned int get_event_cause_type_hash() const;

	unsigned int get_event_hash() const;

	const CVector get_causes() const;

	Intrinsic& get_intrinsic();

private:
	/// returns the causal type
	virtual CausalType get_causal_type() const;


	/// the affordance causality id
	unsigned int id_unique;

	unsigned int event_cause_type_hash;

	/// initial condition (maybe have a list of conditions, where only 1 condition is
	/// required to be met, e.g. something at the front or back)
	ACVector init_cond;

	/// final conditions
	ACVector final_cond;

	/// list of events or indirect affordances
	Event* m_event;

	/// actions or indirect affordances
	/// each CVector contains a list of causes that must be matched/performed
	/// Only one CVector has to be matched/performed
	CVector causes;

	Intrinsic intrinsic;
};


}
