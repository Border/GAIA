#pragma once

#include "affordance-condition.h"
#include "feature-stats.h"
#include "frame-context.h"
#include "cluster.h"
#include "datamap.h"
#include "generic.h"

#include <vector>
#include <atomic>

namespace GAIA
{

struct FeatureGroup
{
	/// ctor
	FeatureGroup(Entity* ent, FrameContext context);

	/// The state for this group
	const DataMap& state;
	/// a list of entities and frame context an entity state is identical to
	std::vector<std::pair<Entity*, const FrameContext>> entity_states;
	/// A ist of exemplar neighbours within a specified range
	std::vector<std::pair<double, FeatureGroup* const>> neighbours;
	/// the number of other entities that matched this
	unsigned int count;
	/// The clustering status of the feature group
	ClusterState cluster_state;
};

class ConditionCluster : public Cluster<FrameContext, FrameContext>
{
public:
	/// ctor
	ConditionCluster();

	/// group entities together based on a specific feature
	std::vector<std::vector<FeatureGroup*>> cluster();

	/// returns a reference to stats
	std::vector<std::pair<unsigned int, IFeatureStats*>> get_stats_ref() const;

	/// updates the thresh values for this cluster
	void update_cluster(unsigned int i) override;

	/// calculates the thresh
	void process_cluster() override;

	/// returns the current threshold for performing clustering
	double get_thresh() const;

private:
	/// creates multiple clusters based on features
	std::vector<std::vector<FeatureGroup*>> cluster_features(const std::pair<const unsigned int, IFeatureStats*>& fstat);

	/// makes a feature cluster
	std::vector<FeatureGroup*> make_feature_cluster(FeatureGroup* fg);


	/// the list of feature groups generated from the state_groups
	std::vector<std::unique_ptr<FeatureGroup>> feature_groups;

	/// the stats for each feature of an entity state
	std::vector<std::pair<unsigned int, IFeatureStats*>> stats;

	/// the min number of neighbours required to form a cluster link
	unsigned int min_neighbours;

	/// the threshold for a group to be considered a neighbour
	double neighbour_thresh;

	/// used to determine when there is sufficient mass of the cluster to form an
	/// affordance
	double thresh;
};

class ConditionGroup : public ClusterGenerator<FrameContext, FrameContext, ConditionCluster>
{
public:
	/// ctor
	ConditionGroup();

	void add_exemplar(const FrameContext& ex) override;

private:
	/// updates the stats
	void update_stats(const FrameContext& ex) override;

	/// obtains the delta from the affordance exemplar
	const FrameContext& get_data(const FrameContext& ex) const override;

	/// returns the distance between the input frame and the state group
	double exemplar_distance(const FrameContext& ex1, const FrameContext& ex2) const override;

	/// the stats for each delta
	std::vector<std::pair<unsigned int, IFeatureStats*>> stats;

	std::vector<FrameContext> contexts;
};

class ConditionClassifier
{
public:
	/// default ctor
	ConditionClassifier();

	/// copy ctor
	ConditionClassifier(ConditionClassifier& cc);

	/// Obtains an instance of this class
	/// \return The instance for this class
	static ConditionClassifier* Instance();

	/// Creates an instance
	static void Create();

	/// Generates condition(s) from the condition group if possible, returning the list of
	/// affordance conditions
	std::vector<AffordanceCondition*> compose_affordance_conditions(const ConditionGroup& cond);

	/// Generates a single affordance condition from the input cluster. Either a matching
	/// affordance condition is found or a new one is created.
	AffordanceCondition* compose_affordance_condition(ConditionCluster* cluster);

	/// Set the filtered features
	void set_filtered_features(const std::vector<tid_t>& filtered_features);

private:
	/// find a matching affordance condition
	std::pair<AffordanceCondition*, unsigned int> match_affordance_condition(std::vector<State*>& state_list) const;

	/// singleton instance
	static std::unique_ptr<ConditionClassifier> singleton;

	/// The list of known affordance conditions
	std::unordered_map<unsigned int, std::unique_ptr<AffordanceCondition>> conditions;

	/// the number of generated AffordanceConditions
	std::atomic_uint n_conditions;

	// The features to filter from condition generation
	std::vector<tid_t> filtered_features;
};

}
