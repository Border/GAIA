#include "feature-classifier.h"
#include "feature-association.h"
#include "transformers/transformer-com.h"
#include "transformers/transformer-covariance.h"
#include "transformers/transformer-magnitude.h"
#include "transformers/transformer-range.h"

#include <tiny_dnn/tiny_dnn.h>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <stdexcept>

using namespace GAIA;

std::unique_ptr<FeatureClassifier> FeatureClassifier::singleton = nullptr;


class ClassiferImpl : public IClassifier
{
public:
	ClassiferImpl(const std::string& filename)
	{
		nn.load(filename);
	}

	std::vector<float> predict(const std::vector<float>& data) override
	{
		tiny_dnn::vec_t tinydnn_data(data.begin(), data.end());
		auto res = nn.predict(tinydnn_data);
		return std::vector<float>(res.begin(), res.end());
	}

private:
	tiny_dnn::network<tiny_dnn::sequential> nn;
};



FeatureClassifier::FeatureClassifier():
	nn_int(std::make_unique<ClassiferImpl>("data/fc-model-int")),
	nn_double(std::make_unique<ClassiferImpl>("data/fc-model-double")),
	nn_vec(std::make_unique<ClassiferImpl>("data/fc-model-vec"))
{
}

FeatureClassifier* FeatureClassifier::Instance() { return singleton.get(); }

void FeatureClassifier::Create()
{
	if (singleton == nullptr)
	{
		singleton = std::make_unique<FeatureClassifier>();
	}
}

std::vector<std::unique_ptr<ITransformer>> FeatureClassifier::make_feature_transformers(const std::vector<FeatureDataContext>& values)
{
	std::vector<std::unique_ptr<ITransformer>> transformers;

	// obtain the feature data type
	const auto type = values.front().data.get_type();

	// generate basic stats about the data
	std::unique_ptr<IFeatureStats> fstats;
	switch (type)
	{
	case Gint:		fstats = std::make_unique<FeatureStats<int>>(); break;
	case Gdouble:	fstats = std::make_unique<FeatureStats<double>>(); break;
	case GVec:		fstats = std::make_unique<FeatureStats<GVec2>>(); break;
	default:		throw std::runtime_error("Unknown type: " + std::to_string(type)); break;
	}

	for (const auto& v : values)
	{
		fstats->update(v.data);
	}

	auto transformer_types = identify_feature_transformer_type(values, fstats.get());

	for (const auto& tf : transformer_types)
	{
		transformers.push_back(make_transformer(type, tf.second, values));
	}

	return transformers;
}

std::vector<std::pair<double, TransformerType>> FeatureClassifier::identify_feature_transformer_type(const std::vector<FeatureDataContext>& values, IFeatureStats* fstats)
{
	std::vector<std::pair<double, TransformerType>> transformer_types;

	std::vector<float> res;

	switch (fstats->get_type())
	{
	case Gint:
	{
		auto data = generate_feature_image(values, static_cast<FeatureStats<int>*>(fstats));
		res = nn_int->predict(data);
		break;
	}
	case Gdouble:
	{
		auto data = generate_feature_image(values, static_cast<FeatureStats<double>*>(fstats));
		res = nn_double->predict(data);
		break;
	}
	case GVec:
	{
		auto data = generate_feature_image(values, static_cast<FeatureStats<GVec2>*>(fstats));
		res = nn_vec->predict(data);
		break;
	}
	default:
	{
		return transformer_types;
	}
	}

	for (int i = 0; i < TransformerType::num_features; i++)
	{
		if (res[i] > 0.7)
		{
			transformer_types.push_back(std::make_pair(res[i], static_cast<TransformerType>(i)));
		}
	}

	return transformer_types;
}

std::unique_ptr<ITransformer> FeatureClassifier::make_transformer(GType gtype, TransformerType ttype, const std::vector<FeatureDataContext>& values)
{
	switch (gtype)
	{
	case Gint:
	{
		switch (ttype)
		{
		case com:			return std::make_unique<Transformer_com<int>>(values);
		case covariance:	return std::make_unique<Transformer_covariance<int>>(values);
		case magnitude:		return std::make_unique<Transformer_magnitude<int>>(values);
		case range:			return std::make_unique<Transformer_range<int>>(values);
		default:			throw std::runtime_error("Unknown transformation type: " + std::to_string(ttype));
		}
	}
	case Gdouble:
	{
		switch (ttype)
		{
		case com:			return std::make_unique<Transformer_com<double>>(values);
		case covariance:	return std::make_unique<Transformer_covariance<double>>(values);
		case magnitude:		return std::make_unique<Transformer_magnitude<double>>(values);
		case range:			return std::make_unique<Transformer_range<double>>(values);
		default:			throw std::runtime_error("Unknown transformation type: " + std::to_string(ttype));
		}
	}
	case GVec:
	{
		switch (ttype)
		{
		case com:			return std::make_unique<Transformer_com<GVec2>>(values);
		case covariance:	return std::make_unique<Transformer_covariance<GVec2>>(values);
		case magnitude:		return std::make_unique<Transformer_magnitude<GVec2>>(values);
		case range:			return std::make_unique<Transformer_range<GVec2>>(values);
		default:			throw std::runtime_error("Unknown transformation type: " + std::to_string(ttype));
		}
	}
	default:		throw std::runtime_error("Unknown type: " + std::to_string(gtype));
	}
}

std::vector<float> FeatureClassifier::generate_feature_image(const std::vector<FeatureDataContext>& values, const FeatureStats<int>* fstats) const
{
	std::vector<float> data;
	// fill up (32x1=32) with 0
	constexpr unsigned int size = 32;
	constexpr unsigned int hsize = size / 2;
	data.resize(size, 0);

	float max_v = -1.0f;

	// create an image from the values and convert it to a vec_t to use with the nn
	for (const auto& v : values)
	{
		auto i = (unsigned int)std::round(hsize + (hsize * (v.data.get_const_ref<int>() - fstats->mean)) / (fstats->stdev == 0 ? 1 : fstats->stdev));

		// if the sample is outside the bounds, just
		if (i < size)
		{
			data[i] += 1.0f;
			if (data[i] > max_v) { max_v = data[i]; }
		}
	}

	// to scale between -1 and 1 for tanh layers
	auto scale = 0.5f * max_v;

	for (unsigned int i = 0; i < size; ++i)
	{
		data[i] = (data[i] / scale) - 1;
	}

	return data;
}

std::vector<float> FeatureClassifier::generate_feature_image(const std::vector<FeatureDataContext>& values, const FeatureStats<double>* fstats) const
{
	std::vector<float> data;
	// fill up (32x1=32) with 0
	static const unsigned int size = 32;
	data.resize(size, 0);

	float max_v = -1.0f;

	// create an image from the values and convert it to a vec_t to use with the nn
	for (const auto& v : values)
	{
		auto i = (unsigned int)std::round(
			size * 0.5f * (1.0f + (v.data.get_const_ref<double>() - fstats->mean) /
				std::abs(fstats->stdev) < std::numeric_limits<double>::epsilon() ? 1.0f : fstats->stdev));

		// if the sample is outside the bounds, just
		if (i < size)
		{
			data[i] += 1.0f;
			if (data[i] > max_v) { max_v = data[i]; }
		}
	}

	// to scale between -1 and 1 for tanh layers
	auto scale = 0.5f * max_v;

	for (unsigned int i = 0; i < size; ++i)
	{
		data[i] = (data[i] / scale) - 1;
	}

	return data;
}

std::vector<float> FeatureClassifier::generate_feature_image(const std::vector<FeatureDataContext>& values, const FeatureStats<GVec2>* fstats) const
{
	std::vector<float> data;
	// fill up (32x32=1024) with 0
	static const unsigned int size = 1024;
	static const unsigned int w = 32;
	data.resize(size, 0);

	float max_v = -1.0f;

	// create an image from the values and convert it to a vec_t to use with the nn
	for (const auto& v : values)
	{
		auto pos = (v.data.get_const_ref<GVec2>() - fstats->mean);
		if (fstats->stdev.Length() > std::numeric_limits<double>::epsilon())
		{
			pos = pos / fstats->stdev;
		}

		const unsigned int x = (unsigned int)std::round(w * 0.5f * (1.0f + pos.x));
		const unsigned int y = (unsigned int)std::round(w * 0.5f * (1.0f + pos.y));

		// if the sample is outside the bounds, just
		if (x < w && y < w)
		{
			const unsigned int i = x + w * y;
			data[i] += 1.0f;
			if (data[i] > max_v) { max_v = data[i]; }
		}
	}

	// to scale between -1 and 1 for tanh layers
	auto scale = 0.5f * max_v;

	for (unsigned int i = 0; i < size; ++i)
	{
		data[i] = (data[i] / scale) - 1;
	}


	return data;
}



bool GAIA::match_feature(const IFeature* f, const FeatureContext& context, const Generic& data)
{
	auto error_data = f->generate_error(context, data);

	// only proceed further if the feature data was successfully obtained
	if (error_data != nullptr)
	{
		// check the error data is within the features threshold
		return error_data->get_result();
	}

	return false;
}
