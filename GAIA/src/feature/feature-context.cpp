#include "feature-context.h"
#include "entity.h"
#include "affordance-action.h"

using namespace GAIA;

FeatureReference::FeatureReference(tid_t type, FeatureReferenceType ref_type, ReferenceScope ref_scope, const Generic& feature_val, EntityInterface* ent)
	: feature_type_id(type)
	, reference_type(ref_type)
	, scope(ref_scope)
	, feature(feature_val)
	, entity(ent)
{

}

Generic FeatureReference::get_feature() const
{
	// If the feature reference is a target entity type, then
	// return the entity feature.
	if (reference_type == FeatureReferenceType::target_entity)
	{
        auto& state = entity->get_state();
		auto feature = state.find(feature_type_id);
		if (feature != std::end(state))
		{
			// TODO: Only features which are reference types can be used,
			// This restriction might be done elsewhere though.
			return feature->second;
		}
	}

	// otherwise just return the actual feature value.
	return feature;
}

tid_t FeatureReference::get_feature_type() const { return feature_type_id; }

bool FeatureReference::is_scope_valid(EntityInterface* ent) const
{
	if (scope == ReferenceScope::entity_type)
	{
		return ent->get_type_id() != scope_id;
	}
	else if (scope == ReferenceScope::entity_id)
	{
		return ent->get_persistent_id() != scope_id;
	}
	else if (scope == ReferenceScope::all)
	{
		return true;
	}

	return false;
}

FeatureReference::ReferenceScope FeatureReference::get_scope() const { return scope; }


FeatureContext::FeatureContext(Entity* ent)
	: entity(ent)
{
	// look at the actions in the entity from the initial frame
	// check the types of the features in the action data
	// check if they are external
	// if no target is supplied (entity or value), then use the agents frame as the reference

	for (auto action : ent->get_actions())
	{
		for (auto& data : action->get_data())
		{
			// check if the data type is internal/target/target val
			// if target add the feature matching feature type for the target entity as a reference
			// or if target val use the action target feature value as the reference.
			// TODO: choose the right scope
			if (data.ref_type == FeatureReferenceType::target_entity)
			{
                // TODO: Fix this to use the frame to get the entity
				// find the entity that matches using the supplied ID
				// auto entity = frame->get_entity(std::get<ActionData>(data).get_const_ref<int>());
				// if (entity != nullptr)
				// {
				// 	feature_references.push_back(FeatureReference(ftype, ref_type, FeatureReference::ReferenceScope::all, std::get<ActionData>(data), entity.get()));
				// }
			}
			else if (data.ref_type == FeatureReferenceType::target_value)
			{
				feature_references.push_back(FeatureReference(data.data_type, data.ref_type, FeatureReference::ReferenceScope::all, data.data));
			}
			else if (data.ref_type == FeatureReferenceType::relative)
			{
				auto& estate = entity->get_state();
				auto state_feature = estate.find(data.data_type);
				if (state_feature != std::end(estate))
				{
					feature_references.push_back(FeatureReference(data.data_type, data.ref_type, FeatureReference::ReferenceScope::all, state_feature->second));
				}
			}
		}
	}
}

FeatureContext::FeatureContext(Cause* cause, EntityInterface* _entity)
	: entity(_entity)
{
	assert(cause != nullptr);

	if (cause->get_causal_type() == CausalType::Causal_Action)
	{
		for (auto action : static_cast<AffordanceAction*>(cause)->get_actions())
		{
			for (auto feature : action->get_features(DataMapType::Position))
			{
				unsigned int ftype = feature->get_type_id();
				// TODO: this is just a temporary hack, the agent should use the set of rules (described in my notes under entry 29/04/2019) to determine the reference type
				if (ftype == DataMapType::Position)
				{
					auto& estate = entity->get_state();
					auto state_feature = estate.find(ftype);
					if (state_feature != std::end(estate))
					{
						feature_references.push_back(FeatureReference(ftype, FeatureReferenceType::relative, FeatureReference::ReferenceScope::all, state_feature->second));
					}
				}
			}
		}
	}
	else
	{
		// TODO: Only actions are supported atm
		assert("Unimplemented" && false);
	}
}

std::pair<FeatureReferenceType, Generic> FeatureContext::get_reference(tid_t feature_type, EntityInterface* ent) const
{
	FeatureReferenceType ref_type = FeatureReferenceType::internal;
	Generic reference_feature;
	FeatureReference::ReferenceScope reference_scope = FeatureReference::ReferenceScope::none;

	// TODO: this does not handle multiple scopes of the same level, it only consideres the first.

	for (const auto& feature : feature_references)
	{
		if (feature.get_feature_type() == feature_type)
		{
			if (feature.is_scope_valid(ent))
			{
				// If the scope is more specific, then override it
				auto scope = feature.get_scope();
				if (scope > reference_scope)
				{
					reference_feature = feature.get_feature();
					reference_scope = scope;
				}
			}
		}
	}

	// Note, if nothing matched, return a empty or 'none' type
	return std::make_pair(ref_type, reference_feature);
}
