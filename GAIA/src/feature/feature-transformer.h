#pragma once

#include "feature-data.h"
#include "generic.h"

namespace GAIA
{

/// The types of transformers possible
enum TransformerType
{
	com,			// ErrorNorm < thresh
	covariance,		// normalised dot product (parallel) > thresh
	magnitude,		// Magnitude error < thresh (don't care about direction)
	range,			// Range  =>  a < x < b, or x < a, x > b
	num_features	// this must go last
};

/// Transformer interface
class ITransformer
{
public:
	virtual ~ITransformer() = default;

	/// updates the feature based on the supplied error data
	virtual void update_feature(const IFeatureErrorData* error_data) = 0;

	/// Returns the type of transformer this is
	virtual TransformerType get_type() const = 0;

	/// Transforms the input data based on the derrived type
	virtual std::unique_ptr<IFeatureErrorData> generate_error(const Generic& data) = 0;

	/// Transforms the feature data to create output data
	virtual Generic generate_data() const = 0;

	/// Transforms the feature data to create output data
	virtual std::vector<Generic> generate_data_range(const IFeatureData* data) const = 0;

	/// Returns the error when matching another transformer object to this
	virtual double match(ITransformer* transformer) const = 0;

	/// Generates a CDF based on the input value
	virtual double generate_cdf(const Generic& val) const = 0;
};

}
