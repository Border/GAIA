#include "feature-stats.h"

#include <limits>
#include <algorithm>

using namespace GAIA;

template<>
FeatureStats<int>::FeatureStats() :
	stdev(0),
	S(0),
	sum(0),
	min(0),
	max(0),
	mean(0),
	count(0)
{
}

template<>
FeatureStats<double>::FeatureStats() :
	stdev(0),
	S(0),
	sum(0),
	min(0),
	max(0),
	mean(0),
	count(0)
{
}

template<>
FeatureStats<GVec2>::FeatureStats() :
	stdev(0, 0),
	S(0, 0),
	sum(0,0),
	min(0,0),
	max(0,0),
	mean(0,0),
	count(0)
{
}

template<typename T>
FeatureStats<T>::FeatureStats(const Generic& input) :
	sum(input.get_const_ref<T>()),
	min(input.get_const_ref<T>()),
	max(input.get_const_ref<T>()),
	mean(input.get_const_ref<T>()),
	S(input.generate_zero().get_const_ref<T>()),
	stdev(input.generate_zero().get_const_ref<T>()),
	count(1)
{
}

template <typename T>
void FeatureStats<T>::init(const T& input)
{
	sum = input;
	min = input;
	max = input;
	mean = input;
}

template<typename T>
void FeatureStats<T>::update(const Generic& input)
{
	assert(input.get_type() == get_type());
	update_specific(input.get_const_ref<T>());
}

template<> GType FeatureStats<int>::get_type() const { return Gint; }

template<> GType FeatureStats<double>::get_type() const { return Gdouble; }

template<> GType FeatureStats<GVec2>::get_type() const { return GVec; }

template<> bool FeatureStats<int>::is_stdev_zero() const
{
	return stdev == 0;
}

template<> bool FeatureStats<double>::is_stdev_zero() const
{
	return std::abs(stdev) < std::numeric_limits<double>::epsilon();
}

template<> bool FeatureStats<GVec2>::is_stdev_zero() const
{
	return stdev.Length() < std::numeric_limits<GVec2>::epsilon();
}

template<> void FeatureStats<GVec2>::update_specific(const GVec2& input)
{
	// init if first time
	if (count == 0)
	{
		init(input);
		count = 1;
		return;
	}

	sum += input;

	if (input.x < min.x) { min.x = input.x; }
	if (input.y < min.y) { min.y = input.y; }
	if (input.x > max.x) { max.x = input.x; }
	if (input.y > max.y) { max.y = input.y; }

	++count;

	auto new_mean = sum / count;

	S += (input - mean)*(input - new_mean);

	stdev = vSqrt(S / count);

	mean = new_mean;
}

template<typename T>
void FeatureStats<T>::update_specific(const T& input)
{
	// init if first time
	if (count == 0)
	{
		init(input);
		count = 1;
		return;
	}

	sum += input;

	if (input < min) { min = input; }
	if (input > max) { max = input; }

	++count;

	const auto new_mean = sum / count;

	S += (input - mean)*(input - new_mean);

	stdev = std::sqrt(S / count);

	mean = new_mean;
}



IFeatureStats* GAIA::make_stats_from_generic(const Generic& input)
{
	switch (input.get_type())
	{
	case Gint: return new FeatureStats<int>(input);
	case Gdouble: return new FeatureStats<double>(input);
	case GVec: return new FeatureStats<GVec2>(input);
	default: throw std::runtime_error("Unknown type");
	}
}

double GAIA::generic_error(const Generic& a, const Generic& b, const IFeatureStats* stats)
{
	switch (a.get_type())
	{
	case Gint: return a == b ? 0 : 1; break;
	case Gdouble: return error_rbf(a.get_const_ref<double>() - b.get_const_ref<double>(), static_cast<const FeatureStats<double>*>(stats)->stdev); break;
	case GVec: return error_rbf(a.get_const_ref<GVec2>() - b.get_const_ref<GVec2>(), static_cast<const FeatureStats<GVec2>*>(stats)->stdev); break;
	}

	throw std::runtime_error("Unknown type!");
}

double GAIA::error_rbf(double error, double stdev, double tolerance)
{
	// Scale the error and normalise it between 0 and 1 (0 being a perfect match
	// and 1 being a very large mismatch)

	if (stdev < feature_error_thresh)
	{
		return std::abs(error) < feature_error_thresh ? 0.0 : 1.0;
	}
	else
	{
		return 2 - 2 / (1 + std::exp(-std::exp(-tolerance * 2 * M_PI * (std::abs(error) - stdev) / stdev)));
	}
}

double GAIA::error_rbf(const GVec2& error, const GVec2& stdev, double tolerance)
{
	if (stdev.Length() < feature_error_thresh)
	{
		return error.Length() < feature_error_thresh ? 0.0 : 1.0;
	}
	else
	{
		const GVec2 err_norm = (vAbs(error) - stdev) / stdev;

		return 2 - 2 / (1 + std::exp(-std::exp(-tolerance * 2 * M_PI * std::max(err_norm.x, err_norm.y))));
	}
}

// specific instantiations
template class FeatureStats<int>;
template class FeatureStats<double>;
template class FeatureStats<GVec2>;
