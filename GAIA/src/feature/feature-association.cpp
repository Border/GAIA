#include "feature-association.h"
#include "feature.h"
#include "mhash.h"

using namespace GAIA;

std::pair<size_t, size_t> FeatureHasher::generate() const
{
	return std::make_pair(
		generate_hash(type_ids.data(), type_ids.size()),
		generate_hash(unique_ids.data(), unique_ids.size()));
}

std::pair<size_t, size_t> GAIA::generate_multi_hashes(const std::vector<std::pair<unsigned int, FVector>>& features)
{
	std::vector<unsigned int> uids;

	for (const auto& f_group : features)
	{
		for (auto f : f_group.second)
		{
			uids.push_back(f->get_unique_id());
		}
	}

	return std::make_pair(
		generate_hash(std::begin(features), std::end(features), [](const std::pair<unsigned int, FVector>& f) { return f.first; }),
		generate_hash(uids.data(), uids.size()));
}

/// recursive function for generating permutations (stores them in perms)
template<typename Iter, typename PermList>
void recurse_hash_perm(const Iter& iter, const Iter& end, size_t size, PermList temp, std::vector<PermList>& perms)
{
	if (size > 0)
	{
		for (auto it = std::next(iter); it != end; ++it)
		{
			temp.push_back(*it);
			recurse_hash_perm(it, end, size - 1, temp, perms);
			temp.pop_back();
		}
	}
	else
	{
		perms.push_back(std::move(temp));
	}
}

std::unordered_multimap<size_t, std::pair<size_t, FVector>> GAIA::gen_hash_perm(const FIter& first, const FIter& last, size_t size)
{
	std::unordered_multimap<size_t, std::pair<size_t, FVector>> hash_list;
	for (auto it = first; it != last && (size_t)std::distance(first, last) >= size; ++it)
	{
		std::vector<FVector> perms;
		// generate a single permutation of features
		recurse_hash_perm(it, last, size - 1, { *it }, perms);

		for (auto& perm : perms)
		{
			// generate the hashes from type id and unique id
			auto type_hash = generate_hash(std::begin(perm), std::end(perm), [](IFeature* f) { return f->get_type_id(); });
			auto unique_hash = generate_hash(std::begin(perm), std::end(perm), [](IFeature* f) { return f->get_unique_id(); });

			// then add to the hash_list
			hash_list.emplace(type_hash, std::make_pair(unique_hash, std::move(perm)));
		}
	}

	return hash_list;
}

std::vector<std::pair<uint32_t, std::vector<std::pair<unsigned int, Generic>>>> GAIA::gen_type_hash_perm(const DataMap::iterator& first, const DataMap::iterator& last)
{
	std::vector<std::pair<uint32_t, std::vector<std::pair<unsigned int, Generic>>>> hash_list;
	size_t size = std::distance(first, last);
	for (auto it = first; it != last; ++it)
	{
		std::vector<std::vector<std::pair<unsigned int, Generic>>> perms;
		// generate a single permutation of features
		recurse_hash_perm(it, last, size - 1, { *it }, perms);

		for (auto& perm : perms)
		{
			// generate the hashes from type id and unique id
			auto type_hash = generate_hash(std::begin(perm), std::end(perm), [](std::pair<unsigned int, Generic>& d) { return d.first; });

			// then add to the hash_list
			hash_list.push_back(std::make_pair(type_hash, std::move(perm)));
		}
	}

	return hash_list;
}
