#pragma once

#include "entity-interface.h"
#include "generic.h"
#include "feature-types.h"
#include "types.h"

#include <vector>

namespace GAIA
{

// references are for all features of the same type. I.e, position features only affect other
// position features, they cant be used for health - they aren't compatible.
class FeatureReference
{
public:
	enum ReferenceScope
	{
		none = -1,
		all = 0,
		entity_type,
		entity_id,
	};

	FeatureReference() = delete;
	FeatureReference(tid_t type, FeatureReferenceType ref_type, ReferenceScope ref_scope, const Generic& feature_val, EntityInterface* ent = nullptr);

	// This returns the reference feature based on the FeatureReferenceType
	Generic get_feature() const;
	tid_t get_feature_type() const;
	bool is_scope_valid(EntityInterface* ent) const;
	ReferenceScope get_scope() const;

private:
	// Feature type
	tid_t feature_type_id;

	FeatureReferenceType reference_type;
	// target entity
	// target feature value (i.e. location)
	// internal - this is needed to override other references

	EntityInterface* entity; // TODO: is this required?
	Generic feature;

	// indicate if its for for all entities
	// or if its for entities of the same type
	// or if its for a specific entity (which uses a state to identify)
	ReferenceScope scope;

	// The scope_id is entity type id if the scope is the entity type, or the entity PID if the scope is a specific entity
	unsigned int scope_id;

	// The action type hash - used to constrain the reference feature to the relative action
	// unsigned int scope_type_hash;
};

class FeatureContext
{
public:
	FeatureContext() = default;
    FeatureContext(Entity* entity);
	FeatureContext(Cause* cause, EntityInterface* entity);

	std::pair<FeatureReferenceType, Generic> get_reference(tid_t feature_type, EntityInterface* ent) const;

	EntityInterface* entity;

	std::vector<FeatureReference> feature_references;
};

}
