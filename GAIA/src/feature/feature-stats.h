#pragma once

#include "generic.h"

namespace GAIA
{

class IFeatureStats
{
public:
	virtual void update(const Generic& input) = 0;
	virtual bool is_stdev_zero() const = 0;
	virtual GType get_type() const = 0;
};

template <typename T>
class FeatureStats : public IFeatureStats
{
public:
	/// The constructor
	FeatureStats();

	/// ctor for generic
	explicit FeatureStats(const Generic& input);

	/// Updates the stats based on the supplied input. The generic will throw if its the
	/// wrong type
	virtual void update(const Generic& input);

	/// returns true if the stdev is less than epsilon
	virtual bool is_stdev_zero() const;

	/// returns the type of the stats object
	virtual GType get_type() const;


	/// Initialises the stats
	void init(const T& input);

	/// Updates the stats based on the supplied input
	void update_specific(const T& input);

	/// The min value this feature is likely to have
	T min;
	/// The max value this feature is likely to have
	T max;
	/// Mean
	T mean;
	/// standard deviation
	T stdev;
	/// The sum of the error terms
	T sum;
	/// stdev temporary
	T S;
	/// number of times the feature has been updated
	unsigned int count;
};

const static double feature_error_thresh = 1e-10;

/// creates a stats fromthe same type as a generic and returns a feature stats interface
IFeatureStats* make_stats_from_generic(const Generic& input);

/// calculates the error using the ErrorNorm
double generic_error(const Generic& a, const Generic& b, const IFeatureStats* stats);

/// Returns a normalised error based on the stdev using a radial basis function
double error_rbf(double error, double stdev, double tolerance = 1.0);

/// Returns a normalised error based on the stdev using a radial basis function
double error_rbf(const GVec2& error, const GVec2& stdev, double tolerance = 1.0);

}
