#include "feature-data.h"
#include "container-algorithms.h"

using namespace GAIA;

// specific instantiations
template class FeatureData<int>;
template class FeatureData<double>;
template class FeatureData<GVec2>;

template class FeatureErrorData<int>;
template class FeatureErrorData<double>;
template class FeatureErrorData<GVec2>;



IFeatureData::~IFeatureData() { }



template <typename T>
FeatureData<T>::FeatureData(const Generic& in) : input(in.get_const_ref<T>()) { }

template<> GType FeatureData<int>::get_type() const { return Gint; }
template<> GType FeatureData<double>::get_type() const { return Gdouble; }
template<> GType FeatureData<GVec2>::get_type() const { return GVec; }



IFeatureErrorData::~IFeatureErrorData() { }



template<typename T>
FeatureErrorData<T>::~FeatureErrorData() { }

template <typename T>
double FeatureErrorData<T>::get_error_rbf() const { return error_norm; }

template<typename T>
Generic FeatureErrorData<T>::get_error() const { return Generic(error); }

template<typename T>
bool FeatureErrorData<T>::get_result() const { return result; }
