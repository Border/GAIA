#include "feature-container.h"

#include "entity.h"

#include <algorithm>

using namespace GAIA;

// specific instantiations
template class FeatureContainer<int>;
template class FeatureContainer<double>;
template class FeatureContainer<GVec2>;



template<>
FeatureContainer<int>::FeatureContainer(unsigned int t_id, size_t uni_id, std::unique_ptr<ITransformer>& tform) :
	type_id(t_id),
	unique_id(uni_id),
	transformer(std::move(tform))
{
}

template<>
FeatureContainer<double>::FeatureContainer(unsigned int t_id, size_t uni_id, std::unique_ptr<ITransformer>& tform) :
	type_id(t_id),
	unique_id(uni_id),
	transformer(std::move(tform))
{
}

template<>
FeatureContainer<GVec2>::FeatureContainer(unsigned int t_id, size_t uni_id, std::unique_ptr<ITransformer>& tform) :
	type_id(t_id),
	unique_id(uni_id),
	transformer(std::move(tform))
{
}

template <typename T>
size_t FeatureContainer<T>::get_unique_id() const { return unique_id; }

template <typename T>
unsigned int FeatureContainer<T>::get_type_id() const { return type_id; }

template<typename T>
std::unique_ptr<IFeatureErrorData> FeatureContainer<T>::generate_error(const FeatureContext& context, Generic data) const
{
	return transformer->generate_error(data);
}

template <typename T>
void FeatureContainer<T>::update_feature(const IFeatureErrorData* error)
{
	transformer->update_feature(error);
}

template<typename T>
void FeatureContainer<T>::update_feature(const FeatureContext& context, Generic data)
{
	transformer->update_feature(generate_error(context, data).get());
}

template<typename T>
Generic FeatureContainer<T>::get_feature() const
{
	return transformer->generate_data();
}

template<typename T>
std::vector<Generic> FeatureContainer<T>::get_feature_range(const FeatureContext& context, const IFeatureData* data) const
{
	return transformer->generate_data_range(data);
}

template<typename T>
double FeatureContainer<T>::match(ITransformer* transformer) const { return this->transformer->match(transformer); }

template<typename T>
double FeatureContainer<T>::generate_cdf(const Generic& val) const
{
	return transformer->generate_cdf(val);
}

template<> GType FeatureContainer<int>::get_data_type() const { return Gint; }
template<> GType FeatureContainer<double>::get_data_type() const { return Gdouble; }
template<> GType FeatureContainer<GVec2>::get_data_type() const { return GVec; }
