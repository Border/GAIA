#pragma once

#include "feature.h"
#include "feature-transformer.h"
#include "generic.h"

namespace GAIA
{

template <typename T>
class FeatureContainer : public IFeature
{
	friend class IO;
	friend class DebugDisplay;
public:
	/// Constructor
	FeatureContainer(unsigned int t_id, size_t uni_id, std::unique_ptr<ITransformer>& tform);
	/// Destrctor
	virtual ~FeatureContainer() = default;

	/// returns the unqiue id
	size_t get_unique_id() const override;
	///  returns the id indicating the type of feature
	unsigned int get_type_id() const override;
	/// returns the features type
	GType get_data_type() const override;

	/// Calculates the error between the feature and input
	std::unique_ptr<IFeatureErrorData> generate_error(const FeatureContext& context, Generic data) const override;
	/// If the type is appropriate the feature is updated based on recent data
	void update_feature(const IFeatureErrorData* error) override;
	/// Updates the feature based on the input exemplar
	void update_feature(const FeatureContext& context, Generic data) override;
	/// Returns the feature based on the average feature values (currently does not factor the current world state)
	Generic get_feature() const override;
	/// Returns the feature based on the current world state encoded in IFeatureData
	std::vector<Generic> get_feature_range(const FeatureContext& context, const IFeatureData* data) const override;
	/// Returns the error of a tranformer match for this feature
	double match(ITransformer* transformer) const override;
	/// Generates a CDF based on the input value
	double generate_cdf(const Generic& val) const override;

protected:
	/// Indicates the type of data being stored
	unsigned int type_id;
	/// a unique id for generating hashes of groups of features
	size_t unique_id;

	/// Contains the specific transformer functionality
	std::unique_ptr<ITransformer> transformer;
};

}
