#pragma once

#include "feature-types.h"
#include "datamap.h"
#include "feature.h"
#include "generic.h"

#include <functional>
#include <utility>
#include <vector>
#include <unordered_map>

namespace GAIA
{

class FeatureHasher
{
public:
    // Adds the featues to the list to generate a hash
    template<typename Iter>
    void update(Iter begin, const Iter& end, std::function<std::pair<tid_t, std::vector<uid_t>>(const Iter&)> get_ids)
	{
		for (; begin != end; begin++)
		{
			auto uids = get_ids(begin);
			type_ids.push_back(uids.first);
			for (auto& uid : uids.second)
			{
				unique_ids.push_back(uid);
			}
		}
	}

    // Uses the current list to generate a hash.
    std::pair<size_t, size_t> generate() const;

private:
    // The unique IDs of the features
    std::vector<unsigned int> unique_ids;
    // The type IDs of th features
    std::vector<unsigned int> type_ids;
};

/// Returns the hashes generated from the type id and the unique id of a series of state features
std::pair<size_t, size_t> generate_multi_hashes(const std::vector<std::pair<unsigned int, FVector>>& features);

/// Generates all permutations of size and orders them based on their type hash and then unique hash
/// \param first The start iterator of the list to form permutations from
/// \param last The end iterator of the list to form permutations from
/// \param size The desired length of the permuations
/// \return A map of the different permutations of length size
std::unordered_multimap<size_t, std::pair<size_t, FVector>> gen_hash_perm(const FIter& first, const FIter& last, size_t size);


/// Returns a list of permutations of the iterator range passed in and the type hash generated for the permutation.
std::vector<std::pair<uint32_t, std::vector<std::pair<unsigned int, Generic>>>> gen_type_hash_perm(const DataMap::iterator& first, const DataMap::iterator& last);

const auto action_hasher = [](const ActionFeatures::iterator& action_feature)
{
	std::vector<uid_t> ids;
	for (auto& feature : action_feature->features)
	{
		ids.push_back(feature->get_unique_id());
	}

	return std::make_pair(action_feature->feature_type, std::move(ids));
};

}
