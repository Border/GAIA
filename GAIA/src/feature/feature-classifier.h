#pragma once

#include "feature.h"
#include "feature-transformer.h"
#include "feature-data.h"
#include "feature-stats.h"
#include "entity/entity-context.h"
#include "frame-context.h"
#include "feature-types.h"
#include "feature-context.h"
#include "generic.h"
#include "types.h"

#include <vector>
#include <deque>
#include <memory>
#include <unordered_map>

namespace GAIA
{

/// abstracts the classifier used. This is done as tiny-dnn is a header implementation
/// which needs to be run each time changes are made. The implementation is put in the cpp
/// file to prevent this.
class IClassifier
{
public:
	virtual std::vector<float> predict(const std::vector<float>& data) = 0;
};

/// Contains recognised features. Can aquire new and identify known features
class FeatureClassifier
{
	friend class IO;
	friend class DebugDisplay;

public:
	/// The constructor
	FeatureClassifier();

	/// Obtains an instance of this class
	/// \return The instance for this class
	static FeatureClassifier* Instance();

	/// Creates an instance
	static void Create();

	/// Creates transformers based on the supplied
	std::vector<std::unique_ptr<ITransformer>> make_feature_transformers(const std::vector<FeatureDataContext>& values);

private:
	/// Identifies 1 or more transformers for features, returning the type
	std::vector<std::pair<double, TransformerType>> identify_feature_transformer_type(const std::vector<FeatureDataContext>& values, IFeatureStats* fstats);

	/// Create a feature transformer
	std::unique_ptr<ITransformer> make_transformer(GType gtype, TransformerType ttype, const std::vector<FeatureDataContext>& values);

	/// generates a 2D image based onthe feature values and scaled based on fstats
	std::vector<float> generate_feature_image(const std::vector<FeatureDataContext>& values, const FeatureStats<int>* fstats) const;

	/// generates a 2D image based onthe feature values and scaled based on fstats
	std::vector<float> generate_feature_image(const std::vector<FeatureDataContext>& values, const FeatureStats<double>* fstats) const;

	/// generates a 2D image based onthe feature values and scaled based on fstats
	std::vector<float> generate_feature_image(const std::vector<FeatureDataContext>& values, const FeatureStats<GVec2>* fstats) const;


	/// the neural network for 1D/int features
	std::unique_ptr<IClassifier> nn_int;

	/// the neural network for 1D/double features
	std::unique_ptr<IClassifier> nn_double;

	/// the neural network for 2D/vec features
	std::unique_ptr<IClassifier> nn_vec;

	/// A singleton
	static std::unique_ptr<FeatureClassifier> singleton;
};

/// returns true if the input matches the feature, based on the FrameContext
bool match_feature(const IFeature* f, const FeatureContext& context, const Generic& data);

}
