#pragma once

#include "frame-context.h"
#include "feature-types.h"
#include "feature-data.h"
#include "generic.h"

namespace GAIA
{

class ITransformer;

/// Interface feature class used to access either ConstantFeature
/// or TemporalFeature objects
class IFeature
{
public:
	virtual ~IFeature() = default;

	/// The data type of the feature
	/// \sa GType
	/// \return the data type
	virtual GType get_data_type() const = 0;

	/// The unqiue id of the feature
	/// \return the unique id
	virtual size_t get_unique_id() const = 0;

	/// The id indicating the type of feature
	/// \return the feature id
	virtual unsigned int get_type_id() const = 0;

	/// Calculates the normalised error between the feature and input
	/// \param context the FrameContext used by the feature.
	/// \param data the input feature data from the world frame
	/// \return a data structure containing temporary working data from the comparison
	///			process as well as the resulting error
	virtual std::unique_ptr<IFeatureErrorData> generate_error(const FeatureContext& context, Generic data) const = 0;

	/// Adds the input to the history, updating the feature
	/// \param error an interface for a data structure which contains the error
	///			and relevant comparison information
	virtual void update_feature(const IFeatureErrorData* error) = 0;

	/// Updates the feature based on the input data
	/// \param input_data the generated input feature data
	virtual void update_feature(const FeatureContext& context, Generic data) = 0;

	/// Returns the feature based on the average feature value
	/// \param context the FrameContext used by the feature.
	/// \param data the input feature data from the world frame
	virtual Generic get_feature() const = 0;

	/// Returns a range of possible features based on the current world state encoded in IFeatureData
	/// \param data The feature data generated from the world frame
	/// \return The feature generated from this feature and the supplied feature data
	virtual std::vector<Generic> get_feature_range(const FeatureContext& context, const IFeatureData* data) const = 0;

	/// Returns the error of a tranformer match for this feature
	/// \param transformer The transformer to match against
	/// \return The error scaled to 0-1
	virtual double match(ITransformer* transformer) const = 0;

	/// Generates a CDF based on the input value
	virtual double generate_cdf(const Generic& val) const = 0;
};

}
