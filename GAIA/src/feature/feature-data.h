#pragma once

#include "generic.h"

#include <vector>

namespace GAIA
{

class IFeatureData
{
public:
	/// dtor
	virtual ~IFeatureData() = 0;
	/// Returns the feature type
	virtual GType get_type() const = 0;
};

template<typename T>
class FeatureData : public IFeatureData
{
public:
	/// ctor
	explicit FeatureData(const Generic& in);
	/// Returns the feature type
	virtual GType get_type() const;

	/// The input data
	T input;
};



/// Interface for accessing feature error data
class IFeatureErrorData
{
public:
	/// Destructor
	virtual ~IFeatureErrorData() = 0;
	/// Returns normalised error
	virtual double get_error_rbf() const = 0;
	/// Returns absolute error
	virtual Generic get_error() const = 0;
	/// Returns a boolean indicating the error result
	virtual bool get_result() const = 0;
};

/// Feature error data
/// Contains error data for feature comparisons
template <typename T>
class FeatureErrorData : public IFeatureErrorData
{
public:
	virtual ~FeatureErrorData();
	/// Returns the normalised error
	/// \return normalised error
	virtual double get_error_rbf() const;
	/// Returns absolute error
	virtual Generic get_error() const;
	/// Returns a boolean indicating the error result
	virtual bool get_result() const;

	/// The generated and transformed input data
	T input;
	/// The error
	T error;
	/// The normalised error
	double error_norm;
	/// indicates if the error is within a threshold
	bool result;
};

}
