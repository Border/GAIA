#pragma once

#include "generic.h"
#include "types.h"

#include <vector>
#include <unordered_map>

namespace GAIA
{

class IFeature;
class Entity;
struct FrameContext;

using FVector = std::vector<IFeature*>;
using FIter = std::vector<IFeature*>::iterator;

using FUMap = std::unordered_map<unsigned int, IFeature*>;
using FUMMap = std::unordered_multimap<unsigned int, IFeature*>;

enum FeatureReferenceType
{
	internal,
	relative,
	target_entity,
	target_value,
};

struct FeatureDataContext
{
    const FrameContext& context;
    FeatureReferenceType ref_type;
	Generic data;
	tid_t type;
    Entity* entity;
};

}
