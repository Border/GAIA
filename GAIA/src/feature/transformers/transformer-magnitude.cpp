#include "transformers/transformer-magnitude.h"

#include <limits>

using namespace GAIA;


template<>
Transformer_magnitude<int>::Transformer_magnitude(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update_specific(abs<int>(v.data));
		feature_normal.update_specific(normalise<int>(v.data));
	}
}

template<>
Transformer_magnitude<double>::Transformer_magnitude(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update_specific(abs<double>(v.data));
		feature_normal.update_specific(normalise<double>(v.data));
	}
}

template<>
Transformer_magnitude<GVec2>::Transformer_magnitude(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update_specific(abs<GVec2>(v.data));
		feature_normal.update_specific(normalise<GVec2>(v.data));
	}
}

template<typename T>
TransformerType Transformer_magnitude<T>::get_type() const { return TransformerType::magnitude; }


template<typename T>
void Transformer_magnitude<T>::update_feature(const IFeatureErrorData* error_data)
{
	auto e = static_cast<const FeatureErrorData<T>*>(error_data);

	feature.update(e->input);
}


template<> std::unique_ptr<IFeatureErrorData> Transformer_magnitude<int>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<int>>();
	e->input = data.get_const_ref<int>();
	e->error = std::abs(std::abs(e->input) - std::abs(feature.mean));

	e->result = e->error == 0;
	e->error_norm = e->result ? 0.0 : 1.0;

	return e;
}

template<> std::unique_ptr<IFeatureErrorData> Transformer_magnitude<double>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<double>>();
	e->input = data.get_const_ref<double>();
	e->error = std::abs(std::abs(e->input) - std::abs(feature.mean));

	// Calculate the normalised error between the input and the feature value
	e->error_norm = error_rbf(e->error, feature.stdev);
	e->result = e->error_norm < 0.1;

	return e;
}

template<> std::unique_ptr<IFeatureErrorData> Transformer_magnitude<GVec2>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<GVec2>>();
	e->input = data.get_const_ref<GVec2>();
	e->error = vAbs(e->input) - vAbs(feature.mean);

	// Calculate the normalised error between the input and the feature value
	e->error_norm = error_rbf(e->error, feature.stdev);
	e->result = e->error_norm < 0.1;

	return e;
}


template<typename T>
Generic Transformer_magnitude<T>::generate_data() const
{
	// there is a case where if the feature_norm.mean is 0, there are 2 equally correct
	// solutions. For the moment we are ignoring it, but it will likely become an issue
	// later on. The same is true for the data ranges below.
	return Generic(feature.mean * feature_normal.mean);
}

template<> std::vector<Generic> Transformer_magnitude<int>::generate_data_range(const IFeatureData* data) const
{
	std::vector<Generic> feature_data;

	if (feature_normal.stdev == 0)
	{
		feature_data.push_back(feature.mean * feature_normal.mean);
	}
	else
	{
		for (size_t i = 0; i < 5; ++i)
		{
			const int fa = feature_normal.mean * feature.mean + feature.stdev * i;
			const int fb = feature_normal.mean * feature.mean - feature.stdev * i;

			if (fa < feature_normal.max * feature.mean) { feature_data.push_back(fa); }
			if (fb > feature_normal.min * feature.mean) { feature_data.push_back(fb); }
		}
	}

	return feature_data;
}

template<> std::vector<Generic> Transformer_magnitude<double>::generate_data_range(const IFeatureData* data) const
{
	std::vector<Generic> feature_data;

	if (std::abs(feature.stdev) < std::numeric_limits<double>::epsilon())
	{
		feature_data.push_back(feature.mean * feature_normal.mean);
	}
	else
	{
		for (size_t i = 0; i < 5; ++i)
		{
			const double fa = feature_normal.mean * feature.mean + feature.stdev * i;
			const double fb = feature_normal.mean * feature.mean - feature.stdev * i;

			if (fa < feature_normal.max * feature.mean) { feature_data.push_back(fa); }
			if (fb > feature_normal.min * feature.mean) { feature_data.push_back(fb); }
		}
	}

	return feature_data;
}

template<> std::vector<Generic> Transformer_magnitude<GVec2>::generate_data_range(const IFeatureData* data) const
{
	std::vector<Generic> feature_data;

	if (feature.stdev.Length() < std::numeric_limits<double>::epsilon())
	{
		// technically this should never happen as the feature should be classified as a
		// com feature
		feature_data.push_back(feature.mean * feature_normal.mean);
	}
	else
	{
		for (size_t i = 0; i < 5; ++i)
		{
			const GVec2 norm = feature_normal.mean.GetNormalised();
			const GVec2 x = (double)i * feature.stdev;
			const GVec2 fa = norm * (feature.mean + x);
			const GVec2 fb = norm * (feature.mean - x);

			const auto max = norm * feature.max;
			const auto min = norm * feature.min;

			if (fa.x < max.x && fa.x > max.x &&
				fa.y < max.y && fa.y > max.y)
			{
				feature_data.push_back(fa);
			}
			if (fb.x < max.x && fb.x > max.x &&
				fb.y < max.y && fb.y > max.y)
			{
				feature_data.push_back(fb);
			}
		}
	}

	return feature_data;
}

template<>
double Transformer_magnitude<int>::generate_cdf(const Generic& val) const
{
	if (feature.stdev == 0)
	{
		return 1.0;
	}
	else
	{
		return 1.0 - std::erf((double)(val.get_const_ref<int>() - feature.mean) / (std::sqrt(2.0) * (double)feature.stdev));
	}
}

template<>
double Transformer_magnitude<double>::generate_cdf(const Generic& val) const
{
	if (feature.stdev <= std::numeric_limits<double>::epsilon())
	{
		return 1.0;
	}
	else
	{
		return 1.0 - std::erf((val.get_const_ref<double>() - feature.mean) / (std::sqrt(2.0) * feature.stdev));
	}
}

template<>
double Transformer_magnitude<GVec2>::generate_cdf(const Generic& val) const
{
	GVec2 x = (val.get_const_ref<GVec2>() - feature.mean) / (std::sqrt(2.0) * feature.stdev);
	GVec2 err(1.0, 1.0);
	if (x.x != std::numeric_limits<double>::infinity())
	{
		err.x -= std::erf(x.x);
	}
	if (x.y != std::numeric_limits<double>::infinity())
	{
		err.y -= std::erf(x.y);
	}

	return std::sqrt(err.x * err.y);
}


template<> double Transformer_magnitude<int>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() &&
		feature.mean != static_cast<Transformer_magnitude<int>*>(transformer)->feature.mean	? 0.0 : 1.0;
}

template<typename T>
double Transformer_magnitude<T>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() ?
		error_rbf(feature.mean - static_cast<Transformer_magnitude<T>*>(transformer)->feature.mean, feature.stdev) : 1.0;
}

namespace GAIA
{

// specific implimentations
template class Transformer_magnitude<int>;
template class Transformer_magnitude<double>;
template class Transformer_magnitude<GVec2>;

}