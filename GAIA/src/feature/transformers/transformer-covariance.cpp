#include "transformers/transformer-covariance.h"

#include <limits>

using namespace GAIA;


template<>
Transformer_covariance<int>::Transformer_covariance(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update_specific(normalise<int>(v.data));
		feature_magnitude.update_specific(abs<int>(v.data));
	}
}

template<>
Transformer_covariance<double>::Transformer_covariance(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update_specific(normalise<double>(v.data));
		feature_magnitude.update_specific(abs<double>(v.data));
	}
}

template<>
Transformer_covariance<GVec2>::Transformer_covariance(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update_specific(normalise<GVec2>(v.data));
		feature_magnitude.update_specific(abs<GVec2>(v.data));
	}
}

template<typename T>
TransformerType Transformer_covariance<T>::get_type() const { return TransformerType::covariance; }


template<typename T>
void Transformer_covariance<T>::update_feature(const IFeatureErrorData* error_data)
{
	auto e = static_cast<const FeatureErrorData<T>*>(error_data);

	feature.update(e->input);
}


template<> std::unique_ptr<IFeatureErrorData> Transformer_covariance<int>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<int>>();
	e->input = data.get_const_ref<int>();
	e->error = e->input / std::abs(e->input) - feature.mean;

	e->result = e->error == 0;
	e->error_norm = e->result ? 0.0 : 1.0;

	return e;
}

template<> std::unique_ptr<IFeatureErrorData> Transformer_covariance<double>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<double>>();
	e->input = data.get_const_ref<double>();
	e->error = e->input / std::abs(e->input) - feature.mean / std::abs(feature.mean);

	// Calculate the normalised error between the input and the feature value
	e->result = std::abs(e->error) < std::numeric_limits<double>::epsilon();
	e->error_norm = e->result ? 0.0 : 1.0;

	return e;
}

template<> std::unique_ptr<IFeatureErrorData> Transformer_covariance<GVec2>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<GVec2>>();
	e->input = data.get_const_ref<GVec2>();
	e->error = e->input.GetNormalised() - feature.mean.GetNormalised();

	// Calculate the normalised error between the input and the feature value
	e->error_norm = 1.0 - (vPara(e->input, feature.mean) + 1.0) / 2.0;
	e->result = e->error_norm < 0.1;

	return e;
}


template<typename T>
Generic Transformer_covariance<T>::generate_data() const
{
	return Generic(feature_magnitude.mean * feature.mean);
}

template<> std::vector<Generic> Transformer_covariance<int>::generate_data_range(const IFeatureData* data) const
{
	std::vector<Generic> feature_data;

	if (feature_magnitude.stdev == 0)
	{
		feature_data.push_back(feature_magnitude.mean * feature.mean);
	}
	else
	{
		for (size_t i = 0; i < 5; ++i)
		{
			const int fa = feature_magnitude.mean * feature.mean + feature_magnitude.stdev * i;
			const int fb = feature_magnitude.mean * feature.mean - feature_magnitude.stdev * i;

			if (fa < feature_magnitude.max * feature.mean) { feature_data.push_back(fa); }
			if (fb > feature_magnitude.min * feature.mean) { feature_data.push_back(fb); }
		}
	}

	return feature_data;
}

template<> std::vector<Generic> Transformer_covariance<double>::generate_data_range(const IFeatureData* data) const
{
	std::vector<Generic> feature_data;

	if (std::abs(feature_magnitude.stdev) < std::numeric_limits<double>::epsilon())
	{
		feature_data.push_back(feature_magnitude.mean * feature.mean);
	}
	else
	{
		for (size_t i = 0; i < 5; ++i)
		{
			const double fa = feature_magnitude.mean * feature.mean + feature_magnitude.stdev * i;
			const double fb = feature_magnitude.mean * feature.mean - feature_magnitude.stdev * i;

			if (fa < feature_magnitude.max * feature.mean) { feature_data.push_back(fa); }
			if (fb > feature_magnitude.min * feature.mean) { feature_data.push_back(fb); }
		}
	}

	return feature_data;
}

template<> std::vector<Generic> Transformer_covariance<GVec2>::generate_data_range(const IFeatureData* data) const
{
	std::vector<Generic> feature_data;

	if (feature_magnitude.stdev.Length() < std::numeric_limits<double>::epsilon())
	{
		// technically this should never happen as the feature should be classified as a
		// com feature
		feature_data.push_back(feature_magnitude.mean * feature.mean);
	}
	else
	{
		for (size_t i = 0; i < 5; ++i)
		{
			const GVec2 norm = feature.mean.GetNormalised();
			const GVec2 x = (double)i * feature_magnitude.stdev;
			const GVec2 fa = norm * (feature_magnitude.mean + x);
			const GVec2 fb = norm * (feature_magnitude.mean - x);

			const auto max = norm * feature_magnitude.max;
			const auto min = norm * feature_magnitude.min;

			if (fa.x < max.x && fa.x > max.x &&
				fa.y < max.y && fa.y > max.y)
			{
				feature_data.push_back(fa);
			}
			if (fb.x < max.x && fb.x > max.x &&
				fb.y < max.y && fb.y > max.y)
			{
				feature_data.push_back(fb);
			}
		}
	}

	return feature_data;
}

template<>
double Transformer_covariance<int>::generate_cdf(const Generic& val) const
{
	int mag = std::abs(val.get_const_ref<int>());
	int dir = val.get_const_ref<int>() / mag;

	double mag_err = 1.0f, dir_err = 1.0f;
	if (feature.stdev != 0)
	{
		mag_err = (1 - std::erf((dir - feature.mean) / (std::sqrt(2) * feature.stdev)));
	}
	if (feature_magnitude.stdev != 0)
	{
		dir_err = (1 - std::erf((mag - feature_magnitude.mean) / (std::sqrt(2) * feature_magnitude.stdev)));
	}

	return std::sqrt(mag_err * dir_err);
}

template<>
double Transformer_covariance<double>::generate_cdf(const Generic& val) const
{
	double mag = std::abs(val.get_const_ref<double>());
	double dir = val.get_const_ref<double>() / mag;
	double mag_err = 1.0f, dir_err = 1.0f;
	if (feature.stdev != 0)
	{
		mag_err = (1 - std::erf((dir - feature.mean) / (std::sqrt(2) * feature.stdev)));
	}
	if (feature_magnitude.stdev != 0)
	{
		dir_err = (1 - std::erf((mag - feature_magnitude.mean) / (std::sqrt(2) * feature_magnitude.stdev)));
	}

	return std::sqrt(mag_err * dir_err);
}

template<>
double Transformer_covariance<GVec2>::generate_cdf(const Generic& val) const
{
	// TODO: check this works
	GVec2 x_mag = (val.get_const_ref<GVec2>() - feature_magnitude.mean) / (std::sqrt(2.0) * feature_magnitude.stdev);
	GVec2 x_dir = (val.get_const_ref<GVec2>() - feature.mean) / (std::sqrt(2.0) * feature.stdev);
	GVec2 err_mag(1.0, 1.0), err_dir(1.0, 1.0);
	if (x_mag.x != std::numeric_limits<double>::infinity())
	{
		err_mag.x -= std::erf(x_mag.x);
	}
	if (x_mag.y != std::numeric_limits<double>::infinity())
	{
		err_mag.y -= std::erf(x_mag.y);
	}
	if (x_dir.x != std::numeric_limits<double>::infinity())
	{
		err_dir.x -= std::erf(x_dir.x);
	}
	if (x_dir.y != std::numeric_limits<double>::infinity())
	{
		err_dir.y -= std::erf(x_dir.y);
	}

	return std::sqrt(err_mag.x * err_mag.y * err_dir.x * err_dir.y);
}


template<> double Transformer_covariance<int>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() &&
		feature.mean == static_cast<Transformer_covariance<int>*>(transformer)->feature.mean ? 0.0 : 1.0;
}

template<> double Transformer_covariance<double>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() ?
		error_rbf(feature.mean - static_cast<Transformer_covariance<double>*>(transformer)->feature.mean, feature.stdev) : 1.0;
}

template<> double Transformer_covariance<GVec2>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() ?
		1.0 - (vPara(feature.mean, static_cast<Transformer_covariance<GVec2>*>(transformer)->feature.mean) + 1.0) / 2.0 : 1.0;
}

namespace GAIA
{

// specific implimentations
template class Transformer_covariance<int>;
template class Transformer_covariance<double>;
template class Transformer_covariance<GVec2>;

}
