#include "transformers/transformer-range.h"

#include <limits>

using namespace GAIA;


template<>
Transformer_range<int>::Transformer_range(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update(v.data);
	}
}

template<>
Transformer_range<double>::Transformer_range(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update(v.data);
	}
}

template<>
Transformer_range<GVec2>::Transformer_range(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update(v.data);
	}
}

template<typename T>
TransformerType Transformer_range<T>::get_type() const { return TransformerType::range; }


template<typename T>
void Transformer_range<T>::update_feature(const IFeatureErrorData* error_data)
{
	auto e = static_cast<const FeatureErrorData<T>*>(error_data);

	feature.update(e->input);
}


template<typename T>
std::unique_ptr<IFeatureErrorData> Transformer_range<T>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<T>>();
	e->input = data.get_const_ref<T>();

	e->result = e->input <= feature.max && e->input >= feature.min;
	e->error_norm = e->result ? 0.0 : 1.0;
	e->error = e->result ? e->input - e->input : e->input;

	return e;
}

template<typename T>
Generic Transformer_range<T>::generate_data() const
{
	return Generic(feature.mean);
}

template<> std::vector<Generic> Transformer_range<int>::generate_data_range(const IFeatureData* data) const
{
	// ideally any value between the min and max is acceptable
	std::vector<Generic> feature_data;

	return feature_data;
}

template<> std::vector<Generic> Transformer_range<double>::generate_data_range(const IFeatureData* data) const
{
	std::vector<Generic> feature_data;

	return feature_data;
}

template<> std::vector<Generic> Transformer_range<GVec2>::generate_data_range(const IFeatureData* data) const
{
	std::vector<Generic> feature_data;

	return feature_data;
}

template<>
double Transformer_range<int>::generate_cdf(const Generic& val) const
{
	if (val.get_const_ref<int>() >= feature.min && val.get_const_ref<int>() <= feature.max)
	{
		return 0.0f;
	}
	else
	{
		return 1.0f;
	}
}

template<>
double Transformer_range<double>::generate_cdf(const Generic& val) const
{
	if (val.get_const_ref<double>() >= feature.min && val.get_const_ref<double>() <= feature.max)
	{
		return 0.0f;
	}
	else
	{
		return 1.0f;
	}
}

template<>
double Transformer_range<GVec2>::generate_cdf(const Generic& val) const
{
	if (val.get_const_ref<GVec2>() >= feature.min && val.get_const_ref<GVec2>() <= feature.max)
	{
		return 0.0f;
	}
	else
	{
		return 1.0f;
	}
}

template<> double Transformer_range<int>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() &&
		feature.min == static_cast<Transformer_range<int>*>(transformer)->feature.min &&
		feature.max == static_cast<Transformer_range<int>*>(transformer)->feature.max ? 0.0 : 1.0;
}

template<> double Transformer_range<double>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() &&
		std::abs(feature.min - static_cast<Transformer_range<double>*>(transformer)->feature.min) < feature_error_thresh &&
		std::abs(feature.max - static_cast<Transformer_range<double>*>(transformer)->feature.max) < feature_error_thresh
		? 0.0 : 1.0;
}

template<> double Transformer_range<GVec2>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() &&
		vDistance(feature.min, static_cast<Transformer_range<GVec2>*>(transformer)->feature.min) < feature_error_thresh &&
		vDistance(feature.max, static_cast<Transformer_range<GVec2>*>(transformer)->feature.max) < feature_error_thresh
		? 0.0 : 1.0;
}

namespace GAIA
{

// specific implimentations
template class Transformer_range<int>;
template class Transformer_range<double>;
template class Transformer_range<GVec2>;

}
