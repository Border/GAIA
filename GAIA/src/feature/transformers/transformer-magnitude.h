#pragma once

#include "feature-transformer.h"
#include "feature-stats.h"
#include "entity-context.h"
#include "frame-context.h"
#include "generic.h"

namespace GAIA
{

/// ErrorNorm < thresh
template<typename T>
class Transformer_magnitude : public ITransformer
{
public:
	/// ctor
	Transformer_magnitude(const std::vector<FeatureDataContext>& values);

	/// dtor
	virtual ~Transformer_magnitude() = default;

	/// Returns the type of transformer this is
	TransformerType get_type() const override;

	/// updates the feature based on the supplied error data
	void update_feature(const IFeatureErrorData* error_data) override;

	/// Returns the error when matching another transformer object to this
	double match(ITransformer* transformer) const override;

	/// Transforms the input data based on the type parameter of the class tempalte
	std::unique_ptr<IFeatureErrorData> generate_error(const Generic& data) override;

	/// Transforms the feature data to create output data
	Generic generate_data() const override;

	/// Transforms the feature data to create a range of output data
	std::vector<Generic> generate_data_range(const IFeatureData* data) const override;

	/// Generates a CDF based on the input value
	double generate_cdf(const Generic& val) const override;

	/// Stats
	FeatureStats<T> feature;
	FeatureStats<T> feature_normal;
};

}
