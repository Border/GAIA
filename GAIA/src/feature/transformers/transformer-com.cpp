#include "transformers/transformer-com.h"

#include <limits>
#include <cmath>

using namespace GAIA;


template<>
Transformer_com<int>::Transformer_com(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update(v.data);
	}
}

template<>
Transformer_com<double>::Transformer_com(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update(v.data);
	}
}

template<>
Transformer_com<GVec2>::Transformer_com(const std::vector<FeatureDataContext>& values)
{
	for (const auto& v : values)
	{
		feature.update(v.data);
	}
}

template<typename T>
TransformerType Transformer_com<T>::get_type() const { return TransformerType::com; }

template<typename T>
void Transformer_com<T>::update_feature(const IFeatureErrorData* error_data)
{
	auto e = static_cast<const FeatureErrorData<T>*>(error_data);

	feature.update(e->input);
}

template<>
std::unique_ptr<IFeatureErrorData> Transformer_com<int>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<int>>();
	e->input = data.get_const_ref<int>();
	e->error = e->input - feature.mean;

	e->result = e->error == 0;
	e->error_norm = e->result ? 0.0 : 1.0;

	return e;
}

template<>
std::unique_ptr<IFeatureErrorData> Transformer_com<double>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<double>>();
	e->input = data.get_const_ref<double>();
	e->error = e->input - feature.mean;

	// Calculate the normalised error between the input and the feature value
	e->error_norm = error_rbf(e->error, 2 * feature.stdev);
	e->result = e->error_norm < 0.1;

	return e;
}

template<>
std::unique_ptr<IFeatureErrorData> Transformer_com<GVec2>::generate_error(const Generic& data)
{
	auto e = std::make_unique<FeatureErrorData<GVec2>>();
	e->input = data.get_const_ref<GVec2>();
	e->error = e->input - feature.mean;

	// Calculate the normalised error between the input and the feature value
	e->error_norm = error_rbf(e->error, 2 * feature.stdev);
	e->result = e->error_norm < 0.1;

	return e;
}

template<typename T>
Generic Transformer_com<T>::generate_data() const
{
	return Generic(feature.mean);
}

template<typename T>
std::vector<Generic> Transformer_com<T>::generate_data_range(const IFeatureData* data) const
{
	// maybe use a normal distribution to generate a range of random data about the mean?
	return std::vector<Generic>({ Generic(feature.mean) });
}

template<>
const FeatureStats<int>& Transformer_com<int>::get_const_ref_feature() const
{
	return feature;
}

template<>
const FeatureStats<double>& Transformer_com<double>::get_const_ref_feature() const
{
	return feature;
}

template<>
const FeatureStats<GVec2>& Transformer_com<GVec2>::get_const_ref_feature() const
{
	return feature;
}

template<>
double Transformer_com<int>::generate_cdf(const Generic& val) const
{
	if (feature.stdev == 0)
	{
		return 1.0;
	}
	else
	{
		return 1.0 - std::erf((double)(val.get_const_ref<int>() - feature.mean) / (std::sqrt(2.0) * (double)feature.stdev));
	}
}

template<>
double Transformer_com<double>::generate_cdf(const Generic& val) const
{
	if (feature.stdev <= std::numeric_limits<double>::epsilon())
	{
		return 1.0;
	}
	else
	{
		return 1.0 - std::erf((val.get_const_ref<double>() - feature.mean) / (std::sqrt(2.0) * feature.stdev));
	}
}

template<>
double Transformer_com<GVec2>::generate_cdf(const Generic& val) const
{
	GVec2 x = (val.get_const_ref<GVec2>() - feature.mean) / (std::sqrt(2.0) * feature.stdev);
	GVec2 err(1.0, 1.0);
	if (x.x != std::numeric_limits<double>::infinity())
	{
		err.x -= std::erf(x.x);
	}
	if (x.y != std::numeric_limits<double>::infinity())
	{
		err.y -= std::erf(x.y);
	}

	return std::sqrt(err.x * err.y);
}

template<>
double Transformer_com<int>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() &&
		feature.mean == static_cast<Transformer_com<int>*>(transformer)->feature.mean ? 0.0 : 1.0;
}

template<>
double Transformer_com<double>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() ?
		error_rbf(feature.mean - static_cast<Transformer_com<double>*>(transformer)->feature.mean, feature.stdev) : 1.0;
}

template<>
double Transformer_com<GVec2>::match(ITransformer* transformer) const
{
	return transformer->get_type() == get_type() ?
		error_rbf(feature.mean - static_cast<Transformer_com<GVec2>*>(transformer)->feature.mean, feature.stdev) : 1.0;
}

namespace GAIA
{

// specific implimentations
template class Transformer_com<int>;
template class Transformer_com<double>;
template class Transformer_com<GVec2>;

}
