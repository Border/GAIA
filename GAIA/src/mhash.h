#pragma once
// MurmurHash3 implementation based on the hash function written by Austin Appleby.

#include <cstdint>
#include <functional>
#include <cassert>
#include <vector>

namespace GAIA
{

namespace HASH
{

uint32_t mhash_32(const uint32_t* key, int len, uint32_t seed);

}

#define SEED 1195460929

inline uint32_t generate_hash(const uint32_t* key, int len)
{
	return HASH::mhash_32(key, len * 4, SEED);
}

template<typename Iter, typename Retriever>
inline uint32_t generate_hash(Iter first, const Iter last, Retriever get_value)
{
	const int len = std::distance(first, last);

	std::vector<uint32_t> key;
	key.reserve(len);

	for (; first != last; ++first)
	{
		key.push_back(get_value(*first));
	}

	return HASH::mhash_32(key.data(), len * 4, SEED);
}

}
