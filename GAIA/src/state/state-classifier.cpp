#include "state-classifier.h"
#include "feature-classifier.h"
#include "feature-container.h"
#include "feature-association.h"

#include <algorithm>

using namespace GAIA;

std::unique_ptr<StateClassifier> StateClassifier::singleton = nullptr;



StateClassifier::StateClassifier() :
	n_features(0),
	n_states(0)
{
}

StateClassifier::StateClassifier(const StateClassifier& stcl) :
	n_features(0),
	n_states(0)
{
}

StateClassifier* StateClassifier::Instance() { return singleton.get(); }

void StateClassifier::Create()
{
	if (singleton == nullptr)
	{
		singleton = std::make_unique<StateClassifier>();
	}
}


State* StateClassifier::compose_state(const std::unordered_map<unsigned int, std::vector<FeatureDataContext>>& state_feature_contexts)
{
	std::vector<std::pair<unsigned int, FVector>> state_features;

	for (const auto& feature_contexts : state_feature_contexts)
	{
		// return a matching series of features or a new series of features
		state_features.push_back(std::make_pair(feature_contexts.first, compose_features(feature_contexts.first, feature_contexts.second)));
	}

	// generate hashes from the state_features to see if there is an existing state
	auto hashes = generate_multi_hashes(state_features);
	auto state = match_state(hashes.second);

	if (state == nullptr)
	{
		auto result = states.emplace(hashes.second, std::make_unique<State>(state_features, hashes.first, hashes.second, ++n_states));

		// the result should be true for insertion, otherwise a state already exists with the same hash
		assert(result.second);

		state = result.first->second.get();
	}

	return state;
}

std::vector<IFeature*> StateClassifier::compose_features(unsigned int id, const std::vector<FeatureDataContext>& feature_contexts)
{
	// Currently this does not create an IFeatureDataRetriever object, we are assuming
	// there is no dependent data atm. However, ideally here we attempt to identify any
	// dependencies, trying to find ways to make the data invariant based on various
	// features from the frame exemplars in the entity exemplars

	// find the best combination of features that most closely match the feature exemplars,
	// creating a new feature if necessary

	// obtain the feature data type
	const auto type = feature_contexts.front().data.get_type();

	// the list of features that best describe the feature exemplars
	std::vector<IFeature*> state_features;

	// create transformer(s) from feature exemplars
	auto transformers = FeatureClassifier::Instance()->make_feature_transformers(feature_contexts);

	// filter out all other features that are not of the desired transformers
	auto filtered_features = features.find(id);
	if (filtered_features != std::end(features))
	{
		// match created transformers with the filtered list of features
		// if no match, create a new feature using the transformer

		for (auto& t : transformers)
		{
			auto range = filtered_features->second.equal_range(t->get_type());
			std::vector<std::pair<double, IFeature*>> matched_list;

			for (; range.first != range.second; ++range.first)
			{
				auto match_error = range.first->second->match(t.get());

				if (match_error < 0.2)
				{
					matched_list.push_back(std::make_pair(match_error,range.first->second.get()));
				}
			}

			// no matches were found so create a new feature
			if (matched_list.empty())
			{
				state_features.push_back(make_feature(id, type, filtered_features->second, t));
			}
			else
			{
				// sort the list
				std::sort(std::begin(matched_list), std::end(matched_list), [](const std::pair<double, IFeature*>& a, const std::pair<double, IFeature*>& b) { return a.first < b.first; });
				// add the smallest error item to the state_features list
				state_features.push_back(matched_list.front().second);
			}
		}
	}
	else
	{
		// no features with this id exists yet, so make them
		for (auto& t : transformers)
		{
			std::unordered_multimap<TransformerType, std::unique_ptr<IFeature>, std::hash<size_t>> flist;
			state_features.push_back(make_feature(id, type, flist, t));
			features.emplace(id, std::move(flist));
		}
	}

	return state_features;
}

IFeature* StateClassifier::make_feature(unsigned int id, GType type, std::unordered_multimap<TransformerType, std::unique_ptr<IFeature>, std::hash<size_t>>& filtered_feature, std::unique_ptr<ITransformer>& transformer)
{
	IFeature* fnew = nullptr;

	auto transformer_type = transformer->get_type();

	switch (type)
	{
	case Gint:		fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<int>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	case Gdouble:	fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<double>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	case GVec:		fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<GVec2>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	default:		throw std::runtime_error("Unknown type"); break;
	}

	return fnew;
}

State* StateClassifier::match_state(unsigned int unique_hash) const
{
	auto result = states.find(unique_hash);

	return result != std::end(states) ? result->second.get() : nullptr;
}
