#pragma once

#include "feature.h"
#include "feature-types.h"
#include "frame-context.h"
#include "datamap.h"

#include <vector>
#include <unordered_map>
#include <functional>

namespace GAIA
{

/// Associations between features
class State
{
	friend class IO;
	friend class DebugDisplay;
public:
	/// Constructor - copy features and generate hashes
	State(std::vector<std::pair<unsigned int, FVector>>& features, unsigned int uid);
	/// Constructor - copy features and hashes
	State(std::vector<std::pair<unsigned int, FVector>>& features, unsigned int hash_type, unsigned int hash_unique, unsigned int uid);

	/// returns the type hash - a hash generated from the type id
	unsigned int get_type_hash() const;
	/// returns the unique hash - a hash generated from the unique id
	unsigned int get_unique_hash() const;
	///  returns the id indicating the type of feature
	unsigned int get_type_id() const;
	/// Returns the number of features that make up the association
	size_t get_size() const;
	/// Returns true if the FVector matches
	bool match(const std::vector<std::pair<unsigned int, FVector>>& mfeatures) const;
	/// Returns true if the hash matches
	bool match_hash(unsigned int _hash) const;
	/// Returns true if the associations matches
	bool operator == (const State* a) const;

	/// Returns true if the supplied raw state DataMap matches the features of this state
	bool match(const DataMap& state, const FeatureContext& context);

protected:
	/// a unique id for generating hashes of groups of features
	const unsigned int id_unique;
	/// the hash of the features unique id's that form the association
	unsigned int hash_unique;
	/// the hash of the feature id's that for the association
	unsigned int hash_type;

	/// Vector of features that form the association
	std::vector<std::pair<unsigned int, FVector>> features;
};

typedef std::vector<State*> SVector;

}
