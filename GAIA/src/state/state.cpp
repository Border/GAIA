#include "state.h"
#include "entity.h"
#include "feature-classifier.h"
#include "feature-association.h"
#include "datamap.h"

#include <algorithm>

using namespace GAIA;

State::State(std::vector<std::pair<unsigned int, FVector>>& _features, unsigned int uid) :
	id_unique(uid),
	features(std::move(_features))
{
	std::tie(hash_type, hash_unique) = generate_multi_hashes(features);
}

State::State(std::vector<std::pair<unsigned int, FVector>>& _features, unsigned int _hash_type, unsigned int _hash_unique, unsigned int uid) :
	id_unique(uid),
	features(std::move(_features)),
	hash_type(_hash_type),
	hash_unique(_hash_unique)
{
}

unsigned int State::get_type_hash() const { return hash_type; }

unsigned int State::get_unique_hash() const { return hash_unique; }

unsigned int State::get_type_id() const { return id_unique; }

size_t State::get_size() const { return features.size(); }

bool State::match(const std::vector<std::pair<unsigned int, FVector>>& features) const { return generate_multi_hashes(features).second == hash_unique; }

bool State::match_hash(unsigned int _hash) const { return hash_unique == _hash; }

bool State::operator == (const State* a) const { return hash_unique == a->get_unique_hash(); }

bool State::match(const DataMap& state, const FeatureContext& context)
{
	for (const auto& f_pair : features)
	{
		auto data = state.find(f_pair.first);

		if (data != std::end(state))
		{
			for (auto f : f_pair.second)
			{
				Generic reference = context.get_reference(f_pair.first, context.entity).second;
				if (!GAIA::match_feature(f, context, reference.is_none() ? data->second : data->second - reference))
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}

	return true;
}
