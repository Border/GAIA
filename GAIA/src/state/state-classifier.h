#pragma once

#include "feature.h"
#include "feature-transformer.h"
#include "entity-context.h"
#include "frame-context.h"
#include "state.h"
#include "feature-types.h"
#include "generic.h"

#include <mutex>
#include <unordered_map>

namespace GAIA
{

class StateClassifier
{
protected:
	/// copy ctor, but nothing should be coppied and protected so it cant be created on stack
	StateClassifier(const StateClassifier& stcl);

public:
	/// ctor
	StateClassifier();

	/// Obtains an instance of this class
	/// \return The instance for this class
	static StateClassifier* Instance();

	/// Creates an instance
	static void Create();

	/// Composes a state, attempts to use an existing one if matches, otherwise creates a new one
	State* compose_state(const std::unordered_map<unsigned int, std::vector<FeatureDataContext>>& state_feature_exemplars);

	/// Composes one or more features to encompass the feature exemplars, either unsing existing known features or creating new ones
	std::vector<IFeature*> compose_features(unsigned int id, const std::vector<FeatureDataContext>& feature_exemplars);

private:
	/// Creates a new features based on the transformer
	IFeature* make_feature(unsigned int id, GType type, std::unordered_multimap<TransformerType, std::unique_ptr<IFeature>, std::hash<size_t>>& filtered_feature, std::unique_ptr<ITransformer>& transformer);

	/// Returns an event if it matches the unique hash
	State* match_state(unsigned int unique_hash) const;


	/// protects access
	/// Maybe use shared mutex to handle writes, allowing reads to occur simultaneously
	mutable std::mutex m_wframe;

	/// list of event feature grouped based on thier type, this owns them
	std::unordered_map<unsigned int, std::unordered_multimap<TransformerType, std::unique_ptr<IFeature>, std::hash<size_t>>> features;

	/// list of state the agent has learned, indexed by thier unique hash
	std::unordered_map<unsigned int, std::unique_ptr<State>> states;

	/// the current number of event based features known to the agent
	unsigned int n_features;

	/// the current number of events known to the agent
	unsigned int n_states;

	/// A singleton
	static std::unique_ptr<StateClassifier> singleton;
};

}
