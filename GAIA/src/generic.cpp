#include "generic.h"

#include <limits>
#include <assert.h>
#include <stdexcept>
#include <sstream>

namespace GAIA
{

IGeneric::~IGeneric() {}

template <typename T> GenericImpl<T>::GenericImpl(T _t) : t(std::move(_t)) {}
template <typename T> std::unique_ptr<IGeneric> GenericImpl<T>::clone() const { return std::make_unique<GenericImpl<T>>(t); }
template <typename T> T GenericImpl<T>::get() const { return t; }
template <typename T> const T& GenericImpl<T>::get_const_ref() const { return t; }
template <typename T> T& GenericImpl<T>::get_ref() { return t; }
template <typename T> void GenericImpl<T>::set(const T& _t) { t = _t; }

template <> GType GenericImpl<int>::get_type() const { return Gint; }
template <> GType GenericImpl<double>::get_type() const { return Gdouble; }
template <> GType GenericImpl<GVec2>::get_type() const { return GVec; }


Generic::Generic() { }
Generic::Generic(bool t) : p(std::make_unique<GenericImpl<int>>(std::move((int)t))) { }
Generic::Generic(int t) : p(std::make_unique<GenericImpl<int>>(std::move(t))) { }
Generic::Generic(double t) : p(std::make_unique<GenericImpl<double>>(std::move(t))) { }
Generic::Generic(const GVec2& t) : p(std::make_unique<GenericImpl<GVec2>>(std::move(t))) { }
Generic::Generic(Generic&& other) : p(std::move(other.p)) { }
Generic::Generic(const Generic& other)
{
	if (! other.is_none())
	{
		p = other.p->clone();
	}
}
Generic::Generic(GType type)
{
	switch (type)
	{
	case GNone:		break;
	case Gint:		p = std::make_unique<GenericImpl<int>>(0); break;
	case Gdouble:	p = std::make_unique<GenericImpl<double>>(0.0); break;
	case GVec:		p = std::make_unique<GenericImpl<GVec2>>(GVec2(0, 0)); break;
	default:		throw std::runtime_error("Unknown type");
	}
}

void Generic::swap(Generic& other)
{
	p.swap(other.p);
}

Generic& Generic::operator=(Generic other)
{
	swap(other);
	return *this;
}

GType Generic::get_type() const { return p.get() == nullptr ? GNone : p->get_type(); }

template <typename T> const T& Generic::get_const_ref() const
{
	assert(p.get() != nullptr);
	return static_cast<GenericImpl<T>*>(p.get())->get_const_ref();
}

template <typename T> T& Generic::get_ref() const
{
	assert(p.get() != nullptr);
	return static_cast<GenericImpl<T>*>(p.get())->get_ref();
}

template<typename T> void Generic::set(const T& val)
{
	assert(p.get() != nullptr);
	static_cast<GenericImpl<T>*>(p.get())->set(val);
}

bool Generic::is_none() const
{
	return p == nullptr || p->get_type() == GNone;
}

bool Generic::is_zero() const
{
	switch (p->get_type())
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return get_const_ref<int>() == 0;
	case Gdouble:	return std::abs(get_const_ref<double>()) < std::numeric_limits<double>::epsilon();
	case GVec:		return get_const_ref<GVec2>().Length() < std::numeric_limits<double>::epsilon();
	}

	throw std::runtime_error("Unknown type");
}

Generic Generic::generate_zero() const
{
	assert(p.get() != nullptr);
	return Generic(p->get_type());
}

std::string Generic::to_string() const
{
	std::ostringstream sstream;

	switch (p->get_type())
	{
	case GNone:		sstream << ""; break;
	case Gint:		sstream << get_const_ref<int>(); break;
	case Gdouble:	sstream << get_const_ref<double>(); break;
	case GVec:
	{
		const auto& v = get_const_ref<GVec2>();
		sstream << v.x << "," << v.y;
		break;
	}
	default: throw std::runtime_error("Unknown type");
	}

	return sstream.str();
}


Generic operator - (const Generic& a)
{
	switch (a.get_type())
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return Generic(-a.get_const_ref<int>());
	case Gdouble:	return Generic(-a.get_const_ref<double>());
	case GVec:		return Generic(-a.get_const_ref<GVec2>());
	}

	throw std::runtime_error("Unknown type");
}

Generic operator - (const Generic& a, const Generic& b)
{
	const auto type = a.get_type();
	assert(type == b.get_type());

	switch (type)
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return Generic(a.get_const_ref<int>() - b.get_const_ref<int>());
	case Gdouble:	return Generic(a.get_const_ref<double>() - b.get_const_ref<double>());
	case GVec:		return Generic(a.get_const_ref<GVec2>() - b.get_const_ref<GVec2>());
	}

	throw std::runtime_error("Unknown type");
}

Generic operator + (const Generic& a, const Generic& b)
{
	const auto type = a.get_type();
	assert(type == b.get_type());

	switch (type)
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return Generic(a.get_const_ref<int>() + b.get_const_ref<int>());
	case Gdouble:	return Generic(a.get_const_ref<double>() + b.get_const_ref<double>());
	case GVec:		return Generic(a.get_const_ref<GVec2>() + b.get_const_ref<GVec2>());
	}

	throw std::runtime_error("Unknown type");
}

Generic operator += (Generic& a, const Generic& b)
{
	const auto typea = a.get_type();
	const auto typeb = b.get_type();

	switch (typea)
	{
	case GNone:			throw std::runtime_error("Type is none");
	case Gint:
	{
		switch (typeb)
		{
		case GNone:		throw std::runtime_error("Type is none");
		case Gint:		a.get_ref<int>() += b.get_const_ref<int>(); break;
		case Gdouble:	a.get_ref<int>() += static_cast<int>(b.get_const_ref<double>()); break;
		case GVec:		throw std::runtime_error("Incompatible type: int += GVec");
		default:		throw std::runtime_error("Unknown type");
		}
		return Generic(a);
	}
	case Gdouble:
	{
		switch (typeb)
		{
		case GNone:		throw std::runtime_error("Type is none");
		case Gint:		a.get_ref<double>() += static_cast<double>(b.get_const_ref<int>()); break;
		case Gdouble:	a.get_ref<double>() += b.get_const_ref<double>(); break;
		case GVec:		throw std::runtime_error("Incompatible type: double += GVec");
		default:		throw std::runtime_error("Unknown type");
		}
		return Generic(a);
	}
	case GVec:
	{
		if (typea != typeb)
		{
			throw std::runtime_error("Type missmatch! Type not GVec");
		}

		a.get_ref<GVec2>() += b.get_const_ref<GVec2>();
		return Generic(a);
	}
	}

	throw std::runtime_error("Unknown type");
}

Generic operator -= (Generic& a, const Generic& b)
{
	const auto typea = a.get_type();
	const auto typeb = b.get_type();

	switch (typea)
	{
	case GNone:			throw std::runtime_error("Type is none");
	case Gint:
	{
		switch (typeb)
		{
		case GNone:		throw std::runtime_error("Type is none");
		case Gint:		a.get_ref<int>() -= b.get_const_ref<int>(); break;
		case Gdouble:	a.get_ref<int>() -= static_cast<int>(b.get_const_ref<double>()); break;
		case GVec:		throw std::runtime_error("Incompatible type: int -= GVec");
		default:		throw std::runtime_error("Unknown type");
		}
		return Generic(a);
	}
	case Gdouble:
	{
		switch (typeb)
		{
		case GNone:		throw std::runtime_error("Type is none");
		case Gint:		a.get_ref<double>() -= static_cast<double>(b.get_const_ref<int>()); break;
		case Gdouble:	a.get_ref<double>() -= b.get_const_ref<double>(); break;
		case GVec:		throw std::runtime_error("Incompatible type: double -= GVec");
		default:		throw std::runtime_error("Unknown type");
		}
		return Generic(a);
	}
	case GVec:
	{
		if (typea != typeb)
		{
			throw std::runtime_error("Type missmatch! Type not GVec");
		}

		a.get_ref<GVec2>() -= b.get_const_ref<GVec2>();
		return Generic(a);
	}
	}

	throw std::runtime_error("Unknown type");
}

Generic operator * (const Generic& a, const Generic& b)
{
	const auto type = a.get_type();
	assert(type == b.get_type());

	switch (type)
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return Generic(a.get_const_ref<int>() * b.get_const_ref<int>());
	case Gdouble:	return Generic(a.get_const_ref<double>() * b.get_const_ref<double>());
	case GVec:		return Generic(a.get_const_ref<GVec2>() * b.get_const_ref<GVec2>());
	}

	throw std::runtime_error("Unknown type");
}

template <typename T>
Generic operator * (const Generic& a, const T& b)
{
	switch (a.get_type())
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return Generic(static_cast<int>(a.get_const_ref<int>() * b));
	case Gdouble:	return Generic(static_cast<double>(a.get_const_ref<double>() * b));
	case GVec:		return Generic(static_cast<GVec2>(b * a.get_const_ref<GVec2>()));
	}

	throw std::runtime_error("Unknown type");
}

Generic operator / (const Generic& a, const Generic& b)
{
	const auto type = a.get_type();
	assert(type == b.get_type());

	switch (type)
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return Generic(a.get_const_ref<int>() / b.get_const_ref<int>());
	case Gdouble:	return Generic(a.get_const_ref<double>() / b.get_const_ref<double>());
	case GVec:		return Generic(a.get_const_ref<GVec2>() / b.get_const_ref<GVec2>());
	}

	throw std::runtime_error("Unknown type");
}

bool operator == (const Generic& a, const Generic& b)
{
	const auto type = a.get_type();
	assert(type == b.get_type());

	switch (type)
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return a.get_const_ref<int>() == b.get_const_ref<int>();
	case Gdouble:	return std::abs(a.get_const_ref<double>() - b.get_const_ref<double>()) < std::numeric_limits<double>::epsilon();
	case GVec:		return (a.get_const_ref<GVec2>() - b.get_const_ref<GVec2>()).Length() < std::numeric_limits<double>::epsilon();
	}

	throw std::runtime_error("Unknown type");
}

bool operator != (const Generic& a, const Generic& b)
{
	return !(a == b);
}

bool operator < (const Generic& a, const Generic& b)
{
	const auto type = a.get_type();
	assert(type == b.get_type());

	switch (type)
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return a.get_const_ref<int>() < b.get_const_ref<int>();
	case Gdouble:	return a.get_const_ref<double>() < b.get_const_ref<double>();
	case GVec:		return a.get_const_ref<GVec2>().Length() < b.get_const_ref<GVec2>().Length();
	}

	throw std::runtime_error("Unknown type");
}


template<> int abs<int>(const Generic& a)
{
	return std::abs(a.get_const_ref<int>());
}

template<> double abs<double>(const Generic& a)
{
	return std::abs(a.get_const_ref<double>());
}

template<> GVec2 abs<GVec2>(const Generic& a)
{
	return vAbs(a.get_const_ref<GVec2>());
}

template<> Generic abs(const Generic& a)
{
	const auto type = a.get_type();

	switch (type)
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return Generic(std::abs(a.get_const_ref<int>()));
	case Gdouble:	return Generic(std::abs(a.get_const_ref<double>()));
	case GVec:		return Generic(vAbs(a.get_const_ref<GVec2>()));
	}

	throw std::runtime_error("Unknown type");
}



template<> int normalise<int>(const Generic& a)
{
	const int norm = a.get_const_ref<int>();
	return norm == 0 ? norm : norm / std::abs(norm);
}

template<> double normalise<double>(const Generic& a)
{
	const double norm = a.get_const_ref<double>();
	const double abs_norm = std::abs(norm);
	return abs_norm < std::numeric_limits<double>::epsilon() ? norm : norm / abs_norm;
}

template<> GVec2 normalise<GVec2>(const Generic& a)
{
	return a.get_const_ref<GVec2>().GetNormalised();
}

template<> Generic normalise(const Generic& a)
{
	const auto type = a.get_type();

	switch (type)
	{
	case GNone:		throw std::runtime_error("Type is none");
	case Gint:		return Generic(normalise<int>(a));
	case Gdouble:	return Generic(normalise<double>(a));
	case GVec:		return Generic(normalise<GVec2>(a));
	}

	throw std::runtime_error("Unknown type");
}

template void Generic::set(const int& val);
template void Generic::set(const double& val);
template void Generic::set(const GVec2& val);

};
