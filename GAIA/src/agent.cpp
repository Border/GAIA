#include "agent.h"
#include "affordance-classifier.h"
#include "affordance-condition-classifier.h"
#include "affordance-action-classifier.h"
#include "event-classifier.h"
#include "state-classifier.h"
#include "action-classifier.h"
#include "feature-classifier.h"
#include "frame-context.h"
#include "entity-persistent.h"
#include "generic.h"
#include "container-algorithms.h"

#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <mutex>
#include <tuple>

using namespace GAIA;

Agent::Agent() :
	chronometer(0)
{
	match_compare = std::bind(&Agent::default_match_compare, this, std::placeholders::_1, std::placeholders::_2);

	reset();
}

Agent::~Agent()
{
}

void Agent::add_action_ranges(std::vector<std::shared_ptr<IActionDomain>>& actions)
{
	// this should be the entire list of actions the agent can perform on any given entity
	// or at least a partial list, with this function being called multiple times

	// if an action range does not have a specific entity type that it is limited to, it is added to all persistent entities
	// if there is a restriction, the action range is only added to the persistent entity types allowed

	// the action ranges are only added to the persistent entity when it is initially created
	// although ideally it would be good for the agent to be able to 'discover' a new action range or gain a new ability
	// for simplicity the agent will be given all possible actions on initialisation

	PersistentEntityGenerator::Instance()->add_action_ranges(actions);
}

void Agent::add_world_frame(const std::shared_ptr<WorldFrame>& frame)
{
	Memory::Instance()->add_world_frame(frame);
}

void Agent::add_frame_pair(const std::shared_ptr<WorldFrame>& inital_frame, const std::shared_ptr<WorldFrame>& final_frame)
{
	// generate difference map between final and inital frames, linking entities
	generate_deltamap(inital_frame, final_frame);

	// adds data to the affordance classifier in order to form affordances to describe the characteristics
	generate_affordances(inital_frame, final_frame);
}

void Agent::clear_frames()
{
	Memory::Instance()->clear_frames();
}

void Agent::reset()
{
	// create the instances of modules to allocate themselves
	Memory::Create();
	PersistentEntityGenerator::Create();
	FeatureClassifier::Create();
	EventClassifier::Create();
	StateClassifier::Create();
	ActionClassifier::Create();
	AffordanceClassifier::Create();
	AffordanceActionClassifier::Create();
	ConditionClassifier::Create();
}

void Agent::set_filtered_condition_features(const std::vector<tid_t>& filtered_features)
{
	ConditionClassifier::Instance()->set_filtered_features(filtered_features);
}

std::vector<std::pair<Entity*, std::vector<Affordance*>>> Agent::test_frame_pair(const std::shared_ptr<WorldFrame>& inital_frame, const std::shared_ptr<WorldFrame>& final_frame)
{
	// generate difference map between final and inital frames, linking entities
	generate_deltamap(inital_frame, final_frame);

	std::vector<std::pair<Entity*, std::vector<Affordance*>>> results;
	for (auto& e : final_frame->entities)
	{
		auto& ent = e.second;

		std::vector<Affordance*> matched_affordances;
		AffordanceContext ex(ent.get(), inital_frame, final_frame);
		for (auto affd : AffordanceClassifier::Instance()->get_affordances())
		{
			if (affd->match(ex)) { matched_affordances.push_back(affd); }
		}

		results.push_back(std::make_pair(ent.get(), std::move(matched_affordances)));
	}

	return results;
}

std::vector<EntityAction*> Agent::get_actions() const
{
	std::vector<EntityAction*> actions;

	auto frame = Memory::Instance()->get_current_frame();

	if (frame != nullptr)
	{
		for (auto& ent : frame->get_entities())
		{
			for (auto& a : ent.second->actions)
			{
				actions.push_back(a.get());
			}
		}
	}

	return actions;
}

size_t Agent::get_frame_count() const { return chronometer; }

void Agent::process()
{
	// process data (make as much multi-threaded as possible)

	++chronometer;

	auto pair = Memory::Instance()->get_frame_pair();
	auto& curr = pair.first;
	auto& prev = pair.second;

	if (prev != nullptr)
	{
		// generate difference map between current and previous frames, linking entities
		generate_deltamap(prev, curr);

		// generate affordances
		generate_affordances(prev, curr);

		// call planner
		generate_plans();

		// call intrinsic/focus/interest code
		// *Note: the focus area may be a frame behind
		update_intrinsics(curr);

		// call update to prediction model
		// need to predict external actions (should this be part of the planner?)

		// call action generator
		generate_actions(curr);
	}
	else
	{
		for (auto& e : curr->entities)
		{
			if (e.second->get_persistent_entity() == nullptr)
			{
				e.second->compose_persistent_entity();
			}
		}
	}

	// wait for all threads to finish processing
	// However, if still processing and alotted time
	// expires, then we need to continue on.

	// maybe use std::condition_variable
}



void Agent::generate_deltamap(const std::shared_ptr<WorldFrame>& previous_frame, const std::shared_ptr<WorldFrame>& current_frame)
{
	// Perhaps need to use a better compare system which perhaps uses prediction and better
	// generates a list of all items that match the descriptor and compare the list to get
	// a best fit. Would use various filters to shorten the list (if they are a large
	// distance appart).

	// This method does not factor large moving groups well. Ideally we need to cluster
	// the groups that are moving and use previous motion as an indicator
	// Furthermore this method cannot handle some edge cases where there are multiple
	// possibilities. We need to use affordances to determine what possibilities are
	// valid, then use prior knowledge to determine the likely outcome. Or just not care
	// and just consider the blob a single entity while its too hard to identify
	// individual ojbects.

	auto& current = current_frame->entities;
	auto& previous = previous_frame->entities;

	unsigned int prev_descriptor = 0;

	for (auto cit = current.begin(); cit != current.end(); ++cit)
	{
		auto descriptor = cit->first;

		if (prev_descriptor == descriptor && cit != current.begin()) continue;

		prev_descriptor = descriptor;

		const auto pmcount = previous.count(descriptor);
		const auto cmcount = current.count(descriptor);

		// Get all occurences of each object of type descriptor
		// for both the current and previous frames

		auto prange = previous.equal_range(descriptor);
		auto crange = current.equal_range(descriptor);

		// then put them in a temporary list

		std::vector<std::shared_ptr<Entity>> plist;
		plist.reserve(pmcount);

		for (auto i = prange.first; i != prange.second; ++i)
			plist.push_back(i->second);

		std::vector<std::shared_ptr<Entity>> clist;
		clist.reserve(cmcount);

		for (auto i = crange.first; i != crange.second; ++i)
			clist.push_back(i->second);


		// detect if there is a large group of objects within close proximity
		// (low variance) if there is then use clusters to compare blob movement
		// use the cluster from the previous frame if it exists, rather than
		// calculating a new one for the previous frame (which we would have done
		// last frame anyway)


		// find all stationary match ups (position delta = 0)
		// create deltas if necessary and remove from plist/clist
		// we probably want other means to match the object for cases
		// when objects exactly swap positions - perhaps use things like
		// velocity and orientation as well

		for (auto pit = plist.begin(); pit != plist.end();)
		{
			bool matched = false;

			for (auto cit = clist.begin(); cit != clist.end();)
			{
				if (match_compare(*pit, *cit) < std::numeric_limits<double>::epsilon())
				{
					create_delta(*pit, cit->get());

					cit = clist.erase(cit);
					pit = plist.erase(pit);
					matched = true;
					break;
				}

				++cit;
			}

			if (!matched) { ++pit; }
		}

		if (plist.size() == 0 && clist.size() == 0) { continue; }

		// If there are still entities left to match, generate statistics for them
		// This allows the agent to generate error functions for features and better
		// ascertain the similarity
		stats.clear();

		auto update_stats = [&](const std::pair<unsigned int, Generic>& feature)
		{
			auto match = std::find_if(std::begin(stats), std::end(stats), [&](const std::pair<unsigned int, IFeatureStats*>& fstat)
				{ return feature.first == fstat.first; });
			if (match != std::end(stats))
			{
				match->second->update(feature.second);
			}
			else
			{
				stats.push_back(std::make_pair(feature.first, make_stats_from_generic(feature.second)));
			}
		};

		for (auto& ent : plist)
		{
			for (auto& feature : ent->get_state())
			{
				update_stats(feature);
			}
		}

		for (auto& ent : clist)
		{
			for (auto& feature : ent->get_state())
			{
				update_stats(feature);
			}
		}

		// we dont want to calculate the min dist of moved objects
		// in the loop above as the closest match might be an object
		// that is stationary, but hasn't been matched yet - we only
		// want to match other moving objects

		// if any object is still in plist/clist find all the
		// objects with unique matches then calculate the delta.
		// Anything with multiple matches goes to the best_match
		// section to find the best order that produces a match

		std::vector<int> minlist;

		for (auto pit = plist.begin(); pit != plist.end(); ++pit)
		{
			auto min = std::numeric_limits<double>::max();
			int minc = -1;

			for (auto cit = clist.begin(); cit != clist.end(); ++cit)
			{
				const auto d = match_compare(*pit, *cit);

				if (d < min)
				{
					minc = std::distance(clist.begin(), cit);
					min = d;
				}
			}

			bool match = false;
			for (auto& m : minlist)
			{
				if (m == minc)
				{
					m = -1;
					match = true;
				}
			}

			if (match) { minc = -1; }

			minlist.push_back(minc);
		}

		// we cant calculate deltas inside the loop above as we
		// need to ensure every object matched is unique. Hence
		// calculation is deffered until now.

		// minlist should equal plist size, hence its safe to base iteration
		// for minlist from plist. The index from minlist should be
		// valid for clist iterators.
		assert(minlist.size() == plist.size());
		auto mit = minlist.begin();
		for (auto pit = plist.begin(); pit != plist.end();)
		{
			auto min = *mit;

			if (min >= 0)
			{
				create_delta(*pit, std::next(clist.begin(), min)->get());

				pit = plist.erase(pit);
			}
			else
				++pit;

			++mit;
		}

		// we cant delete clist objects in the loop above as
		// it will invalidate the indexes of minlist. Hence
		// we remove them here.

		// we need to sort minlist so that the largest is at the end
		// we can then remove objects from the back of clist without
		// invalidating the begin() iterator of clist()
		std::sort(minlist.begin(), minlist.end());

		for (auto mit = minlist.rbegin(); mit != minlist.rend(); ++mit)
		{
			if (*mit >= 0)
				clist.erase(std::next(clist.begin(), *mit));
		}

		if (plist.size() == 0 && clist.size() == 0) { continue; }

		// we should be left with only difficult to match objects. Ideally
		// we would rank use affordances

		// find the order of clist that produces the minimum difference
		// between each component of plist. This can get computationally
		// expensive for sizemax > 5.

		auto order = calg::best_match(plist.cbegin(), plist.cend(), clist.cbegin(), clist.cend(), match_compare).second;
		// for now we are just using the position, however
		// ideally the system would learn what to use in this function
		// perhapse it could learn based on the value of descriptor

		for (size_t i = 0, len = order.size(); i < len; ++i)
		{
			auto io = order[i];
			if (i < plist.size())
			{
				if (io >= 0)
				{
					create_delta(plist[i], clist[io].get());
				}
				else
				{
					// if the number is -ve then no suitable match was found
					// so add the prev items to a separate delta
					create_delta_prev(plist[i]);

					// set order[i] to 0 so that in the loop below we can easily check for
					// unmatched objects
					order[i] = 0;
				}
			}
			// if i is >= than plist size, then all remaining unmatched objects are new
		}

		for (size_t i = plist.size(), len = order.size(); i < len; ++i)
		{
			if (order[i] < 0)
			{
				create_delta_curr(clist[i].get());
			}
		}
	}
}

void Agent::create_delta(const std::shared_ptr<Entity>& pent, Entity* cent)
{
	// The entity has a valid past state, hence link the persistent data.
	cent->obtain_persistent_entity(pent.get());
	// Then calculate the delta from the previous state to the current state
	auto delta = datamap_delta(cent->state, pent->state);
	cent->update(delta, chronometer, pent);
}

void Agent::create_delta_prev(const std::shared_ptr<Entity>& pent)
{
	// the entity is no longer observable - this may be temporary, or the entity may no
	// longer exist. I'm not sure how to go about this yet.
}

void Agent::create_delta_curr(Entity* cent)
{
	// we are assuming the agent can perfectly track the entity and has a link which
	// is maintained for the lifetime of the unit

	// However, there are cases where an enemy unit may disapear for a brief period
	// so some more robust tracking (looking back in time and prediction, for example
	// if the unit can no longer be seen, but many frames later it can be again, and it
	// hasn't moved (much) then its probably safe to assume its the same unit). This is
	// only important up to a point. It may not matter exactly which unit is which.

	// create persistent data
	cent->compose_persistent_entity();

	// The entity appears - being previously unobservable
	cent->update(cent->get_state(), chronometer);
}

// default tracking match compare
// this function is used to compare the similarity of 2 entities for tracking them between frames.
double Agent::default_match_compare(const std::shared_ptr<Entity>& a, const std::shared_ptr<Entity>& b)
{
	double d = 0;
	auto& a_state = a->get_state();

	for (auto& ait = std::begin(a_state); ait != std::end(a_state); ait++)
	{
		auto bit = b->get_state().find(ait->first);
		double e = 1.0;
		if (bit != std::end(b->get_state()))
		{
			auto match = std::find_if(std::begin(stats), std::end(stats), [&](const std::pair<unsigned int, IFeatureStats*>& fstat)
				{ return ait->first == fstat.first; });
			if (match != std::end(stats))
			{
				e = generic_error(ait->second, bit->second, match->second);
			}
		}
		d += e * e;
	}

	return d;
}

void Agent::generate_affordances(const std::shared_ptr<WorldFrame>& previous_frame, const std::shared_ptr<WorldFrame>& current_frame)
{
	std::vector<AffordanceContext> affordance_contexts;

	// loop through the list of entities in the frame and generate exemplars from deltas and/or actions
	for (auto& e : current_frame->entities)
	{
		auto& ent = e.second;
		if (! ent->delta.empty())
		{
			AffordanceContext affordance_context(ent.get(), previous_frame, current_frame);
			AffordanceClassifier::Instance()->match_events(affordance_context);
			if (! affordance_context.event_type_hashes.empty())
			{
				// there are still remaining unknown events, so add the sample context to the affordance classifier
				AffordanceClassifier::Instance()->add_exemplar(affordance_context);
			}
			AffordanceClassifier::Instance()->match_actions(affordance_context);
			affordance_contexts.push_back(std::move(affordance_context));
		}
	}

	// Continuously loop through the list of affordance contexts and match with learned affordances
	// until no more matches are found.
	bool match_found = true;
	while (match_found)
	{
		match_found = false;
		for (auto& affordance_context : affordance_contexts)
		{
			auto affds = AffordanceClassifier::Instance()->match_affordances(affordance_context);
			if (! affds.empty())
			{
				for (auto affd : affds)
				{
					++affd->get_intrinsic();
				}

				// add the affordance to the entity
				auto ent = affordance_context.final_frame.entity;
				ent->add_affordances(affds);
				Memory::Instance()->add_entity_affordances(ent->get_type_id(), affds);
				match_found = true;


				// update this affordance exemplar frame contexts to include these new affordances.
			}
		}
	}

	// any events that are left over are ones which have no affordance, but likely have an affordance prototype
	// so for each exemplar, find the matching event to affordance prototype and add the exemplar to each event
	for (auto& affordance_context : affordance_contexts)
	{
		auto affds = AffordanceClassifier::Instance()->add_prototype_exemplar(affordance_context);
		if (! affds.empty())
		{
			affordance_context.matched_affordances.insert(std::begin(affordance_context.matched_affordances), std::begin(affds), std::end(affds));
			// add the affordance to the entity
			auto ent = affordance_context.final_frame.entity;
			ent->add_affordances(affds);
			Memory::Instance()->add_entity_affordances(ent->get_type_id(), affds);
		}
	}

	for (auto& affordance_context : affordance_contexts)
	{
		// Check if something didn't happen (action performed produced no result), but also if the correct outcome occurred for a given action.
		// TODO: If an action was performed which was was expected to produce a certain event and another/different event occurs,
		// this should be added to the fail conditions of the action as well.
		if (affordance_context.matched_affordances.empty() && affordance_context.matched_events.empty() && affordance_context.delta.empty())
		{
			// when the entity itself did not change, but it performed an action which may
			// have had an effect on another entity or the world. We treat it as a failure
			// and try to learn the conditions for failure.

			if (!affordance_context.init_frame.entity->actions.empty())
			{
				// add failed_exemplar
				AffordanceActionClassifier::Instance()->add_fail_exemplar(FrameContext(affordance_context.init_frame.entity, affordance_context.init_frame.frame));
			}
		}
	}
}

void Agent::update_intrinsics(const std::shared_ptr<WorldFrame>& frame)
{
	// get the goals frames from plans
	// get_goal_success_frames()

	// now get all dependent entity frames and match against world entity frames via pid.
	// also check each affordance in the entity and update its intrinsics.
	// each continuous affordance needs to generate a pdf to direct its exploration.
	// each affordance is used to generate a pdf for each entity in order to select an affordance for the enitt to perform.
	// a performance metric is required in order to force the agent to use the optimal affordance when given a task to perform.
	// use boredom as well to focus exploration initially, then transition to use more performance.

	for (const auto& ent : frame->entities)
	{
		// Generate a discrete pdf for all possible affordance types
		ent.second->update_intrinsics(frame);
	}

	// now generate actions
}

void Agent::generate_actions(const std::shared_ptr<WorldFrame>& frame)
{
	for (const auto& ent : frame->entities)
	{
		ent.second->generate_actions(frame);
	}
}

void Agent::generate_plans()
{
	// generate plans for all goal frames and store the result
	// maybe check if previously generated plans can simply be updated if they exists

	// now update intrinsics
}
