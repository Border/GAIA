#include "io.h"

#include "feature-container.h"
#include "action-entity.h"
#include "transformers/transformer-com.h"
#include "transformers/transformer-covariance.h"
#include "transformers/transformer-magnitude.h"
#include "transformers/transformer-range.h"

#include <fstream>
#include <stdexcept>
#include <tuple>
#include <iomanip>

using namespace GAIA;

nlohmann::json to_json(const GVec2& v)
{
	return nlohmann::json{ { "x", v.x },{ "y", v.y } };
}

GVec2 from_json(const nlohmann::json& j)
{
	return GVec2(j.at("x").get<double>(), j.at("y").get<double>());
}



IO::exemplar_vec IO::read_exemplar_scenarios(const std::string& filename)
{
	std::ifstream ifs(filename);

	nlohmann::json j = nlohmann::json::parse(ifs);

	exemplar_vec examples;

	auto sc = j["scenario"];
	for (auto& example : sc)
	{
		auto type = example["type"].get<std::string>();

		auto finit = read_world_frame(example["init"]);

		std::shared_ptr<WorldFrame> ffinal = nullptr;
		if (type == "success")
		{
			ffinal = read_world_frame(example["final"]);
		}
		else if (type == "fail")
		{
			ffinal = finit->copy_next();
		}

		ffinal->set_previous_frame(finit);

		examples.push_back(std::make_pair(finit, ffinal));
	}

	return examples;
}

std::vector<std::shared_ptr<IActionDomain>> IO::read_action_ranges(const std::string& filename)
{
	std::ifstream ifs(filename);

	nlohmann::json j = nlohmann::json::parse(ifs);

	std::vector<std::shared_ptr<IActionDomain>> actions;

	auto acts = j["actions"];
	for (const auto& a : acts)
	{
		actions.push_back(read_action_range(a));
	}

	return actions;
}

Scenario IO::read_scenario(const std::string& filename)
{
	std::ifstream ifs(filename);

	nlohmann::json j = nlohmann::json::parse(ifs);

	// load comment

	// load actions
	const auto& scenario_actions = j["actions"];
	std::vector<std::shared_ptr<IActionDomain>> actions;

	for (const auto& action : scenario_actions)
	{
		actions.push_back(read_action_range(action));
	}

	PersistentEntityGenerator::Instance()->add_action_ranges(actions);

	// load levels
	auto levels_list = j["levels"];

	std::vector<std::unique_ptr<Level>> levels;
	for (const auto& level : levels_list)
	{
		levels.push_back(read_level(level));
	}

	return Scenario(actions, levels);
}

SingleTest IO::read_single_test(const std::string& filename)
{
	std::ifstream ifs(filename);

	auto test = nlohmann::json::parse(ifs);

	SingleTest single_test;

	single_test.start = read_world_frame(test["start"]);
	for (const auto& ent : single_test.start->entities)
	{
		ent.second->compose_persistent_entity();
	}

	single_test.goal = read_world_frame(test["goal"]);

	return single_test;
}



void IO::write_plan_frames(const std::string& filename, const Plan* plan)
{
	nlohmann::json j;

	// generate json frames from the plan object
	nlohmann::json frames = nlohmann::json::array();
	for (const auto& f : plan->frames)
	{
		frames.push_back(create_plan_frame_json(f.get()));
	}

	j["frames"] = frames;

	// serialise and write the json object to file
	std::ofstream o(filename);
	o << std::setw(4) << j << std::endl;
}

void IO::write_frame(const std::string& filename, const PlannerFrame* frame)
{
	frame->lock_shared();
	nlohmann::json j = create_plan_frame_json(frame);
	frame->unlock_shared();

	// serialise and write the json object to file
	std::ofstream o(filename);
	o << std::setw(4) << j << std::endl;
}



nlohmann::json IO::create_generic_json(const Generic& data)
{
	nlohmann::json item;
	const auto type = data.get_type();
	item["type"] = (unsigned int)type;

	if (type == GType::Gint)
	{
		item["data"] = data.get_const_ref<int>();
	}
	else if (type == GType::Gdouble)
	{
		item["data"] = data.get_const_ref<double>();
	}
	else if (type == GType::GVec)
	{
		item["data"] = to_json(data.get_const_ref<GVec2>());
	}

	return item;
}

nlohmann::json IO::create_datamap_json(const DataMap& dmap)
{
	nlohmann::json item;
	for (auto& d : dmap)
	{
		auto ditem = create_generic_json(d.second);
		item["id"] = d.first;
		item.push_back(std::move(ditem));
	}
	return item;
}

nlohmann::json IO::create_entity_json(const Entity* entity)
{
	nlohmann::json item;
	item["type_id"] = entity->get_type_id();
	item["state"] = create_datamap_json(entity->state);
	item["delta"] = create_datamap_json(entity->delta);
	return item;
}

nlohmann::json IO::create_feature_json(const IFeature* feature)
{
	nlohmann::json item;

	// id
	item["id"] = feature->get_type_id();
	// unique id
	item["uid"] = feature->get_unique_id();
	// the data type
	item["type"] = (unsigned int)feature->get_data_type();

	// data retriever

	switch (feature->get_data_type())
	{
	case GType::Gint:
	{
		item["transformer"] = create_transformer_json<int>(static_cast<const FeatureContainer<int>*>(feature)->transformer.get());
		break;
	}
	case GType::Gdouble:
	{
		item["transformer"] = create_transformer_json<double>(static_cast<const FeatureContainer<double>*>(feature)->transformer.get());
		break;
	}
	case GType::GVec:
	{
		item["transformer"] = create_transformer_json<GVec2>(static_cast<const FeatureContainer<GVec2>*>(feature)->transformer.get());
		break;
	}
	default: throw std::runtime_error("Feature has no valid data type!");
	}

	return item;
}

nlohmann::json IO::create_operation_json(const Operation* op)
{
	nlohmann::json j;

	if (op->affd == nullptr)
	{
		j["nop"] = true;
	}
	else
	{
		j["affd_id"] = op->affd->get_unique_id();
		j["delta"] = create_datamap_json(op->delta);
	}

	return j;
}

nlohmann::json IO::create_entity_frame_json(const EntityFrame* eframe)
{
	nlohmann::json j;

	j["unique_id"] = eframe->unique_id;
	j["depth"] = eframe->depth;
	j["goal_dist"] = create_datamap_json(eframe->goal_distance);
	j["state"] = create_datamap_json(eframe->state);

	if (!eframe->dependencies.empty())
	{
		nlohmann::json p;

		for (const auto& dep : eframe->dependencies)
		{
			p.push_back(dep.frame_genealogy.get_current()->unique_id);
		}

		j["parent_id"] = p;
	}

	nlohmann::json operations = nlohmann::json::array();
	for (const auto& op : eframe->operations)
	{
		operations.push_back(create_operation_json(op.get()));
	}

	j["op"] = operations;

	return j;
}

nlohmann::json IO::create_entity_group_json(const EntityGroup* egroup)
{
	nlohmann::json j;

	nlohmann::json entity_frames = nlohmann::json::array();
	egroup->lock_shared();
	for (const auto& ent : egroup->get_entities())
	{
		entity_frames.push_back(create_entity_frame_json(ent.get()));
	}
	egroup->unlock_shared();
	j["entities"] = entity_frames;

	return j;
}

nlohmann::json IO::create_plan_frame_json(const PlannerFrame* frame)
{
	nlohmann::json j;

	nlohmann::json entity_groups = nlohmann::json::array();
	frame->lock_shared();
	for (const auto& eg : frame->get_entity_groups())
	{
		if (!eg->get_entities().empty() && eg->get_entities().front()->get_type_id() != 2)
		{
			entity_groups.push_back(create_entity_group_json(eg.get()));
		}
	}
	frame->unlock_shared();

	j["entity_groups"] = entity_groups;

	return j;
}

template<typename T>
nlohmann::json IO::create_transformer_json(const ITransformer* transformer)
{
	nlohmann::json item;

	item["type"] = (unsigned int)transformer->get_type();

	switch (transformer->get_type())
	{
	case TransformerType::com:			create_feature_stats_json<T>(&static_cast<const Transformer_com<T>*>(transformer)->get_const_ref_feature()); break;
	case TransformerType::covariance:
	{
		create_feature_stats_json<T>(&static_cast<const Transformer_covariance<T>*>(transformer)->feature);
		create_feature_stats_json<T>(&static_cast<const Transformer_covariance<T>*>(transformer)->feature_magnitude);
		break;
	}
	case TransformerType::magnitude:
	{
		create_feature_stats_json<T>(&static_cast<const Transformer_magnitude<T>*>(transformer)->feature);
		create_feature_stats_json<T>(&static_cast<const Transformer_magnitude<T>*>(transformer)->feature_normal);
		break;
	}
	case TransformerType::range:		create_feature_stats_json<T>(&static_cast<const Transformer_range<T>*>(transformer)->feature); break;
	default: throw std::runtime_error("Transformer has no valid type!");
	}

	return item;
}

template<>
nlohmann::json IO::create_feature_stats_json<GVec2>(const IFeatureStats* stats)
{
	nlohmann::json item;

	item["type"] = (unsigned int)GType::GVec;

	item["min"] = to_json(static_cast<const FeatureStats<GVec2>*>(stats)->min);
	item["max"] = to_json(static_cast<const FeatureStats<GVec2>*>(stats)->max);
	item["mean"] = to_json(static_cast<const FeatureStats<GVec2>*>(stats)->mean);
	item["S"] = to_json(static_cast<const FeatureStats<GVec2>*>(stats)->S);
	item["sum"] = to_json(static_cast<const FeatureStats<GVec2>*>(stats)->sum);
	item["stdev"] = to_json(static_cast<const FeatureStats<GVec2>*>(stats)->stdev);
	item["count"] = static_cast<const FeatureStats<GVec2>*>(stats)->count;

	return item;
}

template<typename T>
nlohmann::json IO::create_feature_stats_json(const IFeatureStats* stats)
{
	nlohmann::json item;

	item["type"] = (unsigned int)stats->get_type();

	item["min"] = static_cast<const FeatureStats<T>*>(stats)->min;
	item["max"] = static_cast<const FeatureStats<T>*>(stats)->max;
	item["mean"] = static_cast<const FeatureStats<T>*>(stats)->mean;
	item["S"] = static_cast<const FeatureStats<T>*>(stats)->S;
	item["sum"] = static_cast<const FeatureStats<T>*>(stats)->sum;
	item["stdev"] = static_cast<const FeatureStats<T>*>(stats)->stdev;
	item["count"] = static_cast<const FeatureStats<T>*>(stats)->count;

	return item;
}



Generic IO::read_generic(const nlohmann::json& item)
{
	const auto& data = item["data"];
	switch (item["type"].get<GType>())
	{
	case Gint:		return Generic(data.get<int>());
	case Gdouble:	return Generic(data.get<double>());
	case GVec:		return Generic(from_json(data));
	default: throw std::runtime_error("Unknown type");
	}
}

std::shared_ptr<Entity> IO::read_entity(const nlohmann::json& ent)
{
	const auto& edata = ent["state"];
	DataMap state_map;

	const auto uid = ent["entity"].get<unsigned int>();
	// add the uid to the state_map to make things easier for goal assesment for the moment
	state_map.emplace(GAIA::ID, Generic((int)uid));
	state_map.emplace(GAIA::Type, Generic(ent["type"].get<int>()));

	for (const auto& state : edata)
	{
		const auto id = state["id"].get<unsigned int>();

		state_map.emplace(id, read_generic(state));
	}

	std::shared_ptr<Entity> entity = std::make_shared<ScenarioEntity>(state_map, uid);

	const auto actions = ent.find("actions");
	if (actions != ent.cend())
	{
		for (const auto& action : *actions)
		{
			entity->actions.push_back(read_action(action, entity.get()));
		}
	}

	return entity;
}

std::unique_ptr<EntityAction> IO::read_action(const nlohmann::json& action, Entity* ent)
{
	const auto id = action["id"].get<unsigned int>();
	const auto& data = action["data"];

	std::vector<ActionDataContainer> adata;
	for (const auto& d : data) { adata.push_back({ d["id"].get<unsigned int>(), (FeatureReferenceType)d["ref_type"].get<unsigned int>(), read_generic(d) }); }

	return std::make_unique<EntityAction>(id, adata, ent);
}

std::shared_ptr<IActionDomain> IO::read_action_range(const nlohmann::json& act)
{
	std::shared_ptr<IActionDomain> action_range;
	auto id = act["id"].get<unsigned int>();
	auto type = act["type"].get<unsigned int>();
	std::vector<unsigned int> entities = act["entity_types"];
	auto data = act["data"];

	if (type == 0)
	{
		std::vector<ActionRangeDataContainer> action_data;

		for (const auto& d : data)
		{
			action_data.push_back({ d["id"].get<unsigned int>(), (FeatureReferenceType)d["ref_type"].get<unsigned int>(), read_generic(d["min"]), read_generic(d["max"]) });
		}

		action_range = std::make_shared<ActionRange>(id, entities, action_data);
	}
	else if (type == 1)
	{
		std::vector<ActionSetDataContainer> action_data;

		for (const auto& d : data)
		{
			const auto& range = d["range"];

			std::vector<Generic> sets;
			for (const auto& r : range)
			{
				sets.push_back(read_generic(r));
			}

			action_data.push_back({ d["id"].get<unsigned int>(), (FeatureReferenceType)d["ref_type"].get<unsigned int>(), std::move(sets) });
		}

		action_range = std::make_shared<ActionSet>(id, entities, action_data);
	}

	return action_range;
}

std::shared_ptr<WorldFrame> IO::read_world_frame(const nlohmann::json & wframe)
{
	auto frame = std::make_shared<WorldFrame>();

	for (const auto& ent : wframe)
	{
		const auto type_id = ent["type"].get<unsigned int>();

		frame->add_entity(type_id, read_entity(ent));
	}

	return frame;
}

std::unique_ptr<Level> IO::read_level(const nlohmann::json& level)
{
	auto level_data = std::make_unique<Level>();
	level_data->id = level["id"].get<unsigned int>();
	level_data->name = level["name"].get<std::string>();
	level_data->comment = level["comment"].get<std::string>();

	// start frame
	const auto& level_start = level["start"];
	level_data->start = read_world_frame(level_start);

	// actors

	// read constraints
	const auto& constraints = level["constraints"];
	const auto& boundary = constraints["boundary"];
	const auto& ignore_features = constraints["ignore_features"];

	level_data->boundary_min = GVec2(boundary["xmin"].get<double>(), boundary["ymin"].get<double>());
	level_data->boundary_max = GVec2(boundary["xmax"].get<double>(), boundary["ymax"].get<double>());

	for (auto& feature : ignore_features)
	{
		level_data->ignore_features.push_back(feature.get<size_t>());
	}

	const auto& level_tests = level["tests"];

	for (const auto& test : level_tests)
	{
		level_data->tests.push_back(read_test(test));
	}

	return level_data;
}

Test IO::read_test(const nlohmann::json& test)
{
	Test test_data;
	const auto& test_goal = test["goal"];
	test_data.goal = read_world_frame(test_goal);

	// read restrictions
	const auto& restrictions = test.find("restrictions");

	if (restrictions != std::end(test))
	{
		const auto& restr = *restrictions;
		test_data.depth_max = restr["depth_max"].get<unsigned int>();
	}

	return test_data;
}
