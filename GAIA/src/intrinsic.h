#pragma once

#include <unordered_map>

namespace GAIA
{

/// Intrinsics which contain what the agent internal
/// state is regarding the associated feature
class Intrinsic
{
public:
	/// Constructor
	Intrinsic();
	virtual ~Intrinsic() = default;

	double operator++ ();

	double generate_interest() const;
	double get_novelty() const;

private:
	void update();

	double novelty;
	size_t count = 1;
};

}
