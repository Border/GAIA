#pragma once


#define _USE_MATH_DEFINES // must be defined before math.h (to access PI defines)

#include <math.h>
#include <cmath>
#include <float.h>
#include <assert.h>
#include <stdexcept>


/// A 2D vector with x-y components and optimised for 2D planar operations
template <typename T>
struct Vec2
{
	/// Constructor
	Vec2() = default;

	/// Construct using coordinates.
	Vec2(T _x, T _y) : x(_x), y(_y) {}

	/// Set this vector to all zeros.
	void SetZero() { x = 0; y = 0;}

	/// Set this vector to some specified coordinates.
	void Set(T _x, T _y) { x = _x; y = _y; }

	/// Negate this vector.
	Vec2<T> operator -() const { return Vec2<T>(-x, -y); }

	/// Multiply a vector by this vector.
	Vec2<T> operator * (const Vec2<T>& v)
	{
		return Vec2<T>(v.x * x, v.y * y);
	}

	/// Add a vector to this vector.
	void operator += (const Vec2<T>& v)
	{
		x += v.x; y += v.y;
	}

	/// Subtract a vector from this vector.
	void operator -= (const Vec2<T>& v)
	{
		x -= v.x; y -= v.y;
	}

	/// Multiply this vector by a scalar.
	void operator *= (double s)
	{
		x *= s; y *= s;
	}

	/// test if this vector is greater than a vector
	bool operator > (const Vec2<T>& v) const
	{
		return (x > v.x) && (y > v.y);
	}

	/// test if this vector is greater than a vector
	bool operator >= (const Vec2<T>& v) const
	{
		return (x >= v.x) && (y >= v.y);
	}

	/// test if this vector is less than a vector
	bool operator < (const Vec2<T>& v) const
	{
		return (x < v.x) && (y < v.y);
	}

	/// test if this vector is less than a vector
	bool operator <= (const Vec2<T>& v) const
	{
		return (x <= v.x) && (y <= v.y);
	}

	/// Gets the total length of the vector
	double Length() const
	{
		return std::sqrt(x * x + y * y);
	}

	/// Convert this vector into a unit vector. Returns the length.
	double Normalise()
	{
		double length = Length();
		if (length < DBL_EPSILON)
		{
			return 0.0;
		}
		double invLength = 1.0 / length;
		x *= invLength;
		y *= invLength;

		return length;
	}

	/// Sums all components together
	double Sum() const
	{
		return x + y;
	}

	/// Gets the normalised vector and returns it
	Vec2<T> GetNormalised() const
	{
		const double length = Length();
		if (length < DBL_EPSILON)
		{
			return Vec2<T>(0,0);
		}

		return Vec2<T>(x / length,y / length);
	}

	/// Calculates the angle of the vector in radians
	double Angle() const
	{
		return std::atan2(y,x);
	}

	/// make the vector absolute
	Vec2<T> Abs() const
	{
		x = std::abs(x);
		y = std::abs(y);

		return *this;
	}

	T x,y;
};

/// A 3D vector with x-y-z components and optimised for 3D world operations
template <typename T>
struct Vec3
{
	/// Constructor
	Vec3() { };

	/// Construct using coordinates.
	Vec3(T _x, T _y, T _z) : x(_x), y(_y), z(_z) {}

	/// Set this vector to all zeros.
	void SetZero() { x = 0; y = 0; z = 0; }

	/// Set this vector to some specified coordinates.
	void Set(T _x, T _y, T _z) { x = _x; y = _y; z = _z; }

	/// Negate this vector.
	Vec3<T> operator -() const { return Vec3<T>(-x, -y, -z); }

	/// Multiply a vector by this vector.
	Vec3<T> operator * (const Vec3<T>& v)
	{
		return Vec3(v.x * x, v.y * y, v.z * z);
	}

	/// Add a vector to this vector.
	void operator += (const Vec3<T>& v)
	{
		x += v.x; y += v.y; z += v.z;
	}

	/// Subtract a vector from this vector.
	void operator -= (const Vec3<T>& v)
	{
		x -= v.x; y -= v.y; z -= v.z;
	}

	/// Multiply this vector by a scalar.
	void operator *= (double s)
	{
		x *= s; y *= s; z *= s;
	}

	/// Gets the total length of the vector
	double Length() const
	{
		return std::sqrt(x * x + y * y + z * z);
	}

	/// Gets only the length of the xy components
	double Lengthxy() const
	{
		return std::sqrt(x * x + y * y);
	}

	/// Convert this vector into a unit vector. Returns the length.
	double Normalise()
	{
		double length = Length();
		if (length < DBL_EPSILON) { return 0.0; }
		double invLength = 1.0 / length;
		x = (T)((double)x * invLength);
		y = (T)((double)y * invLength);
		z = (T)((double)z * invLength);

		return length;
	}

	/// Sums all components together
	double Sum() const
	{
		return x + y + z;
	}

	/// Raises each element to the power specified
	Vec3<T> Pow(double p) const
	{
		return Vec3<T>(std::pow(x, p), std::pow(y, p), std::pow(z, p));
	}

	/// Gets the normalised vector and returns it
	Vec3<T> GetNormalised() const
	{
		double length = Length();
		if (length < DBL_EPSILON)
		{
			return Vec3<T>(0,0,0);
		}

		return Vec3<T>(x / length,y / length, z / length);
	}

	/// Gets the xy component and returns in a Vec2 variable
	Vec2<T> GetXY() const
	{
		return Vec2<T>(x, y);
	}

	/// Gets the polar coordinates of the vector (long, lat) in radians
	Vec2<T> Angle() const
	{
		return Vec2<T>(std::atan2(y, x), std::atan2(y, z));
	}

	/// make the vector absolute
	Vec3<T> Abs() const
	{
		x = std::abs(x);
		y = std::abs(y);
		z = std::abs(z);

		return *this;
	}

	/// Rotate about Z axis by a
	Vec3<T> RotateZ(double a) const
	{
		return Vec3<T>(
			x * std::cos(a) - y * std::sin(a),
			x * std::sin(a) + y * std::cos(a),
			z);
	}

	/// Rotate about Y axis by a
	Vec3<T> RotateY(double a) const 
	{
		return Vec3<T>(
			x *  std::cos(a) + z * std::sin(a),
			y,
			x * -std::sin(a) + z * std::cos(a));
	}

	/// Rotate about X axis by a
	Vec3<T> RotateX(double a) const
	{
		return Vec3<T>(
			x,
			y * std::cos(a) - z * std::sin(a),
			y * std::sin(a) + z * std::cos(a));
	}

	T x,y,z;
};



/// Add two vectors component-wise.
template <typename T>
inline Vec2<T> operator + (const Vec2<T>& a, const Vec2<T>& b)
{
	return Vec2<T>(a.x + b.x, a.y + b.y);
}

/// Subtract two vectors component-wise.
template <typename T>
inline Vec2<T> operator - (const Vec2<T>& a, const Vec2<T>& b)
{
	return Vec2<T>(a.x - b.x, a.y - b.y);
}

/// Multiply two vectors component-wise.
template <typename T>
inline Vec2<T> operator * (const Vec2<T>& a, const Vec2<T>& b)
{
	return Vec2<T>(a.x * b.x, a.y * b.y);
}

/// multiply 2 vectors component-wise
template <typename T>
inline Vec2<T> operator * (double s, const Vec2<T>& a)
{
	return Vec2<T>(s * a.x, s * a.y);
}

/// Divide a scalar s by a vector component-wise
template <typename T>
inline Vec2<T> operator / (double s, const Vec2<T>& a)
{
	return Vec2<T>(s / a.x, s / a.y);
}

/// Divide a vectors by scalar s component-wise
template <typename T>
inline Vec2<T> operator / (const Vec2<T>& a, double s)
{
	if (s == 0) { throw std::runtime_error("Divide by 0"); }

	return Vec2<T>(a.x / s, a.y / s);
}

/// Divide 2 vectors component-wise.
template <typename T>
inline Vec2<T> operator / (const Vec2<T>& a, const Vec2<T>& b)
{
	return Vec2<T>(a.x / b.x, a.y / b.y);
}

/// equate 2 vectors component-wise
template <typename T>
inline bool operator == (const Vec2<T>& a, const Vec2<T>& b)
{
	return a.x == b.x && a.y == b.y;
}

/// equate 2 vectors component-wise, return true if not equal
template <typename T>
inline bool operator != (const Vec2<T>& a, const Vec2<T>& b)
{
	return a.x != b.x && a.y != b.y;
}



/// Add two vectors component-wise.
template <typename T>
inline Vec3<T> operator + (const Vec3<T>& a, const Vec3<T>& b)
{
	return Vec3<T>(a.x + b.x, a.y + b.y, a.z + b.z);
}

/// Subtract two vectors component-wise.
template <typename T>
inline Vec3<T> operator - (const Vec3<T>& a, const Vec3<T>& b)
{
	return Vec3<T>(a.x - b.x, a.y - b.y, a.z - b.z);
}

/// Multiply two vectors component-wise.
template <typename T>
inline Vec3<T> operator * (const Vec3<T>& a, const Vec3<T>& b)
{
	return Vec3<T>(a.x * b.x, a.y * b.y, a.z * b.z);
}

/// multiply 2 vectors component-wise
template <typename T>
inline Vec3<T> operator * (double s, const Vec3<T>& a)
{
	return Vec3<T>(s * a.x, s * a.y, s * a.z);
}

/// Divide a vectors by scalar s component-wise
template <typename T>
inline Vec3<T> operator / (double s, const Vec3<T>& a)
{
	return Vec3<T>(s / a.x, s / a.y, s / a.z);
}

/// Divide a vectors by scalar s component-wise
template <typename T>
inline Vec3<T> operator / (const Vec3<T>& a, double s)
{
	if (s == 0) { throw std::runtime_error("Divide by 0"); }

	return Vec3<T>(a.x / s, a.y / s, a.z / s);
}

/// Divide 2 vectors component-wise.
template <typename T>
inline Vec3<T> operator / (const Vec3<T>& a, const Vec3<T>& b)
{
	return Vec3<T>(a.x / b.x, a.y / b.y, a.z / b.z);
}

/// equate 2 vectors component-wise
template <typename T>
inline bool operator == (const Vec3<T>& a, const Vec3<T>& b)
{
	return a.x == b.x && a.y == b.y && a.z == b.z;
}

/// equate 2 vectors component-wise
template <typename T>
inline bool operator != (const Vec3<T>& a, const Vec3<T>& b)
{
	return a.x != b.x && a.y != b.y && a.z != b.z;
}


/// perform cross product
template <typename T>
inline double vCross(const Vec2<T>& a, const Vec2<T>& b)
{
	return a.x * b.y - a.y * b.x;
}

/// perform cross product
template <typename T>
inline Vec3<T> vCross(const Vec3<T>& a, const Vec3<T>& b)
{
	return Vec3<T>(a.y * b.z - a.z * b.y
		         , a.z * b.x - a.x * b.z
		         , a.x * b.y - a.y * b.x);
}

/// determine the shortest signed angle between 2 vectors (-PI - PI)
template <typename T>
inline double vAngle(const Vec2<T>& a, const Vec2<T>& b)
{
	return std::atan2(vCross(a,b), vDot(a,b)); // perpendicular dot product
}

/// determine the shortest angle between 2 vectors (0 - PI)
template <typename T>
inline double vAngle(const Vec3<T>& a, const Vec3<T>& b)
{
	return std::atan2(vCross(a, b).Length(), vDot(a, b)); // perpendicular dot product
}

/// rotate vector about by 'angle'
template <typename T>
inline Vec2<T> vRotate(const Vec2<T>& a, double angle)
{
	return Vec2<T>(a.x * std::cos(angle) - a.y * std::sin(angle), a.x * std::sin(angle) + a.y * std::cos(angle));
}

/// rotate vector about by 'angle'
template <typename T>
inline Vec3<T> vRotate(const Vec3<T>& v, Vec3<T> angle)
{
	// need to impliment rotation about each axis
	return Vec3<T>(v);
}

/// returns the square root of each component the vector
template <typename T>
inline Vec2<T> vSqrt(const Vec2<T>& a)
{
	return Vec2<T>(std::sqrt(a.x), std::sqrt(a.y));
}

/// returns the square root of each component the vector
template <typename T>
inline Vec3<T> vSqrt(const Vec3<T>& a)
{
	return Vec3<T>(std::sqrt(a.x), std::sqrt(a.y), std::sqrt(a.z));
}

/// returns the absolute value of each component the vector
template <typename T>
inline Vec2<T> vAbs(const Vec2<T>& a)
{
	return Vec2<T>(std::abs(a.x), std::abs(a.y));
}



/// determine distance between 2 vectors
template <typename T>
inline double vDistance(const T& a, const T& b)
{
	return (a - b).Length();
}

/// perform dot product
template <typename T>
inline double vDot(const T& a, const T& b)
{
	return (a * b).Sum();
}

/// returns a number from -1 to 1 indicating the closeness to vect a being parallel to b. 1 is same direction and parallel, 0 is perpendicular, -1 is opposite direction and parallel
template <typename T>
inline double vPara(const T& a, const T& b)
{
	const double alen = a.Length();
	const double blen = b.Length();
	const double l = alen * blen;

	if(l > 0)
		return vDot(a,b) / l;
	else if((alen == 0) && (blen == 0))
		return 1;
	else
		return 0;
}

/// Vector from point to line = (a-p)-((a-p).n)n | n = normal (unit vector) of line, a = point on line, p = point to find vector to line
template <typename T>
inline T vVectorToLine(const T& n, const T& a, const T& p)
{
	T a_p = a - p;
	return a_p - vDot(a_p, n)*n;
}

/// Distance from point to line = (a-p)-((a-p).n)n | n = normal (unit vector) of line, a = point on line, p = point to find distance to line
template <typename T>
inline double vDistanceToLine(const T& n, const T& a, const T& p)
{
	return  vVectorToLine(n, a, p).Length();
}

/// returns true if the lines a1->a2 and b1->b2 intersect
template <typename T>
inline bool vLinesIntersect(const Vec2<T>& a1, const Vec2<T>& a2, const Vec2<T>& b1, const Vec2<T>& b2)
{
	Vec2<T> r = a2 - a1;
	Vec2<T> s = b2 - b1;
	Vec2<T> q_p = b1 - a1;

	double r_s = vCross(r,s);
	if(r_s == 0) { return false; } // lines are parralell in some way

	double t = vCross(q_p, s) / r_s;
	double u = vCross(q_p, r) / r_s;

	if((0 <= t) && (t <= 1) && (0 <= u) && (u <= 1)) { return true; }

	return false; // line intersects outside valid domain
}
