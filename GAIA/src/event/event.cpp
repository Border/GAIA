#include "event.h"
#include "entity.h"
#include "feature-classifier.h"
#include "feature-association.h"

using namespace GAIA;

Event::Event(std::vector<std::pair<unsigned int, FVector>>& _features, unsigned int uid) :
	id_unique(uid),
	features(std::move(_features))
{
	std::tie(hash_type, hash_unique) = generate_multi_hashes(features);
}

Event::Event(std::vector<std::pair<unsigned int, FVector>>& _features, unsigned int _hash_type, unsigned int _hash_unique, unsigned int uid) :
	id_unique(uid),
	features(std::move(_features)),
	hash_type(_hash_type),
	hash_unique(_hash_unique)
{

}

unsigned int Event::get_type_hash() const { return hash_type; }

unsigned int Event::get_unique_hash() const { return hash_unique; }

unsigned int Event::get_unique_id() const { return id_unique; }

size_t Event::get_size() const { return features.size(); }

bool Event::match(const FrameContext& context) const
{
	for (const auto& f_pair : features)
	{
		auto data = context.entity->delta.find(f_pair.first);

		if (data != std::end(context.entity->delta))
		{
			for (auto f : f_pair.second)
			{
				if (!GAIA::match_feature(f, context.feature_references, data->second))
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}

	return true;
}

bool Event::match_hash(unsigned int _hash) const { return hash_unique == _hash; }

bool Event::operator == (const Event* a) const { return hash_unique == a->get_unique_hash(); }

const std::vector<std::pair<unsigned int, FVector>>& Event::get_features() const { return features; }

CausalType Event::get_causal_type() const { return CausalType::Causal_Event; }
