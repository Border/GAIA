#include "event-classifier.h"
#include "feature-classifier.h"
#include "feature-container.h"
#include "feature-association.h"
#include "memory.h"

using namespace GAIA;

std::unique_ptr<EventClassifier> EventClassifier::singleton = nullptr;



EventClassifier::EventClassifier() :
	n_events(0),
	n_features(0)
{
}

EventClassifier::EventClassifier(const EventClassifier& evcl) :
	n_events(0),
	n_features(0)
{
}

EventClassifier* EventClassifier::Instance() { return singleton.get(); }

void EventClassifier::Create()
{
	if (singleton == nullptr)
	{
		singleton = std::make_unique<EventClassifier>(EventClassifier());
	}
}

Event* EventClassifier::compose_event(const std::unordered_map<unsigned int, std::vector<FeatureDataContext>>& event_feature_contexts)
{
	std::vector<std::pair<unsigned int, FVector>> event_features;

	for (const auto& feature_contexts : event_feature_contexts)
	{
		// return a matching series of features or a new series of features
		event_features.push_back(std::make_pair(feature_contexts.first, compose_features(feature_contexts.first, feature_contexts.second)));
	}

	// generate hashes from the event_features to see if there is an existing event
	auto hashes = generate_multi_hashes(event_features);
	auto event = match_event(hashes.second);

	if (event == nullptr)
	{
		auto result = m_events.emplace(hashes.second, std::make_unique<Event>(event_features, hashes.first, hashes.second, ++n_events));

		// the result should be true for insertion, otherwise an event already exists with the same hash
		assert(result.second);

		event = result.first->second.get();
	}

	return event;
}

std::vector<IFeature*> EventClassifier::compose_features(unsigned int id, const std::vector<FeatureDataContext>& feature_contexts)
{
	// Currently this does not create an IFeatureDataRetriever object, we are assuming
	// there is no dependent data atm. However, ideally here we attempt to identify any
	// dependencies, trying to find ways to make the data invariant based on various
	// features from the frame exemplars in the entity exemplars

	// find the best combination of features that most closely match the feature exemplars,
	// creating a new feature if necessary

	// obtain the feature data type
	const auto type = feature_contexts.front().data.get_type();

	// the list of features that best describe the feature exemplars
	std::vector<IFeature*> event_features;

	// create transformer(s) from feature exemplars
	auto transformers = FeatureClassifier::Instance()->make_feature_transformers(feature_contexts);

	// filter out all other features that are not of the desired transformers
	auto filtered_features = features.find(id);
	if (filtered_features != std::end(features))
	{
		// match created transformers with the filtered list of features
		// if no match, create a new feature using the transformer

		for (auto& t : transformers)
		{
			auto range = filtered_features->second.equal_range(t->get_type());
			std::vector<std::pair<double, IFeature*>> matched_list;

			for (; range.first != range.second; ++range.first)
			{
				auto match_error = range.first->second->match(t.get());

				if (match_error < 0.2)
				{
					matched_list.push_back(std::make_pair(match_error, range.first->second.get()));
				}
			}

			// no matches were found so create a new feature
			if (matched_list.empty())
			{
				event_features.push_back(make_feature(id, type, filtered_features->second, t));
			}
			else
			{
				// sort the list
				std::sort(std::begin(matched_list), std::end(matched_list), [](const std::pair<double, IFeature*>& a, const std::pair<double, IFeature*>& b) { return a.first < b.first; });
				// add the smallest error item to the state_features list
				event_features.push_back(matched_list.front().second);
			}
		}
	}
	else
	{
		// no features with this id exists yet, so make them
		for (auto& t : transformers)
		{
			EventClassifier::feature_map flist;
			event_features.push_back(make_feature(id, type, flist, t));
			features.emplace(id, std::move(flist));
		}
	}

	return event_features;
}

std::vector<Event*> EventClassifier::get_event_types(unsigned int type_hash) const
{
	std::vector<Event*> events;

	for (const auto& e : m_events)
	{
		if (e.second->get_type_hash() == type_hash)
		{
			events.push_back(e.second.get());
		}
	}

	return events;
}

IFeature* EventClassifier::make_feature(unsigned int id, GType type, EventClassifier::feature_map& filtered_feature, std::unique_ptr<ITransformer>& transformer)
{
	IFeature* fnew = nullptr;

	auto transformer_type = transformer->get_type();

	switch (type)
	{
	case Gint:		fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<int>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	case Gdouble:	fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<double>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	case GVec:		fnew = filtered_feature.emplace(transformer_type, std::make_unique<FeatureContainer<GVec2>>(
							id,
							n_features++,
							transformer))->second.get(); break;
	default:		throw std::runtime_error("Unknown type"); break;
	}

	return fnew;
}

Event* EventClassifier::match_event(unsigned int unique_hash) const
{
	auto result = m_events.find(unique_hash);

	return result != std::end(m_events) ? result->second.get() : nullptr;
}
