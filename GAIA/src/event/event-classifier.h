#pragma once

#include "event.h"
#include "feature.h"
#include "feature-transformer.h"
#include "entity-context.h"
#include "frame-context.h"
#include "feature-types.h"
#include "generic.h"

#include <mutex>
#include <unordered_map>
#include <vector>

namespace GAIA
{

class EventClassifier
{
	using feature_map = std::unordered_multimap<TransformerType, std::unique_ptr<IFeature>, std::hash<size_t>>;
protected:
	/// ctor - protected so it cant be created on stack
	EventClassifier();

public:
	/// copy ctor, but nothing should be coppied
	EventClassifier(const EventClassifier& evcl);

	/// Obtains an instance of this class
	/// \return The instance for this class
	static EventClassifier* Instance();

	/// Creates an instance
	static void Create();

	/// Composes a event, attempts to use an existing one if matches, otherwise creates a new one
	Event* compose_event(const std::unordered_map<unsigned int, std::vector<FeatureDataContext>>& event_feature_contexts);

	/// Composes one or more features to encompass the feature exemplars, either unsing existing known features or creating new ones
	std::vector<IFeature*> compose_features(unsigned int id, const std::vector<FeatureDataContext>& feature_contexts);

	/// Gets a list of events which match the event type hash
	std::vector<Event*> get_event_types(unsigned int type_hash) const;

private:
	/// Creates a new features based on the transformer
	IFeature* make_feature(unsigned int id, GType type, feature_map& filtered_feature, std::unique_ptr<ITransformer>& transformer);

	/// Returns an event if it matches the unique hash
	Event* match_event(unsigned int unique_hash) const;

	/// protects access
	/// Maybe use shared mutex to handle writes, allowing reads to occur simultaneously
	mutable std::mutex m_wframe;

	/// list of event feature grouped based on thier type, this owns them
	std::unordered_map<unsigned int, feature_map> features;

	/// list of events that form affordances
	std::unordered_map<unsigned int, std::unique_ptr<Event>> m_events;

	/// the current number of event based features known to the agent
	unsigned int n_features;

	/// The number of events the agent has created
	unsigned int n_events;

	/// A singleton
	static std::unique_ptr<EventClassifier> singleton;
};

}
