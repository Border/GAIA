#pragma once

#include "feature.h"
#include "feature-types.h"
#include "frame-context.h"
#include "causality.h"

#include <vector>
#include <unordered_map>

namespace GAIA
{

/// Desscribes the change bewteen 2 frames using a series of known features
class Event : public Effect
{
	friend class IO;
	friend class DebugDisplay;
public:
	/// Constructor - copy features and generate hashes
	Event(std::vector<std::pair<unsigned int, FVector>>& features, unsigned int uid);
	/// Constructor - copy features and hashes
	Event(std::vector<std::pair<unsigned int, FVector>>& features, unsigned int hash_type, unsigned int hash_unique, unsigned int uid);

	/// returns the type hash - a hash generated from the type id
	unsigned int get_type_hash() const;
	/// returns the unique hash - a hash generated from the unique id
	unsigned int get_unique_hash() const;
	///  returns the id indicating the type of feature
	unsigned int get_unique_id() const override;
	/// Returns the number of features that make up the association
	size_t get_size() const;
	/// Returns true if the FrameContext delta matches the features
	bool match(const FrameContext& context) const override;
	/// Returns true if the hash matches
	bool match_hash(unsigned int _hash) const;
	/// Returns true if the associations matches
	bool operator == (const Event* a) const;
	/// Checks if the specified feature type id is present in the association
	const std::vector<std::pair<unsigned int, FVector>>& get_features() const;
	/// returns the causal type
	CausalType get_causal_type() const override;

protected:
	/// a unique id for generating hashes of groups of features
	const unsigned int id_unique;
	/// the hash of the features unique id's that form the association
	unsigned int hash_unique;
	/// the hash of the feature id's that for the association
	unsigned int hash_type;

	/// vector of fvectors which describe the event
	std::vector<std::pair<unsigned int, FVector>> features;
};

typedef std::unordered_multimap<unsigned int, Event> EventMap;

}
