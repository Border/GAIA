#include "intrinsic.h"

#include <cmath>

using namespace GAIA;

namespace
{
	constexpr double S_A = 12.20703125;
};

Intrinsic::Intrinsic()
{
	update();
}

double Intrinsic::operator++ ()
{
	count++;

	update();

	return novelty;
}

double Intrinsic::generate_interest() const
{
	// TODO: use other values to generate interest, for the moment just return the novelty
	return novelty;
}

double Intrinsic::get_novelty() const { return novelty; }

void Intrinsic::update()
{
	const double b = 1 - std::exp(-0.2 * count);
	const double t = std::exp(-b * count);
	novelty = S_A * t * std::pow(1.0 - t, 4);
}
