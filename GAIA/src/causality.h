#pragma once

#include "feature-types.h"

namespace GAIA
{

enum CausalType
{
	Causal_Affordance,
	Causal_Event,
	Causal_Action
};

struct FrameContext;

/// Causal interface. Anything with this interface can be a causal source
class Cause
{
public:
	/// virtual destructor
	virtual ~Cause() = default;
	/// returns true if this cause matches the supplied one
	virtual bool match(Cause* cause) const = 0;
	/// matches the effect based on the supplied FVector
	virtual bool match(const FrameContext& fex) const = 0;
	/// returns the causal type
	virtual CausalType get_causal_type() const = 0;
	/// returns the causality id
	virtual unsigned int get_unique_id() const = 0;
};

using CVector = std::vector<Cause*>;
using CIter = std::vector<Cause*>::iterator;


/// Effect interface. Anything with this interface can be an effect source
class Effect
{
public:
	/// virtual destructor
	virtual ~Effect() = default;
	/// returns the causal type
	virtual CausalType get_causal_type() const = 0;
	/// matches the effect based on the supplied FrameContext
	virtual bool match(const FrameContext& fex) const = 0;
	/// returns the causality id
	virtual unsigned int get_unique_id() const = 0;
};

using EVector = std::vector<Effect*>;
using EIter = std::vector<Effect*>::iterator;

}
