#include "worldframe.h"

using namespace GAIA;

WorldFrame::WorldFrame() : complexity(0.0), frame(0) { }

WorldFrame::WorldFrame(size_t _frame) : complexity(0.0), frame(_frame) { }

void WorldFrame::add_data(unsigned int type, Generic&& _data) { data.emplace(type, _data); }

void WorldFrame::add_entity(unsigned int type, std::shared_ptr<Entity>& ent) { entities.emplace(type, ent); }

void WorldFrame::add_entity(unsigned int type, std::shared_ptr<Entity>&& ent) { entities.emplace(type, ent); }

std::weak_ptr<WorldFrame> WorldFrame::get_previous_frame() const { return prev; }

void WorldFrame::set_previous_frame(std::weak_ptr<WorldFrame> frame) { prev = frame; }

const EntityMap& WorldFrame::get_entities() const { return entities; }

EntityMap& WorldFrame::get_entities_mut() { return entities; }

std::shared_ptr<Entity> WorldFrame::find_entity(unsigned int pid) const
{
	for (const auto& ent : entities)
	{
		if (ent.second->get_persistent_id() == pid)
		{
			return ent.second;
		}
	}

	return nullptr;
}

std::shared_ptr<Entity> WorldFrame::get_entity(unsigned int id) const
{
	auto entity = entities.find(id);
	if (entity != std::end(entities))
	{
		return entity->second;
	}

	return nullptr;
}

std::shared_ptr<WorldFrame> WorldFrame::copy(const std::function<std::shared_ptr<Entity>(std::shared_ptr<Entity>)>& allocator) const
{
	auto fcopy = std::make_shared<WorldFrame>(frame);

	fcopy->data = this->data;

	for (const auto& ent : this->entities)
	{
		fcopy->entities.emplace(ent.first, allocator(ent.second));
	}

	return fcopy;
}

std::shared_ptr<WorldFrame> WorldFrame::copy_next(const std::function<std::shared_ptr<Entity>(std::shared_ptr<Entity>)>& allocator) const
{
	auto fcopy = std::make_shared<WorldFrame>(frame + 1);

	fcopy->data = this->data;

	for (const auto& ent : this->entities)
	{
		fcopy->entities.emplace(ent.first, allocator(ent.second));
	}

	return fcopy;
}

size_t WorldFrame::get_frame_number() const { return frame; }
