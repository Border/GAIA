#include "planner-dependency.h"
#include "planner-entity.h"
#include "affordance-action.h"
#include "affordance.h"
#include "entity.h"
#include "mhash.h"
#include "container-algorithms.h"

#include <algorithm>

using namespace GAIA;

ConditionEntityDependencies::ConditionEntityDependencies(AffordanceCondition* cond) :
	condition(cond)
{
	for (const auto& s : cond->get_states())
	{
		dependency_matches.push_back(EntityFrameMatches({ s, {} }));
	}
}

void ConditionEntityDependencies::add_matching_entity(EntityFrame* ent, FeatureContext& context)
{
	// TODO: need some way to add feature context data to use for matching

	for (auto& dep : dependency_matches)
	{
		if (dep.state->match(ent->state, context))
		{
			// keep the vector sorted by unique id
			calg::insert_sorted(dep.matched_entity_frames, ent,
				[](EntityFrame* e1, EntityFrame* e2) { return e1->unique_id < e2->unique_id; });
		}
	}
}

bool ConditionEntityDependencies::match_result() const
{
	for (auto& dep : dependency_matches)
	{
		if (dep.matched_entity_frames.empty()) { return false; }
	}

	return true;
}

/// recursive function for generating permutations (stores them in perms)
template<typename Iter>
void recurse_deps_perm(
	const Iter& iter,
	const Iter& end,
	EFVector temp,
	EntityPermutations& perms,
	EntityFrame* eframe)
{
	auto it = std::next(iter);
	if (it != end)
	{
		for (const auto ent : it->matched_entity_frames)
		{
			// we ignore ent's that are the same as this entity (eframes have the same pid)
			// and only add to temp if the entity is not already in it
			if (ent->get_persistent_id() != eframe->get_persistent_id() &&
				std::none_of(std::begin(temp), std::end(temp),
					[ent](EntityFrame* ef) { return ef != ent; }))
			{
				temp.push_back(ent);
				recurse_deps_perm(it, end, temp, perms, eframe);
				temp.pop_back();
			}
			else
			{
				recurse_deps_perm(it, end, temp, perms, eframe);
			}
		}
	}
	else
	{
		if (!temp.empty())
		{
			std::sort(
				std::begin(temp),
				std::end(temp),
				[](EntityFrame* e1, EntityFrame* e2) {
				return e1->get_persistent_id() < e2->get_persistent_id() ?
					true : e1->unique_id < e2->unique_id;
			});

			// generate a hash of based on the entities unique id
			auto hash = generate_hash(std::begin(temp), std::end(temp),
				[](EntityFrame* ef) { return ef->unique_id; });

			// only add if not a duplicate by comparing hashes
			if (std::none_of(std::begin(perms), std::end(perms),
				[hash](const std::pair<unsigned int, EFVector>& x) { return x.first != hash; }))
			{
				perms.push_back(std::make_pair(hash, std::move(temp)));
			}
		}
	}
}

EntityPermutations ConditionEntityDependencies::generate_dependency_permutations(EntityFrame* eframe) const
{
	// a list of different combinations of entity frames that are required for the condition to be matched
	EntityPermutations perm_list;

	const auto begin = std::begin(dependency_matches);
	const auto end = std::end(dependency_matches);
	for (const auto ent : begin->matched_entity_frames)
	{
		EFVector temp;
		if (ent->get_persistent_id() != eframe->get_persistent_id())
		{
			temp.push_back(ent);
		}

		recurse_deps_perm<EntityFrameMatchesList::const_iterator>(begin, end, temp, perm_list, eframe);
	}

	return perm_list;
}

CauseEntityDependencies::CauseEntityDependencies(Cause* cause)
{
	// TODO: a cause can consist of multiple causes itself
	// if cause is action, ignore
	// if cause is affordance, add
	// if cause is a list of causes, iterate over it and perform the last 2 steps (maybe use recursive function)

	dependency_matches.push_back(EntityFrameMatches({ cause, {} }));
}

void CauseEntityDependencies::add_matching_entity(EntityFrame* ent)
{
	// Searches for an entity with an operation with the appropriate affordance.
	// The action of the affordance will be another affordance, so search for entities with operations that are generated from the affordance.
	// Then when a match is found, add the cause entity to the effect entities dependency list

	// Also add the effect entity to the list of possible entities the cause entity can use to produce the operation.
	// Only do this when calling check_cause_entity or verify_causes, or maybe even at some resolve_dependencies stage.
	// Entities could be culled for various reasons, so we don't want to add them to the cause entity unless we know they are vlaid.

	for (auto& op : ent->operations)
	{
		for (auto& eframe_matches : dependency_matches)
		{
			if (op->affd->get_unique_id() == eframe_matches.cause->get_unique_id())
			{
				// add this ent as a dependency
				eframe_matches.matched_entity_frames.push_back(ent);
			}
		}
	}
}

bool CauseEntityDependencies::match_result() const
{
	for (auto& dep : dependency_matches)
	{
		if (dep.matched_entity_frames.empty()) { return false; }
	}

	return true;
}

EntityPermutations CauseEntityDependencies::generate_dependency_permutations(EntityFrame* eframe) const
{
	// a list of different combinations of entity frames that are required for the condition to be matched
	EntityPermutations perm_list;

	const auto begin = std::begin(dependency_matches);
	const auto end = std::end(dependency_matches);
	for (const auto ent : begin->matched_entity_frames)
	{
		EFVector temp;
		if (ent->get_persistent_id() != eframe->get_persistent_id())
		{
			temp.push_back(ent);
		}

		recurse_deps_perm<EntityFrameMatchesList::const_iterator>(begin, end, temp, perm_list, eframe);
	}

	return perm_list;
}



ActionFailureEntityDependencies::ActionFailureEntityDependencies(AffordanceCondition* cond, EntityFrame* entframe) :
	condition(cond),
	eframe(entframe)
{
	for (const auto& s : cond->get_states())
	{
		dependency_matches.push_back(std::make_pair(s, std::map<unsigned int, std::pair<EntityGroup*, std::vector<EntityFrame*>>>()));
	}
}

void ActionFailureEntityDependencies::match_entity(EntityFrame* ent, EntityGroup* egrp, const FeatureContext& context)
{
	for (auto& dep : dependency_matches)
	{
		if (dep.first->match(ent->state, context))
		{
			auto pid = ent->get_persistent_id();
			auto item = dep.second.find(pid);

			if (item != std::end(dep.second))
			{
				item->second.second.push_back(ent);
			}
			else
			{
				std::vector<EntityFrame*> ef_list({ ent });
				dep.second.emplace(pid, std::make_pair(egrp, std::move(ef_list)));
			}
		}
	}
}

std::pair<ActionFailureEntityDependencies::DependencyResult, EFVector> ActionFailureEntityDependencies::action_valid() const
{
	DependencyResult result = NonDependent;
	EFVector ignore_list;

	for (auto& dep : dependency_matches)
	{
		// if matches exist
		if (!dep.second.empty())
		{
			// there were some cases where an entities state matched the fail condition
			// state of the affordance action

			// if all the entities within the entity group matched a fail condition
			// this means that there is no possible solution and the operation
			// cannot be performed in this current state, so return failed

			for (auto& d : dep.second)
			{
				const auto size = d.second.second.size();
				const auto esize = d.second.first->get_entities().size();

				const auto diff = std::abs((int)esize - (int)size);

				// diff should never be less than 0
				assert(diff >= 0);

				if (diff == 0)
				{
					if (esize == 1)
					{
						if (eframe->unique_id != d.second.second.front()->unique_id)
						{
							// there was only 1 entity in the entity group and it matched
							// the fail state condition, so set result to fail and return.
							// We need to ignore if it is this entity though.

							return std::make_pair(Fail, ignore_list);
						}
					}
					else
					{
						// all entities produced fail state conditions, so there are no
						// other alternative entities to avoid the fail state, so set
						// result to fail and return.

						return std::make_pair(Fail, ignore_list);
					}
				}

				// there are other entities which can be used to avoid the fail conditions
				result = Dependent;
				ignore_list.insert(std::end(ignore_list), std::begin(d.second.second), std::end(d.second.second));
			}
		}
		else
		{
			// if this state did not match, the entire condition does not match (regardless
			// of other states that match), hence return non dependent
			result = NonDependent;
			break;
		}
	}

	return std::make_pair(result, ignore_list);
}



DependencyContainer::DependencyContainer(EntityFrame* eframe) :
	eframe(eframe),
	valid(true)
{
}

void DependencyContainer::add_conditions(const ACVector& conditions)
{
	for (auto cond : conditions)
	{
		condition_entity_dependencies.push_back(ConditionEntityDependencies(cond));
	}
}

void DependencyContainer::add_causes(const CVector& causes)
{
	// for each cause generate a feature context
	if (causes.size() == 1)
	{
		feature_context = FeatureContext(causes.front(), eframe);
	}
	else
	{
		// TODO: Not implemented for multiple causes
		assert("Unimplemented" && false);
	}
}

void DependencyContainer::check_entity(EntityFrame* ent)
{
	for (auto& cond_dep : condition_entity_dependencies)
	{
		cond_dep.add_matching_entity(ent, feature_context);
	}
}

bool DependencyContainer::verify()
{
	if (valid)
	{
		for (const auto& dep : condition_entity_dependencies)
		{
			if (!dep.match_result())
			{
				valid = false;
				break;
			}
		}
	}

	return valid;
}

bool DependencyContainer::is_valid() const { return valid; }

EntityPermutations DependencyContainer::generate_dependency_permutations(EntityFrame* ceframe) const
{
	EntityPermutations perm_list;

	for (auto& deps : condition_entity_dependencies)
	{
		auto perms = deps.generate_dependency_permutations(ceframe);

		perm_list.insert(std::end(perm_list), std::make_move_iterator(std::begin(perms)), std::make_move_iterator(std::end(perms)));
	}

	// Sort smallest to largest
	std::sort(std::begin(perm_list), std::end(perm_list),
		[](const std::pair<unsigned int, EFVector>& a, const std::pair<unsigned int, EFVector>& b)
		{
			return a.second.size() < b.second.size();
		}
	);

	// TODO: Also check if the pid is the same, if so make sure the unique id of the entity frame are the same,
	// otherwise the permutation is invalid.

	return perm_list;
}

const FeatureContext& DependencyContainer::get_feature_context() const
{
	return feature_context;
}


InitialDependencyContainer::InitialDependencyContainer(EntityFrame* eframe) : DependencyContainer(eframe)
{
}

void InitialDependencyContainer::add_causes(const CVector& causes)
{
	// Call the base method, or maybe just also handle it in here rather than iteratting over causes multiple times.
	DependencyContainer::add_causes(causes);

	// TODO: each cause should be able to contain multiple actions/affordances (or do all causes need to be matched)
	for (auto c : causes)
	{
		if (c->get_causal_type() == CausalType::Causal_Action)
		{
			std::vector<ActionFailureEntityDependencies> fail_conds;
			auto affd_action = static_cast<AffordanceAction*>(c);
			for (auto fcond : affd_action->get_fail_conditions())
			{
				fail_conds.push_back(ActionFailureEntityDependencies(fcond, eframe));
			}
			affordance_actions.push_back({ affd_action, std::move(fail_conds) });
		}
		else if (c->get_causal_type() == CausalType::Causal_Affordance)
		{
			// Add this to a cause dependency list to check later.
			cause_dependencies.push_back(CauseEntityDependencies(c));
		}
	}
}

void InitialDependencyContainer::check_cause_entity(EntityFrame* ent, EntityGroup* eg)
{
	for (auto& affd_action : affordance_actions)
	{
		for (auto& fail_cond_dep : affd_action.action_failure_entity_dependencies)
		{
			fail_cond_dep.match_entity(ent, eg, feature_context);
		}
	}

	// Check if ent has an operation whos affordance matches one in the cause dependency list
	// If it does, then add ent as a dependency for eframe.
	for (auto& cause_dep : cause_dependencies)
	{
		cause_dep.add_matching_entity(ent);
	}
}

void InitialDependencyContainer::check_entity(EntityFrame* ent)
{
	// if the entity is in the ignore list then skip it
	auto filter = std::find_if(std::begin(ignore_entities), std::end(ignore_entities), [&](EntityFrame* eframe) { return eframe == ent; });
	if (filter != std::end(ignore_entities)) { return; }

	for (auto& cond_dep : condition_entity_dependencies)
	{
		cond_dep.add_matching_entity(ent, feature_context);
	}
}

bool InitialDependencyContainer::verify_causes()
{
	if (affordance_actions.size() == 1)
	{
		auto& affd_action = affordance_actions.front();
		ActionFailureEntityDependencies::DependencyResult dependency_result = ActionFailureEntityDependencies::NonDependent;

		for (const auto& dep : affd_action.action_failure_entity_dependencies)
		{
			auto result = dep.action_valid();
			if (result.first == ActionFailureEntityDependencies::Fail)
			{
				// as there is only 1 failure condition (and it has been matched), the
				// operation is no longer valid
				valid = false;
				dependency_result = result.first;
				break;
			}
			else if (result.first == ActionFailureEntityDependencies::Dependent)
			{
				// get the entities which cause an action fail condition to be matched and
				// add them to the ignore list
				ignore_entities.insert(std::end(ignore_entities), std::make_move_iterator(std::begin(result.second)), std::make_move_iterator(std::end(result.second)));
				dependency_result = result.first;
			}
			else if (result.first == ActionFailureEntityDependencies::NonDependent)
			{
				// Nothing matched the fail conditions
			}
		}

		affd_action.dependency_result = dependency_result;
	}
	else
	{
		// multi action for single entity
		assert("Unimplemented" && false);
	}

	bool failed = false;
	// Only need one to match for this to be valid
	for (const auto& dep : cause_dependencies)
	{
		failed |= ! dep.match_result();
	}

	return valid &= ! failed;
}

EntityPermutations InitialDependencyContainer::generate_dependency_permutations(EntityFrame* ceframe) const
{
	EntityPermutations cond_perm_list;

	for (auto& deps : condition_entity_dependencies)
	{
		auto perms = deps.generate_dependency_permutations(ceframe);

		cond_perm_list.insert(std::end(cond_perm_list), std::make_move_iterator(std::begin(perms)), std::make_move_iterator(std::end(perms)));
	}

	// now generate the cause dependency permutations
	EntityPermutations cause_perm_list;

	for (auto& deps : cause_dependencies)
	{
		auto perms = deps.generate_dependency_permutations(ceframe);

		cause_perm_list.insert(std::end(cause_perm_list), std::make_move_iterator(std::begin(perms)), std::make_move_iterator(std::end(perms)));
	}

	// a list of different combinations of entity frames that are required for the conditions and/or causes to be matched
	EntityPermutations perms;

	// Combine the 2 permutations together

	for (auto& eperma : cond_perm_list)
	{
		for (auto& epermb : cause_perm_list)
		{
			EFVector temp = eperma.second;
			// only add to temp if the entity is not already in it

			for (auto& ent : epermb.second)
			{
				if (std::none_of(std::begin(temp), std::end(temp),
						[ent](EntityFrame* ef) { return ef != ent; }))
				{
					temp.push_back(ent);
				}
			}

			// generate a hash of based on the entities unique id
			auto hash = generate_hash(std::begin(temp), std::end(temp),
				[](EntityFrame* ef) { return ef->unique_id; });

			// only add if not a duplicate by comparing hashes
			if (std::none_of(std::begin(perms), std::end(perms),
				[hash](const std::pair<unsigned int, EFVector>& x) { return x.first != hash; }))
			{
				perms.push_back(std::make_pair(hash, std::move(temp)));
			}
		}
	}

	// Sort smallest to largest
	std::sort(std::begin(perms), std::end(perms),
		[](const std::pair<unsigned int, EFVector>& a, const std::pair<unsigned int, EFVector>& b)
		{
			return a.second.size() < b.second.size();
		}
	);

	// TODO: Also check if the pid is the same, if so make sure the unique id of the entity frame are the same,
	// otherwise the permutation is invalid.

	return perms;
}
