#include "planner-genealogy.h"
#include "planner-entity.h"
#include "entity.h"
#include "mhash.h"

#include <algorithm>
#include <cassert>
#include <tuple>

using namespace GAIA;

EntityFrameGenealogy::EntityFrameGenealogy(EntityFrame* frame) :
	pid(frame->get_persistent_id()),
	eframe_ancestors({ frame }),
	hash_unique(0)
{
}

EntityFrameGenealogy::EntityFrameGenealogy(const EntityFrameGenealogy& parent, EntityFrame* frame):
	eframe_ancestors(parent.eframe_ancestors),
	pid(parent.pid),
	hash_unique(0)
{
	add_eframe_descendant(frame);
}

const EntityFrame* EntityFrameGenealogy::get_current() const
{
	// it cant be empty
	assert(!eframe_ancestors.empty());

	return eframe_ancestors.back();
}

const EntityFrame* EntityFrameGenealogy::get_level(unsigned int level) const
{
	// it cant be empty
	assert(!eframe_ancestors.empty());

	auto match = std::find_if(std::begin(eframe_ancestors), std::end(eframe_ancestors),
		[level](EntityFrame* eframe) { return eframe->depth == level; });

	return match != std::end(eframe_ancestors) ? *match : nullptr;
}

bool EntityFrameGenealogy::has_common_ancestor(const EntityFrame* eframe) const
{
	auto ancestor_frame = get_current();

	if (ancestor_frame->depth == eframe->depth)
	{
		// depth already matches, so check if the ids match
		return ancestor_frame->unique_id == eframe->unique_id;
	}
	else
	{
		// depth doesnt match, so we need to dig deeper
		// check each dependency possibility
		for (auto& dep : eframe->dependencies)
		{
			// get the ancestor frame from the same level as the eframe
			auto ancestor_match_frame = dep.frame_genealogy.get_level(ancestor_frame->depth);

			// if the ancestor_match_frame is nullptr, it is likely the ancestor frame no logner exists
			if (ancestor_match_frame != nullptr && ancestor_match_frame->unique_id == eframe->unique_id)
			{
				return true;
			}
		}

		// nothing matched when checked at the same level (or an ancestor dependency is
		// no longer present)
		return false;
	}
}

bool EntityFrameGenealogy::is_ancestor(const EntityFrame* eframe) const
{
	return std::any_of(std::begin(eframe_ancestors), std::end(eframe_ancestors),
		[eframe](const EntityFrame* ef) { return ef->unique_id == eframe->unique_id;});
}

void EntityFrameGenealogy::add_eframe_descendant(EntityFrame* eframe)
{
	// the entities must be of the same type
	assert(eframe->get_persistent_id() == pid);

	// there can not be another entity of the same level that already exists
	auto match = std::find_if(std::begin(eframe_ancestors), std::end(eframe_ancestors),
		[eframe](EntityFrame* ef) { return ef->depth == eframe->depth; });
	assert(match == std::end(eframe_ancestors));

	eframe_ancestors.push_back(eframe);

	hash_unique = generate_hash(std::begin(eframe_ancestors), std::end(eframe_ancestors),
		[](const EntityFrame* ef) { return ef->unique_id; });
}

unsigned int EntityFrameGenealogy::get_pid() const { return pid; }

unsigned int EntityFrameGenealogy::get_unique_hash() const { return hash_unique; }

const std::vector<EntityFrame*>& EntityFrameGenealogy::get_ancestors() const { return eframe_ancestors; }

bool EntityFrameGenealogy::merge(const EntityFrameGenealogy& genealogy)
{
	// we assume the entire genealogy back to the root is listed in eframe_ancestors

	// check the unique hash's first
	if (hash_unique == genealogy.hash_unique) { return true; }

	auto iter_a = std::begin(eframe_ancestors);
	auto iter_b = std::begin(genealogy.eframe_ancestors);

	for (;;)
	{
		if (iter_a != std::end(eframe_ancestors))
		{
			if (iter_b != std::end(genealogy.eframe_ancestors))
			{
				if ((*iter_a)->depth == (*iter_b)->depth &&
					(*iter_a)->unique_id != (*iter_b)->unique_id)
				{
					return false;
				}
				else
				{
					++iter_a;
					++iter_b;
				}
			}
			else
			{
				// dont need to iterate any more
				return true;
			}
		}
		else
		{
			if (iter_b != std::end(genealogy.eframe_ancestors))
			{
				eframe_ancestors.push_back(*iter_b);
				iter_a = eframe_ancestors.end();
				++iter_b;
			}
			else
			{
				// everything is matched and merged and we reached the end for both iterators
				break;
			}
		}
	}

	// now update the hash value as frames were added to eframe_ancestors
	hash_unique = generate_hash(std::begin(eframe_ancestors), std::end(eframe_ancestors),
		[](const EntityFrame* ef) { return ef->unique_id; });

	return true;
}



EntityDependencyGenealogy::EntityDependencyGenealogy(std::vector<EntityFrameGenealogy>& deps, unsigned int type_hash) :
	hash_type(type_hash),
	dependencies(std::move(deps))
{
}

bool EntityDependencyGenealogy::has_genealogy_conflict(const EntityFrame* eframe) const
{
	// get the dependency with the same pid (if it exists)
	const auto pid = eframe->get_persistent_id();
	auto entity_dep = std::find_if(std::begin(dependencies), std::end(dependencies),
		[pid](const EntityFrameGenealogy& efg) { return efg.get_pid() == pid; });

	// if the pid does not exist, then there has been no previous enitity that is part of
	// this dependency, so its valid and there are no conflicts.
	if (entity_dep == std::end(dependencies))
	{
		return false;
	}
	else
	{
		return !entity_dep->has_common_ancestor(eframe);
	}
}

std::vector<std::tuple<unsigned int, unsigned int, std::vector<EntityFrameGenealogy>>> EntityDependencyGenealogy::generate_child_dependencies(
	const EntityFrameGenealogy& frame_genealogy,
	const std::vector<EntityFrame*>& matched_dependencies) const
{
	// For the purpose of clarity for this function, we call this objects genealogy list of
	// dependencies the parent_dependencies (its actual variable name in here is
	// dependencies) and the input list of entitiy frames the matched_dependencies. To make
	// it easier to follow the process, we define an example, where the frame currently
	// being generated is [E53] with pid=4

	// parent_dependencies: [E1->E6->E12]
	// matched_dependencies: [E34][E37]

	// The parent_dependencies is simply carried over from the parent frame dependencies.
	// matched_dependencies is the list of dependencies that have been matched as required
	// states/conditions for this frame to be produced. We (this program/function)
	// currently do not know their genealogy. However, it is listed here for reference:

	// [E31]: [E1->E6->E12->E21->E31]

	// [E34]: [E2->E8->E15->E25->E34]
	// [E37]: [E3->E10->E16->E27->E37]
	// [E53]: [E4->E11->E20->E30->E39->E53]

	// In order to check the genealogies of the matched_dependencies frames, we need to
	// match up the existing 'carried over' dependencies from the parent_dependencies to
	// the matched_dependencies frames e.g:

	//     pid |        1        |       2       |       3       |
	//  parent |  [E1->E6->E12]  |       -       |       -       |
	// matched |        -        |     [E34]     |     [E37]     |

	// As can be seen, there are 3 entities this frame depends on in its lifetime. However,
	// the dependencies of E34 and E37 are currently not factored in (they could depend on
	// other entities).

	std::vector<std::tuple<unsigned int, unsigned int, std::vector<EntityFrameGenealogy>>> new_dependencies;

	if (matched_dependencies.empty()) { return new_dependencies; }

	std::vector<std::tuple<unsigned int, const EntityFrame*, const EntityFrameGenealogy*>> linked_dependencies;

	for (const auto eframe : matched_dependencies)
	{
		// get the dependency with the same pid (if it exists)
		auto pid = eframe->get_persistent_id();
		auto entity_dep = std::find_if(std::begin(dependencies), std::end(dependencies),
			[pid](const EntityFrameGenealogy& efg) { return efg.get_pid() == pid; });

		// if the pid does not exist, then there has been no previous enitity that is part of
		// this dependency
		if (entity_dep == std::end(dependencies))
		{
			linked_dependencies.push_back(std::make_tuple(pid, eframe, nullptr));
		}
		else
		{
			// if there is an ancesor conflict, the frame cannot be generated
			// Note: this might already have been done when initially adding the
			//       EntityFrame as a dependency in ConditionEntityDependencies
			if (entity_dep->has_common_ancestor(eframe))
			{
				return new_dependencies;
			}

			linked_dependencies.push_back(std::make_tuple(pid, eframe, &(*entity_dep)));
		}
	}

	// also add parent_dependencies for pid's that were not in matched_dependencies
	for (const auto& dep : dependencies)
	{
		auto pid = dep.get_pid();
		if (std::none_of(std::begin(matched_dependencies), std::end(matched_dependencies),
			[pid](const EntityFrame* ef) { return ef->get_persistent_id() == pid; }))
		{
			linked_dependencies.push_back(std::make_tuple(pid, nullptr, &dep));
		}
	}

	// The parent_dependencies and matched_dependencies are now linked and direct ancestor
	// conflicts are checked. Now we need to check for conflicts with the dependencies of
	// the matched_dependencies. We only need to do this for cases where there is no
	// parent_dependencies linked (i.e [E34] and [E37]).

	// However, as each EntityFrame can have more than one parent frame (due to a merged
	// frame), they can have different dependencies for each parent, hence we need to
	// factor it in. First we need to permute all the possible combinations. In this case,
	// if each frame only has one parent, there will only be one permutation. If [E34] had
	// two parents, there would be 2 permutations, if [E37] also had two parents, there
	// would be 4 permutations.

	// as an example only, the 4 permutations could be:

	// 1: [E2->E8->E15->E25->E34][E3->E10->E16->E27->E37]
	// 2: [E2->E8->E15->E25->E34][E3->E10->E17->E28->E37]
	// 3: [E2->E8->E15->E24->E34][E3->E10->E16->E27->E37]
	// 4: [E2->E8->E15->E24->E34][E3->E10->E17->E28->E37]

	// To generate this, we find the links where an EntityFrame exists, e.g. [E34] and
	// [E37].

	std::vector<depgene_vector> permutations;

	{
		auto begin = std::begin(linked_dependencies);

		// skip to next item if the EntityFrame is nullptr
		while (begin != std::end(linked_dependencies) && std::get<1>(*begin) == nullptr)
		{
			++begin;
		}

		// there should be at least 1 Entity Frame, so no need to check for begin being at
		// the end of the vector

		for (const auto& dep : std::get<1>(*begin)->dependencies)
		{
			linked_permutation_recurse(begin, std::end(linked_dependencies), { &dep }, permutations);
		}
	}

	// Now that we have a list of the possible combinations of primary frame genealogies
	// accounting for multiple parent genealogies, we can generate the permutations for
	// their dependencies. For example, the dependencies for [E34] and [E37] are:

	// [E2->E8->E15->E25->E34]:  [E1->E6->E12->E21][E4->E11->E20->E30][E3->E10->E16]
	// [E3->E10->E16->E27->E37]: [E2->E8->E15->E25][E4->E11->E20->E30][E1->E6->E12]

	// However, to illustrate this example, [E34] and [E37] also have an alternate list of
	// dependencies which are valid:

	// [E2->E8->E15->E25->E34]:  [E4->E11->E20->E30][E3->E10->E16->27]
	// [E3->E10->E16->E27->E37]: [E2->E8->E15->E25][E1->E6->E12]

	// This would generate the following permutations:

	// 1: [E1->E6->E12->E21][E4->E11->E20->E30][E3->E10->E16] && [E2->E8->E15->E25][E4->E11->E20->E30][E1->E6->E12]
	// 2: [E1->E6->E12->E21][E4->E11->E20->E30][E3->E10->E16] && [E2->E8->E15->E25][E1->E6->E12]
	// 3: [E4->E11->E20->E30][E3->E10->E16->27] && [E2->E8->E15->E25][E4->E11->E20->E30][E1->E6->E12]
	// 4: [E4->E11->E20->E30][E3->E10->E16->27] && [E2->E8->E15->E25][E1->E6->E12]

	// in order to filter out permutation with conflicting genealogies, they are compared
	// with each other. If there are conflicts, the permutation is removed. Hence we get:

	// 1: [E1->E6->E12->E21][E2->E8->E15->E25][E3->E10->E16][E4->E11->E20->E30]
	// 2: [E1->E6->E12->E21][E2->E8->E15->E25][E3->E10->E16][E4->E11->E20->E30]
	// 3: [E1->E6->E12][E2->E8->E15->E25][E3->E10->E16->27][E4->E11->E20->E30]
	// 4: [E1->E6->E12][E2->E8->E15->E25][E3->E10->E16->27][E4->E11->E20->E30]

	// We see that there are duplicates, so we remove duplicates:

	// 1: [E1->E6->E12->E21][E2->E8->E15->E25][E3->E10->E16][E4->E11->E20->E30]
	// 2: [E1->E6->E12][E2->E8->E15->E25][E3->E10->E16->27][E4->E11->E20->E30]

	// we also want to check the frame being generated [E53] genealogy has no ancestor
	// conflicts with the permutations. So the pid is used to search for entities of the
	// same pid as [E53]: [E4->E11->E20->E30->E39->E53]. In this case we find
	// [E4->E11->E20->E30], which has no conflicting genealogies and is therefore valid.

	// However, we don't want entities of the genealogy of the frame [E53] (aka same pid)
	// being generated, so we remove them from the permutation list:

	// 1: [E1->E6->E12->E21][E2->E8->E15->E25][E3->E10->E16]
	// 2: [E1->E6->E12][E2->E8->E15->E25][E3->E10->E16->27]

	std::vector<std::vector<const EntityFrame*>> dependency_perms;

	for (auto& perm : permutations)
	{
		auto begin = std::begin(perm);

		// if dependency_genealogy is empty, nothing is added to new_dependencies
		if ((*begin)->dependency_genealogy.empty())
		{
			dependency_permutation_recurse(begin, perm, std::vector<const EntityDependencyGenealogy*>(), new_dependencies, frame_genealogy);
		}
		else
		{
			for (const auto& g : (*begin)->dependency_genealogy)
			{
				dependency_permutation_recurse(begin, perm, { &g }, new_dependencies, frame_genealogy);
			}
		}
	}

	// However, we still need to check the genealogies of these with parent_dependencies
	// that were not linked with a matched_dependency. In this case [E1->E6->E12], which as
	// we can see is valid for both cases. There may be cases where the parent dependency
	// is actually deeper than the permutation dependency. E.g is may be that the parent
	// dependency is [E1->E6->E12->E21], but the permutation dependency is only
	// [E1->E6->E12]. In this case, the parent dependency would be used instead.

	// After all this, we return the list of generated dependencies
	return new_dependencies;
}

bool EntityDependencyGenealogy::empty() const { return dependencies.empty(); }

unsigned int EntityDependencyGenealogy::get_hash() const { return hash_type; }

bool EntityDependencyGenealogy::contains_eframe(const EntityFrame* eframe) const
{
	for (auto& dep : dependencies)
	{
		if (dep.get_current()->unique_id == eframe->unique_id)
		{
			return true;
		}
	}
	return false;
}

std::vector<const EntityFrame*> EntityDependencyGenealogy::get_current_dependencies() const
{
	std::vector<const EntityFrame*> cdeps;

	for (const auto& dep : dependencies)
	{
		cdeps.push_back(dep.get_current());
	}

	return cdeps;
}

const std::vector<EntityFrameGenealogy>& EntityDependencyGenealogy::get_dependencies() const { return dependencies; }


void GAIA::linked_permutation_recurse(
	linked_iter iter,
	const linked_iter& end,
	depgene_vector temp,
	std::vector<depgene_vector>& perms)
{
	++iter;
	// skip to next item if the EntityFrame is nullptr or the EntityFrameGenealogy is not nullptr
	while (iter != end && std::get<1>(*iter) == nullptr)
	{
		++iter;
	}


	if (iter != end)
	{
		for (const auto& dep : std::get<1>(*iter)->dependencies)
		{
			temp.push_back(&dep);
			GAIA::linked_permutation_recurse(iter, end, temp, perms);
			temp.pop_back();
		}
	}
	else
	{
		perms.push_back(std::move(temp));
	}
}

void GAIA::dependency_permutation_recurse(
	depgene_vector::iterator iter,
	const depgene_vector& depgen,
	std::vector<const EntityDependencyGenealogy*> temp,
	efg_permutations& new_dependencies,
	const EntityFrameGenealogy& frame_genealogy)
{
	++iter;
	if (iter != std::end(depgen))
	{
		if ((*iter)->dependency_genealogy.empty())
		{
			GAIA::dependency_permutation_recurse(iter, depgen, temp, new_dependencies, frame_genealogy);
		}
		else
		{
			for (const auto& g : (*iter)->dependency_genealogy)
			{
				temp.push_back(&g);
				GAIA::dependency_permutation_recurse(iter, depgen, temp, new_dependencies, frame_genealogy);
				temp.pop_back();
			}
		}
	}
	else
	{
		// in order to filter out permutations with conflicting genealogies, they are
		// compared with each other.

		std::vector<EntityFrameGenealogy> dependencies;

		for (const auto dep : depgen)
		{
			dependencies.push_back(dep->frame_genealogy);
		}

		for (const auto p : temp)
		{
			for (const auto& dep : p->get_dependencies())
			{
				const unsigned int pid = dep.get_pid();
				auto dep_cmp = std::find_if(std::begin(dependencies), std::end(dependencies),
					[pid](const EntityFrameGenealogy& efg) { return efg.get_pid() == pid; });

				if (dep_cmp != std::end(dependencies))
				{
					if (!dep_cmp->merge(dep))
					{
						// conflict detected, so this permutation is not valid!
						return;
					}
				}
				else
				{
					// the pid doesn't exist in the dependencies list, so add it
					dependencies.push_back(dep);
				}
			}
		}

		// check for conflicts with the parent frame's genealogy
		{
			const unsigned int pid = frame_genealogy.get_pid();
			auto dep_cmp = std::find_if(std::begin(dependencies), std::end(dependencies),
				[pid](const EntityFrameGenealogy& efg) { return efg.get_pid() == pid; });

			if (dep_cmp != std::end(dependencies))
			{
				if (dep_cmp->merge(frame_genealogy))
				{
					// no conflicts, which is good, but we dont want this genealogy in this
					// new entity frames genealogy, as we already have it in a seperate
					// section
					dependencies.erase(dep_cmp);
				}
				else
				{
					// conflict detected, so this permutation is not valid!
					return;
				}
			}
		}

		// Note: there could be some optimisations which do all this during permutation
		//       generation, rather than once the whole permutation is created. This could
		//       improve the best case scenarios, where there is a conflict early on, so
		//       rather than continuing generating potentially exponential permutations, we
		//       exit early.

		// if all valid and not empty, generate the hash

		if (!dependencies.empty())
		{
			auto unique_hash = generate_hash(std::begin(dependencies), std::end(dependencies),
				[](const EntityFrameGenealogy& efg) { return efg.get_unique_hash(); });

			// check if the hash already exists
			if (std::none_of(std::begin(new_dependencies), std::end(new_dependencies),
				[unique_hash](const std::tuple<unsigned int, unsigned int, std::vector<EntityFrameGenealogy>>& x)
			{ return std::get<1>(x) == unique_hash; }))
			{
				auto type_hash = generate_hash(std::begin(dependencies), std::end(dependencies),
					[](const EntityFrameGenealogy& efg) { return efg.get_pid(); });

				new_dependencies.push_back(std::make_tuple(type_hash, unique_hash, std::move(dependencies)));
			}
		}
	}
}



DependencyGenealogy::DependencyGenealogy(const std::shared_ptr<Operation>& operation, EntityFrame* parent):
	op(operation),
	frame_genealogy(EntityFrameGenealogy(parent))
{
}

DependencyGenealogy::DependencyGenealogy(const DependencyGenealogy& gen, const std::shared_ptr<Operation>& operation, EntityFrame* parent):
	op(operation),
	frame_genealogy(gen.frame_genealogy),
	dependency_genealogy(gen.dependency_genealogy)
{
	frame_genealogy.add_eframe_descendant(parent);
}



efg_permutations GAIA::make_dependencies(
	const EntityFrameGenealogy& frame_genealogy,
	const std::vector<EntityFrame*>& matched_dependencies)
{
	std::vector<std::tuple<unsigned int, unsigned int, std::vector<EntityFrameGenealogy>>> new_dependencies;

	if (matched_dependencies.empty()) { return new_dependencies; }

	std::vector<std::tuple<unsigned int, const EntityFrame*, const EntityFrameGenealogy*>> linked_dependencies;
	std::vector<depgene_vector> permutations;
	std::vector<std::vector<const EntityFrame*>> dependency_perms;

	for (const auto eframe : matched_dependencies)
	{
		linked_dependencies.push_back(std::make_tuple(eframe->get_persistent_id(), eframe, nullptr));
	}

	{
		auto begin = std::begin(linked_dependencies);
		// there should be at least 1 Entity Frame, so no need to check for begin being at
		// the end of the vector

		for (const auto& dep : std::get<1>(*begin)->dependencies)
		{
			GAIA::linked_permutation_recurse(begin, std::end(linked_dependencies), { &dep }, permutations);
		}
	}

	for (auto& perm : permutations)
	{
		auto begin = std::begin(perm);

		// if dependency_genealogy is empty, nothing is added to new_dependencies
		if ((*begin)->dependency_genealogy.empty())
		{
			GAIA::dependency_permutation_recurse(begin, perm, std::vector<const EntityDependencyGenealogy*>(), new_dependencies, frame_genealogy);
		}
		else
		{
			for (const auto& g : (*begin)->dependency_genealogy)
			{
				GAIA::dependency_permutation_recurse(begin, perm, { &g }, new_dependencies, frame_genealogy);
			}
		}
	}

	return new_dependencies;
}
