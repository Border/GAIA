#pragma once

#include "planner-frame.h"

#include <vector>
#include <atomic>
#include <mutex>
#include <memory>

namespace GAIA
{

class WorldFrame;

/// Contains all parameters which control the behaviour of the planner
struct PlanParams
{
	/// default ctor
	explicit PlanParams(const std::shared_ptr<WorldFrame>& gframe);

	/// The maximum depth of nodes to generate and search down
	unsigned int max_depth;

	/// the plan should start paused
	bool paused;

	/// the number of threads to use for the planner thread pool
	unsigned int threads;

	/// indicates the planner should use debug mode
	bool debug;

	/// the goal frame
	std::shared_ptr<WorldFrame> goal;
};

/// A container for a plan and temporary data for generating a plan
class Plan
{
	friend class Planner;
	friend class IO;
public:
	/// ctor
	Plan(PlanParams params, const WorldFrame* wframe);

	/// returns a list of the entity frames that match the goal
	std::vector<std::shared_ptr<EntityFrame>> get_goal_success_frames() const;

	/// adds a new planner frame to the plan if one does not exist
	void generate_frame(PlannerFrame* pframe);

	/// adds a frame that was determined to have reached the goal state
	void add_goal_frame(const std::shared_ptr<EntityFrame>& goal_frame);

	/// returns the number of solutions to the goal that were found
	size_t get_num_entity_solutions() const;

	/// returns a pointer to the current frame
	PlannerFrame* get_current_frame() const;

	/// returns the max depth of the plan
	size_t get_max_depth() const;

	/// returns a pointer to the frame at the specified depth
	const PlannerFrame* get_frame_at_depth(unsigned int depth) const;

private:
	/// Parameters which control the behaviour of the planner
	PlanParams params;

	/// the frames of the planner
	std::vector<std::unique_ptr<PlannerFrame>> frames;

	/// the frames where the goal is matched
	std::vector<std::shared_ptr<EntityFrame>> goal_success_frames;

	/// the current number of entities (used to give entities a unique id)
	std::atomic_uint entity_count;

	/// the current number of entity groups (used to give entity groups a unique id)
	std::atomic_uint entity_group_count;

	/// frame access
	std::mutex m_frame;

	/// goal frame access
	std::mutex m_goal;
};

}
