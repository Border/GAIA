#pragma once

#include "planner-operation.h"
#include "planner-genealogy.h"
#include "entity-interface.h"
#include "entity-persistent.h"
#include "datamap.h"

#include <mutex>
#include <shared_mutex>

namespace GAIA
{

enum EntitFrameState
{
	// fail values
	fail_generate = 0,
	fail_evaluate,
	fail_final_dependency,
	fail_final_dependency_resolve,
	// merged
	merged,
	// currently valid or success values
	valid,
	success,
	success_merged,
	success_goal,
};

class EntityFrame : public EntityInterface
{
public:
	/// ctor - the ctor for the root entity frame
	EntityFrame(const EntityInterface* ent, unsigned int uni_id);
	/// the ctor - requires the previos entity frame and the operation performed to create
	/// this frame
	EntityFrame(std::shared_ptr<EntityFrame> parent, const std::shared_ptr<Operation>& operation, unsigned int uni_id);

	/// Get the type ID of the entity frame
	/// \return The type id of the entity frame
	tid_t get_type_id() const override;

	/// returns the persistent entity id for this entity
	uid_t get_persistent_id() const override;

	/// returns a shared_ptr of the persitent entity
	std::shared_ptr<PersistentEntity> get_persistent_entity() const override;

	/// Gets a reference of the state
	/// \return A const reference to the state of this entity
	const DataMap& get_state() const override;

	/// returns a list of affordances the entity has experienced
	std::vector<Affordance*> get_affordances() const override;

	/// returns a list of all the causes for this entity
	CVector get_causes() const override;

	/// returns a list of entity actions
	std::vector<std::shared_ptr<EntityAction>> get_entity_actions() const override;

	/// returns the previously linked entity
	std::shared_ptr<EntityInterface> get_prev_entity() const override;

	/// adds the entity to the child list
	void add_child_entity(const std::shared_ptr<EntityFrame>& child);

	/// returns false if there were any genealogy conflicts
	bool has_genealogy_conflict(const EntityFrame* eframe) const;

	/// returns true if the entity state is a match
	bool is_match(const EntityFrame* eframe) const;

	/// merges eframe with this EntityFrame
	void merge(EntityFrame* eframe);


	/// the parent entity
	std::shared_ptr<EntityFrame> parent;

	/// the unique id of the frame
	const uid_t unique_id;

	/// the depth of this frame
	const size_t depth;

	/// Contains the raw state values of this entity
	const DataMap state;

	/// the distance to the goal
	DataMap goal_distance;

	/// the current state of the entity
	EntitFrameState frame_state;

	/// the operation performed to create this frame
	std::shared_ptr<Operation> op;

	/// Persistent data pertaining to this entity
	std::shared_ptr<PersistentEntity> pentity;

	/// keeps track of the dependencies of the operation taken to produce this entity frame
	std::vector<DependencyGenealogy> dependencies;

	/// The list of available operations for this frame
	std::vector<std::shared_ptr<Operation>> operations;

	/// Child nodes of possible frames after this one
	std::vector<std::shared_ptr<EntityFrame>> nodes;

	/// protects access to the entities child nodes
	std::mutex m_nodes;
};

class EntityGroup
{
public:
	/// ctor - initial
	EntityGroup(unsigned int pid, unsigned int uid);

	/// ctor
	EntityGroup(unsigned int pid, unsigned int uid, unsigned int depth);

	/// returns the next entity group
	EntityGroup* get_next() const;

	/// returns the previous entity group
	EntityGroup* get_prev() const;

	/// sets the next entity group for this entity and set this entity as the previous for
	/// the next
	void set_next(EntityGroup* next);

	/// adds the entity frame to this entity group
	void add_entity_frame(const std::shared_ptr<EntityFrame>& eframe);

	/// removes the specified entity frame from this group if it is present
	void remove_entity_frame(const std::shared_ptr<EntityFrame>& eframe);

	/// write lock for syncronising access to entities
	void lock();

	/// write unlock for syncronising access to entities
	void unlock();

	/// read lock for syncronising access to entities
	void lock_shared() const;

	/// read unlock for syncronising access to entities
	void unlock_shared() const;

	/// obtains a const ref of the list of entities
	const std::vector<std::shared_ptr<EntityFrame>>& get_entities() const;

	/// obtains a const ref of the list of entities
	std::vector<std::shared_ptr<EntityFrame>>& get_entities_mut();

	/// returns the number of entities in this group
	size_t num_entities() const;

	/// returns the persistent id of the entity this group represents
	unsigned int get_persistent_id() const;

	/// returns the unique id of the entity group
	unsigned int get_unique_id() const;

	/// returns the current depth of the entity frame in the plan tree
	unsigned int get_depth() const;

private:
	/// the entities linked to this group
	std::vector<std::shared_ptr<EntityFrame>> entities;

	/// the persistent id of the entity
	const unsigned int persistent_id;

	/// a unqiue id given to this entity group
	const unsigned int unique_id;

	/// the depth of this entity in the plan tree
	const unsigned int depth;

	/// the entity group from the previous frame
	EntityGroup* prev;

	/// the entity group from the next frame
	EntityGroup* next;

	/// mutex for multi threaded access control
	mutable std::shared_mutex m_entity_group;

	/// mutex for controlling access to the next variable member
	std::mutex m_next;
};

}
