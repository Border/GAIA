#include "planner-operation.h"
#include "affordance.h"

using namespace GAIA;

Operation::Operation() :
	affd(nullptr)
{
}

Operation::Operation(Affordance* affordance, DataMap delta) :
	affd(affordance),
	delta(std::move(delta))
{

}
