#include "planner-frame.h"
#include "affordance.h"
#include "affordance-action.h"
#include "entity.h"
#include "generic.h"

#include <algorithm>

using namespace GAIA;

PlannerFrame::PlannerFrame() :
	next_frame(nullptr)
{
}

PlannerFrame::PlannerFrame(const WorldFrame* wframe, std::atomic_uint& ecount, std::atomic_uint& egcount) :
	next_frame(nullptr)
{
	// generates the components for planner frame from the world frame
	for (auto& entity : wframe->get_entities())
	{
		auto egroup = std::make_unique<EntityGroup>(entity.second->get_persistent_id(), egcount++);
		auto eframe = std::make_shared<EntityFrame>(entity.second.get(), ecount++);
		egroup->add_entity_frame(eframe);

		entity_groups.push_back(std::move(egroup));
	}
}

void PlannerFrame::add_entity_group(std::unique_ptr<EntityGroup>& egroup)
{
	std::lock_guard<std::shared_mutex> lock(m_pframe);
	entity_groups.push_back(std::move(egroup));
}

const std::vector<std::unique_ptr<EntityGroup>>& PlannerFrame::get_entity_groups() const
{
	return entity_groups;
}

PlannerFrame* PlannerFrame::get_next_frame() const { return next_frame; }

void PlannerFrame::set_next_frame(PlannerFrame* nframe) { next_frame = nframe; }

void PlannerFrame::lock() { m_pframe.lock(); }

void PlannerFrame::unlock() { m_pframe.unlock(); }

void PlannerFrame::lock_shared() const { m_pframe.lock_shared(); }

void PlannerFrame::unlock_shared() const { m_pframe.unlock_shared(); }



FrameGroup::FrameGroup(const std::shared_ptr<EntityFrame>& e_frame, EntityGroup* e_group, PlannerFrame* p_frame) :
	eframe(e_frame),
	egroup(e_group),
	pframe(p_frame)
{}

FrameGroup::FrameGroup(EntityGroup* e_group, PlannerFrame* p_frame) :
	egroup(e_group),
	pframe(p_frame)
{
}

std::optional<InitialDependencyContainer> FrameGroup::find_initial_affordance_dependencies(const Affordance* affd) const
{
	InitialDependencyContainer dependencies(eframe.get());

	dependencies.add_causes(affd->causes);

	// find all entities whos state matches the affordance action fail conditions
	pframe->lock_shared();
	for (const auto& eg : pframe->get_entity_groups())
	{
		eg->lock_shared();
		for (const auto& ent : eg->get_entities())
		{
			if (ent->frame_state >= valid && !eframe->has_genealogy_conflict(ent.get()))
			{
				dependencies.check_cause_entity(ent.get(), eg.get());
			}
		}
		eg->unlock_shared();
	}
	pframe->unlock_shared();

	// check the causes are valid, if not return
	if (dependencies.verify_causes())
	{
		if (process_dependencies(affd->init_cond, dependencies))
		{
			return { dependencies };
		}
	}

	return std::nullopt;
}

bool FrameGroup::find_final_affordance_dependencies(const Affordance* affd, EntityFrame* prev_eframe) const
{
	DependencyContainer dependencies(prev_eframe);

	dependencies.add_causes(affd->causes);

	return process_dependencies(affd->final_cond, dependencies);
}

std::vector<std::shared_ptr<Operation>> FrameGroup::generate_operations(Affordance* affd, InitialDependencyContainer& dependencies) const
{
	std::vector<std::shared_ptr<Operation>> operations;

	auto deltas = affd->generate_deltas(dependencies.get_feature_context());

	for (auto& d : deltas)
	{
		operations.push_back(std::make_shared<Operation>(affd, std::move(d)));
	}

	return operations;
}

EntityGroup* FrameGroup::get_next_entity_group(std::atomic_uint& egcount)
{
	egroup->lock();

	auto egroup_next = egroup->get_next();
	if (egroup_next == nullptr)
	{
		// create a new entity group as it does not exist yet
		std::unique_ptr<EntityGroup> egrp = std::make_unique<EntityGroup>(eframe->get_persistent_id(), egcount++, eframe->depth+1);
		egroup_next = egrp.get();

		egroup->set_next(egrp.get());

		assert(pframe->get_next_frame() != nullptr);

		pframe->get_next_frame()->add_entity_group(egrp);
	}

	egroup->unlock();

	return egroup_next;
}

bool FrameGroup::resolve_dependencies(std::vector<std::shared_ptr<EntityFrame>>& invalid)
{
	// if the list is empty, just return now
	if (invalid.empty()) { return true; }

	for (auto it_dep = std::begin(eframe->dependencies); it_dep != std::end(eframe->dependencies);)
	{
		auto& dependency_genealogy = it_dep->dependency_genealogy;
		if (!dependency_genealogy.empty())
		{
			for (auto it = std::begin(dependency_genealogy); it != std::end(dependency_genealogy);)
			{
				auto invalid_dependency = std::find_if(std::begin(invalid), std::end(invalid),
					[&](const std::shared_ptr<EntityFrame>& frame) { return it->contains_eframe(frame.get()); });
				if (invalid_dependency != std::end(invalid))
				{
					it = dependency_genealogy.erase(it);
				}
				else
				{
					++it;
				}
			}

			if (dependency_genealogy.empty())
			{
				// the dependencies were all removed, so this dependency is no longer valid
				it_dep = eframe->dependencies.erase(it_dep);
			}
			else
			{
				++it_dep;
			}
		}
		else
		{
			++it_dep;
		}
	}

	return !eframe->dependencies.empty();
}

bool FrameGroup::process_dependencies(const ACVector& conditions, DependencyContainer& dependencies) const
{
	// enumerate the list of conditions
	dependencies.add_conditions(conditions);

	pframe->lock_shared();
	for (const auto& eg : pframe->get_entity_groups())
	{
		eg->lock_shared();
		for (const auto& ent : eg->get_entities())
		{
			if (ent->frame_state >= valid && !eframe->has_genealogy_conflict(ent.get()))
			{
				dependencies.check_entity(ent.get());
			}
		}
		eg->unlock_shared();
	}
	pframe->unlock_shared();

	bool valid = false;

	if (dependencies.verify())
	{
		// get all the permutations of dependencies
		auto perms = dependencies.generate_dependency_permutations(eframe.get());

		valid = true;

		if (!perms.empty())
		{
			for (auto& dep : eframe->dependencies)
			{
				// check ancestor conflicts
				std::vector<EntityDependencyGenealogy> dependency_genealogy;

				for (auto& p : perms)
				{
					if (dep.dependency_genealogy.empty())
					{
						// there are no genealogies yet, so no need to check for
						// duplicates, just make directly
						add_new_dependencies(dependency_genealogy,
							make_dependencies(dep.frame_genealogy, p.second));
					}
					else
					{
						for (const auto& dep_gene : dep.dependency_genealogy)
						{
							add_new_dependencies(dependency_genealogy,
								dep_gene.generate_child_dependencies(dep.frame_genealogy, p.second));
						}
					}
				}

				if (dependency_genealogy.empty())
				{
					valid = false;
				}
				else
				{
					dep.dependency_genealogy = std::move(dependency_genealogy);
				}
			}
		}
	}

	return valid;
}

void FrameGroup::add_new_dependencies(std::vector<EntityDependencyGenealogy>& dependency_genealogy,
	std::vector<std::tuple<unsigned int, unsigned int, std::vector<EntityFrameGenealogy>>>&& perms) const
{
	for (auto& child_deps : perms)
	{
		// only add if it is unique (hash does not match existing)
		const auto hash = std::get<0>(child_deps);

		if (std::none_of(std::begin(dependency_genealogy), std::end(dependency_genealogy),
			[hash](const EntityDependencyGenealogy& dep) { return dep.get_hash() == hash; }))
		{
			dependency_genealogy.push_back(EntityDependencyGenealogy(std::get<2>(child_deps), hash));
		}
	}
}



FrameGroupQueueItem::FrameGroupQueueItem() :
	mode(none),
	goal(false)
{
}

FrameGroupQueueItem::FrameGroupQueueItem(const std::shared_ptr<EntityFrame>& e_frame, EntityGroup* e_group, PlannerFrame* p_frame) :
	mode(generate),
	current_frame(std::make_unique<FrameGroup>(e_frame, e_group, p_frame))
{
}

bool FrameGroupQueueItem::operator>(const FrameGroupQueueItem& fgqi) const
{
	// this > fgqi

	const bool is_gen_mode = fgqi.mode == generate || fgqi.mode == generate_ping || fgqi.mode == generate_pong;

	switch (mode)
	{
		case generate:
		case generate_ping:
		case generate_pong:
		{
			if (is_gen_mode)
			{
				return current_frame->egroup->get_depth() > fgqi.current_frame->egroup->get_depth();
			}
			else if(fgqi.mode == merge || fgqi.mode == evaluate || fgqi.mode == resolve || fgqi.mode == add)
			{
				return current_frame->egroup->get_depth() >= fgqi.next_frame->egroup->get_depth();
			}
		}
		case merge:
		{
			if (is_gen_mode)
			{
				return next_frame->egroup->get_depth() > fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == merge)
			{
				return next_frame->egroup->get_depth() > fgqi.next_frame->egroup->get_depth();
			}
			else if (fgqi.mode == evaluate || fgqi.mode == resolve || fgqi.mode == add)
			{
				return next_frame->egroup->get_depth() >= fgqi.next_frame->egroup->get_depth();
			}
		}
		case evaluate:
		{
			if (is_gen_mode)
			{
				return next_frame->egroup->get_depth() > fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == merge || fgqi.mode == evaluate)
			{
				return next_frame->egroup->get_depth() > fgqi.next_frame->egroup->get_depth();
			}
			else if (fgqi.mode == resolve || fgqi.mode == add)
			{
				return next_frame->egroup->get_depth() >= fgqi.next_frame->egroup->get_depth();
			}
		}
		case resolve:
		{
			if (is_gen_mode)
			{
				return next_frame->egroup->get_depth() > fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == merge || fgqi.mode == evaluate)
			{
				return next_frame->egroup->get_depth() >= fgqi.next_frame->egroup->get_depth();
			}
			else if (fgqi.mode == resolve || fgqi.mode == add)
			{
				return next_frame->egroup->get_depth() > fgqi.next_frame->egroup->get_depth();
			}
		}
		case add:
		{
			if (is_gen_mode)
			{
				return next_frame->egroup->get_depth() > fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == merge || fgqi.mode == evaluate || fgqi.mode == resolve)
			{
				return next_frame->egroup->get_depth() >= fgqi.next_frame->egroup->get_depth();
			}
			else if (fgqi.mode == add)
			{
				return next_frame->egroup->get_depth() > fgqi.next_frame->egroup->get_depth();
			}
		}
	}

	return false;
}

bool FrameGroupQueueItem::operator<(const FrameGroupQueueItem& fgqi) const
{
	// this < fgqi

	const bool is_gen_mode = fgqi.mode == generate || fgqi.mode == generate_ping || fgqi.mode == generate_pong;

	switch (mode)
	{
		case generate:
		case generate_ping:
		case generate_pong:
		{
			if (is_gen_mode)
			{
				return current_frame->egroup->get_depth() < fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == evaluate || fgqi.mode == resolve || fgqi.mode == add)
			{
				return current_frame->egroup->get_depth() < fgqi.next_frame->egroup->get_depth();
			}
			else if(fgqi.mode == merge)
			{
				return next_frame->egroup->get_depth() < fgqi.next_frame->egroup->get_depth();
			}
		}
		case merge:
		{
			if (is_gen_mode)
			{
				return next_frame->egroup->get_depth() <= fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == merge)
			{
				return next_frame->egroup->get_depth() < fgqi.next_frame->egroup->get_depth();
			}
			else if (fgqi.mode == evaluate || fgqi.mode == resolve || fgqi.mode == add)
			{
				return next_frame->egroup->get_depth() <= fgqi.next_frame->egroup->get_depth();
			}
		}
		case evaluate:
		{
			if (is_gen_mode)
			{
				return next_frame->egroup->get_depth() <= fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == merge || fgqi.mode == evaluate)
			{
				return next_frame->egroup->get_depth() < fgqi.next_frame->egroup->get_depth();
			}
			else if (fgqi.mode == resolve || fgqi.mode == add)
			{
				return next_frame->egroup->get_depth() <= fgqi.next_frame->egroup->get_depth();
			}
		}
		case resolve:
		{
			if (is_gen_mode)
			{
				return next_frame->egroup->get_depth() <= fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == merge || fgqi.mode == evaluate || fgqi.mode == resolve)
			{
				return next_frame->egroup->get_depth() < fgqi.next_frame->egroup->get_depth();
			}
			else if (fgqi.mode == add)
			{
				return next_frame->egroup->get_depth() <= fgqi.next_frame->egroup->get_depth();
			}
		}
		case add:
		{
			if (is_gen_mode)
			{
				return next_frame->egroup->get_depth() <= fgqi.current_frame->egroup->get_depth();
			}
			else if (fgqi.mode == merge || fgqi.mode == evaluate || fgqi.mode == resolve || fgqi.mode == add)
			{
				return next_frame->egroup->get_depth() < fgqi.next_frame->egroup->get_depth();
			}
		}
	}

	return false;
}

FrameGroupQueueItem FrameGroupQueueItem::generate_next()
{
	FrameGroupQueueItem next_frame_item;

	next_frame_item.mode = generate;
	next_frame_item.current_frame = next_frame;

	return next_frame_item;
}

FrameGroupQueueItem FrameGroupQueueItem::generate_next(std::shared_ptr<FrameGroup> nframe)
{
	FrameGroupQueueItem eval_frame_item;

	eval_frame_item.mode = evaluate;
	eval_frame_item.current_frame = current_frame;
	eval_frame_item.next_frame = std::move(nframe);

	return eval_frame_item;
}

FrameGroupQueueItem FrameGroupQueueItem::generate_next(bool goal)
{
	FrameGroupQueueItem eval_frame_item;

	eval_frame_item.mode = resolve;
	eval_frame_item.current_frame = current_frame;
	eval_frame_item.next_frame = next_frame;
	eval_frame_item.goal = goal;

	return eval_frame_item;
}

bool FrameGroupQueueItem::is_valid(unsigned int current_depth, ProcessMode current_mode) const
{
	if (mode == current_mode)
	{
		if (mode == generate || mode == generate_ping || mode == generate_pong)
		{
			return current_frame->egroup->get_depth() <= current_depth;
		}
		else if (mode == merge || mode == evaluate || mode == resolve || mode == add)
		{
			return next_frame->egroup->get_depth() <= current_depth;
		}
	}

	return false;
}
