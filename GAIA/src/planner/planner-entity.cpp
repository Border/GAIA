#include "planner-entity.h"

#include <algorithm>

using namespace GAIA;

EntityFrame::EntityFrame(const EntityInterface* ent, unsigned int uni_id) :
	pentity(ent->get_persistent_entity()),
	state(ent->get_state()),
	unique_id(uni_id),
	depth(0),
	frame_state(valid)
{
}

EntityFrame::EntityFrame(std::shared_ptr<EntityFrame> _parent, const std::shared_ptr<Operation>& operation, unsigned int uni_id) :
	parent(_parent),
	op(operation),
	state(parent->state + operation->delta),
	pentity(parent->pentity),
	unique_id(uni_id),
	depth(parent->depth + 1),
	frame_state(valid)
{
	if (parent->dependencies.empty())
	{
		dependencies.push_back(DependencyGenealogy(operation, parent.get()));
	}
	else
	{
		for (const auto& gen : parent->dependencies)
		{
			dependencies.push_back(DependencyGenealogy(gen, operation, parent.get()));
		}
	}
}

void EntityFrame::add_child_entity(const std::shared_ptr<EntityFrame>& child)
{
	std::lock_guard<std::mutex> lock(m_nodes);
	nodes.push_back(child);
}

bool EntityFrame::has_genealogy_conflict(const EntityFrame* eframe) const
{
	// return false if the entities are of the same pid, but do not have the same unique id
	// (same root ancestor, but different branch).
	if (eframe->get_persistent_id() == get_persistent_id() && eframe->unique_id != unique_id) { return true; }

	if (dependencies.empty()) { return false; }

	for (const auto& dep : dependencies)
	{
		if (dep.dependency_genealogy.empty()) { return false; }

		for (const auto& gen : dep.dependency_genealogy)
		{
			// I have a list of entity frames which are dependencies (gen) for this frame
			// (the new frame, this). I want to check if another frame (eframe) is a
			// descendant of an entity in gen, or is an entity in gen.

			if (!gen.has_genealogy_conflict(eframe))
			{
				return false;
			}
		}
	}

	return true;
}

bool EntityFrame::is_match(const EntityFrame* eframe) const
{
	return state == eframe->state;
}

void EntityFrame::merge(EntityFrame* eframe)
{
	for (auto& dep : eframe->dependencies)
	{
		dependencies.push_back(std::move(dep));
	}
}

unsigned int EntityFrame::get_persistent_id() const { return pentity->id; }

std::shared_ptr<PersistentEntity> EntityFrame::get_persistent_entity() const { return pentity; }

unsigned int EntityFrame::get_type_id() const
{
	auto tid = state.find(GAIA::Type);

	if (tid == std::end(state))
	{
		throw std::runtime_error("No type data for entity! Entity must have type data.");
	}

	return tid->second.get_const_ref<int>();
}

const DataMap& EntityFrame::get_state() const { return state; }

std::vector<Affordance*> EntityFrame::get_affordances() const
{
	std::vector<Affordance*> affordances;

	// TODO: should this somehow only return affordance that are actually used.
	for (auto op : operations)
	{
		affordances.push_back(op->affd);
	}

	return affordances;
}

CVector EntityFrame::get_causes() const
{
	assert("unimplemented!" && false);
	return CVector();
}

std::vector<std::shared_ptr<EntityAction>> EntityFrame::get_entity_actions() const
{
	std::vector<std::shared_ptr<EntityAction>> entity_actions;

	// TODO: generate the entity actions from the operations.

	return entity_actions;
}

std::shared_ptr<EntityInterface> EntityFrame::get_prev_entity() const
{
	return parent;
}


EntityGroup::EntityGroup(unsigned int pid, unsigned int uid) :
	persistent_id(pid),
	unique_id(uid),
	depth(0),
	next(nullptr),
	prev(nullptr)
{
}

EntityGroup::EntityGroup(unsigned int pid, unsigned int uid, unsigned int _depth) :
	persistent_id(pid),
	unique_id(uid),
	depth(_depth),
	next(nullptr),
	prev(nullptr)
{
}

EntityGroup* EntityGroup::get_next() const { return next; }

EntityGroup* EntityGroup::get_prev() const { return prev; }

void EntityGroup::set_next(EntityGroup* next_ent)
{
	std::lock_guard<std::mutex> lock(m_next);
	next = next_ent;
	next->prev = this;
}

void EntityGroup::add_entity_frame(const std::shared_ptr<EntityFrame>& eframe)
{
	std::lock_guard<std::shared_mutex> lock(m_entity_group);
	entities.push_back(eframe);
}

void EntityGroup::remove_entity_frame(const std::shared_ptr<EntityFrame>& eframe)
{
	std::lock_guard<std::shared_mutex> lock(m_entity_group);

	auto entity_frame_iter = std::find_if(std::begin(entities), std::end(entities), [&](const std::shared_ptr<EntityFrame>& ef) { return ef.get() == eframe.get(); });
	if (entity_frame_iter != std::end(entities))
	{
		entities.erase(entity_frame_iter);
	}
}

void EntityGroup::lock() { m_entity_group.lock(); }

void EntityGroup::unlock() { m_entity_group.unlock(); }

void EntityGroup::lock_shared() const { m_entity_group.lock_shared(); }

void EntityGroup::unlock_shared() const { m_entity_group.unlock_shared(); }

const std::vector<std::shared_ptr<EntityFrame>>& EntityGroup::get_entities() const { return entities; }

std::vector<std::shared_ptr<EntityFrame>>& EntityGroup::get_entities_mut() { return entities; }

size_t EntityGroup::num_entities() const { std::shared_lock<std::shared_mutex> lock(m_entity_group); return entities.size(); }

unsigned int EntityGroup::get_persistent_id() const { return persistent_id; }

unsigned int EntityGroup::get_unique_id() const { return unique_id; }

unsigned int EntityGroup::get_depth() const { return depth; }
