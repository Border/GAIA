#pragma once

#include "planner-operation.h"

#include <vector>
#include <memory>

namespace GAIA
{

class EntityFrame;

class EntityFrameGenealogy
{
public:
	/// ctor for root frames with no existing genealogies
	explicit EntityFrameGenealogy(EntityFrame* frame);

	/// ctor for existing genealogies
	EntityFrameGenealogy(const EntityFrameGenealogy& parent, EntityFrame* frame);

	/// returns the most recent ancestor
	const EntityFrame* get_current() const;

	/// returns the ancestor from the specified level
	const EntityFrame* get_level(unsigned int level) const;

	/// returns true if the genealogies have a common ancestor at some point
	bool has_common_ancestor(const EntityFrame* eframe) const;

	/// returns true if the eframe is listed as an ancestor in this genealogy
	bool is_ancestor(const EntityFrame* eframe) const;

	/// adds a new eframe descendant to the genealogy
	void add_eframe_descendant(EntityFrame* eframe);

	/// returns the pid of the entity frame
	unsigned int get_pid() const;

	/// returns the unique hash
	unsigned int get_unique_hash() const;

	/// returns the list of ancestors
	const std::vector<EntityFrame*>& get_ancestors() const;

	/// merges the supplied EntityFrameGenealogy with this. Returns false if there was a
	/// conflict, otherwise returns true. If returns false, the contents of this object are
	/// not modified.
	bool merge(const EntityFrameGenealogy& genealogy);

private:
	/// the persistent entity id
	unsigned int pid;

	/// the unique hash generated from the eframe ancestors
	unsigned int hash_unique;

	/// the previous/parent lineage of entity frames that are marked as dependent
	std::vector<EntityFrame*> eframe_ancestors;
};

struct DependencyGenealogy;
using linked_iter = std::vector<std::tuple<unsigned int, const EntityFrame*, const EntityFrameGenealogy*>>::iterator;
using depgene_vector = std::vector<const DependencyGenealogy*>;
using efg_permutations = std::vector<std::tuple<unsigned int, unsigned int, std::vector<EntityFrameGenealogy>>>;

class EntityDependencyGenealogy
{
public:
	/// ctor - provided with the frame genealogies and type hash
	EntityDependencyGenealogy(std::vector<EntityFrameGenealogy>& deps, unsigned int type_hash);

	/// checks for conflicts in genealogy dependencies and returns the result. A conflict
	/// occurs where a dependency has the same entity group (persistent id) as eframe, but
	/// different ancestor.
	/// \param eframe The EntityFrame to check for conflicting sibling lineages (eg.
	/// descended from different required siblings
	/// \return Returns false if eframe has a different sibling lineage (conflict)
	bool has_genealogy_conflict(const EntityFrame* eframe) const;

	/// uses the supplied permutations to generate a child EntityDependencyGenealogy to this one
	efg_permutations generate_child_dependencies(const EntityFrameGenealogy& frame_genealogy, const std::vector<EntityFrame*>& matched_dependencies) const;

	/// returns true of the list of dependencies is empty
	bool empty() const;

	/// return the hash of the dependencies
	unsigned int get_hash() const;

	/// returns true if the entity frame is a dependency in these genealogies
	bool contains_eframe(const EntityFrame* eframe) const;

	/// returns the list of current frame dependencies
	std::vector<const EntityFrame*> get_current_dependencies() const;

	/// return a const ref of the dependencies
	const std::vector<EntityFrameGenealogy>& get_dependencies() const;

private:
	/// the hash of the dependencies
	unsigned int hash_type;

	/// the list of dependencies (prior and current) the operation requires to be performed
	/// successfully. This needs to be sorted by pid.
	std::vector<EntityFrameGenealogy> dependencies;
};

/// generates all permutations of the parent frame dependencies
void linked_permutation_recurse(linked_iter iter, const linked_iter& end, depgene_vector temp, std::vector<depgene_vector>& perms);

/// generates all the permutations of the depentent frames dpendencies
void dependency_permutation_recurse(depgene_vector::iterator iter, const depgene_vector& depgen, std::vector<const EntityDependencyGenealogy*> temp, efg_permutations& new_dependencies, const EntityFrameGenealogy& frame_genealogy);

struct DependencyGenealogy
{
	/// initial/root frame ctor
	DependencyGenealogy(const std::shared_ptr<Operation>& operation, EntityFrame* parent);

	/// normal ctor
	DependencyGenealogy(const DependencyGenealogy& gen, const std::shared_ptr<Operation>& operation, EntityFrame* parent);

	/// the operation used to generate the frame
	std::shared_ptr<Operation> op;

	/// the genealogy of this frame
	EntityFrameGenealogy frame_genealogy;

	/// the dependencies and their genealogies for this frame
	std::vector<EntityDependencyGenealogy> dependency_genealogy;
};

efg_permutations make_dependencies(const EntityFrameGenealogy& frame_genealogy, const std::vector<EntityFrame*>& matched_dependencies);

}
