#pragma once

#include "action-types.h"
#include "datamap.h"

#include <vector>
#include <memory>

namespace GAIA
{

class Affordance;

/// A generated instance of an affordance
struct Operation
{
	/// ctor - empty/ no op
	Operation();
	/// ctor
	Operation(Affordance* affordance, DataMap delta);

	/// The affordance this operation is based on
	Affordance* affd;

	/// The change in entity state generated based on the the affordance
	DataMap delta;
};

}
