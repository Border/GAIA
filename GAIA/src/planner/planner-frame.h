#pragma once

#include "planner-dependency.h"
#include "planner-entity.h"
#include "planner-genealogy.h"
#include "worldframe.h"
#include "datamap.h"

#include <memory>
#include <vector>
#include <unordered_map>
#include <mutex>
#include <shared_mutex>
#include <atomic>
#include <optional>

namespace GAIA
{

/// A container for data of a simulated frame for the planner
class PlannerFrame
{
public:
	/// default ctor
	PlannerFrame();

	/// ctor
	PlannerFrame(const WorldFrame* wframe, std::atomic_uint& ecount, std::atomic_uint& egcount);

	/// adds an entity group
	void add_entity_group(std::unique_ptr<EntityGroup>& egroup);

	/// returns a const reference to the entity groups
	const std::vector<std::unique_ptr<EntityGroup>>& get_entity_groups() const;

	/// returns a pointer to the next frame from this frame
	PlannerFrame* get_next_frame() const;

	/// sets the next frame from this frame
	void set_next_frame(PlannerFrame* nframe);

	/// write lock for access to this frame
	void lock();

	/// write unlock for access to this frame
	void unlock();

	/// read lock for access to this frame
	void lock_shared() const;

	/// read unlock for access to this frame
	void unlock_shared() const;

private:
	/// protects access to the next frame
	mutable std::shared_mutex m_pframe;


	/// entity group
	std::vector<std::unique_ptr<EntityGroup>> entity_groups;

	/// the next frame
	PlannerFrame* next_frame;
};


class FrameGroup
{
public:
	/// remove default ctor
	FrameGroup() = delete;

	/// ctor
	FrameGroup(const std::shared_ptr<EntityFrame>& e_frame, EntityGroup* e_group, PlannerFrame* p_frame);

	/// ctor - for merge mode
	FrameGroup(EntityGroup* e_group, PlannerFrame* p_frame);

	/// uses the planner frame (pframe) to find dependencies for the initial frame
	std::optional<InitialDependencyContainer> find_initial_affordance_dependencies(const Affordance* affd) const;

	/// uses the planner frame (pframe) to find dependencies for the final frame
	bool find_final_affordance_dependencies(const Affordance* affd, EntityFrame* prev_eframe) const;

	/// Uses the initial dependencies previously generated combined with an affordance to generate a range of operations
	std::vector<std::shared_ptr<Operation>> generate_operations(Affordance* affd, InitialDependencyContainer& dependencies) const;

	/// returns the next entity group, if one does not exist yet, create it
	EntityGroup* get_next_entity_group(std::atomic_uint& egcount);

	/// resolve the dependencies, by removing any dependencies that have eframes in the
	/// invalid list. Returns true if the dependency is still valid, otherwise returns
	/// false.
	bool resolve_dependencies(std::vector<std::shared_ptr<EntityFrame>>& invalid);

	/// Uses the planner frame (pframe) to find references from initial affordances
	void generate_references();

private:
	/// processes the dependency graph to identify any dependencies required from other
	/// entities or previous frames
	bool process_dependencies(const ACVector& conditions, DependencyContainer& dependencies) const;

	/// adds the permutations to dependency_genealogy if they are unique
	void add_new_dependencies(std::vector<EntityDependencyGenealogy>& dependency_genealogy,
		std::vector<std::tuple<unsigned int, unsigned int, std::vector<EntityFrameGenealogy>>>&& perms) const;

public:

	/// the entity frame
	std::shared_ptr<EntityFrame> eframe;

	/// the entity group
	EntityGroup* const egroup;

	/// the planner frame
	PlannerFrame* const pframe;
};

enum ProcessMode
{
	none,
	generate,
	generate_ping,
	generate_pong,
	merge,
	evaluate,
	resolve,
	add,
	count
};

struct FrameGroupQueueItem
{
	/// default ctor
	FrameGroupQueueItem();

	/// ctor
	FrameGroupQueueItem(const std::shared_ptr<EntityFrame>& e_frame, EntityGroup* e_group, PlannerFrame* p_frame);

	/// use default copy ctor
	FrameGroupQueueItem(const FrameGroupQueueItem& item) = default;

	/// use default rval ctor
	FrameGroupQueueItem(FrameGroupQueueItem&& item) = default;

	/// use default copy assignment ctor
	FrameGroupQueueItem& operator= (FrameGroupQueueItem&& item) = default;

	/// greater than comparison operator for sorting
	bool operator > (const FrameGroupQueueItem& fgqi) const;

	/// less than comparison operator for sorting
	bool operator < (const FrameGroupQueueItem& fgqi) const;


	/// moves the next frome to current frame and sets the mode to generate
	FrameGroupQueueItem generate_next();

	/// sets the next frame and sets the mode to evaluate
	FrameGroupQueueItem generate_next(std::shared_ptr<FrameGroup> nframe);

	/// sets the next frame to resolve
	FrameGroupQueueItem generate_next(bool goal);

	/// returns the depth of the frame (based on the mode)
	bool is_valid(unsigned int current_depth, ProcessMode current_mode) const;

	/// The type of frame processing that must occur
	ProcessMode mode;

	/// the current frame which generation uses to create the next frame
	std::shared_ptr<FrameGroup> current_frame;

	/// the next frame as generated from the current frame and used in evaluate mode
	std::shared_ptr<FrameGroup> next_frame;

	/// in resolve mode indicates the if the frame meets a goal
	bool goal;
};

}
