#pragma once

#include "affordance-condition.h"
#include "affordance-action.h"
#include "state.h"
#include "feature-context.h"
#include "causality.h"
#include "types.h"

#include <memory>
#include <vector>
#include <map>

namespace GAIA
{

class EntityFrame;
class EntityGroup;

using EFVector = std::vector<EntityFrame*>;
using EFIter = std::vector<EntityFrame*>::iterator;
using EntityPermutations = std::vector<std::pair<hash_t, EFVector>>;


class ConditionEntityDependencies
{
public:
	explicit ConditionEntityDependencies(AffordanceCondition* cond);

	// TODO: need some way to add feature context data to use for matching

	/// matches the supplied entity to the states of the condition and adds it to the list of matching entity frames.
	void add_matching_entity(EntityFrame* ent, FeatureContext& context);

	/// returns true if all states/causes were matched at least once
	bool match_result() const;

	/// generate dependency permutations
	EntityPermutations generate_dependency_permutations(EntityFrame* ceframe) const;

private:

	// a list of Entity Frames which matched the state
	struct EntityFrameMatches
	{
		State* state;
		EFVector matched_entity_frames;
	};

	using EntityFrameMatchesList = std::vector<EntityFrameMatches>;

	/// the list of states for the condition and a list of Entity Frames which matched them
	EntityFrameMatchesList dependency_matches;

	/// the affordance condition to match
	AffordanceCondition* const condition;
};

class CauseEntityDependencies
{
public:
	CauseEntityDependencies(Cause* cause);

	void add_matching_entity(EntityFrame* ent);

	bool match_result() const;

	/// generate dependency permutations
	EntityPermutations generate_dependency_permutations(EntityFrame* ceframe) const;

private:
	// a list of Entity Frames which matched the state
	struct EntityFrameMatches
	{
		Cause* cause;
		EFVector matched_entity_frames;
	};

	using EntityFrameMatchesList = std::vector<EntityFrameMatches>;

	/// the list of states for the condition and a list of Entity Frames which matched them
	EntityFrameMatchesList dependency_matches;
};

struct ActionFailureEntityDependencies
{
	enum DependencyResult
	{
		Fail,
		Dependent,
		NonDependent
	};

	/// ctor
	ActionFailureEntityDependencies(AffordanceCondition* cond, EntityFrame* entframe);

	/// matches the supplied entity to the states of the action fail condition (stores the entity group if a match)
	void match_entity(EntityFrame* ent, EntityGroup* egrp, const FeatureContext& context);

	/// returns the result of the action matching the fail conditions
	std::pair<DependencyResult, EFVector> action_valid() const;

	/// the list of states for the action fail condition and a list of Entity Frames which matched them
	std::vector<std::pair<State*, std::map<unsigned int, std::pair<EntityGroup*, std::vector<EntityFrame*>>>>> dependency_matches;

	/// the actions affordance failure condition to match
	AffordanceCondition* condition;

	/// the entity frame this condition is in relation to
	EntityFrame* eframe;
};

class DependencyContainer
{
public:
	/// ctor
	explicit DependencyContainer(EntityFrame* eframe);

	/// adds the list of affordance conditions to ascertain dependencies
	void add_conditions(const ACVector& conditions);

	/// adds the list of causes to generate the feature context
	virtual void add_causes(const CVector& causes);

	/// adds an entity to check for dependencies for conditions
	virtual void check_entity(EntityFrame* ent);

	/// determins if all dependencies resolve into a valid set of dependencies, sets the
	/// valid variable to true if valid, or false if not all items were matched.
	/// Returns the valid variable.
	bool verify();

	/// returns true if sufficient entity frames were found to match the entity dependencies
	bool is_valid() const;

	/// generate dependency permutations for all condition_entity_dependencies
	virtual EntityPermutations generate_dependency_permutations(EntityFrame* ceframe) const;

	const FeatureContext& get_feature_context() const;

protected:
	/// the list of dependencies for each condition
	std::vector<ConditionEntityDependencies> condition_entity_dependencies;

	// The feature context
	FeatureContext feature_context;

	/// a reference to the entity frame
	EntityFrame* eframe;

	/// indicates the validity of the depnedencies
	bool valid;
};

class InitialDependencyContainer : public DependencyContainer
{
public:
	/// ctor
	explicit InitialDependencyContainer(EntityFrame* eframe);

	/// adds the list of causes to ascertain dependencies and validity of an operation as well as generate the feature context
	void add_causes(const CVector& causes) override;

	/// adds an entity to check for depenedcies for actions
	void check_cause_entity(EntityFrame* ent, EntityGroup* eg);

	/// adds an entity to check for dependencies for conditions also filters based on
	/// dependencies of the causes
	void check_entity(EntityFrame* ent) override;

	/// returns true if all causes are valid (i.e. no fail conditions are met)
	bool verify_causes();

	EntityPermutations generate_dependency_permutations(EntityFrame* ceframe) const override;

protected:
	struct AffordanceActionDependencies
	{
		AffordanceAction* affordance_action;
		/// the list of dependencies for each action failure condition
		std::vector<ActionFailureEntityDependencies> action_failure_entity_dependencies;

		ActionFailureEntityDependencies::DependencyResult dependency_result;
	};

	std::vector<AffordanceActionDependencies> affordance_actions;

	std::vector<CauseEntityDependencies> cause_dependencies;

	/// the list of entities to ignore when checking the initial conditions
	EFVector ignore_entities;
};

}
