#include "plan.h"

using namespace GAIA;


PlanParams::PlanParams(const std::shared_ptr<WorldFrame>& gframe) :
	max_depth(6),
	goal(gframe),
	paused(false),
	debug(false),
	threads(std::thread::hardware_concurrency())
{
}



Plan::Plan(PlanParams _params, const WorldFrame* wframe) :
	params(std::move(_params)),
	entity_count(0),
	entity_group_count(0)
{
	auto frame = std::make_unique<PlannerFrame>(wframe, entity_count, entity_group_count);

	for (auto& eg : frame->get_entity_groups())
	{
		const auto tid = eg->get_entities().front()->get_type_id();
		for (auto& ent : params.goal->get_entities())
		{
			if (ent.first == tid)
			{
				eg->get_entities().front()->goal_distance = ent.second->state - eg->get_entities().front()->state;
			}
		}
	}

	frames.push_back(std::move(frame));
}

std::vector<std::shared_ptr<EntityFrame>> Plan::get_goal_success_frames() const
{
	return goal_success_frames;
}

void Plan::generate_frame(PlannerFrame* pframe)
{
	pframe->lock();
	if (pframe->get_next_frame() == nullptr)
	{
		std::lock_guard<std::mutex> lock(m_frame);
		frames.push_back(std::make_unique<PlannerFrame>());
		pframe->set_next_frame(frames.back().get());
	}
	pframe->unlock();
}

void Plan::add_goal_frame(const std::shared_ptr<EntityFrame>& goal_frame)
{
	std::lock_guard<std::mutex> lock(m_goal);
	goal_success_frames.push_back(goal_frame);
}

size_t Plan::get_num_entity_solutions() const { return goal_success_frames.size(); }

PlannerFrame* Plan::get_current_frame() const
{
	assert(!frames.empty());
	return frames.back().get();
}

size_t Plan::get_max_depth() const { return frames.size(); }

const PlannerFrame* Plan::get_frame_at_depth(unsigned int depth) const
{
	assert(depth < frames.size());

	return frames[depth].get();
}
