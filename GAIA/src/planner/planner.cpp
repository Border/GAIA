#include "planner.h"
#include "affordance.h"
#include "affordance-classifier.h"
#include "worldframe.h"
#include "memory.h"
#include "io.h"
#include "container-algorithms.h"

#include <thread>
#include <cassert>
#include <exception>
#include <algorithm>
#include <utility>

using namespace GAIA;


Planner::Planner() :
	thread_exit(0),
	thread_count(0),
	current_depth(0)
{ }

Planner::~Planner()
{
	run_threads = false;
	// unpause if paused
	play();
	// wake up any threads that might be idle
	cv_frame_queue_idle.notify_all();
	// wake up generator thread (if not async, otherwise does nothing)
	cv_pool.notify_all();

	while (thread_exit < thread_count);
}

std::shared_ptr<Plan> Planner::generate_plan(const PlanParams& params, const WorldFrame* wframe)
{
	plan = std::make_shared<Plan>(params, wframe);

	frames_processed = 0;

	// add entity frames to the thread pool queue
	init_planner_frame(plan->get_current_frame());

	// start queue processing and wait for it to finish
	run_frame_queue_threadpool();

	prev_plans.push_back(plan);
	return plan;
}

std::shared_ptr<Plan> Planner::generate_plan_async(const PlanParams& params, const WorldFrame* wframe)
{
	plan = std::make_shared<Plan>(params, wframe);

	frames_processed = 0;

	// add entity frames to the thread pool queue
	init_planner_frame(plan->get_current_frame());

	// start queue processing, but don't wait for it to finish
	run_frame_queue_threadpool_async();

	prev_plans.push_back(plan);
	return plan;
}

ProcessMode Planner::get_process_mode() const { return current_mode; }

void Planner::play()
{
	_pause = false;
	_step = false;
	_step_frame_state = false;
	_pause_frame_state = false;
	cv_pause.notify_all();
}

void Planner::pause()
{
	_pause = true;
	_pause_frame_state = true;
}

void Planner::step()
{
	_step = false;
	_pause = false;
	_step_frame_state = false;
	_pause_frame_state = false;
	cv_pause.notify_one();
	// use spin lock (as it is very short) to wait for _step to be set, indicating the
	// thread has woken up
	while (!_step);
	_pause = true;
	_step = false;
}

void Planner::step_state()
{
	_pause = false;
	_step = false;
	_step_frame_state = false;
	if (_frame_state_paused)
	{
		// its already paused, so unpause
		_pause_frame_state = false;
		// need to wake up all threads so they can start processing
		cv_pause.notify_all();
		// use spin lock (as it is very short) to wait for _step_frame_state to be set,
		// indicating the thread has woken up
		while (!_step_frame_state);
		_pause_frame_state = true;
		_step_frame_state = false;
		_frame_state_paused = false;
	}
	else
	{
		// its not paused yet, so we need to pause it
		_pause_frame_state = true;
		// need to wake up all threads so they can start processing
		cv_pause.notify_all();
	}
}

bool Planner::done() const { return !run_threads; }

QueueStats Planner::get_queue_stats()
{
	QueueStats stats;
	std::lock_guard<std::mutex> lock(m_frame_queue);

	for (auto& frame : frame_queue)
	{
		stats.mode_frames[frame.mode]++;
	}

	stats.total_frames = frame_queue.size();
	stats.total_frames_processed = frames_processed;
	stats.current_mode = current_mode;

	return stats;
}

void Planner::init_planner_frame(PlannerFrame* pframe)
{
	for (const auto& grp : pframe->get_entity_groups())
	{
		for (const auto& ent : grp->get_entities())
		{
			add_frame_to_queue(FrameGroupQueueItem(ent, grp.get(), pframe));
		}
	}
}

void Planner::add_frame_to_queue(FrameGroupQueueItem&& frame)
{
	std::lock_guard<std::mutex> lock(m_frame_queue);

	calg::insert_sorted(frame_queue, std::move(frame),
		[](const FrameGroupQueueItem&a, const FrameGroupQueueItem& b) { return a < b; });

	// notify a waiting thread that a new frame is available on the queue
	cv_frame_queue_idle.notify_all();
}

void Planner::remove_frame_from_queue(EntityFrame* eframe)
{
	std::lock_guard<std::mutex> lock(m_frame_queue);

	auto item = std::find_if(std::begin(frame_queue), std::end(frame_queue),
		[eframe](const FrameGroupQueueItem& frame) {
		return frame.mode == ProcessMode::evaluate
		&& frame.next_frame->eframe->unique_id == eframe->unique_id;
	});

	if (item != std::end(frame_queue))
	{
		frame_queue.erase(item);
	}
	else
	{
		throw std::runtime_error("Could not find the frame in the queue!");
	}
}

FrameGroupQueueItem Planner::get_frame_from_queue()
{
	std::lock_guard<std::mutex> lock(m_frame_queue);
	if (!frame_queue.empty() && frame_queue.front().is_valid(current_depth, current_mode))
	{
		FrameGroupQueueItem frame(std::move(frame_queue.front()));
		frame_queue.pop_front();
		frames_processed++;
		return frame;
	}
	else
	{
		return FrameGroupQueueItem();
	}
}

void Planner::initialise_threadpool()
{
	_pause = plan->params.paused;
	_pause_frame_state = plan->params.paused;
	run_threads = true;
	idle_thread = 0;
	thread_exit = 0;
	current_depth = 0;
	recheck = false;
	current_mode = ProcessMode::generate;
	thread_count = plan->params.threads;

	assert(thread_count > 0);
}

void Planner::run_frame_queue_threadpool()
{
	if (frame_queue.empty()) { return; }

	// reset variables for threadpool
	initialise_threadpool();

	std::vector<std::thread> threadpool;

	for (unsigned int i = 0; i < thread_count; ++i)
		threadpool.push_back(std::thread(&Planner::process_frame_queue, this));

	// wait until all threads are idle
	std::unique_lock<std::mutex> lk(m_cv_pool);
	cv_pool.wait(lk, [&] { return !run_threads; });

	// worker threads should stop now, so wake them all up so they can exit
	cv_frame_queue_idle.notify_all();

	// ensure the threads have exited
	for (auto& th : threadpool)
	{
		// notify threads again in case they missed it for some reason (we shouldnt need
		// this, but apparently we do...)
		cv_frame_queue_idle.notify_all();
		th.join();
	}

	// if everything went correctly, idle_thread should be 0
	assert(idle_thread == 0);
}

void Planner::run_frame_queue_threadpool_async()
{
	if (frame_queue.empty()) { return; }

	// reset variables for threadpool
	initialise_threadpool();

	for (unsigned int i = 0; i < thread_count; ++i)
	{
		auto thread = std::thread(&Planner::process_frame_queue, this);
		thread.detach();
	}
}

void Planner::process_frame_queue()
{
	while (run_threads)
	{
		if (_pause)
		{
			std::unique_lock<std::mutex> lk(m_cv_pause);
			cv_pause.wait(lk, [&] { return !_pause; });
			_step = true;
		}

		auto frame = get_frame_from_queue();

		switch (frame.mode)
		{
		case ProcessMode::generate:
		case ProcessMode::generate_ping:
		case ProcessMode::generate_pong:		generate_entity_frame(frame); break;
		case ProcessMode::merge:				merge_entity_frame(frame); break;
		case ProcessMode::evaluate: 			evaluate_entity_frame(frame); break;
		case ProcessMode::resolve:  			resolve_entity_frame(frame); break;
		case ProcessMode::add:					add_entity_frame(frame); break;
		case ProcessMode::none:					no_frame(); break;
		}
	}

	++thread_exit;
}

void Planner::no_frame()
{
	// incriment the idle thread count and wait until notified of another frame available,
	// or initiate worker termination if no more frames in the queue. If there are frames
	// in the queue though it means we need to transition to the next mode for processing.
	++idle_thread;
	if (idle_thread >= thread_count)
	{
		// get a copy of the current mode
		const auto cmode = current_mode;

		// if all worker threads are idle
		// check if the queue is actually empty
		std::lock_guard<std::mutex> lock(m_frame_queue);
		if (frame_queue.empty())
		{
			// if it is, notify the main thread to wake up and tell the worker
			// threads to stop running
			run_threads = false;
			current_mode = ProcessMode::none;
			cv_pool.notify_all();
		}
		// make sure another thread didn't modify current_mode while waiting
		else if (cmode == current_mode)
		{
			// if not, then we must have processed all the entities in the frame
			// for the current mode. So switch modes.

			switch (current_mode)
			{
			case ProcessMode::generate: current_mode = ProcessMode::generate_ping; break; // Will always need to recheck at least once
			case ProcessMode::generate_ping:
			case ProcessMode::generate_pong:
			{
				// Only advance beyond the generate states if no more new frames have been generated
				if (! recheck)
				{
					current_mode = ProcessMode::merge;
					++current_depth;

					// Remove all frames that are in generate_(ping|pong) mode
					for (auto it = std::begin(frame_queue); it != std::end(frame_queue); )
					{
						if (it->mode == generate_ping || it->mode == generate_pong)
						{
							it = frame_queue.erase(it);
						}
						else
						{
							++it;
						}
					}

					// Generate frame queue items for each entity group. Each frame
					// queue item will merge all matching entities within the entity
					// group.
					// This inserts extra frames to the queue which do the merging
					// The evaluate frames are still sitting there waiting.
					auto pframe = plan->get_current_frame();
					for (const auto& eg : pframe->get_entity_groups())
					{
						// only create a merge frame if there is more than 1 entity
						// in the group.
						if (eg->get_entities().size() > 1)
						{
							FrameGroupQueueItem item;

							item.next_frame = std::make_shared<FrameGroup>(eg.get(), pframe);
							item.mode = ProcessMode::merge;

							// add the frame to the queue
							calg::insert_sorted(frame_queue, std::move(item),
								[](const FrameGroupQueueItem&a, const FrameGroupQueueItem& b) { return a < b; });
						}
					}

					// TODO: is this needed (or is the notify below enough?)
					// notify all the waiting threads that there are new frames
					// available on the queue
					// cv_frame_queue_idle.notify_all();
				}
				else
				{
					auto previous_mode = current_mode;
					// New affordances were added, so go back and generate some more frames
					current_mode = previous_mode == ProcessMode::generate_ping ? ProcessMode::generate_pong : ProcessMode::generate_ping;

					// go through the queue and change all items that have the previous mode to the current mode
					for (auto& item : frame_queue)
					{
						if (item.mode == previous_mode)
						{
							item.mode = current_mode;
						}
					}
				}

				// clear recheck for next time around
				recheck = false;
				break;
			}
			case ProcessMode::merge: current_mode = ProcessMode::evaluate; break;
			case ProcessMode::evaluate: current_mode = ProcessMode::resolve; break;
			case ProcessMode::resolve:
			{
				// only advance to the next state/mode if nothing needs to be rechecked
				if (recheck)
				{
					// go through the queue and change all items from add mode to resolve mode
					for (auto& item : frame_queue)
					{
						if (item.mode == ProcessMode::add)
						{
							item.mode = ProcessMode::resolve;
						}
					}

					// clear recheck for next time around
					recheck = false;
				}
				else
				{
					current_mode = ProcessMode::add;
				}
				break;
			}
			case ProcessMode::add:
			{
				current_mode = ProcessMode::generate;
				// clear the invalid frames as we are moving to the next
				// generation/layer
				invalid_frame_list.clear();
				break;
			}
			default: throw std::runtime_error("Invalid frame processing mode!");
			}

			// pause until stepped for the next state mode
			if (_pause_frame_state)
			{
				_frame_state_paused = true;
				std::unique_lock<std::mutex> lk(m_cv_pause);
				cv_pause.wait(lk, [&] { return !_pause_frame_state; });
				_step_frame_state = true;
			}

			// then notify (wake) all process_frame_queue worker threads.
			cv_frame_queue_idle.notify_all();
		}
	}
	else
	{
		std::unique_lock<std::mutex> lk(m_cv_frame_queue_idle);
		// we dont realy mind about spurious wakeups
		cv_frame_queue_idle.wait(lk);
	}

	--idle_thread;
}

std::vector<std::shared_ptr<Operation>> Planner::generate_operations(const FrameGroup* frame)
{
	std::vector<std::shared_ptr<Operation>> operations;

	// get the list of affordances thi type of entity can perform
	auto affordances = Memory::Instance()->get_entity_affordances(frame->eframe->get_type_id());

	// For each affordance, check if the initial state is valid. If it is, then generate a
	// delta based on the affordances event and create an operation, adding it to the list
	// of operations for this entity.
	for (auto affd : affordances)
	{
		// Filter affordances which have already generated an operation
		if (std::any_of(std::begin(frame->eframe->operations), std::end(frame->eframe->operations),
			[affd](const std::shared_ptr<Operation>& op){ return op->affd != nullptr && op->affd->get_unique_id() == affd->get_unique_id(); }
		))
		{
			continue;
		}

		// could also perform an optimisation which records which initial states
		// matched, but only the action/affordance was not performed.

		auto dependencies = frame->find_initial_affordance_dependencies(affd);

		if (dependencies.has_value())
		{
			auto ops = frame->generate_operations(affd, dependencies.value());

			operations.insert(std::end(operations), std::make_move_iterator(std::begin(ops)), std::make_move_iterator(std::end(ops)));
		}
	}

	// TODO: This should loop until no more affordances are generated

	return operations;
}

void Planner::generate_entity_frame(FrameGroupQueueItem& frame_item)
{
	const auto& frame = frame_item.current_frame;

	if (!check_params(frame->eframe.get())) { return; }

	// if there is no next frame, create one
	plan->generate_frame(frame->pframe);

	// generate the operations and store them in the eframe's list of operations
	auto operations = generate_operations(frame.get());

	if (frame_item.mode == generate)
	{
		// Add the nop operation - a do nothing operation, regardless of previous affordances
		// Note, there may be cases where a nop should not occur (like a ball rolling down a
		// hill, however, rather than disabling the nop, it should perhaps use computational
		// motivation and probability to reduce the likelihood of the nop being performed to 0.
		// The nop should only be added once.
		operations.push_back(std::make_shared<Operation>());
	}

	// get the next entity group if it exists, create a new one if not
	EntityGroup* egroup = frame->get_next_entity_group(plan->entity_group_count);

	// for each operation, create a new entity frame
	for (auto& op : operations)
	{
		// create new entity frame using the op
		// new_entity = entity + op
		auto eframe = std::make_shared<EntityFrame>(frame->eframe, op, plan->entity_count++);

		// create a new frame group for the new entity
		auto nframe = std::make_shared<FrameGroup>(eframe, egroup, frame->pframe->get_next_frame());

		// add the entity to the entity group
		nframe->egroup->add_entity_frame(eframe);

		// add the operation to the frames list of operations
		frame->eframe->operations.push_back(op);

		// add the new frame group as the next frame in the frame_item and switch to eval
		// mode for the frame_item, then add the frame item to the queue again
		add_frame_to_queue(frame_item.generate_next(nframe));

		// If any new frame was added, set recheck as there may be an affordance
		// which is dependent on this new frame which can now be performed
		recheck = true;
	}

	// Add frame_item back to the queue, in either generate_ping or generate_ong mode.
	// This will continue bouncing between generate_ping and generate_pong modes
	// until no more operations are generated.
	frame_item.mode = frame_item.mode == generate_ping ? generate_pong : generate_ping;
	add_frame_to_queue(std::move(frame_item));
}

void Planner::merge_entity_frame(const FrameGroupQueueItem& frame_item)
{
	auto egroup = frame_item.next_frame->egroup;

	egroup->lock();
	auto& entities = egroup->get_entities_mut();
	for (auto itef = std::begin(entities); itef != std::end(entities); itef++)
	{
		for (auto itef_comp = std::next(itef); itef_comp != std::end(entities); )
		{
			if ((*itef)->is_match(itef_comp->get()))
			{
				// its a match, so indicate this frame was merged
				(*itef_comp)->frame_state = merged;

				// merge it (moving itef_comp data into itef)
				(*itef)->merge(itef_comp->get());

				// now find the entity (in the frame queue) that was merged and remove it
				remove_frame_from_queue(itef_comp->get());

				// and remove it from the entity group
				itef_comp = entities.erase(itef_comp);
			}
			else
			{
				++itef_comp;
			}
		}
	}
	egroup->unlock();
}

void Planner::evaluate_entity_frame(FrameGroupQueueItem& frame_item)
{
	const auto& cframe = frame_item.current_frame;
	const auto& nframe = frame_item.next_frame;
	const auto& eframe = nframe->eframe;
	const auto& op = eframe->op;

	// check if the move was valid or meets the goal
	auto eval = evaluate(cframe->eframe.get(), eframe.get());
	if (eval == Evaluation::Invalid)
	{
		// the operation failed to pass evaluation (was not valid in some way).
		eframe->frame_state = fail_evaluate;

		if (plan->params.debug)
		{
			// in debug mode we still want to add the frame to the parent node so we can
			// see it
			cframe->eframe->add_child_entity(eframe);
		}
		else
		{
			// in non debug mode, we no longer need the frame, so it is removed from the
			// entity group. However, there may be other frames that have this as a
			// dependency, so we add it to the invalid_frame_list to keep track of it until
			// everything is resolved.
			{
				std::lock_guard<std::shared_mutex> lock(m_invalid_frame_list);
				invalid_frame_list.push_back(eframe);
			}
			nframe->egroup->remove_entity_frame(eframe);
		}
	}
	else
	{
		// if the affordance of the operation is nullptr, it is a nop, hence there are no
		// dependencies.
		if (op->affd == nullptr)
		{
			// the operation was valid, but a nop, so just add the frame to the child nodes
			// vector and continue generating frames. There is no need for a resolve
			// process as this has no new dependencies.
			cframe->eframe->add_child_entity(eframe);
			add_frame_to_queue(frame_item.generate_next());
			eframe->frame_state = success;
		}
		else if (nframe->find_final_affordance_dependencies(op->affd, cframe->eframe.get()))
		{
			// put the frame_item back into the queue to resolve dependencies and to wait
			// for all the other frames to finish evaluating.
			add_frame_to_queue(frame_item.generate_next(eval == Evaluation::Goal));
		}
		else
		{
			// the affordance required dependencies, but failed to find any that were valid
			eframe->frame_state = fail_final_dependency;
			if (plan->params.debug)
			{
				// in debug mode we still want to add the frame to the parent node so we
				// can see it
				cframe->eframe->add_child_entity(eframe);
			}
			else
			{
				// the frame was invalid, so in order to keep track of it we add it to the
				// invalid list. Hence, if there were any frames with it as a dependency, we
				// can check later.
				{
					std::lock_guard<std::shared_mutex> lock(m_invalid_frame_list);
					invalid_frame_list.push_back(eframe);
				}
				nframe->egroup->remove_entity_frame(eframe);
			}
		}
	}
}

void Planner::resolve_entity_frame(const FrameGroupQueueItem& frame_item)
{
	const auto& nframe = frame_item.next_frame;

	// check that no frames have become invalid and other frames have them as dependencies,
	// invalidating them. This means that potentially every single frame could be removed
	// if they are all interdependent.

	bool result = false;
	{
		// read only lock
		std::shared_lock<std::shared_mutex> shared_lock(m_invalid_frame_list);
		result = nframe->resolve_dependencies(invalid_frame_list);
	}

	if (!result)
	{
		nframe->eframe->frame_state = fail_final_dependency_resolve;
		if (plan->params.debug)
		{
			frame_item.current_frame->eframe->add_child_entity(nframe->eframe);
		}
		else
		{
			// the frame needs to be removed
			{
				std::lock_guard<std::shared_mutex> lock(m_invalid_frame_list);
				invalid_frame_list.push_back(nframe->eframe);
			}
			nframe->egroup->remove_entity_frame(nframe->eframe);
		}

		// this frame was removed, so all frames need to be checked again. For the moment a
		// simple approach is taken, where we just loop over all frames until there are no
		// more frames that are removed.

		recheck = true;
	}
	else
	{
		// just add the frames back to the queue to be rechecked in the process frame
		// section, check if recheck is true, if not, then change the mode to an add_frame
		// mode.
		auto fitem = FrameGroupQueueItem(frame_item);
		fitem.mode = ProcessMode::add;
		add_frame_to_queue(std::move(fitem));
	}
}

void Planner::add_entity_frame(FrameGroupQueueItem& frame_item)
{
	const auto& cframe = frame_item.current_frame;
	const auto& nframe = frame_item.next_frame;

	if (frame_item.goal)
	{
		// the goal frame was reached, add the frame to the child nodes vector
		cframe->eframe->add_child_entity(nframe->eframe);
		// also add the frame to the list of frames that have reached thier goal
		plan->add_goal_frame(nframe->eframe);
		nframe->eframe->frame_state = success_goal;
	}
	else
	{
		// the operation was valid, add the frame to the child nodes vector
		cframe->eframe->add_child_entity(nframe->eframe);
		// but the goal is not reached yet, so continue generating frames
		add_frame_to_queue(frame_item.generate_next());
		nframe->eframe->frame_state = success;
	}
}

bool Planner::check_params(const EntityFrame* const eframe) const
{
	return eframe->depth < plan->params.max_depth;
}

Planner::Evaluation Planner::evaluate(const EntityFrame* const prev_frame, EntityFrame* const next_frame) const
{
	bool checked = false;
	const auto tid = next_frame->get_type_id();
	for (const auto& entity : plan->params.goal->get_entities())
	{
		if (entity.second->get_type_id() == tid)
		{
			checked = true;
			// calculate the distance
			next_frame->goal_distance = entity.second->state - next_frame->state;

			// compare the distance
			bool goal = true;
			bool valid = true;
			for (auto& nf_gd : next_frame->goal_distance)
			{
				const auto& d = nf_gd.second;

				auto prev_d = prev_frame->goal_distance.find(nf_gd.first);
				if (prev_d == std::end(prev_frame->goal_distance)) { valid = false; break; }

				// should really sum the generic error using some stats. i.e. focus the
				// error on where it matters. There may be some values which dont matter
				// much, but others matter a lot
				if (d.get_type() == Gint)
				{
					if (goal && std::abs(d.get_const_ref<int>()) != 0) { goal = false; }
					if (std::abs(d.get_const_ref<int>()) > std::abs(prev_d->second.get_const_ref<int>())) { valid = false; break; }
					// TODO: need to handle this better. There could be a series of
					//       different states which must be transitioned through. They may
					//       be varying in value wildly, but are actually improving
				}
				else if (d.get_type() == Gdouble)
				{
					if (goal && std::abs(d.get_const_ref<double>()) > feature_error_thresh) { goal = false; }
					const double err = std::abs(d.get_const_ref<double>()) - std::abs(prev_d->second.get_const_ref<double>());
					if (err > feature_error_thresh) { valid = false; break; }
				}
				else if (d.get_type() == GVec)
				{
					if (goal && d.get_const_ref<GVec2>().Length() > feature_error_thresh) { goal = false; }
					const double err = d.get_const_ref<GVec2>().Length() - prev_d->second.get_const_ref<GVec2>().Length();
					if (err > feature_error_thresh) { valid = false; break; }
				}
				else { throw std::runtime_error("Unknown type!"); }
			}

			if (valid) { return goal ? Evaluation::Goal : Evaluation::Valid; }
		}
	}

	return checked ? Evaluation::Invalid : Evaluation::Valid;
}
