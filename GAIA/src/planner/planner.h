#pragma once

#include "planner-frame.h"
#include "plan.h"

#include <vector>
#include <array>
#include <queue>
#include <mutex>
#include <atomic>
#include <condition_variable>

namespace GAIA
{

class WorldFrame;

struct QueueStats
{
	ProcessMode current_mode;
	std::array<size_t, ProcessMode::count> mode_frames = { 0 };
	size_t total_frames = 0;
	size_t total_frames_processed = 0;
};

/// Generates plans and stores them in the class Plan
class Planner
{
	enum Evaluation
	{
		Valid,
		Goal,
		Invalid
	};

public:
	/// defualt ctor
	Planner();

	/// ctor to shut down any threads stull running in the thread pool
	~Planner();

	/// Generates a plan, returning the plan
	std::shared_ptr<Plan> generate_plan(const PlanParams& params, const WorldFrame* wframe);

	/// Generates a plan, returning the plan
	std::shared_ptr<Plan> generate_plan_async(const PlanParams& params, const WorldFrame* wframe);

	/// returns the current process mode
	ProcessMode get_process_mode() const;

	/// resume generating/processing frames
	void play();

	/// pause generating/processing frames
	void pause();

	/// step a single frame
	void step();

	/// step a EntitFrameState
	void step_state();

	/// returns true if the planner has finished generating
	bool done() const;

	/// gets the queue stats
	QueueStats get_queue_stats();

protected:

	/// Starts queueing entity frames for processing
	void init_planner_frame(PlannerFrame* pframe);

	/// Adds the frame to the queue to be processed
	void add_frame_to_queue(FrameGroupQueueItem&& frame);

	/// Removes the specified frame from the queue
	void remove_frame_from_queue(EntityFrame* eframe);

	/// Returns a frame from the queue
	FrameGroupQueueItem get_frame_from_queue();

	/// initialise threadpool
	void initialise_threadpool();

	/// initiates threads for processing the frame_queue and waits until its empty and all
	/// threads are finished
	void run_frame_queue_threadpool();

	/// waits for the threads to become emtpy
	void run_frame_queue_threadpool_async();

	/// processes the frame_queue until empty
	void process_frame_queue();

	/// there was no frame in the queue
	void no_frame();

	/// Generates all the available operations for the frame
	std::vector<std::shared_ptr<Operation>> generate_operations(const FrameGroup* frame);

	/// Generates all possible frames for the entity based on the operations possible
	void generate_entity_frame(FrameGroupQueueItem& frame_item);

	/// Merges any frames with the same entity state and entity group
	void merge_entity_frame(const FrameGroupQueueItem& frame_item);

	/// Evaluates the new frame by checking the final conditions and the goal distance
	void evaluate_entity_frame(FrameGroupQueueItem& frame_item);

	/// Resolves the new frame dependencies
	void resolve_entity_frame(const FrameGroupQueueItem& frame_item);

	/// finally adds the new entity frame as a child frame
	void add_entity_frame(FrameGroupQueueItem& frame_item);

	/// returns true if all the params are satisfied
	bool check_params(const EntityFrame* const eframe) const;

	/// returns true if the frame has not moved further from the goal
	Evaluation evaluate(const EntityFrame* const prev_frame, EntityFrame* const next_frame) const;


	/// The current plan being generated
	std::shared_ptr<Plan> plan;

	/// old plans
	std::vector<std::shared_ptr<Plan>> prev_plans;

	/// frame queue - uses a vector and sorts by tree depth (shallowest frame first)
	std::deque<FrameGroupQueueItem> frame_queue;

	/// the current depth of the frames being processed
	unsigned int current_depth;

	/// the current mode of frame processing
	ProcessMode current_mode;

	/// The number of frames that have been processed
	size_t frames_processed;

	/// when in resolve mode, indicated the frames need to be rechecked due to one or more
	/// frames being removed
	std::atomic_bool recheck;

	/// protect queue access
	std::mutex m_frame_queue;

	/// condition for running or stopping the threads
	std::atomic_bool run_threads;

	/// contains the number of idle threads in the pool
	std::atomic_uint idle_thread;

	/// contains the number of threads that have exited
	std::atomic_uint thread_exit;

	/// the number of threads to run workers on
	unsigned int thread_count;

	/// condition variable for making idle threads wait for available frames to process
	std::condition_variable cv_frame_queue_idle;

	/// mutex for the condition variable cv_frame_queue_idle
	std::mutex m_cv_frame_queue_idle;

	/// condition variable for making the pool spawn thread wait for everything to complete
	std::condition_variable cv_pool;

	/// mutex for the condition variable cv_pool
	std::mutex m_cv_pool;

	/// the list of frames that were no valid at the current depth
	std::vector<std::shared_ptr<EntityFrame>> invalid_frame_list;

	/// mutex for the condition variable cv_pool
	std::shared_mutex m_invalid_frame_list;

	/// condition for pausing the threads
	std::atomic_bool _pause;

	/// condition for pausing the threads
	std::atomic_bool _step;

	/// condition for pausing the threads
	std::atomic_bool _pause_frame_state;

	/// condition for pausing the threads
	std::atomic_bool _frame_state_paused;

	/// condition for pausing the threads
	std::atomic_bool _step_frame_state;

	/// condition variable for pausing the thread
	std::condition_variable cv_pause;

	/// mutex for the condition variable cv_pause
	std::mutex m_cv_pause;
};

}
