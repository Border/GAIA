# Add all header files in include into a glob for easier reference
file(GLOB_RECURSE GAIA_HDRS "include/*.h")

# Add all source files in src into a glob for easier reference
file(GLOB_RECURSE GAIA_SRC "src/*.cpp")

# Add the headers
include_directories(
    include
    ${JSON_DIR}/single_include/nlohmann
    ${TDNN_DIR}
)

# If enabled build the shared library (dll)
if(GAIA_BUILD_SHARED)
    add_library(GAIA_shared SHARED
        ${GAIA_HDRS}
        ${GAIA_SRC}
    )
    set_target_properties(GAIA_shared PROPERTIES
        OUTPUT_NAME "GAIA"
        CLEAN_DIRECT_OUTPUT 1
    )
endif()

# If enabled build the static library
if(GAIA_BUILD_STATIC)
    add_library(GAIA STATIC
        ${GAIA_HDRS}
        ${GAIA_SRC}
    )
    set_target_properties(GAIA PROPERTIES
        CLEAN_DIRECT_OUTPUT 1
    )
endif()
