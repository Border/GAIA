#pragma once

#include "entity.h"
#include "entity-types.h"
#include "generic.h"
#include "datamap.h"

#include <memory>
#include <vector>
#include <functional>

namespace GAIA
{

/// forward declerations
class Entity;
class Event;


class WorldFrame
{
	friend class IO;
	friend class Agent;
	friend class DebugDisplay;
	friend class FeatureClassifier;
	friend class ActionManager;

public:
	/// Constructor
	WorldFrame();
	/// Constructor
	explicit WorldFrame(size_t _frame);

	/// Adds pure data about the world (anything that has no physical presence)
	/// \param type The type of the data to add to the entity
	/// \param _data
	void add_data(unsigned int type, Generic&& _data);

	/// Adds physical entities within the world
	/// \param type The type id of the entity
	/// \param ent The pre-formed entity to add to the frame
	void add_entity(unsigned int type, std::shared_ptr<Entity>& ent);

	/// Adds physical entities within the world
	/// \param type The type id of the entity
	/// \param ent The lvalue pre-formed entity to add to the frame
	void add_entity(unsigned int type, std::shared_ptr<Entity>&& ent);

	/// Returns the previous world frame
	std::weak_ptr<WorldFrame> get_previous_frame() const;

	/// Sets the previous world frame to this one
	void set_previous_frame(std::weak_ptr<WorldFrame> frame);

	/// Returns a const ref to the entities
	const EntityMap& get_entities() const;

	/// returns a ref to the entities
	EntityMap& get_entities_mut();

	/// returns an entity if it matches the persistent entity id
	std::shared_ptr<Entity> find_entity(unsigned int pid) const;

	/// returns an entity if it matches the id
	std::shared_ptr<Entity> get_entity(unsigned int id) const;

	/// create a copy of entities and thier states
	std::shared_ptr<WorldFrame> copy(const std::function<std::shared_ptr<Entity>(std::shared_ptr<Entity>)>& allocate =
		[](const std::shared_ptr<Entity>& ent) { return std::make_shared<Entity>(ent); }) const;

	/// create a copy of entities and thier states, intending it as the next frame
	std::shared_ptr<WorldFrame> copy_next(const std::function<std::shared_ptr<Entity>(std::shared_ptr<Entity>)>& allocate =
		[](const std::shared_ptr<Entity>& ent) { return std::make_shared<Entity>(ent); }) const;

	/// returns the frame number
	size_t get_frame_number() const;

protected:
	/// Contains information about the world state
	DataMap data;

	/// Contains all the entities within the world enviornment
	EntityMap entities;

	/// The used complexity capability of the agent
	double complexity;

	/// The frame number
	const size_t frame;

	/// The previous world frame
	std::weak_ptr<WorldFrame> prev;
};

}
