#pragma once

#include "feature.h"
#include "feature-stats.h"
#include "action-domain.h"
#include "affordance.h"
#include "planner.h"
#include "scenario.h"
#include "entity.h"
#include "worldframe.h"
#include "datamap.h"
#include "generic.h"

#include <json.hpp>


namespace GAIA
{

class IO
{
	using w_frame = std::shared_ptr<WorldFrame>;
	using exemplar_pair = std::pair<w_frame, w_frame>;
	using exemplar_vec = std::vector<exemplar_pair>;

public:
	/// Reads an example scenario containing an initial and final WorldFrame pair. The initial can contain actions.
	static exemplar_vec read_exemplar_scenarios(const std::string& filename);

	/// Reads the available actions and thier ranges
	static std::vector<std::shared_ptr<IActionDomain>> read_action_ranges(const std::string& filename);

	/// Reads a tutorial/level scenario
	static Scenario read_scenario(const std::string& filename);

	/// Reads a single test from file
	static SingleTest read_single_test(const std::string& filename);

	/// Writes the generated plan frames
	static void write_plan_frames(const std::string& filename, const Plan* plan);

	/// Writes the frame to a json file, appends the frame depth to the filename
	static void write_frame(const std::string& filename, const PlannerFrame* frame);

private:
	/// Create a json object from the Generic
	static nlohmann::json create_generic_json(const Generic& data);

	/// Create a json object from the DataMap
	static nlohmann::json create_datamap_json(const DataMap& dmap);

	/// Create a json object from the Entity
	static nlohmann::json create_entity_json(const Entity* entity);

	/// Create a json object from the IFeature
	static nlohmann::json create_feature_json(const IFeature* feature);

	/// Create a json object from the operation
	static nlohmann::json create_operation_json(const Operation* op);

	/// Create a json object from the EntityFrame
	static nlohmann::json create_entity_frame_json(const EntityFrame* eframe);

	/// Create a json object from the entity group
	static nlohmann::json create_entity_group_json(const EntityGroup* egroup);

	/// Create a json object from the planner frame
	static nlohmann::json create_plan_frame_json(const PlannerFrame* frame);

	/// Create a json object from the ITransformer
	template<typename T>
	static nlohmann::json create_transformer_json(const ITransformer* transformer);

	/// Create a json object from the IFeatureStats
	template<typename T>
	static nlohmann::json create_feature_stats_json(const IFeatureStats* stats);

	/// Reads a generic item from a json element
	static Generic read_generic(const nlohmann::json& item);

	/// Reads an entity from a json element
	static std::shared_ptr<Entity> read_entity(const nlohmann::json& ent);

	/// Reads an action from a json element
	static std::unique_ptr<EntityAction> read_action(const nlohmann::json& action, Entity* ent);

	/// Reads an action range from a json element
	static std::shared_ptr<IActionDomain> read_action_range(const nlohmann::json& ent);

	/// Reads a world frame
	static std::shared_ptr<WorldFrame> read_world_frame(const nlohmann::json& wframe);

	/// reads a level for a scenario
	static std::unique_ptr<Level> read_level(const nlohmann::json& level);

	/// reads a test for a level
	static Test read_test(const nlohmann::json& test);
};

}
