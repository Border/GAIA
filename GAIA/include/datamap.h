#pragma once

#include "generic.h"

#include <unordered_map>


namespace GAIA
{

enum DataMapType
{
	ID,
	Type,
	Position,
	Orientation,
	End
};

using DataMap = std::unordered_map<unsigned int, Generic>;

/// Negates the contents of DataMap
DataMap operator - (const DataMap& a);

/// Adds 2 DataMaps together and returns the result
DataMap operator + (const DataMap& a, const DataMap& b);

/// Subtracts 2 DataMaps together and returns the result
DataMap operator - (const DataMap& a, const DataMap& b);

/// Multiply 2 DataMaps together and returns the result
DataMap operator * (const DataMap& a, const DataMap& b);

/// Multiply 2 DataMaps together and returns the result
template <typename T> DataMap operator * (const T& a, const DataMap& b);

/// Divide 2 DataMaps together and returns the result
DataMap operator / (const DataMap& a, const DataMap& b);

/// Add b to a, storing the result in a and return a copy of a
DataMap operator += (DataMap& a, const DataMap& b);

/// Compare two DataMaps to each other, return true if equal
bool operator == (const DataMap& a, const DataMap& b);

/// Compare two DataMaps to each other, return true if not equal
bool operator != (const DataMap& a, const DataMap& b);

/// Returns true if all elements of the datamap are 0 (or close enough)
bool IsZero(const DataMap& m);

/// Returns the difference between the two data maps. Elements that are identical are not
/// present in the output
DataMap datamap_delta(const DataMap& a, const DataMap& b);

}
