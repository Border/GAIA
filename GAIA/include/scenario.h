#pragma once

#include "worldframe.h"
#include "entity-persistent.h"
#include "entity.h"
#include "action/action-domain.h"
#include "datamap.h"
#include "vecmath.h"
#include "types.h"


#include <vector>

namespace GAIA
{

struct Test
{
	unsigned int depth_max;

	std::shared_ptr<WorldFrame> goal;
};

struct TestState
{
	enum TestValidity
	{
		valid,
		invalid_condition,
		invalid_goal
	};

	Test* test;
	TestValidity validity;
	bool goal_attained;
};

struct SingleTest
{
	std::shared_ptr<WorldFrame> start;
	std::shared_ptr<WorldFrame> goal;
};

struct Level
{
	unsigned int id;
	std::string name;
	std::string comment;

	std::shared_ptr<WorldFrame> start;

	std::vector<Test> tests;

	// actions allowed

	GVec2 boundary_min;
	GVec2 boundary_max;

	// The list of features to ignore
	std::vector<tid_t> ignore_features;
};

struct Scenario
{
	Scenario() = default;
	Scenario(std::vector<std::shared_ptr<IActionDomain>>& acts, std::vector<std::unique_ptr<Level>>& lvls):
		actions(std::move(acts)),
		levels(std::move(lvls))
	{
	}

	std::vector<std::unique_ptr<Level>> levels;
	std::vector<std::shared_ptr<IActionDomain>> actions;
};

/// A container for the unit in GAIA
/// Hides the data from GAIA, but is available to the user here
class ScenarioEntity : public GAIA::Entity
{
public:
	ScenarioEntity(const std::shared_ptr<Entity>& ent):
		Entity(ent),
		id(static_cast<ScenarioEntity*>(ent.get())->id)
	{
	}

	/// Constructor
	ScenarioEntity(GAIA::DataMap& dmap, unsigned int _id) :
		Entity(dmap),
		id(_id)
	{
		auto pent = PersistentEntityGenerator::Instance()->find_entity(id);

		if (pent == nullptr)
		{
			pent = PersistentEntityGenerator::Instance()->gen_entity(id);
		}

		set_persistent_entity(std::move(pent));
	}

	/// The unit the entity will represent in GAIA
	unsigned int id;
};

}
