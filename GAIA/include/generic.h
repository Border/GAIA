#pragma once

#include "vecmath.h"

#include <memory>
#include <string>


namespace GAIA
{

/// typedefs
using GVec2 = Vec2<double>;

/// enums

enum GType
{
	GNone,
	Gint,
	Gdouble,
	GVec
};

/// Interface for the GenericImpl class
/// Stores the type use in GenericImpl to allow the data
/// to be identified and properly used in Generic
class IGeneric
{
public:
	virtual ~IGeneric() = 0;
	virtual std::unique_ptr<IGeneric> clone() const = 0;
	virtual GType get_type() const = 0;
};

/// Contains the data (of unkown type T)
template <typename T> class GenericImpl : public IGeneric
{
public:
	explicit GenericImpl(T _t);

	virtual std::unique_ptr<IGeneric> clone() const;

	/// Returns the type of the generic
	virtual GType get_type() const;

	/// Returns a copy of type T
	T get() const;

	/// Returns a reference of t
	const T& get_const_ref() const;

	/// Returns a reference of t
	T& get_ref();

	/// Sets t
	void set(const T& _t);

private:
	/// the data t
	T t;
};

/// Wrapper for GenericImpl
/// Uses the IGeneric (via a unique pointer to
/// GenericImpl) to abstract/hide the type.
class Generic
{
public:
	/// Constructs with all compatible types
	/// Note bool is converted to int (0,1)
	Generic();
	Generic(bool t);
	Generic(int t);
	Generic(double t);
	Generic(const GVec2& t);

	/// Constructs a GenericImpl of the type specified, initialises it to 0
	explicit Generic(GType type);

	/// Copy constructor uses the same type as other
	Generic(const Generic& other);

	/// Constructs from an lvalue temporary by moving the temporary
	Generic(Generic&& other);

	/// Swaps the contents of this with other
	void swap(Generic& other);

	/// Initialisation operator, performs swap with the lvalue
	Generic& operator=(Generic other);

	/// Gets the type specified at construction
	GType get_type() const;

	/// Cast a const reference of the contents of what p points to, to
	/// a specified type T rather than creating a copy. 
	template <typename T> const T& get_const_ref() const;

	/// Same as above but is not const, hence user can modify the object
	template <typename T> T& get_ref() const;

	/// set the value to the supplied value, casting based on its type
	template <typename T> void set(const T& val);

	/// Returns true if the type is set to none, indicating there is no data contained within
	bool is_none() const;

	/// Determines if the type is zero
	/// returns true if the contents are null
	/// returns false if type is unknown but not null
	bool is_zero() const;

	/// Returns a zero of the current type
	Generic generate_zero() const;

	/// Returns a string of the data converted to text
	/// formatted based on the stored type
	std::string to_string() const;

private:
	/// a unique pointer to an interface of the type
	std::unique_ptr<IGeneric> p;
};


///Operator prototypes for Generic

/// Negate the contents of this and return a copy
Generic operator - (const Generic& a);

/// Subtract two Generics from each other and return the result (a - b)
Generic operator - (const Generic& a, const Generic& b);

/// Add two Generics together and return the result
Generic operator + (const Generic& a, const Generic& b);

/// a += b
Generic operator += (Generic& a, const Generic& b);

/// a -= b
Generic operator -= (Generic& a, const Generic& b);

/// Multiply two Generics together and return the result
Generic operator * (const Generic& a, const Generic& b);

/// Multiply a Generic (a) with a suppled type (a) and return the result as a Generic
template <typename T>
Generic operator * (const Generic& a, const T& b);

/// Divide two Generics together and return the result
Generic operator / (const Generic& a, const Generic& b);

/// Compare two Generics to each other and returns true if equal
bool operator == (const Generic& a, const Generic& b);

/// Compare two Generics to each other and returns true if not equal
bool operator != (const Generic& a, const Generic& b);

/// Compare two Generics to each other and returns true if equal
bool operator < (const Generic& a, const Generic& b);


/// returns the absolute value of the generic as the specified type
template<typename T>
T abs(const Generic& a);

/// returns the normalised value of the generic as the specified type
template<typename T>
T normalise(const Generic& a);

}
