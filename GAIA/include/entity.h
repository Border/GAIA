#pragma once

#include "entity-interface.h"
#include "action-entity.h"
#include "entity-persistent.h"
#include "causality.h"
#include "datamap.h"
#include "generic.h"
#include "types.h"

#include <vector>
#include <map>
#include <unordered_map>
#include <mutex>

namespace GAIA
{

/// forward declerations
class WorldFrame;
class Affordance;

/// A class that contains all relevant information about an entity (object)
/// in the world
class Entity : public EntityInterface
{
public:
	/// Constructor - moves dmap into state, sets up the type id and position, sets frame
	/// number to 0.
	explicit Entity(DataMap& dmap);
	/// Constructor - generates a new state from the ent and delta, coppies the pentity and
	/// sets ent as the prev entity.
	Entity(const std::shared_ptr<Entity>& ent, const DataMap& delta);
	/// Constructor - coppies the state, incriments the frame number and sets the supplied
	/// ent as the prev ent.
	explicit Entity(const std::shared_ptr<Entity>& ent);


	/// Get the type ID of the entity
	/// \return The id indicating the type of the entity
	tid_t get_type_id() const override;

	/// returns the persistent entity id for this entity
	uid_t get_persistent_id() const override;

	/// returns a shared_ptr of the persitent entity
	std::shared_ptr<PersistentEntity> get_persistent_entity() const override;

	/// Gets a reference of the state
	/// \return A const reference to the state of this entity
	const DataMap& get_state() const override;

	/// returns a list of affordances the entity has experienced
	std::vector<Affordance*> get_affordances() const override;

	/// returns a list of all the causes for this entity
	CVector get_causes() const override;

	/// returns a list of entity actions
	std::vector<std::shared_ptr<EntityAction>> get_entity_actions() const override;

	/// returns the previously linked entity
	std::shared_ptr<EntityInterface> get_prev_entity() const override;

	/// sets the suspected previous frame version of this entity (if identified) and also
	/// sets the deltamap
	/// \param delta a list of the features that changed in the current frame
	/// \param _frame the current frame number
	/// \param _prev a pointer to the previous entity
	void update(DataMap delta, size_t _frame, const std::shared_ptr<Entity>& _prev);

	/// sets the delta and frame
	void update(DataMap delta, size_t _frame);

	/// Creates a new persistent data for this entity
	void compose_persistent_entity();

	/// obtains the persistent entity from the supplied entity
	void obtain_persistent_entity(const Entity* ent);

	/// generates a list of the actions in this
	std::vector<std::shared_ptr<EntityAction>> get_actions() const;

	/// adds the affordance to the entity
	void add_affordances(const std::vector<Affordance*>& affds);

	/// generates 1 or more actions fro the actions ranges in the persistent entity
	void generate_actions(const std::shared_ptr<WorldFrame>& frame);

	void update_intrinsics(const std::shared_ptr<WorldFrame>& frame);

protected:
	/// sets the persistent entity
	void set_persistent_entity(std::shared_ptr<PersistentEntity> p);

private:
	/// gets the id
	unsigned int setup_id();

public:
	/// Contains the raw state values of this entity
	DataMap state;

	/// Contains the difference between this entities state and the previous
	DataMap delta;

	/// The current frame number
	size_t frame;

	/// The list of action this entity performed this frame
	std::vector<std::shared_ptr<EntityAction>> actions;

	/// protect the entity from external access while it is modifying contents
	std::mutex m_entity;

private:
	/// Pointer to the agents estimated previous entity
	std::weak_ptr<EntityInterface> prev;

	/// Pointer to the agents estimated next entity (might not be needed)
	std::weak_ptr<EntityInterface> next;

	/// The type identifier of the unit
	tid_t type_id;

	/// Affordances that occured in this frame
	std::vector<Affordance*> affordances;

	/// Persistent data pertaining to this entity
	std::shared_ptr<PersistentEntity> pentity;
};

}
