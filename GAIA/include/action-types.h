#pragma once

#include "intrinsic.h"
#include "feature-types.h"
#include "generic.h"

#include <vector>
#include <memory>

namespace GAIA
{

class IActionDomain;

struct ActionDomainIntrinsicData
{
	tid_t type; // TODO: the type is in the features below, but we can store it here for convenience?
	Intrinsic intrinsic;
	FVector features;
};

struct EntityActionData
{
	// The action domain can have multiple feature types
	std::shared_ptr<IActionDomain> action_domain;
	std::vector<ActionDomainIntrinsicData> intrinsic_data;
};

struct ActionDataContainer
{
	unsigned int data_type;
	FeatureReferenceType ref_type;
	Generic data;
};

// Action feature type and a list of the feature and its contexts
struct ActionFeaturesList
{
	tid_t feature_type;
	FeatureReferenceType ref_type;
	std::vector<FeatureDataContext> data_samples;
};

// A action type and a list of action features
struct ActionSamples
{
	tid_t type;
	std::vector<ActionFeaturesList> feature_samples;
};

struct ActionFeatureContainer
{
	tid_t feature_type;
	FeatureReferenceType reference_type;
	FVector features;
};

class Action;

using ActionFeatures = std::vector<ActionFeatureContainer>;
using AVector = std::vector<Action*>;

}
