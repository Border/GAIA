#pragma once

#include "entity.h"
#include "action-entity.h"
#include "planner.h"
#include "action-domain.h"
#include "worldframe.h"
#include "memory.h"
#include "io.h"
#include "datamap.h"


#include <memory>
#include <deque>
#include <vector>
#include <functional>
#include <utility>
#include <random>
#include <atomic>
#include <mutex>

namespace GAIA
{

class Agent
{
public:
	/// Constructor. Initialises any variables needed
	Agent();

	/// Destructor
	~Agent();

	/// adds the action ranges to the agents list of possible actions
	void add_action_ranges(std::vector<std::shared_ptr<IActionDomain>>& actions);

	/// Obtains a frame map describing the current state of thw world
	void add_world_frame(const std::shared_ptr<WorldFrame>& frame);

	/// Adds a frame pair exemplar for learning, the actions should already be added to their respective entities
	void add_frame_pair(const std::shared_ptr<WorldFrame>& inital_frame, const std::shared_ptr<WorldFrame>& final_frame);

	/// clears all previous frames from the memory of the agent
	void clear_frames();

	/// reset the agent without losing its learned affordances/features etc
	void reset();

	/// set the list of feature that are ignored when creating conditions
	void set_filtered_condition_features(const std::vector<tid_t>& ignore_features);

	/// Test frame pair, returns results
	std::vector<std::pair<Entity*, std::vector<Affordance*>>> test_frame_pair(const std::shared_ptr<WorldFrame>& inital_frame, const std::shared_ptr<WorldFrame>& final_frame);

	/// Gets the list of the actions the agent desires to perform for the current frame
	std::vector<EntityAction*> get_actions() const;

	/// Returns the internal number of frames processed
	size_t get_frame_count() const;

	/// Processes the current frame map info
	void process();

private:
	/// Generates a difference map containing only the items which are different from the previous frame
	void generate_deltamap(const std::shared_ptr<WorldFrame>& previous_frame, const std::shared_ptr<WorldFrame>& current_frame);

	/// Creates a delta in the supplied deltamap with descriptor
	void create_delta(const std::shared_ptr<Entity>& pent, Entity* cent);

	/// Creates a delta in the supplied deltamap with descriptor
	void create_delta_prev(const std::shared_ptr<Entity>& pent);

	/// Creates a delta in the supplied deltamap with descriptor
	void create_delta_curr(Entity* cent);

	/// The default match compare to use
	double default_match_compare(const std::shared_ptr<Entity>& a, const std::shared_ptr<Entity>& b);

	/// Attempts to match affordances to the current state based on the delta map
	/// If no affordances match, it generates affordances from the delta map
	void generate_affordances(const std::shared_ptr<WorldFrame>& previous_frame, const std::shared_ptr<WorldFrame>& current_frame);

	/// Generates the actions for the agent to perform
	void generate_actions(const std::shared_ptr<WorldFrame>& frame);

	/// Updates the intrinsic data for the agent
	void update_intrinsics(const std::shared_ptr<WorldFrame>& frame);

	/// Generates a plan based on the goals and agents desires
	void generate_plans();

protected:
	/// The planner
	Planner planner;

	/// Internal chronometer
	size_t chronometer;

	// Feature stats
	std::vector<std::pair<unsigned int, IFeatureStats*>> stats;

public:
	/// Function that compares specific values of the generic
	std::function<double(const std::shared_ptr<Entity>& a, const std::shared_ptr<Entity>& b)> match_compare;
};

}
