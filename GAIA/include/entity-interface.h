#pragma once

#include "action-entity.h"
#include "causality.h"
#include "datamap.h"
#include "generic.h"
#include "types.h"

#include <vector>
#include <map>
#include <unordered_map>
#include <mutex>

namespace GAIA
{

/// forward declerations
class Affordance;
class PersistentEntity;

/// A class that contains all relevant information about an entity (object)
/// in the world
class EntityInterface
{
public:
	/// Get the type ID of the entity
	/// \return The id indicating the type of the entity
	virtual tid_t get_type_id() const = 0;

    /// Get the persistent entity ID
    /// \return The persistent entity id for this entity
	virtual uid_t get_persistent_id() const = 0;

    /// Get the persistent entity
    /// \return A shared_ptr of the persitent entity object
	virtual std::shared_ptr<PersistentEntity> get_persistent_entity() const = 0;

	/// Gets a reference of the state
	/// \return A const reference to the state of this entity
	virtual const DataMap& get_state() const = 0;

    /// Get a list of affordances
    /// returns a list of affordances the entity has experienced
	virtual std::vector<Affordance*> get_affordances() const = 0;

	/// returns a list of all the causes for this entity
	virtual CVector get_causes() const = 0;

	virtual std::vector<std::shared_ptr<EntityAction>> get_entity_actions() const = 0;

	/// Gets the entity from the previous frame that is linked to this entity (they have the same pid)
	/// \return The previously linked entity
	virtual std::shared_ptr<EntityInterface> get_prev_entity() const = 0;
};

}
