#pragma once

#include "entity.h"
#include "datamap.h"

namespace GAIA
{

/// Contains the delta and pointers to the previous and current entity
struct EntityDelta
{
	/// A list of the features that changed for an entity
	DataMap delta;

	Entity* prev;
	Entity* curr;

	EntityDelta() : prev(nullptr), curr(nullptr) {}
	EntityDelta(DataMap& _delta, Entity* _prev, Entity* _curr) : delta(_delta), prev(_prev), curr(_curr) { }
};

/// an unordered map of entity delta's, indexed by each entity type id
using EntityDeltaMap = std::unordered_multimap<unsigned int, EntityDelta>;

/// an unordered map of entity pointers, indexed by each entity type id
using EntityMap = std::unordered_multimap<unsigned int, std::shared_ptr<Entity>>;

}
