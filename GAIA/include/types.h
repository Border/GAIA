#pragma once

namespace GAIA
{

using uid_t = unsigned int;
using tid_t = unsigned int;
using id_t = unsigned int;
using hash_t = size_t;
using hashid_t = size_t;

}
